package com.ascra.utils;

public class Constant {

    //	public static String PickleUrl = "http://54.251.33.194:8800/";
    public static final String PickleUrl = "http://pickle.tingworks.in/";
    public static final String PickleUrl_V1 = "http://pickle.tingworks.in/api/v1/";

    public static final String LoginSession = "LOGIN";

    public static final String FOLLOWERS = "followers";
    public static final String FOLLOWINGS = "following";

}
