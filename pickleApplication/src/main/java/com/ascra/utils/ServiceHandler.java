package com.ascra.utils;

import android.annotation.SuppressLint;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class ServiceHandler {

    public final static int GET = 1;
    public final static int POST = 2;
    public final static int PUT = 3;
    public final static int DELETE = 4;
    static String response = null;

    public ServiceHandler() {

    }

    /*
     * Making service call
     *
     * @url - url to make request
     *
     * @method - http request method
     */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /*
     * Making service call
     *
     * @url - url to make request
     *
     * @method - http request method
     *
     * @params - http request params
     */
    @SuppressLint("LongLogTag")
    public String makeServiceCall(String url, int method,
                                  List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            Log.d("@Create Service_Method", String.valueOf(method));
            if (method == POST) {

                Log.d("@Create Service_Url", url);
                HttpPost httpPost = new HttpPost(url);
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }
                Log.d("@Create Service_POST_Url", url + " PARAMS " + params);
                httpResponse = httpClient.execute(httpPost);
                Log.d("@Create Service_POST_Res", httpResponse + "" );

            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                    Log.d("@Create Service_GET_Url", url);
                    Log.d("@Create Service_GET_params", params.toString());
                }
                HttpGet httpGet = new HttpGet(url);

                httpResponse = httpClient.execute(httpGet);
                Log.d("@Create Service_GET_Res", httpResponse.toString());
            } else if (method == PUT) {

                HttpPut httpPut = new HttpPut(url);
                if (params != null) {
                    httpPut.setEntity(new UrlEncodedFormEntity(params));
                }
                Log.d("@Create Service_PUT_Url", url);
                httpResponse = httpClient.execute(httpPut);
            } else if (method == DELETE) {

                if (params != null) {
                    Log.e("@Create DELETE params", "" + params);

                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;

                }
                Log.d("@Create Service_DELETE_Url", url);
                HttpDelete httpDelete = new HttpDelete(url);
                httpResponse = httpClient.execute(httpDelete);
            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

            Log.d("@Create Service_DELETE_Res", response);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }
}
