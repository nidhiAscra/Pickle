package com.ascra.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

@SuppressLint({"WorldWriteableFiles", "WorldReadableFiles", "CommitPrefEdits"})
public class SaveUtils {

    //@PK30NOV : GcmRegistraionId, GcmDeviceToken.
    public static final String GcmRegistraionId = "GcmRegistraionId";
    public static final String GcmDeviceToken = "GcmDeviceToken";
    public static final String BrandOrUserName = "BrandOrUserName";
    public static final String USER_ID = "USER_ID";
    public static final String KEY = "KEY";
    public static final String USER = "USER";
    public static final String AUTH = "AUTH";

    public static void saveUserID(String PREFERENCE_FILENAME, Context context,
                                  String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getUserID(String PREFERENCE_FILENAME, Context context,
                                   String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveUserNameToPref(String PREFERENCE_FILENAME,
                                          Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getUserNameFromPref(String PREFERENCE_FILENAME,
                                             Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void savePasswordToPref(String PREFERENCE_FILENAME,
                                          Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getPasswordFromPref(String PREFERENCE_FILENAME,
                                             Context context, String tag, String defaultString) {
        @SuppressLint("WorldWriteableFiles") SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveAuthKeyToPref(String PREFERENCE_FILENAME,
                                         Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getAuthKeyFromPref(String PREFERENCE_FILENAME,
                                            Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveKeyToPref(String PREFERENCE_FILENAME,
                                     Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getKeyFromPref(String PREFERENCE_FILENAME,
                                        Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveUserToPref(String PREFERENCE_FILENAME,
                                      Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getUserFromPref(String PREFERENCE_FILENAME,
                                         Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveLoginSessionPref(String PREFERENCE_FILENAME,
                                            Context context, String tag, boolean booleanValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putBoolean(tag, booleanValue);
        prefsEditor.commit();
    }

    public static boolean getLoginSessionFromPref(String PREFERENCE_FILENAME,
                                                  Context context, String tag) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getBoolean(tag, false);
    }

    public static void saveUserLoginFrom(String PREFERENCE_FILENAME,
                                         Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getUserLoginFrom(String PREFERENCE_FILENAME,
                                          Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void savePickleUserNameFrom(String PREFERENCE_FILENAME,
                                              Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getPickleUserNameFrom(String PREFERENCE_FILENAME,
                                               Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveFacebookUId(String PREFERENCE_FILENAME,
                                       Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getFacebookUIdFrom(String PREFERENCE_FILENAME,
                                            Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    public static void saveFacebookAccessToken(String PREFERENCE_FILENAME,
                                               Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    public static String getFacebooAccessTokenFrom(String PREFERENCE_FILENAME,
                                                   Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    //@PK30NOV
    //Save the Registration Id in another preference so as to to save if from getting cleared when LogOut is clicked
    public static void saveGcmRegistrationId(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    //@PK30NOV
    public static String getGcmRegistrationId(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

    //@PK30NOV
    //Save the device token in another preference so as to to save if from getting cleared when LogOut is clicked
    public static void saveGcmDeviceToken(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    //@PK30NOV
    public static String getGcmDeviceToken(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }
 //@PKDEC01
    //Save the device token in another preference so as to to save if from getting cleared when LogOut is clicked
    public static void saveBrandOrUserName(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();

        prefsEditor.putString(tag, stringValue);
        prefsEditor.commit();
    }

    //@PKDEC01
    public static String getBrandOrUserName(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {
        SharedPreferences myPrefs = context.getSharedPreferences(PREFERENCE_FILENAME,
                Context.MODE_WORLD_READABLE + Context.MODE_WORLD_WRITEABLE);
        return myPrefs.getString(tag, defaultString);
    }

}
