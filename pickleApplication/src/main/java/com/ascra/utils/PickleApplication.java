package com.ascra.utils;

import android.app.Application;

import com.ascra.interfaces.ApiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Pranav on 22 November 2016 @ 5:26 PM IST
 */
public class PickleApplication extends Application {
    private static final String NSDL_BASE_URL = Constant.PickleUrl_V1;//temp
    Retrofit retrofit;
    private Gson gson;
    private HttpLoggingInterceptor interceptor;
    private OkHttpClient client;
    private ApiService api;

    @Override
    public void onCreate() {
        super.onCreate();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(NSDL_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        api = retrofit.create(ApiService.class);
    }

    public Gson getGson() {
        return gson;
    }

    public HttpLoggingInterceptor getInterceptor() {
        return interceptor;
    }

    public OkHttpClient getClient() {
        return client;
    }

    public ApiService getApi() {
        return api;
    }
}
