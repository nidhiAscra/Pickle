package com.ascra.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ascra.pickle.BundlePickleFragment;
import com.ascra.pickle.HomeScreen;
import com.ascra.pickle.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Pranav on 06 December 2016 @ 10:57 AM IST
 */
public class CommonMethods {

    /**
     * This method is used to add the title header the listview.
     *
     * @param activity          : (required) Defines the activity context.
     * @param listView          : (required) The listview to which the header is to be added.
     * @param title             : (required) The title of the fragment/activity to be shown in header.
     * @param logoImageResource : (required) The logo/icon img resource (drawable) of the frag/activity
     *                          to be shown in header.
     * @see BundlePickleFragment#setUpViews() Where this is called.
     * @since 06 December 2016 @ 10:57 AM IST
     */
    public static void addHeaderToListview(Activity activity, ListView listView, String title, int logoImageResource) {
        LayoutInflater myinflater = activity.getLayoutInflater();
        ViewGroup myHeader = (ViewGroup) myinflater.inflate(R.layout.header_common, listView, false);
        TextView headerTitle = (TextView) myHeader.findViewById(R.id.headerTitle);
        ImageView logo = (ImageView) myHeader.findViewById(R.id.logo);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/sniglet-regular.ttf");
        headerTitle.setTypeface(typeface);
        headerTitle.setText(title);
        Picasso.with(activity).load(logoImageResource).into(logo);
        listView.addHeaderView(myHeader, null, false);
    }

    /**
     * This method is used to set the header icon and title.
     *
     * @param activity      : The activity context.
     * @param view          : The instance of view in which the header_title_view is included/present.
     * @param titleResource : The string resource (strings.xml) for the title.
     * @param logoResource  : The drawable resource (drawable folder) for the icon.
     * @since 06 December 2016 @ 12:04 AM IST
     */
    public static void setLogoTitleToHeaderView(Activity activity, View view, int titleResource, int logoResource) {
        TextView headerTitle = (TextView) view.findViewById(R.id.headerTitle);
        ImageView logo = (ImageView) view.findViewById(R.id.logo);
        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/sniglet-regular.ttf");
        headerTitle.setTypeface(typeface);
        headerTitle.setText(activity.getString(titleResource));
        Picasso.with(activity).load(logoResource).into(logo);
    }

    /**
     * Closes/hides the open keyboard.
     *
     * @since 06 Deceomber 2016 @ 06:56 PM IST
     */
    public static void closeKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * Shows/opens the keyboard.
     *
     * @since 07 Deceomber 2016 @ 12:58 PM IST
     */
    public static void showKeyboard(Activity activity, EditText editText) {

//        editText.requestFocus();
//        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        Log.e("showKeyboard", "Active =  " + imm.isActive());
        if (!imm.isActive()) {
            imm.toggleSoftInputFromWindow(editText.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
        }
    }

    /**
     * Shows/opens the keyboard forcefully.
     *
     * @since 08 December 2016 @ 12:13 PM IST
     */
    public static boolean showKeyboardForcefully(Activity activity, EditText editText) {
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED & InputMethodManager.HIDE_IMPLICIT_ONLY);
        try {
            Log.e("showKeyboard", "Forced Active =  " + imm.isActive() + "\tFragment = " + ((HomeScreen) activity).getSupportFragmentManager().findFragmentById(R.id.content_frame));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imm.isActive();
    }

}
