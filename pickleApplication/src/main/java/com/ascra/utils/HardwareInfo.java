package com.ascra.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

/**
 * @author Pranav
 * @since 12 December 2016 @ 12:24 pm IST
 */
public class HardwareInfo {

    private static String getModel() {
        return Build.MODEL;
    }

    private static String getOs() {
        return Build.VERSION.RELEASE + "(" + Build.VERSION.SDK_INT + ")";

    }

    private static String getScreenSize(Activity context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        return screenWidth + "x" + screenHeight;
//        WindowManager wm = (WindowManager) context.getSystemService(Activity.WINDOW_SERVICE);
//        Display disp = wm.getDefaultDisplay();
//        return disp.getWidth() + "x" + disp.getHeight();
    }

    private static String getDpi(Context context) {
        String dpi = null;
        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                dpi = "LDPI";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                dpi = "MDPI";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                dpi = "HDPI";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                dpi = "XHDPI";
                break;
            case DisplayMetrics.DENSITY_XXHIGH:
                dpi = "XXHDPI";
                break;
            case DisplayMetrics.DENSITY_XXXHIGH:
                dpi = "XXXHDPI";
                break;
            case DisplayMetrics.DENSITY_TV:
                dpi = "TVDPI";
                break;

        }

        dpi += "(x" + context.getResources().getDisplayMetrics().density + ")";
        return dpi;
    }

    /**
     * Prints Model - OS(API) - DensityType(DensityRation) - ScreenSize<br>
     * Example - MACHONE - 5.0.2(21) - XHDPI(x2.0) - 720x1280
     */
    public static void print(Activity context) {
        try {
            Log.e("HardwareInfo", HardwareInfo.getModel() + " - " + HardwareInfo.getOs() + " - " + HardwareInfo.getDpi(context)
                    + " - " + HardwareInfo.getScreenSize(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}