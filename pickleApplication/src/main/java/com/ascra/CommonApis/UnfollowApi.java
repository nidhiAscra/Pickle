package com.ascra.CommonApis;

import android.app.Activity;
import android.util.Log;

import com.ascra.interfaces.ApiService;
import com.ascra.interfaces.UnfollowResponse;
import com.ascra.utils.PickleApplication;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pranav on 12 December 2016 @ 5:22 PM IST
 */
public class UnfollowApi {
    public UnfollowResponse getResponse = null;
    private Activity activity;
    private ApiService api;

    public UnfollowApi(Activity activity, UnfollowResponse getResponse) {
        this.activity = activity;
        this.getResponse = getResponse;
        api = ((PickleApplication) this.activity.getApplicationContext()).getApi();
    }

    public void unfollow(String authToken, String key, String user_id, String other_user_id, String type, final int position) {
        api.unfollow(other_user_id, authToken, key, user_id, other_user_id, type).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                Log.e("Create", "Retro response:"+response.toString());
                getResponse.handleResponse(call, response, position);
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                getResponse.handleError(call, t, position);
            }
        });
    }
}
