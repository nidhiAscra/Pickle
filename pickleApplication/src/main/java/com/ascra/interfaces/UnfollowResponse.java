package com.ascra.interfaces;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Pranav on 12 December 2016 @ 5:29 PM IST
 */
public interface UnfollowResponse {
    public void handleResponse(Call<JSONObject> call, Response<JSONObject> response, int position);
    public void handleError(Call<JSONObject> call, Throwable t, int position);
}
