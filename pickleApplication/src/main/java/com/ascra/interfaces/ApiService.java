package com.ascra.interfaces;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Pranav on 12 December 2016 @ 5:23 PM IST
 */
public interface ApiService {
    @POST("followers/{id}.json")
    Call<JSONObject> unfollow(@Path(value = "id", encoded = true) String path_id,
                              @Query("authentication_token") String auth,
                              @Query("key") String key,
                              @Query("user_id") String user_id,
                              @Query("other_id") String other_id,
                              @Query("type") String type);
}
