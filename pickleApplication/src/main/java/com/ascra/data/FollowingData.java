package com.ascra.data;

import org.json.JSONObject;

public class FollowingData {

	int following_id;
	String following_email;
	String following_username;
	String following_profile_pic;
	String following_gender;
	String following_phone_no;
	String following_provider;
	String following_name;
	JSONObject objFollowings;
	boolean user_follow;
	boolean is_me;
	int userid;

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public boolean is_me() {
		return is_me;
	}

	public FollowingData(int following_id, String following_email,
						 String following_username, String following_profile_pic,
						 String following_gender, String following_phone_no,
						 String following_provider, String following_name,
						 JSONObject objFollowings, boolean user_follow, boolean is_me, int userid) {

		this.following_id = following_id;
		this.following_email = following_email;
		this.following_username = following_username;
		this.following_profile_pic = following_profile_pic;
		this.following_gender = following_gender;
		this.following_phone_no = following_phone_no;
		this.following_provider = following_provider;
		this.following_name = following_name;
		this.objFollowings = objFollowings;
		this.user_follow = user_follow;
		this.is_me = is_me;
		this.userid=userid;
	}

	public boolean isIs_me() {
		return is_me;
	}

	public void setIs_me(boolean is_me) {
		this.is_me = is_me;
	}

	public boolean isUser_follow() {
		return user_follow;
	}

	public void setUser_follow(boolean user_follow) {
		this.user_follow = user_follow;
	}

	public JSONObject getObjFollowings() {
		return objFollowings;
	}

	public void setObjFollowings(JSONObject objFollowings) {
		this.objFollowings = objFollowings;
	}

	public int getFollowing_id() {
		return following_id;
	}

	public void setFollowing_id(int following_id) {
		this.following_id = following_id;
	}

	public String getFollowing_email() {
		return following_email;
	}

	public void setFollowing_email(String following_email) {
		this.following_email = following_email;
	}

	public String getFollowing_username() {
		return following_username;
	}

	public void setFollowing_username(String following_username) {
		this.following_username = following_username;
	}

	public String getFollowing_profile_pic() {
		return following_profile_pic;
	}

	public void setFollowing_profile_pic(String following_profile_pic) {
		this.following_profile_pic = following_profile_pic;
	}

	public String getFollowing_gender() {
		return following_gender;
	}

	public void setFollowing_gender(String following_gender) {
		this.following_gender = following_gender;
	}

	public String getFollowing_phone_no() {
		return following_phone_no;
	}

	public void setFollowing_phone_no(String following_phone_no) {
		this.following_phone_no = following_phone_no;
	}

	public String getFollowing_provider() {
		return following_provider;
	}

	public void setFollowing_provider(String following_provider) {
		this.following_provider = following_provider;
	}

	public String getFollowing_name() {
		return following_name;
	}

	public void setFollowing_name(String following_name) {
		this.following_name = following_name;
	}

}
