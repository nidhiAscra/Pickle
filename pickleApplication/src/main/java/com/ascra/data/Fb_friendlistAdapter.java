package com.ascra.data;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.NameValuePair;

import com.ascra.pickle.Fb_friendlist.passselected;
import com.ascra.pickle.R;

import com.squareup.picasso.Picasso;



import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Fb_friendlistAdapter extends BaseAdapter {
	private Context mContext;
	private ArrayList<Fb_model> fb_friendlist;
	
	
	

	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Connection detector class
	

	@SuppressWarnings("unused")
	private LayoutInflater mLayoutInflater = null;

	public Fb_friendlistAdapter(Context context,
			ArrayList<Fb_model> Favorite_list) {
		this.mContext = context;
		this.fb_friendlist = Favorite_list;
		
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		
		
	}

	@Override
	public int getCount() {
		return fb_friendlist.size();
	}

	@Override
	public Object getItem(int pos) {
		return fb_friendlist.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View v = convertView;
		CompleteListViewHolder viewHolder;
		if (convertView == null) {
			LayoutInflater li = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = li.inflate(R.layout.fb_list_row, null);
			viewHolder = new CompleteListViewHolder(v);
			Animation myFadeInAnimation = AnimationUtils.loadAnimation(mContext,
					R.anim.image_animation);
			

						

			
			
			

			v.setTag(viewHolder);
		} else {
			viewHolder = (CompleteListViewHolder) v.getTag();
		}
		
		Picasso.with(mContext)
		.load(fb_friendlist.get(position)
				.getUrl())
		.into(viewHolder.friend_image);
		
		
		viewHolder.friend_name.setText(fb_friendlist.get(position).getFriend_name());

		  
		return v;
	}

	

	

}

class CompleteListViewHolder {
	// public TextView mTVItem;
	ImageView friend_image;
	TextView friend_name;
	

	public CompleteListViewHolder(View base) {
		friend_image = (ImageView) base.findViewById(R.id.friend_image);
		friend_name = (TextView) base.findViewById(R.id.friend_name);
		

	}

}