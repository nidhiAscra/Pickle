package com.ascra.data;

import java.util.ArrayList;

public class Model {

	public static Model instatnce;
	ArrayList<AllFeedsModel> arrayListOfFeeddata = new ArrayList<AllFeedsModel>();
	Boolean newcomments=false;

	public Boolean getNewcomments() {
		return newcomments;
	}

	public void setNewcomments(Boolean newcomments) {
		this.newcomments = newcomments;
	}
	public ArrayList<AllFeedsModel> getArrayListOfFeeddata() {
		return arrayListOfFeeddata;
	}

	public void setArrayListOfFeeddata(ArrayList<AllFeedsModel> arrayListOfFeeddata) {
		this.arrayListOfFeeddata = arrayListOfFeeddata;
	}

	public static Model getInstatnce() {
		if (instatnce == null)
			instatnce = new Model();
		return instatnce;
	}

	public static void setInstatnce(Model instatnce) {
		Model.instatnce = instatnce;
	}
}
