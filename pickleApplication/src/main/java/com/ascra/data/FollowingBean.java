package com.ascra.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

//-----------------------------------com.ascra.data.FollowingBean.java-----------------------------------

public class FollowingBean {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("followings")
    @Expose
    private List<Following> followings = null;

    /**
     * No args constructor for use in serialization
     */
    public FollowingBean() {
    }

    /**
     * @param followings
     * @param success
     */
    public FollowingBean(String success, List<Following> followings) {
        super();
        this.success = success;
        this.followings = followings;
    }

    /**
     * @return The success
     */
    public String getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(String success) {
        this.success = success;
    }

    /**
     * @return The followings
     */
    public List<Following> getFollowings() {
        return followings;
    }

    /**
     * @param followings The followings
     */
    public void setFollowings(List<Following> followings) {
        this.followings = followings;
    }

//-----------------------------------com.ascra.data.Following.java-----------------------------------

    public class Following {

        @SerializedName("id")
        @Expose
        private int id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("profile_pic")
        @Expose
        private String profilePic;
        @SerializedName("gender")
        @Expose
        private Object gender;
        @SerializedName("phone_no")
        @Expose
        private Object phoneNo;
        @SerializedName("provider")
        @Expose
        private String provider;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("is_me")
        @Expose
        private String isMe;
        @SerializedName("is_follow")
        @Expose
        private IsFollow isFollow;
        @SerializedName("user_follow")
        @Expose
        private String userFollow;

        /**
         * No args constructor for use in serialization
         */
        public Following() {
        }

        /**
         * @param isFollow
         * @param phoneNo
         * @param id
         * @param username
         * @param email
         * @param name
         * @param profilePic
         * @param gender
         * @param provider
         * @param userFollow
         * @param isMe
         */
        public Following(int id, String email, String username, String profilePic, Object gender, Object phoneNo, String provider, String name, String isMe, IsFollow isFollow, String userFollow) {
            super();
            this.id = id;
            this.email = email;
            this.username = username;
            this.profilePic = profilePic;
            this.gender = gender;
            this.phoneNo = phoneNo;
            this.provider = provider;
            this.name = name;
            this.isMe = isMe;
            this.isFollow = isFollow;
            this.userFollow = userFollow;
        }

        /**
         * @return The id
         */
        public int getId() {
            return id;
        }

        /**
         * @param id The id
         */
        public void setId(int id) {
            this.id = id;
        }

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The username
         */
        public String getUsername() {
            return username;
        }

        /**
         * @param username The username
         */
        public void setUsername(String username) {
            this.username = username;
        }

        /**
         * @return The profilePic
         */
        public String getProfilePic() {
            return profilePic;
        }

        /**
         * @param profilePic The profile_pic
         */
        public void setProfilePic(String profilePic) {
            this.profilePic = profilePic;
        }

        /**
         * @return The gender
         */
        public Object getGender() {
            return gender;
        }

        /**
         * @param gender The gender
         */
        public void setGender(Object gender) {
            this.gender = gender;
        }

        /**
         * @return The phoneNo
         */
        public Object getPhoneNo() {
            return phoneNo;
        }

        /**
         * @param phoneNo The phone_no
         */
        public void setPhoneNo(Object phoneNo) {
            this.phoneNo = phoneNo;
        }

        /**
         * @return The provider
         */
        public String getProvider() {
            return provider;
        }

        /**
         * @param provider The provider
         */
        public void setProvider(String provider) {
            this.provider = provider;
        }

        /**
         * @return The name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The isMe
         */
        public String getIsMe() {
            return isMe;
        }

        /**
         * @param isMe The is_me
         */
        public void setIsMe(String isMe) {
            this.isMe = isMe;
        }

        /**
         * @return The isFollow
         */
        public IsFollow getIsFollow() {
            return isFollow;
        }

        /**
         * @param isFollow The is_follow
         */
        public void setIsFollow(IsFollow isFollow) {
            this.isFollow = isFollow;
        }

        /**
         * @return The userFollow
         */
        public String getUserFollow() {
            return userFollow;
        }

        /**
         * @param userFollow The user_follow
         */
        public void setUserFollow(String userFollow) {
            this.userFollow = userFollow;
        }
//-----------------------------------com.ascra.data.IsFollow.java-----------------------------------

        public class IsFollow {

            @SerializedName("created_at")
            @Expose
            private String createdAt;
            @SerializedName("follower_user_id")
            @Expose
            private int followerUserId;
            @SerializedName("id")
            @Expose
            private int id;
            @SerializedName("updated_at")
            @Expose
            private String updatedAt;
            @SerializedName("user_id")
            @Expose
            private int userId;

            /**
             * No args constructor for use in serialization
             */
            public IsFollow() {
            }

            /**
             * @param updatedAt
             * @param id
             * @param createdAt
             * @param userId
             * @param followerUserId
             */
            public IsFollow(String createdAt, int followerUserId, int id, String updatedAt, int userId) {
                super();
                this.createdAt = createdAt;
                this.followerUserId = followerUserId;
                this.id = id;
                this.updatedAt = updatedAt;
                this.userId = userId;
            }

            /**
             * @return The createdAt
             */
            public String getCreatedAt() {
                return createdAt;
            }

            /**
             * @param createdAt The created_at
             */
            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            /**
             * @return The followerUserId
             */
            public int getFollowerUserId() {
                return followerUserId;
            }

            /**
             * @param followerUserId The follower_user_id
             */
            public void setFollowerUserId(int followerUserId) {
                this.followerUserId = followerUserId;
            }

            /**
             * @return The id
             */
            public int getId() {
                return id;
            }

            /**
             * @param id The id
             */
            public void setId(int id) {
                this.id = id;
            }

            /**
             * @return The updatedAt
             */
            public String getUpdatedAt() {
                return updatedAt;
            }

            /**
             * @param updatedAt The updated_at
             */
            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            /**
             * @return The userId
             */
            public int getUserId() {
                return userId;
            }

            /**
             * @param userId The user_id
             */
            public void setUserId(int userId) {
                this.userId = userId;
            }

        }
    }
}



