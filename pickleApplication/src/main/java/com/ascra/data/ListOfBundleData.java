package com.ascra.data;

public class ListOfBundleData {
	int id;
	String cover_pic_url;
	String name;
	String seo_name;
	boolean deactive;

	public ListOfBundleData(int id, String cover_pic_url, String name,
			String seo_name, boolean deactive) {
		this.id=id;
		this.cover_pic_url=cover_pic_url;
		this.name=name;
		this.seo_name=seo_name;
		this.deactive=deactive;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCover_pic_url() {
		return cover_pic_url;
	}

	public void setCover_pic_url(String cover_pic_url) {
		this.cover_pic_url = cover_pic_url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeo_name() {
		return seo_name;
	}

	public void setSeo_name(String seo_name) {
		this.seo_name = seo_name;
	}

	public boolean isDeactive() {
		return deactive;
	}

	public void setDeactive(boolean deactive) {
		this.deactive = deactive;
	}

}
