package com.ascra.data;

public class ShareBundlePickleData {

	boolean strPDeactive;
	boolean strPFeatured;
	String strPId;
	String strPImage;
	String strPName;
	String strPUserId;
	String pickle_created_at;
	boolean is_shared;
	long strFId;
	String strFOptionImageUrl;
	String strFOptionLable;
	long strFQuestionId;
	String strFirstOptionText;
	long strFirstOption;
	String first_option_voted_id;
	boolean first_option_is_voted;
	long strSId;
	String strSOptionImageUrl;
	String strSOptionLable;
	long strSQuestionId;
	String strSecondOptionText;
	long strSecondOption;
	String second_option_voted_id;
	boolean second_option_is_voted;

	public ShareBundlePickleData(boolean strPDeactive, boolean strPFeatured,
			String strPId, String strPImage, String strPName,
			String strPUserId, String pickle_created_at, boolean is_shared,
			long strFId, String strFOptionImageUrl, String strFOptionLable,
			long strFQuestionId, String strFirstOptionText,
			long strFirstOption, String first_option_voted_id,
			boolean first_option_is_voted, long strSId,
			String strSOptionImageUrl, String strSOptionLable,
			long strSQuestionId, String strSecondOptionText,
			long strSecondOption, String second_option_voted_id,
			boolean second_option_is_voted) {

		this.strPDeactive = strPDeactive;
		this.strPFeatured = strPFeatured;
		this.strPId = strPId;
		this.strPImage = strPImage;
		this.strPName = strPName;
		this.strPUserId = strPUserId;
		this.pickle_created_at = pickle_created_at;
		this.is_shared = is_shared;
		this.strFId = strFId;
		this.strFOptionImageUrl = strFOptionImageUrl;
		this.strFOptionLable = strFOptionLable;
		this.strFQuestionId = strFQuestionId;
		this.strFirstOptionText = strFirstOptionText;
		this.strFirstOption = strFirstOption;
		this.first_option_voted_id = first_option_voted_id;
		this.first_option_is_voted = first_option_is_voted;
		this.strSId = strSId;
		this.strSOptionImageUrl = strSOptionImageUrl;
		this.strSOptionLable = strSOptionLable;
		this.strSQuestionId = strSQuestionId;
		this.strSecondOptionText = strSecondOptionText;
		this.strSecondOption = strSecondOption;
		this.second_option_voted_id = second_option_voted_id;
		this.second_option_is_voted = second_option_is_voted;
	}

	public boolean isIs_shared() {
		return is_shared;
	}

	public void setIs_shared(boolean is_shared) {
		this.is_shared = is_shared;
	}

	public String getFirst_option_voted_id() {
		return first_option_voted_id;
	}

	public void setFirst_option_voted_id(String first_option_voted_id) {
		this.first_option_voted_id = first_option_voted_id;
	}

	public boolean isFirst_option_is_voted() {
		return first_option_is_voted;
	}

	public void setFirst_option_is_voted(boolean first_option_is_voted) {
		this.first_option_is_voted = first_option_is_voted;
	}

	public String getSecond_option_voted_id() {
		return second_option_voted_id;
	}

	public void setSecond_option_voted_id(String second_option_voted_id) {
		this.second_option_voted_id = second_option_voted_id;
	}

	public boolean isSecond_option_is_voted() {
		return second_option_is_voted;
	}

	public void setSecond_option_is_voted(boolean second_option_is_voted) {
		this.second_option_is_voted = second_option_is_voted;
	}

	public String getStrFirstOptionText() {
		return strFirstOptionText;
	}

	public void setStrFirstOptionText(String strFirstOptionText) {
		this.strFirstOptionText = strFirstOptionText;
	}

	public String getStrSecondOptionText() {
		return strSecondOptionText;
	}

	public void setStrSecondOptionText(String strSecondOptionText) {
		this.strSecondOptionText = strSecondOptionText;
	}

	public boolean isStrPDeactive() {
		return strPDeactive;
	}

	public void setStrPDeactive(boolean strPDeactive) {
		this.strPDeactive = strPDeactive;
	}

	public boolean isStrPFeatured() {
		return strPFeatured;
	}

	public void setStrPFeatured(boolean strPFeatured) {
		this.strPFeatured = strPFeatured;
	}

	public String getStrPId() {
		return strPId;
	}

	public void setStrPId(String strPId) {
		this.strPId = strPId;
	}

	public String getStrPImage() {
		return strPImage;
	}

	public void setStrPImage(String strPImage) {
		this.strPImage = strPImage;
	}

	public String getStrPName() {
		return strPName;
	}

	public void setStrPName(String strPName) {
		this.strPName = strPName;
	}

	public String getStrPUserId() {
		return strPUserId;
	}

	public void setStrPUserId(String strPUserId) {
		this.strPUserId = strPUserId;
	}

	public String getPickle_created_at() {
		return pickle_created_at;
	}

	public void setPickle_created_at(String pickle_created_at) {
		this.pickle_created_at = pickle_created_at;
	}

	public long getStrFId() {
		return strFId;
	}

	public void setStrFId(long strFId) {
		this.strFId = strFId;
	}

	public String getStrFOptionImageUrl() {
		return strFOptionImageUrl;
	}

	public void setStrFOptionImageUrl(String strFOptionImageUrl) {
		this.strFOptionImageUrl = strFOptionImageUrl;
	}

	public String getStrFOptionLable() {
		return strFOptionLable;
	}

	public void setStrFOptionLable(String strFOptionLable) {
		this.strFOptionLable = strFOptionLable;
	}

	public long getStrFQuestionId() {
		return strFQuestionId;
	}

	public void setStrFQuestionId(long strFQuestionId) {
		this.strFQuestionId = strFQuestionId;
	}

	public long getStrFirstOption() {
		return strFirstOption;
	}

	public void setStrFirstOption(long strFirstOption) {
		this.strFirstOption = strFirstOption;
	}

	public long getStrSId() {
		return strSId;
	}

	public void setStrSId(long strSId) {
		this.strSId = strSId;
	}

	public String getStrSOptionImageUrl() {
		return strSOptionImageUrl;
	}

	public void setStrSOptionImageUrl(String strSOptionImageUrl) {
		this.strSOptionImageUrl = strSOptionImageUrl;
	}

	public String getStrSOptionLable() {
		return strSOptionLable;
	}

	public void setStrSOptionLable(String strSOptionLable) {
		this.strSOptionLable = strSOptionLable;
	}

	public long getStrSQuestionId() {
		return strSQuestionId;
	}

	public void setStrSQuestionId(long strSQuestionId) {
		this.strSQuestionId = strSQuestionId;
	}

	public long getStrSecondOption() {
		return strSecondOption;
	}

	public void setStrSecondOption(long strSecondOption) {
		this.strSecondOption = strSecondOption;
	}

}
