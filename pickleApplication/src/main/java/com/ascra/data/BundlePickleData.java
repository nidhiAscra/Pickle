package com.ascra.data;

public class BundlePickleData {

	String cover_pic_content_type;
	String cover_pic_file_name;
	long cover_pic_file_size;
	String cover_pic_updated_at;
	String created_at;
	boolean deactive;
	String description;
	int id;
	String name;
	String seo_name;
	String updated_at;

	public BundlePickleData(String cover_pic_content_type,
			String cover_pic_file_name, long cover_pic_file_size,
			String cover_pic_updated_at, String created_at, boolean deactive,
			String description, int id, String name, String seo_name,
			String updated_at) {

		this.cover_pic_content_type = cover_pic_content_type;
		this.cover_pic_file_name = cover_pic_file_name;
		this.cover_pic_file_size = cover_pic_file_size;
		this.cover_pic_updated_at = cover_pic_updated_at;
		this.created_at = created_at;
		this.deactive = deactive;
		this.description = description;
		this.id = id;
		this.name = name;
		this.seo_name = seo_name;
		this.updated_at = updated_at;
	}

	public String getCover_pic_content_type() {
		return cover_pic_content_type;
	}

	public void setCover_pic_content_type(String cover_pic_content_type) {
		this.cover_pic_content_type = cover_pic_content_type;
	}

	public String getCover_pic_file_name() {
		return cover_pic_file_name;
	}

	public void setCover_pic_file_name(String cover_pic_file_name) {
		this.cover_pic_file_name = cover_pic_file_name;
	}

	public long getCover_pic_file_size() {
		return cover_pic_file_size;
	}

	public void setCover_pic_file_size(long cover_pic_file_size) {
		this.cover_pic_file_size = cover_pic_file_size;
	}

	public String getCover_pic_updated_at() {
		return cover_pic_updated_at;
	}

	public void setCover_pic_updated_at(String cover_pic_updated_at) {
		this.cover_pic_updated_at = cover_pic_updated_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public boolean isDeactive() {
		return deactive;
	}

	public void setDeactive(boolean deactive) {
		this.deactive = deactive;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSeo_name() {
		return seo_name;
	}

	public void setSeo_name(String seo_name) {
		this.seo_name = seo_name;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

}
