package com.ascra.data;

import org.json.JSONArray;
import org.json.JSONObject;
public class AllFeedsModel {

	JSONArray auditArray;
	private JSONObject first_option,second_option;
	int strUId2;
	String strUEmail2;
	String strUIs_brand2;
	String strUName2;
	String strUProfilePicUrl2;
	String strUserName2;
	boolean strPDeactive2;
	boolean strPFeatured2;
	String strPId2;
	String strPImage2;
	String strPName2;
	String strPUserId2;
	String strPCreatedDate2;
	boolean strIsPickleShared;
	String sharePickleId;
	boolean is_my_pickle;

	long strFId2;
	String strFOptionImageUrl2;
	String strFOptionLable2;
	long strFQuestionId2;
	long strFirstOption2;
	String firstOptionText;
	boolean first_option_is_voted;
	String first_option_voted_id;

	long strSId2;
	String strSOptionImageUrl2;
	String strSOptionLable2;
	long strSQuestionId2;
	long strSecondOption2;
	String secondOptionText;
	boolean second_option_is_voted;
	String second_option_voted_id;

	JSONArray arrayOfComments2;

	public AllFeedsModel(JSONArray auditArray, int strUId2, String strUEmail2,
			String strUIs_brand2, String strUName2, String strUProfilePicUrl2,
			String strUserName2, boolean strPDeactive2, boolean strPFeatured2,
			String strPId2, String strPImage2, String strPName2,
			String strPUserId2, String strPCreatedDate2,
			boolean isPickleShared, String sharePickleId, boolean isMyPickle,
			long strFId2, String strFOptionImageUrl2, String strFOptionLable2,
			long strFQuestionId2, long strFirstOption2,
			String strFirstOptionText, String first_option_voted_id,
			boolean first_option_is_voted, long strSId2,
			String strSOptionImageUrl2, String strSOptionLable2,
			long strSQuestionId2, long strSecondOption2,
			String strSecondOptionText, String second_option_voted_id,
			boolean second_option_is_voted, JSONArray arrayOfComments2) {

		this.auditArray = auditArray;

		this.strUId2 = strUId2;
		this.strUEmail2 = strUEmail2;
		this.strUIs_brand2 = strUIs_brand2;
		this.strUName2 = strUName2;
		this.strUProfilePicUrl2 = strUProfilePicUrl2;
		this.strUserName2 = strUserName2;
		this.strPDeactive2 = strPDeactive2;
		this.strPFeatured2 = strPFeatured2;
		this.strPId2 = strPId2;
		this.strPImage2 = strPImage2;
		this.strPName2 = strPName2;
		this.strPUserId2 = strPUserId2;
		this.strPCreatedDate2 = strPCreatedDate2;
		this.strIsPickleShared = isPickleShared;
		this.sharePickleId = sharePickleId;
		this.is_my_pickle = isMyPickle;
		this.strFId2 = strFId2;
		this.strFOptionImageUrl2 = strFOptionImageUrl2;
		this.strFOptionLable2 = strFOptionLable2;
		this.strFQuestionId2 = strFQuestionId2;
		this.strFirstOption2 = strFirstOption2;
		this.firstOptionText = strFirstOptionText;
		this.first_option_is_voted = first_option_is_voted;
		this.first_option_voted_id = first_option_voted_id;
		this.strSId2 = strSId2;
		this.strSOptionImageUrl2 = strSOptionImageUrl2;
		this.strSOptionLable2 = strSOptionLable2;
		this.strSQuestionId2 = strSQuestionId2;
		this.strSecondOption2 = strSecondOption2;
		this.secondOptionText = strSecondOptionText;
		this.second_option_is_voted = second_option_is_voted;
		this.second_option_voted_id = second_option_voted_id;
		this.arrayOfComments2 = arrayOfComments2;

	}

	public boolean isFirst_option_is_voted() {
		return first_option_is_voted;
	}

	public void setFirst_option_is_voted(boolean first_option_is_voted) {
		this.first_option_is_voted = first_option_is_voted;
	}

	public boolean isSecond_option_is_voted() {
		return second_option_is_voted;
	}

	public void setSecond_option_is_voted(boolean second_option_is_voted) {
		this.second_option_is_voted = second_option_is_voted;
	}

	public String getFirstOptionText() {
		return firstOptionText;
	}

	public void setFirstOptionText(String firstOptionText) {
		this.firstOptionText = firstOptionText;
	}

	public String getSecondOptionText() {
		return secondOptionText;
	}

	public void setSecondOptionText(String secondOptionText) {
		this.secondOptionText = secondOptionText;
	}

	public JSONArray getAuditArray() {
		return auditArray;
	}

	public void setAuditArray(JSONArray auditArray) {
		this.auditArray = auditArray;
	}

	public int getStrUId2() {
		return strUId2;
	}

	public void setStrUId2(int strUId2) {
		this.strUId2 = strUId2;
	}

	public String getStrUEmail2() {
		return strUEmail2;
	}

	public void setStrUEmail2(String strUEmail2) {
		this.strUEmail2 = strUEmail2;
	}

	public String isStrUIs_brand2() {
		return strUIs_brand2;
	}

	public void setStrUIs_brand2(String strUIs_brand2) {
		this.strUIs_brand2 = strUIs_brand2;
	}

	public String getStrUName2() {
		return strUName2;
	}

	public void setStrUName2(String strUName2) {
		this.strUName2 = strUName2;
	}

	public String getStrUProfilePicUrl2() {
		return strUProfilePicUrl2;
	}

	public void setStrUProfilePicUrl2(String strUProfilePicUrl2) {
		this.strUProfilePicUrl2 = strUProfilePicUrl2;
	}

	public String getStrUserName2() {
		return strUserName2;
	}

	public void setStrUserName2(String strUserName2) {
		this.strUserName2 = strUserName2;
	}

	public boolean isStrPDeactive2() {
		return strPDeactive2;
	}

	public void setStrPDeactive2(boolean strPDeactive2) {
		this.strPDeactive2 = strPDeactive2;
	}

	public boolean isStrPFeatured2() {
		return strPFeatured2;
	}

	public void setStrPFeatured2(boolean strPFeatured2) {
		this.strPFeatured2 = strPFeatured2;
	}

	public String getStrPId2() {
		return strPId2;
	}

	public void setStrPId2(String strPId2) {
		this.strPId2 = strPId2;
	}

	public String getStrPImage2() {
		return strPImage2;
	}

	public void setStrPImage2(String strPImage2) {
		this.strPImage2 = strPImage2;
	}

	public String getStrPName2() {
		return strPName2;
	}

	public void setStrPName2(String strPName2) {
		this.strPName2 = strPName2;
	}

	public String getStrPUserId2() {
		return strPUserId2;
	}

	public void setStrPUserId2(String strPUserId2) {
		this.strPUserId2 = strPUserId2;
	}

	public String getStrPCreatedDate() {
		return strPCreatedDate2;
	}

	public void setStrPCreatedDate(String strPCreatedDate2) {
		this.strPCreatedDate2 = strPCreatedDate2;
	}

	public long getStrFId2() {
		return strFId2;
	}

	public void setStrFId2(long strFId2) {
		this.strFId2 = strFId2;
	}

	public String getStrFOptionImageUrl2() {
		return strFOptionImageUrl2;
	}

	public void setStrFOptionImageUrl2(String strFOptionImageUrl2) {
		this.strFOptionImageUrl2 = strFOptionImageUrl2;
	}

	public String getStrFOptionLable2() {
		return strFOptionLable2;
	}

	public void setStrFOptionLable2(String strFOptionLable2) {
		this.strFOptionLable2 = strFOptionLable2;
	}

	public long getStrFQuestionId2() {
		return strFQuestionId2;
	}

	public void setStrFQuestionId2(long strFQuestionId2) {
		this.strFQuestionId2 = strFQuestionId2;
	}

	public long getStrFirstOption2() {
		return strFirstOption2;
	}

	public void setStrFirstOption2(long strFirstOption2) {
		this.strFirstOption2 = strFirstOption2;
	}

	public long getStrSId2() {
		return strSId2;
	}

	public void setStrSId2(long strSId2) {
		this.strSId2 = strSId2;
	}

	public String getStrSOptionImageUrl2() {
		return strSOptionImageUrl2;
	}

	public void setStrSOptionImageUrl2(String strSOptionImageUrl2) {
		this.strSOptionImageUrl2 = strSOptionImageUrl2;
	}

	public String getStrSOptionLable2() {
		return strSOptionLable2;
	}

	public void setStrSOptionLable2(String strSOptionLable2) {
		this.strSOptionLable2 = strSOptionLable2;
	}

	public long getStrSQuestionId2() {
		return strSQuestionId2;
	}

	public void setStrSQuestionId2(long strSQuestionId2) {
		this.strSQuestionId2 = strSQuestionId2;
	}

	public long getStrSecondOption2() {
		return strSecondOption2;
	}

	public void setStrSecondOption2(long strSecondOption2) {
		this.strSecondOption2 = strSecondOption2;
	}

	public JSONArray getArrayOfComments2() {
		return arrayOfComments2;
	}

	public void setArrayOfComments2(JSONArray arrayOfComments2) {
		this.arrayOfComments2 = arrayOfComments2;
	}

	public String getFirst_option_voted_id() {
		return first_option_voted_id;
	}

	public void setFirst_option_voted_id(String first_option_voted_id) {
		this.first_option_voted_id = first_option_voted_id;
	}

	public String getSecond_option_voted_id() {
		return second_option_voted_id;
	}

	public void setSecond_option_voted_id(String second_option_voted_id) {
		this.second_option_voted_id = second_option_voted_id;
	}

	public boolean isStrIsPickleShared() {
		return strIsPickleShared;
	}

	public void setStrIsPickleShared(boolean strIsPickleShared) {
		this.strIsPickleShared = strIsPickleShared;
	}

	public String getSharePickleId() {
		return sharePickleId;
	}

	public void setSharePickleId(String sharePickleId) {
		this.sharePickleId = sharePickleId;
	}

	public boolean isIs_my_pickle() {
		return is_my_pickle;
	}

	public void setIs_my_pickle(boolean is_my_pickle) {
		this.is_my_pickle = is_my_pickle;
	}

	public JSONObject getFirst_option() {
		return first_option;
	}

	public void setFirst_option(JSONObject first_option) {
		this.first_option = first_option;
	}

	public JSONObject getSecond_option() {
		return second_option;
	}

	public void setSecond_option(JSONObject second_option) {
		this.second_option = second_option;
	}

	
}
