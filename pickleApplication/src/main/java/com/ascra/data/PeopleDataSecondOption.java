package com.ascra.data;

public class PeopleDataSecondOption {

	String tags_id;
	String tagged_user_id;
	String tagged_user_name;
	String tagged_user_email;
	String pickle_id;
	String pickle_name;
	String option_id;
	String option_image;
	String option_label;

	public PeopleDataSecondOption(String tags_id, String tagged_user_name,
			String tagged_user_id, String tagged_user_email, String pickle_id,
			String pickle_name, String option_id, String option_image,
			String option_label) {
		this.tags_id = tags_id;
		this.tagged_user_id = tagged_user_id;
		this.tagged_user_name = tagged_user_name;
		this.tagged_user_email = tagged_user_email;
		this.pickle_id = pickle_id;
		this.pickle_name = pickle_name;
		this.option_id = option_id;
		this.option_image = option_image;
		this.option_label = option_label;
	}

	public String getTags_id() {
		return tags_id;
	}

	public void setTags_id(String tags_id) {
		this.tags_id = tags_id;
	}

	public String getTagged_user_id() {
		return tagged_user_id;
	}

	public void setTagged_user_id(String tagged_user_id) {
		this.tagged_user_id = tagged_user_id;
	}

	public String getTagged_user_name() {
		return tagged_user_name;
	}

	public void setTagged_user_name(String tagged_user_name) {
		this.tagged_user_name = tagged_user_name;
	}

	public String getTagged_user_email() {
		return tagged_user_email;
	}

	public void setTagged_user_email(String tagged_user_email) {
		this.tagged_user_email = tagged_user_email;
	}

	public String getPickle_id() {
		return pickle_id;
	}

	public void setPickle_id(String pickle_id) {
		this.pickle_id = pickle_id;
	}

	public String getPickle_name() {
		return pickle_name;
	}

	public void setPickle_name(String pickle_name) {
		this.pickle_name = pickle_name;
	}

	public String getOption_id() {
		return option_id;
	}

	public void setOption_id(String option_id) {
		this.option_id = option_id;
	}

	public String getOption_image() {
		return option_image;
	}

	public void setOption_image(String option_image) {
		this.option_image = option_image;
	}

	public String getOption_label() {
		return option_label;
	}

	public void setOption_label(String option_label) {
		this.option_label = option_label;
	}

}
