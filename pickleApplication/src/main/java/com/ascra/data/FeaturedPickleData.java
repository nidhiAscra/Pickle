package com.ascra.data;

public class FeaturedPickleData {

	String featured_id;
	String featured_name;
	String featured_user_id;
	String featured_bundle_id;
	String featured_featured;
	String featured_status;
	String featured_deactive;
	String featured_image;
	String featured_is_featured;
	String option_one_option_image;
	String option_one_name;
	String option_one_fb_id;
	String option_two_option_image;
	String option_two_name;
	String option_two_fb_id;

	public FeaturedPickleData(String featured_id, String featured_name,
			String featured_user_id, String featured_bundle_id,
			String featured_featured, String featured_status,
			String featured_deactive, String featured_image,
			String featured_is_featured, String option_one_option_image,
			String option_one_name, String option_one_fb_id,
			String option_two_option_image, String option_two_name,
			String option_two_fb_id) {

		this.featured_id = featured_id;
		this.featured_name = featured_name;
		this.featured_user_id = featured_user_id;
		this.featured_bundle_id = featured_bundle_id;
		this.featured_featured = featured_featured;
		this.featured_status = featured_status;
		this.featured_deactive = featured_deactive;
		this.featured_image = featured_image;
		this.featured_is_featured = featured_is_featured;
		this.option_one_option_image = option_one_option_image;
		this.option_one_name = option_one_name;
		this.option_one_fb_id = option_one_fb_id;
		this.option_two_option_image = option_two_option_image;
		this.option_two_name = option_two_name;
		this.option_two_fb_id = option_two_fb_id;
	}

	public String getOption_one_fb_id() {
		return option_one_fb_id;
	}

	public void setOption_one_fb_id(String option_one_fb_id) {
		this.option_one_fb_id = option_one_fb_id;
	}

	public String getOption_two_fb_id() {
		return option_two_fb_id;
	}

	public void setOption_two_fb_id(String option_two_fb_id) {
		this.option_two_fb_id = option_two_fb_id;
	}

	public String getFeatured_id() {
		return featured_id;
	}

	public void setFeatured_id(String featured_id) {
		this.featured_id = featured_id;
	}

	public String getFeatured_name() {
		return featured_name;
	}

	public void setFeatured_name(String featured_name) {
		this.featured_name = featured_name;
	}

	public String getFeatured_user_id() {
		return featured_user_id;
	}

	public void setFeatured_user_id(String featured_user_id) {
		this.featured_user_id = featured_user_id;
	}

	public String getFeatured_bundle_id() {
		return featured_bundle_id;
	}

	public void setFeatured_bundle_id(String featured_bundle_id) {
		this.featured_bundle_id = featured_bundle_id;
	}

	public String isFeatured_featured() {
		return featured_featured;
	}

	public void setFeatured_featured(String featured_featured) {
		this.featured_featured = featured_featured;
	}

	public String getFeatured_status() {
		return featured_status;
	}

	public void setFeatured_status(String featured_status) {
		this.featured_status = featured_status;
	}

	public String isFeatured_deactive() {
		return featured_deactive;
	}

	public void setFeatured_deactive(String featured_deactive) {
		this.featured_deactive = featured_deactive;
	}

	public String getFeatured_image() {
		return featured_image;
	}

	public void setFeatured_image(String featured_image) {
		this.featured_image = featured_image;
	}

	public String isFeatured_is_featured() {
		return featured_is_featured;
	}

	public void setFeatured_is_featured(String featured_is_featured) {
		this.featured_is_featured = featured_is_featured;
	}

	public String getOption_one_option_image() {
		return option_one_option_image;
	}

	public void setOption_one_option_image(String option_one_option_image) {
		this.option_one_option_image = option_one_option_image;
	}

	public String getOption_one_name() {
		return option_one_name;
	}

	public void setOption_one_name(String option_one_name) {
		this.option_one_name = option_one_name;
	}

	public String getOption_two_option_image() {
		return option_two_option_image;
	}

	public void setOption_two_option_image(String option_two_option_image) {
		this.option_two_option_image = option_two_option_image;
	}

	public String getOption_two_name() {
		return option_two_name;
	}

	public void setOption_two_name(String option_two_name) {
		this.option_two_name = option_two_name;
	}

}
