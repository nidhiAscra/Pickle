package com.ascra.data;

public class Vote {
	String voter_id;
	String option_id;
	String user_id;
	String share_pickle_id;
	String voter_name;
	String voter_username;
	String voter_profile_pic;

	public Vote(String voter_id, String option_id, String user_id,
			String share_pickle_id, String voter_name, String voter_username,
			String voter_profile_pic) {
		this.voter_id = voter_id;
		this.option_id = option_id;
		this.user_id = user_id;
		this.share_pickle_id = share_pickle_id;
		this.voter_name = voter_name;
		this.voter_username = voter_username;
		this.voter_profile_pic = voter_profile_pic;

	}

	public String getVoter_id() {
		return voter_id;
	}

	public void setVoter_id(String voter_id) {
		this.voter_id = voter_id;
	}

	public String getOption_id() {
		return option_id;
	}

	public void setOption_id(String option_id) {
		this.option_id = option_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getShare_pickle_id() {
		return share_pickle_id;
	}

	public void setShare_pickle_id(String share_pickle_id) {
		this.share_pickle_id = share_pickle_id;
	}

	public String getVoter_name() {
		return voter_name;
	}

	public void setVoter_name(String voter_name) {
		this.voter_name = voter_name;
	}

	public String getVoter_username() {
		return voter_username;
	}

	public void setVoter_username(String voter_username) {
		this.voter_username = voter_username;
	}

	public String getVoter_profile_pic() {
		return voter_profile_pic;
	}

	public void setVoter_profile_pic(String voter_profile_pic) {
		this.voter_profile_pic = voter_profile_pic;
	}

}
