package com.ascra.data;

public class TaggedPicklesUserData {

	boolean pickle_deactive;
	boolean pickle_featured;
	String pickle_id;
	String pickle_image;
	String pickle_name;
	int user_id;
	String pickle_created_at;

	long first_option_id;
	String first_option_image_url;
	String first_option_lable;
	String first_option_text;

	long second_option_id;
	String second_option_image_url;
	String second_option_lable;
	String second_option_text;

	public TaggedPicklesUserData(boolean pickle_deactive,
			boolean pickle_featured, String pickle_id, String pickle_image,
			String pickle_name, int user_id, String pickle_created_at,
			long first_option_id, String first_option_image_url,
			String first_option_lable, String first_option_text,
			long second_option_id, String second_option_image_url,
			String second_option_lable, String second_option_text) {

		this.pickle_deactive = pickle_deactive;
		this.pickle_featured = pickle_featured;
		this.pickle_id = pickle_id;
		this.pickle_image = pickle_image;
		this.pickle_name = pickle_name;
		this.user_id = user_id;
		this.pickle_created_at = pickle_created_at;
		this.first_option_id = first_option_id;
		this.first_option_image_url = first_option_image_url;
		this.first_option_lable = first_option_lable;
		this.first_option_text = first_option_text;
		this.second_option_id = second_option_id;
		this.second_option_image_url = second_option_image_url;
		this.second_option_lable = second_option_lable;
		this.second_option_text = second_option_text;
	}

	public String getFirst_option_text() {
		return first_option_text;
	}

	public void setFirst_option_text(String first_option_text) {
		this.first_option_text = first_option_text;
	}

	public String getSecond_option_text() {
		return second_option_text;
	}

	public void setSecond_option_text(String second_option_text) {
		this.second_option_text = second_option_text;
	}

	public boolean isPickle_deactive() {
		return pickle_deactive;
	}

	public void setPickle_deactive(boolean pickle_deactive) {
		this.pickle_deactive = pickle_deactive;
	}

	public boolean isPickle_featured() {
		return pickle_featured;
	}

	public void setPickle_featured(boolean pickle_featured) {
		this.pickle_featured = pickle_featured;
	}

	public String getPickle_id() {
		return pickle_id;
	}

	public void setPickle_id(String pickle_id) {
		this.pickle_id = pickle_id;
	}

	public String getPickle_image() {
		return pickle_image;
	}

	public void setPickle_image(String pickle_image) {
		this.pickle_image = pickle_image;
	}

	public String getPickle_name() {
		return pickle_name;
	}

	public void setPickle_name(String pickle_name) {
		this.pickle_name = pickle_name;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getPickle_created_at() {
		return pickle_created_at;
	}

	public void setPickle_created_at(String pickle_created_at) {
		this.pickle_created_at = pickle_created_at;
	}

	public long getFirst_option_id() {
		return first_option_id;
	}

	public void setFirst_option_id(long first_option_id) {
		this.first_option_id = first_option_id;
	}

	public String getFirst_option_image_url() {
		return first_option_image_url;
	}

	public void setFirst_option_image_url(String first_option_image_url) {
		this.first_option_image_url = first_option_image_url;
	}

	public String getFirst_option_lable() {
		return first_option_lable;
	}

	public void setFirst_option_lable(String first_option_lable) {
		this.first_option_lable = first_option_lable;
	}

	public long getSecond_option_id() {
		return second_option_id;
	}

	public void setSecond_option_id(long second_option_id) {
		this.second_option_id = second_option_id;
	}

	public String getSecond_option_image_url() {
		return second_option_image_url;
	}

	public void setSecond_option_image_url(String second_option_image_url) {
		this.second_option_image_url = second_option_image_url;
	}

	public String getSecond_option_lable() {
		return second_option_lable;
	}

	public void setSecond_option_lable(String second_option_lable) {
		this.second_option_lable = second_option_lable;
	}
}
