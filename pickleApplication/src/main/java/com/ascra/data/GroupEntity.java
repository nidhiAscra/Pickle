package com.ascra.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

public class GroupEntity {
	public List<String> Name;
	public List<GroupItemEntity> GroupItemCollection;

	public GroupEntity()
	{
		GroupItemCollection = new ArrayList<GroupItemEntity>();
	}

	public class GroupItemEntity
	{
		public String Name;
		public HashMap<String, List<JSONObject>> notificationList;
	}
}
