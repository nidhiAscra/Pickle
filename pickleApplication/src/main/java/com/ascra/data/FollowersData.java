package com.ascra.data;

public class FollowersData {

	int user_id;
	int followers_id;
	String followers_email;
	String followers_username;
	String followers_profile_pic;
	String followers_gender;
	String followers_phone_no;
	String followers_provider;
	String followers_name;
	boolean is_me;
	boolean user_follow;

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public FollowersData(int followers_id, String followers_email,
						 String followers_username, String followers_profile_pic,
						 String followers_gender, String followers_phone_no,
						 String followers_provider, String followers_name, boolean is_me,
						 boolean user_follow, int user_id) {

		this.followers_id = followers_id;
		this.followers_email = followers_email;
		this.followers_username = followers_username;
		this.followers_profile_pic = followers_profile_pic;
		this.followers_gender = followers_gender;
		this.followers_phone_no = followers_phone_no;
		this.followers_provider = followers_provider;
		this.followers_name = followers_name;
		this.is_me = is_me;
		this.user_follow = user_follow;
		this.user_id=user_id;
	}

	public boolean isIs_me() {
		return is_me;
	}

	public void setIs_me(boolean is_me) {
		this.is_me = is_me;
	}

	public boolean isUser_follow() {
		return user_follow;
	}

	public void setUser_follow(boolean user_follow) {
		this.user_follow = user_follow;
	}

	public int getfollowers_id() {
		return followers_id;
	}

	public void setfollowers_id(int followers_id) {
		this.followers_id = followers_id;
	}

	public String getfollowers_email() {
		return followers_email;
	}

	public void setfollowers_email(String followers_email) {
		this.followers_email = followers_email;
	}

	public String getfollowers_username() {
		return followers_username;
	}

	public void setfollowers_username(String followers_username) {
		this.followers_username = followers_username;
	}

	public String getfollowers_profile_pic() {
		return followers_profile_pic;
	}

	public void setfollowers_profile_pic(String followers_profile_pic) {
		this.followers_profile_pic = followers_profile_pic;
	}

	public String getfollowers_gender() {
		return followers_gender;
	}

	public void setfollowers_gender(String followers_gender) {
		this.followers_gender = followers_gender;
	}

	public String getfollowers_phone_no() {
		return followers_phone_no;
	}

	public void setfollowers_phone_no(String followers_phone_no) {
		this.followers_phone_no = followers_phone_no;
	}

	public String getfollowers_provider() {
		return followers_provider;
	}

	public void setfollowers_provider(String followers_provider) {
		this.followers_provider = followers_provider;
	}

	public String getfollowers_name() {
		return followers_name;
	}

	public void setfollowers_name(String followers_name) {
		this.followers_name = followers_name;
	}

}
