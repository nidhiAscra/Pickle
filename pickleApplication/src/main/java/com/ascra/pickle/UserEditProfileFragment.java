package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class UserEditProfileFragment extends Fragment implements
        OnClickListener {

    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    View view;

    EditText etdName, etdUserName, etdWebsite, etdDesignation, etdBio,
            etdEmailId, etdPhoneNumber;
    RadioGroup selectGender;
    RadioButton radioSexButton;
    Button btnYes, btnNo, btnSave;

    String strSelection="";
    int selectedId;

    String gender;
    String PhoneNumber;
    HttpEntity resEntity;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    Tracker tracker;
    ProgressDialog rpd;
    Typeface face;
    ImageView imgProfile;
    Button btnChange;
    private String URL_FEED = Constant.PickleUrl + "users.json";
    private int PICK_IMAGE = 11;
    private Bitmap bitmap;
    private Uri filePath;

    String tempPath1;
    private Dialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.edit_profile_fragment, null);

        //@PK30NOV
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HomeScreen.whichFragment = "EDITPROFILE";

        setUpViews();
        setTracker();

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        return view;
    }

    private void setUpViews() {
        //@PKDEC06
        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.editProfileHeader, R.drawable.pickleprofile_color);


        //NS19DEC Added profile pic change functionality

        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        btnChange = (Button) view.findViewById(R.id.btnChange);
        btnChange.setOnClickListener(this);

        etdName = (EditText) view.findViewById(R.id.etdName);
        etdUserName = (EditText) view.findViewById(R.id.etdUserName);
        etdWebsite = (EditText) view.findViewById(R.id.etdWebsite);
        etdDesignation = (EditText) view.findViewById(R.id.etdDesignation);
        etdBio = (EditText) view.findViewById(R.id.etdBio);
        etdEmailId = (EditText) view.findViewById(R.id.etdEmailId);
        etdPhoneNumber = (EditText) view.findViewById(R.id.etdPhoneNumber);
        btnYes = (Button) view.findViewById(R.id.btnYes);
        btnNo = (Button) view.findViewById(R.id.btnNo);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        selectGender = (RadioGroup) view.findViewById(R.id.radioGrp);


        etdName.setText(UserProfileFragment.name);
        if (UserProfileFragment.profilePicUrl != null) {
           new ImageLoader(getActivity()).DisplayImage(UserProfileFragment.profilePicUrl, imgProfile);
        }

        etdUserName.setEnabled(false);
        etdUserName.setText(UserProfileFragment.userName);

        if (!UserProfileFragment.website.equals("null")) {
            etdWebsite.setText(UserProfileFragment.website);
        }

        if (!UserProfileFragment.designation.equals("null")) {
            etdDesignation.setText(UserProfileFragment.designation);
        }

        if (!UserProfileFragment.bio.equals("null")) {
            etdBio.setText(UserProfileFragment.bio);
        }

        etdEmailId.setEnabled(false);
        etdEmailId.setText(UserProfileFragment.emaiId);

        if (!UserProfileFragment.phone_number.equals("null")) {
            etdPhoneNumber.setText(UserProfileFragment.phone_number);
        }

        if (UserProfileFragment.gender.equalsIgnoreCase("male")) {
            ((RadioButton) selectGender.getChildAt(0)).setChecked(true);
        } else {
            ((RadioButton) selectGender.getChildAt(1)).setChecked(true);
        }

        if (UserProfileFragment.posts_are_private == true) {
            btnYes.setSelected(true);
            btnYes.setBackgroundColor(Color.parseColor("#f26f71"));
            btnNo.setBackgroundColor(Color.parseColor("#E8E8E8"));
        } else {
            btnNo.setSelected(true);
            btnYes.setBackgroundColor(Color.parseColor("#E8E8E8"));
            btnNo.setBackgroundColor(Color.parseColor("#f26f71"));
        }

        etdName.setTypeface(face);
        etdUserName.setTypeface(face);
        etdWebsite.setTypeface(face);
        etdDesignation.setTypeface(face);
        etdBio.setTypeface(face);
        etdEmailId.setTypeface(face);
        etdPhoneNumber.setTypeface(face);
        btnYes.setTypeface(face);
        btnNo.setTypeface(face);
        btnSave.setTypeface(face);

        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "EDIT PROFILE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnChange:
                showDialog();
                break;
            case R.id.btnYes:
                strSelection = "1";
                btnYes.setBackgroundColor(Color.parseColor("#f26f71"));
                btnNo.setBackgroundColor(Color.parseColor("#E8E8E8"));
                break;
            case R.id.btnNo:
                strSelection = "0";
                btnYes.setBackgroundColor(Color.parseColor("#E8E8E8"));
                btnNo.setBackgroundColor(Color.parseColor("#f26f71"));
                break;
            case R.id.btnSave:
                final String emailId;

                selectedId = selectGender.getCheckedRadioButtonId();
                radioSexButton = (RadioButton) view.findViewById(selectedId);
                if (radioSexButton.getId() == selectedId) {
                    gender = "male";
                } else {
                    gender = "female";
                }
//@PKDEC06 - pattern validation added
                if (!etdPhoneNumber.getText().toString().trim().isEmpty() && etdPhoneNumber.getText().toString().trim().length() < 10
                        && !Patterns.PHONE.matcher(etdPhoneNumber.getText().toString()).matches()) {
                    Toast.makeText(getActivity(), getString(R.string.validPhoneNumber), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    PhoneNumber = etdPhoneNumber.getText().toString();
                }
//  @PKDEC06 - Email id validation added
                if (!etdEmailId.getText().toString().isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(etdEmailId.getText().toString()).matches()) {
                    emailId = etdEmailId.getText().toString();
                } else {
                    emailId = "";
                    Toast.makeText(getActivity(), getString(R.string.validEmailAddress), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (isInternetPresent) {
                    rpd = ProgressDialog.show(getActivity(), "", "updating Profile ...", true);
                    rpd.setCancelable(true);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            updateUserData(etdName.getText().toString(),
                                    etdUserName.getText().toString(),
                                    etdWebsite.getText().toString(),
                                    etdDesignation.getText().toString(),
                                    etdBio.getText().toString(),
                                    emailId, PhoneNumber, gender, strSelection,
                                    HomeScreen.authentication_token, HomeScreen.key);
                        }
                    }).start();

                } else {
                    String text = "No Internet Connection!";
                    showNoFeedsDialog(text);
                }
                break;
            case R.id.open_Camera:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = new File(Environment.getExternalStorageDirectory(),
                        "tmp_avatar_" + String.valueOf(System.currentTimeMillis())
                                + ".jpg");

                // cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                // mImageCaptureUri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                dialog.dismiss();
                break;
            case R.id.open_Gallery:
                Intent galleryIntent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(galleryIntent, "Select File"),
                        GALLERY_REQUEST);

                dialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void fetchImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }


    private void updateUserData(String stringName, String userName,
                                String stringWebsite, String stringDesignation, String stringBio,
                                String stringEmailId, String stringPhoneNo, String gender,
                                String strSelection, String authToken, String key) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramName = params[0];
                String paramUserName = params[1];
                String paramWebsite = params[2];
                String paramDesignation = params[3];
                String paramBio = params[4];
                String paramEmailId = params[5];
                String paramPhoneNo = params[6];
                String paramGender = params[7];
                String paramSelection = params[8];
                String paramAuthToken = params[9];
                String paramKey = params[10];
                //NS19DEC

                try {
                    File file = new File(tempPath1);
                    FileBody image = new FileBody(file);
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPut httpPut = new HttpPut(URL_FEED);
                    MultipartEntity entity = new MultipartEntity();
                    entity.addPart("user[name]", new StringBody(paramName));
                    entity.addPart("user[username]", new StringBody(paramUserName));
                    entity.addPart(
                            "user[website]", new StringBody(paramWebsite));
                    entity.addPart(
                            "user[designation]",new StringBody(paramDesignation));
                    entity.addPart(
                            "user[bio]", new StringBody(paramBio));
                    entity.addPart(
                            "user[email]", new StringBody(paramEmailId));
                    entity.addPart(
                            "user[phone_no]", new StringBody(paramPhoneNo));
                    entity.addPart(
                            "user[gender]", new StringBody(paramGender));
                    entity.addPart(
                            "user[keep_data_private]", new StringBody(paramSelection));
                    entity.addPart(
                            "authentication_token", new StringBody(paramAuthToken));
                    entity.addPart(
                            "key", new StringBody(paramKey));
                    entity.addPart(
                            "user[profile_pic]", image);
                    httpPut.setEntity(entity);
                    HttpResponse httpResponse = httpClient.execute(httpPut);

                    Log.e("@Create","Profile update status "+httpResponse.getStatusLine());

                    InputStream inputStream = httpResponse.getEntity()
                            .getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(
                            inputStream);
                    BufferedReader bufferedReader = new BufferedReader(
                            inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;
                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    Log.e("@Create","Profile update response"+stringBuilder.toString());
                    return stringBuilder.toString();

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                rpd.dismiss();
                String text = "Update successfully !";
                showUpdateDialog(text);

            }
        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(stringName, userName, stringWebsite,
                stringDesignation, stringBio, stringEmailId, stringPhoneNo,
                gender, strSelection, authToken, key);

    }
    private void showDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_camera_gallery);
        dialog.setCanceledOnTouchOutside(false);

        dialog.findViewById(R.id.open_Camera).setOnClickListener(this);
        dialog.findViewById(R.id.open_Gallery).setOnClickListener(this);

        dialog.show();
    }
    private void showUpdateDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getFragmentManager().popBackStack();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {

                if (data != null) {

                    filePath = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                        Uri selectedImageUri1 = getImageUri(getActivity(), bitmap);
                        tempPath1 = getPath(selectedImageUri1, getActivity());

                        imgProfile.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (requestCode == GALLERY_REQUEST) {
                Uri selectedImageUri1 = data.getData();
                tempPath1 = getPath(selectedImageUri1, getActivity());
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                bitmap = BitmapFactory.decodeFile(tempPath1, btmapOptions);
                imgProfile.setImageBitmap(bitmap);
            }
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri, Activity context) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = context.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


}
