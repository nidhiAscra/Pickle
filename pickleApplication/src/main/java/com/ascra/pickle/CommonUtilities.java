package com.ascra.pickle;

import android.content.Context;

import android.content.Intent;

public final class CommonUtilities {
	
	// give your server registration url here
   public static final String SERVER_URL = "http://staticmagick.in/test/Quizapp/config.php/gcm_server.php"; 

    // Google project id
   public  static final String SENDER_ID = "264226440806";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "Pickle GCM";

    public  static final String DISPLAY_MESSAGE_ACTION =
            "com.ascra.pickle.DISPLAY_MESSAGE";

    public  static final String EXTRA_MESSAGE = "message";


    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message,String flag) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra("questions_set", message);
        intent.putExtra("flag", flag);
        context.sendBroadcast(intent);
    }
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra("message", message);
        
        context.sendBroadcast(intent);
    }
    
    static void displayMessage(Context context, String message,String seconds,String user_id,String answer_type,String flag) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra("answer_seconds", seconds);
        intent.putExtra("answered_users_userid", user_id);
        intent.putExtra("answer_type", answer_type);
        intent.putExtra("flag", flag);
        
        context.sendBroadcast(intent);
    }
}
