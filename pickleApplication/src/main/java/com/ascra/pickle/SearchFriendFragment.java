package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ascra.data.FriendSearchData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchFriendFragment extends Fragment implements
        OnClickListener, OnItemClickListener {

    View view;
    EditText etdSearch;
    Button btnClear;
    ListView listOfSearchData;
    TextView txtNoResult;
    ProgressBar inProcess;

    private String URL_FEED = null;

    String searchText;
    JSONArray jsonFriendSearchArray;

    FriendSearchAdapter friendSearchAdapter;

    ArrayList<FriendSearchData> mAllData = new ArrayList<FriendSearchData>();

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Tracker tracker;

    Typeface face;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.search_friend_fragment, null);

        HomeScreen.whichFragment = "FRIENDSEARCH";

        URL_FEED = Constant.PickleUrl + "users/search_all_users.json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        setUpViews();
        setTracker();

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        return view;
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "FRIEND SEARCH SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void setUpViews() {
        etdSearch = (EditText) view.findViewById(R.id.edSearch);
        btnClear = (Button) view.findViewById(R.id.btnClear);
        listOfSearchData = (ListView) view.findViewById(R.id.mListView);
        txtNoResult = (TextView) view.findViewById(R.id.textview_list);
        inProcess = (ProgressBar) view.findViewById(R.id.search_ProgressBar);

        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.searchFriendHeader, R.drawable.pickleprofile_color);

        btnClear.setOnClickListener(this);

        etdSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // listOfSearchData.setVisibility(View.VISIBLE);
                searchText = "";
                if (0 != etdSearch.getText().length()) {
                    searchText = etdSearch.getText().toString();

                    if (isInternetPresent) {
                        new GetFriendSearchData().execute();
                    } else {
                        String text = "No Internet Connection!";
                        showNoFeedsDialog(text);
                    }
                }
            }
        });
    }

    private class GetFriendSearchData extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            inProcess.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... v) {
            ServiceHandler sh = new ServiceHandler();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("q", searchText));
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));
            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET,
                    params);

            if (jsonStr != null) {
                try {
                    Log.e("SearchFriendFragment", "Response : " + jsonStr);
                    jsonFriendSearchArray = new JSONArray(jsonStr);
                    getActivity().runOnUiThread(new Runnable() {
                        //
                        @Override
                        public void run() {
                            parseJsonFeed(jsonFriendSearchArray);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            try {
                setSearchResult(searchText);
                inProcess.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public void parseJsonFeed(JSONArray array) {

        try {

            inProcess.setVisibility(View.GONE);
            mAllData.clear();
            if (array.length() == 0) {
                txtNoResult.setVisibility(View.VISIBLE);
            } else {
                txtNoResult.setVisibility(View.GONE);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject user = array.getJSONObject(i);

                    String id = user.getString("id");
                    String name = user.getString("name");
                    String profilePic = user.getString("image");
                    String userName = user.getString("username");
                    String email = user.getString("email");
                    String status = user.getString("status");

                    FriendSearchData sm = new FriendSearchData(id, name,
                            profilePic, userName, email, status);
                    friendSearchAdapter.addItem(sm);
                    mAllData.add(sm);
                    listOfSearchData.setAdapter(friendSearchAdapter);
                    friendSearchAdapter.notifyDataSetChanged();

                }

                listOfSearchData.setOnItemClickListener(this);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setSearchResult(String str) {
        friendSearchAdapter = new FriendSearchAdapter(getActivity());

        for (FriendSearchData friendSearchData : mAllData) {
            if (friendSearchData.getName().toLowerCase()
                    .contains(str.toLowerCase())) {
                friendSearchAdapter.addItem(friendSearchData);
            }
        }
        listOfSearchData.setAdapter(friendSearchAdapter);
    }

    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {

        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getWindow().getCurrentFocus()
                .getWindowToken(), 0);

        UserProfileFragment profileFragment = new UserProfileFragment();
        Bundle args = new Bundle();
        args.putString("USER_NAME", mAllData.get(position).getUsername());
        args.putString("FROM", "SEARCH");
        args.putString("GENERATED_ID", "");
        profileFragment.setArguments(args);
        FragmentTransaction profileTransaction = getFragmentManager()
                .beginTransaction();
        profileTransaction.replace(R.id.content_frame, profileFragment);
        profileTransaction.addToBackStack(null);
        profileTransaction.commit();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnClear:
                etdSearch.setText("");
                listOfSearchData.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }

    public class FriendSearchAdapter extends BaseAdapter {

        private ArrayList<FriendSearchData> myListItems = new ArrayList<FriendSearchData>();
        private LayoutInflater myLayoutInflater;
        ImageLoader imageLoader;

        public FriendSearchAdapter(Activity activity) {
            myLayoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            imageLoader = new ImageLoader(activity.getApplicationContext());
        }

        public void addItem(FriendSearchData item) {
            myListItems.add(item);
            notifyDataSetChanged();
        }

        public int getCount() {
            return myListItems.size();
        }

        public FriendSearchData getItem(int position) {
            return myListItems.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder holder;

            if (view == null) {
                holder = new ViewHolder();

                view = myLayoutInflater.inflate(R.layout.friend_search_item,
                        null);
                holder.itemName = (TextView) view.findViewById(R.id.text);
                holder.profile_pic = (ImageView) view
                        .findViewById(R.id.profile_pic);

                holder.itemName.setTypeface(face);

                view.setTag(holder);
            } else {

                holder = (ViewHolder) view.getTag();
            }

            FriendSearchData stringItem = myListItems.get(position);
            if (stringItem != null) {
                if (holder.itemName != null) {
                    holder.itemName.setText(stringItem.getName());

                    imageLoader.DisplayImage(stringItem.getPic(), holder.profile_pic);
                }
            }

            return view;
        }

    }

    private class ViewHolder {

        TextView itemName;
        ImageView profile_pic;

    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }
}
