package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.SessionManager;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BrandRegister extends AppCompatActivity implements
        OnClickListener {

    EditText etdPutBrandName, etdPutEmailId, etdPutPassword,
            etdPutConfirmPassword;
    RelativeLayout btnSignUp;
    TextView txtSignIn, txtAlreadyRegister, txtsignup;
    FrameLayout inProcess;
    Intent intent;

    private String URL_FEED = Constant.PickleUrl + "tokens/user_sign_up";

    Dialog ringProgressDialog;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Tracker tracker;

    Typeface face;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.pickle_register_layout);

        cd = new ConnectionDetector(BrandRegister.this);
        isInternetPresent = cd.isConnectingToInternet();

        face = Typeface.createFromAsset(getAssets(),
                "fonts/sniglet-regular.ttf");
        setUpViews();
        setTracker();

    }

    private void setUpViews() {
        etdPutBrandName = (EditText) findViewById(R.id.etdPutBrandName);
        etdPutEmailId = (EditText) findViewById(R.id.etdPutEmail);
        etdPutPassword = (EditText) findViewById(R.id.etdPutPassword);
        etdPutConfirmPassword = (EditText) findViewById(R.id.etdPutConfirmPassword);
        btnSignUp = (RelativeLayout) findViewById(R.id.btnSignUp);
        txtsignup = (TextView) findViewById(R.id.txtsignup);
        txtSignIn = (TextView) findViewById(R.id.txtSignIn);
        txtAlreadyRegister = (TextView) findViewById(R.id.txtAlreadyRegister);
        inProcess = (FrameLayout) findViewById(R.id.inProcess);
        btnSignUp.setOnClickListener(this);
        txtSignIn.setOnClickListener(this);

        etdPutBrandName.setTypeface(face);
        etdPutEmailId.setTypeface(face);
        etdPutPassword.setTypeface(face);
        etdPutConfirmPassword.setTypeface(face);
        txtAlreadyRegister.setTypeface(face);
        txtSignIn.setTypeface(face);
        txtsignup.setTypeface(face);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(BrandRegister.this).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "REGISTRATION SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSignUp:

                if (isInternetPresent) {

                    if (etdPutBrandName.getText().toString().length() < 1) {
                        String text = "Please enter brand name !";
                        showErrorDialog(text);
                    } else if (etdPutEmailId.getText().toString().length() < 1) {
                        String text = "Please enter email id !";
                        showErrorDialog(text);
                    } else if (!isValid(etdPutEmailId.getText().toString().trim())) {
                        String text = "Please enter valid email id !";
                        showErrorDialog(text);
                    } else if (etdPutPassword.getText().toString().trim().length() < 1) {
                        String text = "Please enter password !";
                        showErrorDialog(text);
                    } else if (etdPutPassword.getText().toString().trim().length() < 8) {
                        String text = "Please enter password minimum 8 characters!";
                        showErrorDialog(text);
                    } else if (!etdPutPassword
                            .getText()
                            .toString()
                            .trim()
                            .equals(etdPutConfirmPassword.getText().toString()
                                    .trim())) {
                        String text = "Passwords does not match !";
                        showErrorDialog(text);
                    } else {
                        userSignUp();
                    }
                } else {
                    String text = "No Internet Connection!";
                    showNoFeedsDialog(text);
                }
                break;
            case R.id.txtSignIn:
                intent = new Intent(BrandRegister.this, BrandLogin.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void userSignUp() {
        ringProgressDialog=new Dialog(BrandRegister.this);
        ringProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ringProgressDialog.setContentView(R.layout.dialog_progress);
        TextView msg = (TextView) ringProgressDialog.findViewById(R.id.msg);
        ringProgressDialog.setCanceledOnTouchOutside(false);
       msg.setText("Registering please wait...");
        ringProgressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    String is_brand = "true";

                    sendPostRequest(etdPutBrandName.getText().toString(),
                            etdPutEmailId.getText().toString(), etdPutPassword
                                    .getText().toString(),
                            etdPutConfirmPassword.getText().toString(),
                            is_brand);

                    SaveUtils.saveUserNameToPref(
                            getResources().getString(
                                    R.string.PreferenceFileName),
                            BrandRegister.this, "USERNAME", etdPutEmailId
                                    .getText().toString().trim());
                    SaveUtils.savePasswordToPref(
                            getResources().getString(
                                    R.string.PreferenceFileName),
                            BrandRegister.this, "PASSWORD", etdPutPassword
                                    .getText().toString().trim());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    private void sendPostRequest(String brandName, String userEmail,
                                 String userPassword, String confirmPassword, String is_brand) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramUserBrandName = params[0];
                String paramUserEmail = params[1];
                String paramUserPassword = params[2];
                String paramUserConfirmPassword = params[3];
                String paramIsBrand = params[4];
                Log.e("RegId", "SESSION " + new SessionManager(BrandRegister.this).getRegID());
                ;
                String regid = new SessionManager(BrandRegister.this).getRegID();

                HttpClient httpClient = new DefaultHttpClient();

                HttpPost httpPost = new HttpPost(URL_FEED);

                BasicNameValuePair brandNameBasicNameValuePair = new BasicNameValuePair(
                        "name", paramUserBrandName);
                BasicNameValuePair emailBasicNameValuePair = new BasicNameValuePair(
                        "email", paramUserEmail);
                BasicNameValuePair passwordBasicNameValuePair = new BasicNameValuePair(
                        "password", paramUserPassword);
                BasicNameValuePair confirmPasswordBasicNameValuePAir = new BasicNameValuePair(
                        "password_confirmation", paramUserConfirmPassword);
                BasicNameValuePair isBrandBasicNameValuePAir = new BasicNameValuePair(
                        "is_brand", paramIsBrand);
                //@PK30NOV regidpair
                BasicNameValuePair regidPair = new BasicNameValuePair(
                        "registration_id", regid);
                //@PK30NOV deviceType
                BasicNameValuePair deviceType = new BasicNameValuePair(
                        "device_type", "Android");
                //@PK30NOV gcm_token
                BasicNameValuePair deviceToken = new BasicNameValuePair(
                        "device_token", new SessionManager(BrandRegister.this).getGcmToken());

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(brandNameBasicNameValuePair);
                nameValuePairList.add(emailBasicNameValuePair);
                nameValuePairList.add(passwordBasicNameValuePair);
                nameValuePairList.add(confirmPasswordBasicNameValuePAir);
                nameValuePairList.add(isBrandBasicNameValuePAir);

                //@PK30NOV regidpair, deviceType
                nameValuePairList.add(regidPair);
                nameValuePairList.add(deviceType);
                nameValuePairList.add(deviceToken);

                for (NameValuePair a : nameValuePairList) {
                    Log.e("NameValue", "Pair : " + a.getName() + " = " + a.getValue());
                }
                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);
                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();

                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);

                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);

                        StringBuilder stringBuilder = new StringBuilder();

                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                ringProgressDialog.dismiss();
                startActivity(new Intent(BrandRegister.this, BrandLogin.class));
                finish();
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(brandName, userEmail, userPassword,
                confirmPassword, is_brand);
    }

    private void showErrorDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                BrandRegister.this);
        builder.setCancelable(true);

        TextView msg = new TextView(BrandRegister.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();

        alert.show();
    }

    public static boolean isValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BrandRegister.this);
        builder.setCancelable(true);

        TextView msg = new TextView(BrandRegister.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
}
