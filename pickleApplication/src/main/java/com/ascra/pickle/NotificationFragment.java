package com.ascra.pickle;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NotificationFragment extends Fragment {

    View view;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    ProgressBar inProcess;
    TextView txtYou, txtFollowing;
    Typeface face;
    private ExpandableListView mExpandableListViewByYou,
            mExpandableListViewByFollowing;
    private String URL_FEED_BY_YOU = null;
    private String URL_FEED_Following = null;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_notification, null);
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtYou = (TextView) view.findViewById(R.id.txtYou);
        txtFollowing = (TextView) view.findViewById(R.id.txtFollowing);

        mExpandableListViewByYou = (ExpandableListView) view.findViewById(R.id.expandableListViewByYou);
        mExpandableListViewByFollowing = (ExpandableListView) view.findViewById(R.id.expandableListViewFollowing);
        mExpandableListViewByYou.setGroupIndicator(null);
        mExpandableListViewByFollowing.setGroupIndicator(null);
        inProcess = (ProgressBar) view.findViewById(R.id.inProcess);

        URL_FEED_BY_YOU = Constant.PickleUrl
                + HomeScreen.session_name.replace(" ", "%20")
                + "/notifications.json";

        URL_FEED_Following = Constant.PickleUrl
                + HomeScreen.session_name.replace(" ", "%20")
                + "/followings-notifications.json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            try {
                new loadingUserNotificationDataByYou().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                new loadingUserNotificationDataFollowing().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String text = "No Internet Connection!";
            showDialog(text);
        }

        mExpandableListViewByYou.setVisibility(View.VISIBLE);
        mExpandableListViewByFollowing.setVisibility(View.GONE);

        txtYou.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mExpandableListViewByYou.setVisibility(View.VISIBLE);
                mExpandableListViewByFollowing.setVisibility(View.GONE);
                txtYou.setBackgroundColor(Color.parseColor("#F08080"));
                txtFollowing.setBackgroundColor(Color.parseColor("#C0c0c0"));
            }
        });

        txtFollowing.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mExpandableListViewByYou.setVisibility(View.GONE);
                mExpandableListViewByFollowing.setVisibility(View.VISIBLE);
                txtYou.setBackgroundColor(Color.parseColor("#C0c0c0"));
                txtFollowing.setBackgroundColor(Color.parseColor("#F08080"));
            }
        });

        face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/sniglet-regular.ttf");
        //@PKDEC06
        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.notificationHeaderTitle, R.drawable.pickle_notification_color);

        return view;
    }

    private void parseJSONByYou(JSONObject mainObject) {

        List<String> Notificationtitle = new ArrayList<String>();
        HashMap<String, List<JSONObject>> Notificationlist = new HashMap<String, List<JSONObject>>();

        try {
            JSONArray arrayOfNotification = mainObject.getJSONArray("notifications");
            for (int NotiElement = 0; NotiElement < arrayOfNotification.length(); NotiElement++) {
                JSONObject objMain = arrayOfNotification.getJSONObject(NotiElement);
                String notificationDate = objMain.getString("notification_date");
                Notificationtitle.add(notificationDate);
                JSONArray arrayOfNotifications = objMain.getJSONArray("notification");
                List<JSONObject> listOfNoti = new ArrayList<JSONObject>();
                for (int subNotiElement = 0; subNotiElement < arrayOfNotifications.length(); subNotiElement++) {
                    JSONObject objSub = arrayOfNotifications.getJSONObject(subNotiElement);
                    listOfNoti.add(objSub);
                    Notificationlist.put(Notificationtitle.get(NotiElement), listOfNoti);
                }

            }

            ExpandableListAdapterByYou notificationAdapter = new ExpandableListAdapterByYou(getActivity(), Notificationtitle, Notificationlist, mExpandableListViewByYou);

            mExpandableListViewByYou.setAdapter(notificationAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseJSONFollowing(JSONObject mainObject) {

        List<String> NotificationtitleFollowing = new ArrayList<String>();
        HashMap<String, List<JSONObject>> NotificationlistFollowing = new HashMap<String, List<JSONObject>>();

        try {
            JSONArray arrayOfNotification = mainObject.getJSONArray("notifications");
            for (int NotiElement = 0; NotiElement < arrayOfNotification.length(); NotiElement++) {
                JSONObject objMain = arrayOfNotification.getJSONObject(NotiElement);
                String notificationDate = objMain.getString("notification_date");
                NotificationtitleFollowing.add(notificationDate);
                JSONArray arrayOfNotifications = objMain.getJSONArray("notification");
                List<JSONObject> listOfNoti = new ArrayList<JSONObject>();
                for (int subNotiElement = 0; subNotiElement < arrayOfNotifications.length(); subNotiElement++) {
                    JSONObject objSub = arrayOfNotifications.getJSONObject(subNotiElement);
                    listOfNoti.add(objSub);
                }
                NotificationlistFollowing.put(NotificationtitleFollowing.get(NotiElement), listOfNoti);
            }

            ExpandableListAdapterByFollowing notificationAdapter = new ExpandableListAdapterByFollowing(
                    getActivity(), NotificationtitleFollowing,
                    NotificationlistFollowing, mExpandableListViewByFollowing);

            mExpandableListViewByFollowing.setAdapter(notificationAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class loadingUserNotificationDataByYou extends
            AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED_BY_YOU, ServiceHandler.GET, params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONByYou(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NetworkOnMainThreadException nme) {
                nme.printStackTrace();
            } catch (Exception ae) {
                ae.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProcess.setVisibility(View.GONE);
        }

    }

    private class loadingUserNotificationDataFollowing extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED_Following,
                    ServiceHandler.GET, params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONFollowing(mainObject);
                    }

                });

            } catch (JSONException je) {
                je.printStackTrace();
            } catch (NetworkOnMainThreadException nme) {
                nme.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProcess.setVisibility(View.GONE);
        }

    }

    public class ExpandableListAdapterByYou extends BaseExpandableListAdapter {

        ImageLoader imageLoader;
        private Context mContext;
        private ExpandableListView mExpandableListView;
        private int[] groupStatus;
        private HashMap<String, List<JSONObject>> notificationlist = new HashMap<String, List<JSONObject>>();
        private List<String> notificationtitle = new ArrayList<String>();

        public ExpandableListAdapterByYou(FragmentActivity activity,
                                          List<String> notificationtitle,
                                          HashMap<String, List<JSONObject>> notificationlist,
                                          ExpandableListView mExpandableListView2) {

            mContext = activity;
            mExpandableListView = mExpandableListView2;
            this.notificationtitle = notificationtitle;
            this.notificationlist = notificationlist;

            imageLoader = new ImageLoader(mContext);

        }

        @Override
        public int getGroupCount() {
            return notificationtitle.size();
        }

        class GroupHolder {
            ImageView img;
            TextView title;
        }

        @Override
        public int getChildrenCount(int arg0) {
            return notificationlist.get(notificationtitle.get(arg0)).size();
        }

        public class ChildHolder {
            TextView userName;
            TextView userMessage, time;
            ImageView img_profile;
            RelativeLayout relimg;
            TextView noti_time;
            ImageView option_image, option_image2;

        }

        @Override
        public JSONObject getChild(int arg0, int arg1) {
            return notificationlist.get(notificationtitle.get(arg0)).get(arg0);
        }

        @Override
        public long getChildId(int arg0, int arg1) {
            return 0;
        }

        @Override
        public View getChildView(int arg0, int arg1, boolean arg2, View arg3,
                                 ViewGroup arg4) {
            ChildHolder childHolder;
            if (arg3 == null) {
                arg3 = LayoutInflater.from(mContext).inflate(
                        R.layout.noti_list_item_layout, null);

                childHolder = new ChildHolder();

                try {

                    childHolder.userName = (TextView) arg3
                            .findViewById(R.id.userName);
                    childHolder.userMessage = (TextView) arg3
                            .findViewById(R.id.message);
                    childHolder.img_profile = (ImageView) arg3
                            .findViewById(R.id.profileImage);
                    childHolder.noti_time = (TextView) arg3
                            .findViewById(R.id.time);
                    childHolder.option_image = (ImageView) arg3
                            .findViewById(R.id.optionimage);

                    //@PKDEC02
                    childHolder.option_image2 = (ImageView) arg3
                            .findViewById(R.id.optionimage2);
                    childHolder.relimg = (RelativeLayout) arg3
                            .findViewById(R.id.relimg);

                    childHolder.userName.setTypeface(face);
                    childHolder.userMessage.setTypeface(face);
                    childHolder.noti_time.setTypeface(face);

                    JSONObject MainObject = notificationlist.get(
                            notificationtitle.get(arg0)).get(arg1);

                    String Notification_Type = MainObject
                            .getString("notification_type");

                    if (Notification_Type.equalsIgnoreCase("Commented On pickle")) {
                        JSONObject objCommentor = MainObject.getJSONObject("commentor");
                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String pickle_name = objPickle.getString("question");
                        String commentor_name = objCommentor.getString("name");
                        String commentor_profile_pic_url = objCommentor.getString("profile_pic_url");
                        JSONObject objComment = MainObject.getJSONObject("comment");
                        String comment_content = objComment.getString("content");

                        childHolder.userName.setText(commentor_name);
                        childHolder.userMessage.setText("commented on your(" + pickle_name + ") pickle: " + comment_content);
                        imageLoader.DisplayImage(commentor_profile_pic_url, childHolder.img_profile);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);
                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);

//                        JSONObject objPickle = MainObject
//                                .getJSONObject("pickle");
                        final String pickle_id = objPickle.getString("id");
//                        final String pickle_name = objPickle
//                                .getString("question");

                        childHolder.option_image.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", pickle_name);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);
                                ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                Bundle args = new Bundle();
                                args.putString("PICKLE_ID", pickle_id);
                                args.putString("PICKLE_NAME", pickle_name);
                                args.putString("FROM", "NOTIFICATION");

                                ShowPickleFragmentobj.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        });

                    } else if (Notification_Type.equalsIgnoreCase("Voted On pickle")) {
                        JSONObject objVoter = MainObject.getJSONObject("voter");
                        String name = objVoter.getString("name");
                        String profile_image = objVoter.getString("profile_pic_url");
                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String textPickle = objPickle.getString("question");
                        final String pickleId = objPickle.getString("id");
                        JSONObject objVote = MainObject.getJSONObject("vote");
                        childHolder.userName.setText(name);
                        childHolder.userMessage.setText("voted on your pickle: " + textPickle);
                        imageLoader.DisplayImage(profile_image, childHolder.img_profile);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03
                        childHolder.relimg
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickleId);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", textPickle);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);
                                        ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                        Bundle args = new Bundle();
                                        args.putString("PICKLE_ID", pickleId);
                                        args.putString("PICKLE_NAME", textPickle);
                                        args.putString("FROM", "NOTIFICATION");

                                        ShowPickleFragmentobj.setArguments(args);
                                        FragmentTransaction transaction = getFragmentManager()
                                                .beginTransaction();
                                        transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                        transaction.addToBackStack(null);
                                        transaction.commit();

                                    }
                                });

                    } else if (Notification_Type.equals("started following you on pickle")) {
                        JSONObject objFollowingUser = MainObject.getJSONObject("following_user");
                        String name = objFollowingUser.getString("name");
                        String user_name = objFollowingUser.getString("username");
                        String profile_image = objFollowingUser.getString("profile_pic_url");
                        JSONObject objFollow = MainObject.getJSONObject("follow");
                        childHolder.userName.setText(name);
                        childHolder.userMessage.setText("started following you on pickle.");
                        imageLoader.DisplayImage(profile_image, childHolder.img_profile);
                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                    } else if (Notification_Type.equals("Tagged On pickle.")) {
                        JSONObject objFollowingUser = MainObject.getJSONObject("tagged_by");
                        String name = objFollowingUser.getString("name");
                        String user_name = objFollowingUser.getString("username");
                        String profile_image = objFollowingUser.getString("profile_pic_url");
                        childHolder.userName.setText(name);
                        childHolder.userMessage.setText("Tagged you on pickle");
                        imageLoader.DisplayImage(profile_image, childHolder.img_profile);

//						String notification_image = MainObject
//								.getString("notification_image_one");
//						childHolder.option_image.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(notification_image,
//								childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);
                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String pickle_id = objPickle.getString("id");
                        final String pickle_name = objPickle.getString("question");
                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03
                        childHolder.relimg.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", pickle_name);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);

                                ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                Bundle args = new Bundle();
                                args.putString("PICKLE_ID", pickle_id);
                                args.putString("PICKLE_NAME", pickle_name);
                                args.putString("FROM", "NOTIFICATION");

                                ShowPickleFragmentobj.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager()
                                        .beginTransaction();
                                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        });
                    } else if (Notification_Type.equals("shared your pickle")) {
                        JSONObject objshared = MainObject.getJSONObject("shared_user");
                        String Shared_name = objshared.getString("name");
                        String sharer_profile_pic_url = objshared.getString("profile_pic_url");
                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String textPickle = objPickle.getString("question");
                        final String pickle_id = objPickle.getString("id");
                        /* JSONObject objPickle1 = MainObject .getJSONObject("user");
                         final String pickle_owner = objPickle1.getString("username");*/
                        childHolder.userName.setText(Shared_name);
                        childHolder.userMessage.setText("Shared your pickle: " + textPickle);
                        imageLoader.DisplayImage(sharer_profile_pic_url, childHolder.img_profile);

//							String notification_image = MainObject
//									.getString("notification_image_one");
//							childHolder.option_image.setVisibility(View.VISIBLE);
//							imageLoader.DisplayImage(notification_image,
//									childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);

                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03

                        childHolder.relimg.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
//											Intent gotoPickleShowIntent = new Intent(
//													getActivity(),
//													ShowPickleFragment.class);
//											gotoPickleShowIntent.putExtra(
//													"PICKLE_ID", pickle_id);
//											gotoPickleShowIntent.putExtra(
//													"PICKLE_NAME", textPickle);
//											gotoPickleShowIntent.putExtra("FROM",
//													"NOTIFICATION");
//											startActivity(gotoPickleShowIntent);

                                ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                Bundle args = new Bundle();
                                args.putString("PICKLE_ID", pickle_id);
                                args.putString("PICKLE_NAME", textPickle);
                                args.putString("FROM", "NOTIFICATION");

                                ShowPickleFragmentobj.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        });

                    } else if (Notification_Type.equals("Tagged On Bundle pickle")) {

                    } else if (Notification_Type.equals("shared your pickle")) {

                    } else if (Notification_Type.equals("has joined pickle")) {

                    } else if (Notification_Type.equals("Commented on bundle pickle")) {

                    }

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

                arg3.setTag(childHolder);
            } else {
                childHolder = (ChildHolder) arg3.getTag();
            }

            return arg3;
        }

        @Override
        public Object getGroup(int arg0) {
            return notificationtitle.get(arg0);
        }

        @Override
        public long getGroupId(int arg0) {
            return arg0;
        }

        @Override
        public View getGroupView(int arg0, boolean arg1, View arg2,
                                 ViewGroup arg3) {

            GroupHolder groupHolder;
            if (arg2 == null) {
                arg2 = LayoutInflater.from(mContext).inflate(R.layout.header, null);
                groupHolder = new GroupHolder();
                groupHolder.title = (TextView) arg2.findViewById(R.id.text1);

                arg2.setTag(groupHolder);
            } else {
                groupHolder = (GroupHolder) arg2.getTag();
            }

            groupHolder.title.setText(notificationtitle.get(arg0));

            for (int i = 0; i < notificationlist.size(); i++) {
                mExpandableListView.expandGroup(i);
            }

            return arg2;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }

    public class ExpandableListAdapterByFollowing extends
            BaseExpandableListAdapter {

        ImageLoader imageLoader;
        private Context mContext;
        private ExpandableListView mExpandableListView;
        private int[] groupStatus;
        private HashMap<String, List<JSONObject>> notificationlist = new HashMap<String, List<JSONObject>>();
        private List<String> notificationtitle = new ArrayList<String>();

        public ExpandableListAdapterByFollowing(FragmentActivity activity,
                                                List<String> notificationtitle,
                                                HashMap<String, List<JSONObject>> notificationlist,
                                                ExpandableListView mExpandableListView2) {

            mContext = activity;
            mExpandableListView = mExpandableListView2;
            this.notificationtitle = notificationtitle;
            this.notificationlist = notificationlist;

            imageLoader = new ImageLoader(mContext);

        }

        class GroupHolder {
            ImageView img;
            TextView title;
        }

        public class ChildHolder {
            TextView userName;
            TextView userMessage, time;
            ImageView img_profile;
            TextView noti_time;
            ImageView option_image, option_image2;
            RelativeLayout relimg;
        }

        @Override
        public List<JSONObject> getChild(int arg0, int arg1) {
            return notificationlist.get(notificationtitle.get(arg0));
        }

        @Override
        public long getChildId(int arg0, int arg1) {
            return 0;
        }

        @Override
        public View getChildView(int arg0, int arg1, boolean arg2,
                                 View convertView, ViewGroup arg4) {
            ChildHolder childHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(mContext).inflate(
                        R.layout.noti_list_item_layout, null);

                childHolder = new ChildHolder();

                try {

                    childHolder.userName = (TextView) convertView.findViewById(R.id.userName);
                    childHolder.userMessage = (TextView) convertView.findViewById(R.id.message);
                    childHolder.img_profile = (ImageView) convertView.findViewById(R.id.profileImage);
                    childHolder.noti_time = (TextView) convertView.findViewById(R.id.time);
                    childHolder.option_image = (ImageView) convertView.findViewById(R.id.optionimage);
                    childHolder.option_image2 = (ImageView) convertView.findViewById(R.id.optionimage2);
                    childHolder.relimg = (RelativeLayout) convertView.findViewById(R.id.relimg);

                    childHolder.userName.setTypeface(face);
                    childHolder.userMessage.setTypeface(face);
                    childHolder.noti_time.setTypeface(face);

                    JSONObject MainObject = notificationlist.get(notificationtitle.get(arg0)).get(arg1);

                    String Notification_Type = MainObject.getString("notification_type");
                    if (Notification_Type.equalsIgnoreCase("Commented On pickle")) {
                        JSONObject objCommentor = MainObject.getJSONObject("commentor");
                        String commentor_name = objCommentor.getString("name");
                        String commentor_profile_pic_url = objCommentor.getString("profile_pic_url");
                        JSONObject objComment = MainObject.getJSONObject("comment");
                        String comment_content = objComment.getString("content");
                        JSONObject objPickle1 = MainObject.getJSONObject("user");
                        final String pickle_owner = objPickle1.getString("username");
                        childHolder.userName.setText(commentor_name);
                        childHolder.userMessage.setText("commented on your pickle: " + comment_content);
                        imageLoader.DisplayImage(commentor_profile_pic_url, childHolder.img_profile);

//						String notification_image = MainObject
//								.getString("notification_image_one");
//						childHolder.option_image.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(notification_image,
//								childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);

                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);

                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String pickle_id = objPickle.getString("id");
                        final String pickle_name = objPickle.getString("question");

                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03
                        childHolder.relimg.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", pickle_name);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);
                                ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                Bundle args = new Bundle();
                                args.putString("PICKLE_ID", pickle_id);
                                args.putString("PICKLE_NAME", pickle_name);
                                args.putString("FROM", "NOTIFICATION");

                                ShowPickleFragmentobj.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager()
                                        .beginTransaction();
                                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        });

                    } else if (Notification_Type.equalsIgnoreCase("Voted On pickle")) {
                        JSONObject objVoter = MainObject.getJSONObject("voter");
                        String name = objVoter.getString("name");
                        String profile_image = objVoter.getString("profile_pic_url");

                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String textPickle = objPickle.getString("question");
                        final String pickle_id = objPickle.getString("id");

                        JSONObject objVote = MainObject.getJSONObject("vote");
                        JSONObject objPickle1 = MainObject.getJSONObject("user");
                        final String pickle_owner = objPickle1.getString("username");
                        childHolder.userName.setText(name);
                        childHolder.userMessage.setText("voted on " + pickle_owner + "'s pickle: " + textPickle);
                        imageLoader.DisplayImage(profile_image, childHolder.img_profile);

//						String notification_image = MainObject
//								.getString("notification_image_one");
//						childHolder.option_image.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(notification_image,
//								childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);

                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);

                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03
                        childHolder.relimg
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", textPickle);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);
                                        ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                        Bundle args = new Bundle();
                                        args.putString("PICKLE_ID", pickle_id);
                                        args.putString("PICKLE_NAME", textPickle);
                                        args.putString("FROM", "NOTIFICATION");

                                        ShowPickleFragmentobj.setArguments(args);
                                        FragmentTransaction transaction = getFragmentManager()
                                                .beginTransaction();
                                        transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                        transaction.addToBackStack(null);
                                        transaction.commit();

                                    }
                                });

                    } else if (Notification_Type
                            .equals("started following you on pickle")) {

                        JSONObject objFollowingUser = MainObject.getJSONObject("following_user");
                        String name = objFollowingUser.getString("name");
                        String user_name = objFollowingUser.getString("username");
                        String profile_image = objFollowingUser.getString("profile_pic_url");

                        JSONObject objFollow = MainObject.getJSONObject("follow");
                        childHolder.userName.setText(name);
                        childHolder.userMessage
                                .setText("started following you on pickle.");
                        imageLoader.DisplayImage(profile_image,
                                childHolder.img_profile);

                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                        childHolder.relimg.setVisibility(View.GONE);//@PKDEC03
                    } else if (Notification_Type.equals("Tagged On pickle.")) {
                        JSONObject objFollowingUser = MainObject.getJSONObject("tagged_by");
                        String name = objFollowingUser.getString("name");
                        String user_name = objFollowingUser.getString("username");
                        String profile_image = objFollowingUser.getString("profile_pic_url");

                        childHolder.userName.setText(name);
                        childHolder.userMessage.setText("Tagged you on pickle");
                        imageLoader.DisplayImage(profile_image, childHolder.img_profile);

//						String notification_image = MainObject
//								.getString("notification_image_one");
//						childHolder.option_image.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(notification_image,
//								childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);

                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);

                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String pickle_id = objPickle.getString("id");
                        final String pickle_name = objPickle.getString("question");
                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03
                        childHolder.relimg
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", pickle_name);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);

                                        ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                        Bundle args = new Bundle();
                                        args.putString("PICKLE_ID", pickle_id);
                                        args.putString("PICKLE_NAME", pickle_name);
                                        args.putString("FROM", "NOTIFICATION");

                                        ShowPickleFragmentobj.setArguments(args);
                                        FragmentTransaction transaction = getFragmentManager()
                                                .beginTransaction();
                                        transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                        transaction.addToBackStack(null);
                                        transaction.commit();

                                    }
                                });

                    } else if (Notification_Type.equals("Tagged On Bundle pickle")) {
                    } else if (Notification_Type.equals("shared your pickle")) {
                        JSONObject objshared = MainObject.getJSONObject("shared_user");
                        String Shared_name = objshared.getString("name");
                        String sharer_profile_pic_url = objshared.getString("profile_pic_url");
                        JSONObject objPickle = MainObject.getJSONObject("pickle");
                        final String textPickle = objPickle.getString("question");
                        final String pickle_id = objPickle.getString("id");

//						JSONObject objPickle1 = MainObject
//								.getJSONObject("user");
//						final String pickle_owner = objPickle1.getString("username");
                        childHolder.userName.setText(Shared_name);
                        childHolder.userMessage.setText("Shared your pickle: " + textPickle);
                        imageLoader.DisplayImage(sharer_profile_pic_url, childHolder.img_profile);

//						String notification_image = MainObject
//								.getString("notification_image_one");
//						childHolder.option_image.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(notification_image,
//								childHolder.option_image);
                        String notification_image = MainObject.getString("notification_image_one");
                        String notification_image2 = MainObject.getString("notification_image_two");
                        childHolder.option_image.setVisibility(View.VISIBLE);
                        childHolder.option_image2.setVisibility(View.VISIBLE);
                        imageLoader.DisplayImage(notification_image, childHolder.option_image);
                        imageLoader.DisplayImage(notification_image2, childHolder.option_image2);
                        String time = MainObject.getString("time");
                        childHolder.noti_time.setText(time);
                        childHolder.relimg.setVisibility(View.VISIBLE);//@PKDEC03

                        childHolder.relimg.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
//										Intent gotoPickleShowIntent = new Intent(
//												getActivity(),
//												ShowPickleFragment.class);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_ID", pickle_id);
//										gotoPickleShowIntent.putExtra(
//												"PICKLE_NAME", textPickle);
//										gotoPickleShowIntent.putExtra("FROM",
//												"NOTIFICATION");
//										startActivity(gotoPickleShowIntent);
                                ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                                Bundle args = new Bundle();
                                args.putString("PICKLE_ID", pickle_id);
                                args.putString("PICKLE_NAME", textPickle);
                                args.putString("FROM", "NOTIFICATION");

                                ShowPickleFragmentobj.setArguments(args);
                                FragmentTransaction transaction = getFragmentManager()
                                        .beginTransaction();
                                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                transaction.addToBackStack(null);
                                transaction.commit();

                            }
                        });
                    } else if (Notification_Type
                            .equals("Tagged On Bundle pickle")) {
                    } else if (Notification_Type.equals("has joined pickle")) {

                    } else if (Notification_Type
                            .equals("Commented on bundle pickle")) {

                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                convertView.setTag(childHolder);
            } else {
                childHolder = (ChildHolder) convertView.getTag();
            }

            return convertView;
        }

        @Override
        public int getChildrenCount(int arg0) {
            return notificationlist.get(notificationtitle.get(arg0)).size();
        }

        @Override
        public Object getGroup(int arg0) {
            return notificationtitle.get(arg0);
        }

        @Override
        public int getGroupCount() {
            return notificationtitle.size();
        }

        @Override
        public long getGroupId(int arg0) {
            return arg0;
        }

        @Override
        public View getGroupView(int arg0, boolean arg1, View arg2,
                                 ViewGroup arg3) {

            GroupHolder groupHolder;
            if (arg2 == null) {
                arg2 = LayoutInflater.from(mContext).inflate(R.layout.header,
                        null);
                groupHolder = new GroupHolder();
                groupHolder.title = (TextView) arg2.findViewById(R.id.text1);

                arg2.setTag(groupHolder);
            } else {
                groupHolder = (GroupHolder) arg2.getTag();
            }

            groupHolder.title.setText(notificationtitle.get(arg0));

            for (int i = 0; i < notificationlist.size(); i++) {
                mExpandableListView.expandGroup(i);
            }

            return arg2;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }

    }
}
