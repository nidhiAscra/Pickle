package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.CommonApis.UnfollowApi;
import com.ascra.data.MyPicklesUserData;
import com.ascra.data.TaggedPicklesUserData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.interfaces.UnfollowResponse;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserProfileFragment extends Fragment implements
        OnClickListener, UnfollowResponse {

    View view;
    private String URL_FEED = null;
    private String URL_FOLLOW_USER;
    private String URL_UNFOLLOW_USER = null;

    private Retrofit retrofit;

    ImageView imageProfilePic, imageEditProfile, imageShowGridMyPickles,
            imageShowListMyPickles, imageShowGridTaggedPickes,
            imageShowListTaggedPickles;

    TextView txtUserName, txtPicklesCounter, txtFollowersCounter,
            txtFollowingsCounter, myPickleText, taggedPickleText;

    RelativeLayout counterPicklesLayout, counterFollowersLayout,
            counterFollowingsLayout, btnMyPicklesLayout,
            btnTaggedPickledLayout, followUnfollowlayout;

    TextView btnFollow, btnUnFollow;

    GridView showMyPickles, showTaggedPickles;
    AVLoadingIndicatorView inProcess;

    ImageLoader imageLoader;
    MyPicklesUserData myPicklesUserData;
    TaggedPicklesUserData taggedPicklesUserData;

    ArrayList<MyPicklesUserData> listOfMyPicklesData = new ArrayList<MyPicklesUserData>();
    ArrayList<TaggedPicklesUserData> listOfTaggedPicklesData = new ArrayList<TaggedPicklesUserData>();

    static String name;
    static String userName;
    static String website;
    static String designation;
    static String bio;
    static String emaiId;
    static String gender;
    static boolean posts_are_private;
    static String phone_number;
    String isFollow;
    String strIsFollow;

    int userId, picklesCount, followersCount, followingsCount;
    static String profilePicUrl;

    public ViewHolder holder;

    static Bitmap compressedImage;

    String userNameFrom;
    String userFrom;
    String generatedId;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    AVLoadingIndicatorView rpd;

    Tracker tracker;

    String whichWay = "grid";

    Typeface face;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.user_profile_fragment, null);

//@PK30NOV
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setUpViews();
        setTracker();

//NS21DEC values are coming from HOmeScreen Activity

        Bundle bundle = this.getArguments();
        userNameFrom = bundle.getString("USER_NAME");
        userFrom = bundle.getString("FROM");
        generatedId = bundle.getString("GENERATED_ID");

        HomeScreen.whichFragment = "USERPROFILE";

        listOfMyPicklesData.clear();
        listOfTaggedPicklesData.clear();

        if (userFrom.equalsIgnoreCase("HOME")) {
            URL_FEED = Constant.PickleUrl + "users/" + userNameFrom.replace(" ", "%20") + ".json";
        } else if (userFrom.equalsIgnoreCase("FOLLOWERS")
                || userFrom.equalsIgnoreCase("SEARCH")
                || userFrom.equalsIgnoreCase("FEEDS") //@PKDEC02 : FEEDS - Added as user can open user profile from feeds header click
                || userFrom.equalsIgnoreCase("FOLLOWING") || userFrom.equalsIgnoreCase("NOTIFICATION")) {
            URL_FEED = Constant.PickleUrl + "users/" + userNameFrom.replace(" ", "%20") + ".json";


            followUnfollowlayout.setVisibility(View.VISIBLE);

            imageEditProfile.setVisibility(View.GONE);
        }

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            rpd = new AVLoadingIndicatorView(getActivity());
            rpd.setIndicator("BallPulseIndicator");
            rpd.setIndicatorColor(R.color.pink);

            rpd.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        new loadingUserProfileData().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }

        imageLoader = new ImageLoader(getActivity().getApplicationContext());

        face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/sniglet-regular.ttf");

        return view;
    }
   /* public void initRetrofit() {
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constant.PickleUrl_V1)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }*/

    private void setUpViews() {
        imageProfilePic = (ImageView) view.findViewById(R.id.profilePicture);
        imageEditProfile = (ImageView) view.findViewById(R.id.editProfile);
        imageShowGridMyPickles = (ImageView) view.findViewById(R.id.btnShowGridMyPickles);
        imageShowListMyPickles = (ImageView) view.findViewById(R.id.btnShowListMyPickles);
        imageShowGridTaggedPickes = (ImageView) view.findViewById(R.id.btnShowGridTaggedPickles);
        imageShowListTaggedPickles = (ImageView) view.findViewById(R.id.btnShowListTaggedPickles);
        txtUserName = (TextView) view.findViewById(R.id.txtUserName);
        txtUserName.setTypeface(face);
        txtPicklesCounter = (TextView) view.findViewById(R.id.pickleCounter);
        txtPicklesCounter.setTypeface(face);
        txtFollowersCounter = (TextView) view.findViewById(R.id.followersCounter);
        txtFollowingsCounter = (TextView) view.findViewById(R.id.followingsCounter);

        btnMyPicklesLayout = (RelativeLayout) view.findViewById(R.id.allMyPicklesLayout);
        btnTaggedPickledLayout = (RelativeLayout) view.findViewById(R.id.allTaggedPickledLayout);
        counterPicklesLayout = (RelativeLayout) view.findViewById(R.id.counterPickelLayout);
        counterFollowersLayout = (RelativeLayout) view.findViewById(R.id.counterFollowersLayout);
        counterFollowingsLayout = (RelativeLayout) view.findViewById(R.id.counterFollowingsLayout);

        followUnfollowlayout = (RelativeLayout) view.findViewById(R.id.followUnfollowlayout);
        showMyPickles = (GridView) view.findViewById(R.id.showMyPickles);
        showTaggedPickles = (GridView) view.findViewById(R.id.showTaggedPickles);

        inProcess = (AVLoadingIndicatorView) view.findViewById(R.id.inProcess);

        btnFollow = (TextView) view.findViewById(R.id.btnFollow);
        btnFollow.setTypeface(face);
        btnUnFollow = (TextView) view.findViewById(R.id.btnUnfollow);
        btnUnFollow.setTypeface(face);

        myPickleText = (TextView) view.findViewById(R.id.mypickletext);
        myPickleText.setTypeface(face);
        taggedPickleText = (TextView) view.findViewById(R.id.teggedpickletext);
        taggedPickleText.setTypeface(face);

        showMyPickles.setVisibility(View.VISIBLE);
        showTaggedPickles.setVisibility(View.GONE);

        imageEditProfile.setOnClickListener(this);
        // btnMyPicklesLayout.setOnClickListener(this);
        // btnTaggedPickledLayout.setOnClickListener(this);
        counterPicklesLayout.setOnClickListener(this);
        counterFollowersLayout.setOnClickListener(this);
        counterFollowingsLayout.setOnClickListener(this);
        btnFollow.setOnClickListener(this);
        btnUnFollow.setOnClickListener(this);

        myPickleText.setOnClickListener(this);
        taggedPickleText.setOnClickListener(this);

        imageShowGridMyPickles.setOnClickListener(this);
        imageShowListMyPickles.setOnClickListener(this);
        imageShowGridTaggedPickes.setOnClickListener(this);
        imageShowListTaggedPickles.setOnClickListener(this);

        followUnfollowlayout.setVisibility(View.GONE);

        btnFollow.setVisibility(View.GONE);
        btnUnFollow.setVisibility(View.GONE);
        //@PKDEC06
        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.userProfileHeaderTitle, R.drawable.pickleprofile_color);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "PROFILE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    @Override
    public void handleResponse(Call<JSONObject> call, Response<JSONObject> response, int position) {
        Log.e("handleResponse", this.getClass().getSimpleName() + " UserProfileFragment -" + position + "- " + response);
    }

    @Override
    public void handleError(Call<JSONObject> call, Throwable t, int position) {
        Log.e("handleError", this.getClass().getSimpleName() + " UserProfileFragment -" + position + "- " + t);
    }

    private class loadingUserProfileData extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            //@PKDEC03 added on ROR Req
            params.add(new BasicNameValuePair("user_id", SaveUtils.getUserID(getResources().getString(R.string.PreferenceFileName), getActivity(), SaveUtils.USER_ID, "")));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET, params);

            Log.e("@Create","Profile Url "+URL_FEED+"?"+params);
            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rpd.hide();
        }

    }

    private void parseJSONUserProfile(JSONObject objMain) {
        Log.e("@Create","Profile url response "+objMain);

        try {
            userName = objMain.getString("user_name");
            name = objMain.getString("name");
            website = objMain.getString("website");
            designation = objMain.getString("designation");
            bio = objMain.getString("bio");
            phone_number = objMain.getString("phone_no");

            JSONObject private_info = objMain.getJSONObject("private_info");
            emaiId = private_info.getString("email");

            gender = objMain.getString("gender");
            posts_are_private = objMain.getBoolean("posts_are_private");

            userId = objMain.getInt("user_id");
            profilePicUrl = objMain.getString("profile_pic_url");
            picklesCount = objMain.getInt("pickles_count");
            followersCount = objMain.getInt("followers_count");
            followingsCount = objMain.getInt("following_count");
            if (userFrom.equalsIgnoreCase("HOME")) {
                strIsFollow = objMain.getString("is_follow");
                followUnfollowlayout.setVisibility(View.GONE);
            }
            Log.e("@Create", "Follow:" + objMain.isNull("follow_record"));
            //NS21DEC
            if(!objMain.isNull("follow_record")) {
                JSONObject follow_record = objMain.getJSONObject("follow_record");



                    generatedId = follow_record.getString("id");

            }
            // @PKDEC02
            //if..else.. chain added to check if is_follow is not null, or the user is himself when coming from Feeds HeaderClick
            // setFollowUnfollowLayout() is called only in specific places where we have the is_follow value and is to be checked, for self profile,
            // it should not be checked and the follow unfollow layout should be hidden, so not called.
             if (userFrom.equalsIgnoreCase("FEEDS")) {
                if (objMain.has("is_follow") && objMain.getString("is_follow") != null && !objMain.getString("is_follow").equalsIgnoreCase("null")) {
                    isFollow = objMain.getString("is_follow");
                    setFollowUnfollowLayout();
                } else if (userName.equalsIgnoreCase(HomeScreen.session_name)) {
                    followUnfollowlayout.setVisibility(View.GONE);
                    btnFollow.setVisibility(View.GONE);
                    btnUnFollow.setVisibility(View.GONE);
                } else {
                    isFollow = "false";
                    setFollowUnfollowLayout();
                }
            } else {
                isFollow = objMain.getString("is_follow");
                setFollowUnfollowLayout();
            }
// @PKDEC02 - Actual place of setFollowUnfollowLayout();
//            setFollowUnfollowLayout();

            if (profilePicUrl != null) {
                imageLoader.DisplayImage(profilePicUrl, imageProfilePic);
            }

            if (name != null) {
                txtUserName.setText(name);
            }

            if (picklesCount != 0) {
                txtPicklesCounter.setText("" + picklesCount);
            } else {
                txtPicklesCounter.setText("0");
            }

            if (followersCount != 0) {
                txtFollowersCounter.setText("" + followersCount);
            } else {
                txtFollowersCounter.setText("0");
            }

            if (followingsCount != 0) {
                txtFollowingsCounter.setText("" + followingsCount);
            } else {
                txtFollowingsCounter.setText("0");
            }

            JSONArray myPicklesArray = objMain.getJSONArray("my_pickles");
            for (int noOfMyPickleArray = 0; noOfMyPickleArray < myPicklesArray
                    .length(); noOfMyPickleArray++) {
                //@PKDEC03 : if condition added as sometimes null data was received and it caused exception,
                //           so any proper data after it wasn't reached and displayed.

                if (myPicklesArray.get(noOfMyPickleArray) instanceof JSONObject) {
                    JSONObject objMyPickles = myPicklesArray.getJSONObject(noOfMyPickleArray);

//                    Original one --v
//                    JSONObject objPickle = objMyPickles.getJSONObject("pickle");

                    //@PKDEC06 REVIEW[Pranav] Check once the response is changed [Temp solution for {"pickle":{"pickle":{}}} (extra pickle obj)]
                    JSONObject objPickle = objMyPickles.getJSONObject("pickle").getJSONObject("pickle");

                    boolean pickle_deactive = objPickle.getBoolean("pickle_deactive");
                    boolean pickle_featured = objPickle.getBoolean("pickle_featured");
                    long pickle_id = objPickle.getLong("pickle_id");
                    String pickle_image = objPickle.getString("pickle_image");
                    String pickle_name = objPickle.getString("pickle_name");
                    int user_id = objPickle.getInt("user_id");
                    String pickle_created_at = objPickle.getString("pickle_created_at");

//                    JSONObject objFirstOption = objMyPickles.getJSONObject("first_options"); //original
                    //REVIEW[Pranav] Check if response changes
                    JSONObject objFirstOption = objMyPickles.getJSONObject("option1").getJSONObject("first_options");

                    long first_option_id = objFirstOption.getLong("first_option_id");
                    String first_option_image_url = objFirstOption.getString("first_option_image_url");
                    String first_option_lable = objFirstOption.getString("first_option_lable");
                    String first_option_text = objFirstOption.getString("first_option_text");

//                    JSONObject objSecondOption = objMyPickles.getJSONObject("second_option"); //original
                    //REVIEW[Pranav] Check if response changes
                    JSONObject objSecondOption = objMyPickles.getJSONObject("option2").getJSONObject("second_option");

                    long second_option_id = objSecondOption.getLong("second_option_id");
                    String second_option_image_url = objSecondOption.getString("second_option_image_url");
                    String second_option_lable = objSecondOption.getString("second_option_lable");
                    String second_option_text = objSecondOption.getString("second_option_text");
                    myPicklesUserData = new MyPicklesUserData(pickle_deactive, pickle_featured, String.valueOf(pickle_id), pickle_image, pickle_name, user_id, pickle_created_at, first_option_id, first_option_image_url, first_option_lable, first_option_text, second_option_id, second_option_image_url, second_option_lable, second_option_text);
                    listOfMyPicklesData.add(myPicklesUserData);
                }

                MyPicklesAdapterNew myPicklesAdapter = new MyPicklesAdapterNew(listOfMyPicklesData, whichWay);
                showMyPickles.setAdapter(myPicklesAdapter);
                showMyPickles.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                        Bundle args = new Bundle();
                        args.putString("PICKLE_ID", listOfMyPicklesData.get(position).getPickle_id());
                        args.putString("PICKLE_NAME", listOfMyPicklesData.get(position).getPickle_name());
                        args.putString("FROM", userFrom);
                        ShowPickleFragmentobj.setArguments(args);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                        transaction.addToBackStack(null);
                        transaction.commit();
//						Intent showPickleIntent = new Intent(getActivity(),
//								ShowPickleFragment.class);
//						showPickleIntent.putExtra("PICKLE_ID", ""
//								+ listOfMyPicklesData.get(position)
//										.getPickle_id());
//						showPickleIntent.putExtra("PICKLE_NAME",
//								listOfMyPicklesData.get(position)
//										.getPickle_name());
//						showPickleIntent.putExtra("FROM", userFrom);
//						startActivity(showPickleIntent);

                    }
                });

                inProcess.setVisibility(View.GONE);
            }

            JSONArray taggedPicklesArray = objMain.getJSONArray("tagged_pickle_pickles");
            for (int noOfTaggedPickleArray = 0; noOfTaggedPickleArray < taggedPicklesArray.length(); noOfTaggedPickleArray++) {
                JSONObject objMyPickles = taggedPicklesArray.getJSONObject(noOfTaggedPickleArray);

                JSONObject objPickle = objMyPickles.getJSONObject("pickle");
                boolean pickle_deactive = objPickle.getBoolean("pickle_deactive");
                boolean pickle_featured = objPickle.getBoolean("pickle_featured");
                long pickle_id = objPickle.getLong("pickle_id");
                String pickle_image = objPickle.getString("pickle_image");
                String pickle_name = objPickle.getString("pickle_name");
                int user_id = objPickle.getInt("user_id");

                String pickle_created_at = objPickle.getString("pickle_created_at");
                JSONObject objFirstOption = objMyPickles.getJSONObject("first_options");
                long first_option_id = objFirstOption.getLong("first_option_id");
                String first_option_image_url = objFirstOption.getString("first_option_image_url");
                String first_option_lable = objFirstOption.getString("first_option_lable");
                String first_option_text = objFirstOption.getString("first_option_text");
                JSONObject objSecondOption = objMyPickles.getJSONObject("second_option");
                long second_option_id = objSecondOption.getLong("second_option_id");
                String second_option_image_url = objSecondOption.getString("second_option_image_url");
                String second_option_lable = objSecondOption.getString("second_option_lable");
                String second_option_text = objSecondOption.getString("second_option_text");

                taggedPicklesUserData = new TaggedPicklesUserData(
                        pickle_deactive, pickle_featured, String.valueOf(pickle_id),
                        pickle_image, pickle_name, user_id, pickle_created_at,
                        first_option_id, first_option_image_url,
                        first_option_lable, first_option_text,
                        second_option_id, second_option_image_url,
                        second_option_lable, second_option_text);

                listOfTaggedPicklesData.add(taggedPicklesUserData);

                TaggedAdapterNew taggedPicklesAdapter = new TaggedAdapterNew(
                        listOfTaggedPicklesData, whichWay);
                showTaggedPickles.setAdapter(taggedPicklesAdapter);

                showTaggedPickles.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view, int position, long id) {
                        ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
                        Bundle args = new Bundle();
                        args.putString("PICKLE_ID", listOfMyPicklesData.get(position).getPickle_id());
                        args.putString("PICKLE_NAME", listOfMyPicklesData.get(position).getPickle_name());
                        args.putString("FROM", userFrom);

                        ShowPickleFragmentobj.setArguments(args);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                        transaction.addToBackStack(null);
                        transaction.commit();

//								Intent showPickleIntent = new Intent(
//										getActivity(), ShowPickleFragment.class);
//								showPickleIntent.putExtra("PICKLE_ID", ""
//										+ listOfTaggedPicklesData.get(position)
//												.getPickle_id());
//								showPickleIntent.putExtra("PICKLE_NAME",
//										listOfTaggedPicklesData.get(position)
//												.getPickle_name());
//								showPickleIntent.putExtra("FROM", userFrom);
//								startActivity(showPickleIntent);
                    }
                });

                inProcess.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFollowUnfollowLayout() {
        Log.e("isFollow", "" + isFollow);
        if (isFollow.equals("true")) {
            followUnfollowlayout.setVisibility(View.VISIBLE);
            btnFollow.setVisibility(View.GONE);
            btnUnFollow.setVisibility(View.VISIBLE);
        } else if(isFollow.equals("false")) {

            followUnfollowlayout.setVisibility(View.VISIBLE);
            btnFollow.setVisibility(View.VISIBLE);
            btnUnFollow.setVisibility(View.GONE);
        } else {
            followUnfollowlayout.setVisibility(View.GONE);
            btnFollow.setVisibility(View.GONE);
            btnUnFollow.setVisibility(View.GONE);
        }
    }

    class MyPicklesAdapterNew extends BaseAdapter {

        ArrayList<MyPicklesUserData> listOfMyPicklesData = new ArrayList<MyPicklesUserData>();
        LayoutInflater inflater;
        String whichWay;

        public MyPicklesAdapterNew(
                ArrayList<MyPicklesUserData> listOfMyPicklesData,
                String whichWay) {
            this.listOfMyPicklesData = listOfMyPicklesData;
            this.whichWay = whichWay;
            inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public int getCount() {
            return listOfMyPicklesData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfMyPicklesData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            holder = new ViewHolder();

            if (view == null) {

                if (whichWay.equals("grid")) {
                    view = inflater.inflate(
                            R.layout.my_pickle_item_layout_in_grid, null);
                } else {
                    view = inflater.inflate(
                            R.layout.my_pickle_item_layout_in_list, null);
                }

                holder.firstOptionImage = (ImageView) view
                        .findViewById(R.id.firstImage);

                holder.firstText = (TextView) view.findViewById(R.id.firstText);

                holder.firstText.setTypeface(face);

                holder.secondOptionImage = (ImageView) view
                        .findViewById(R.id.secondImage);

                holder.secondText = (TextView) view
                        .findViewById(R.id.secondText);

                holder.secondText.setTypeface(face);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (listOfMyPicklesData.get(position).getFirst_option_image_url() != null) {

                if (listOfMyPicklesData.get(position)
                        .getFirst_option_image_url()
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    holder.firstOptionImage.setVisibility(View.GONE);
                } else {
                    holder.firstOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(listOfMyPicklesData.get(position)
                                    .getFirst_option_image_url(),
                            holder.firstOptionImage);
                }

            }

            if (listOfMyPicklesData.get(position).getFrist_option_text() != null) {
                if (listOfMyPicklesData.get(position).getFrist_option_text()
                        .equals("")
                        || listOfMyPicklesData.get(position)
                        .getFrist_option_text().equals("null")) {
                    holder.firstText.setVisibility(View.GONE);
                } else {
                    holder.firstText.setVisibility(View.VISIBLE);
                    holder.firstText.setText(listOfMyPicklesData.get(position)
                            .getFrist_option_text());
                }
            }

            if (listOfMyPicklesData.get(position).getSecond_option_image_url() != null) {
                if (listOfMyPicklesData.get(position)
                        .getSecond_option_image_url()
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    holder.secondOptionImage.setVisibility(View.GONE);
                } else {
                    holder.secondOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(listOfMyPicklesData.get(position)
                                    .getSecond_option_image_url(),
                            holder.secondOptionImage);
                }
            }

            if (listOfMyPicklesData.get(position).getSecond_option_text() != null) {
                if (listOfMyPicklesData.get(position).getSecond_option_text()
                        .equals("")
                        || listOfMyPicklesData.get(position)
                        .getSecond_option_text().equals("null")) {
                    holder.secondText.setVisibility(View.GONE);

                } else {
                    holder.secondText.setVisibility(View.VISIBLE);
                    holder.secondText.setText(listOfMyPicklesData.get(position)
                            .getSecond_option_text());
                }
            }

            return view;
        }

    }

    class TaggedAdapterNew extends BaseAdapter {

        ArrayList<TaggedPicklesUserData> listOfTaggedPicklesData = new ArrayList<TaggedPicklesUserData>();
        LayoutInflater inflater;
        String whichWay;

        public TaggedAdapterNew(
                ArrayList<TaggedPicklesUserData> listOfTaggedPicklesData,
                String whichWay) {
            this.listOfTaggedPicklesData = listOfTaggedPicklesData;
            this.whichWay = whichWay;
            inflater = LayoutInflater.from(getActivity());
        }

        @Override
        public int getCount() {
            return listOfTaggedPicklesData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfTaggedPicklesData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            holder = new ViewHolder();

            if (view == null) {

                if (whichWay.equals("grid")) {
                    view = inflater.inflate(
                            R.layout.tagged_pickle_item_layout_in_grid, null);
                } else {
                    view = inflater.inflate(
                            R.layout.tagged_pickle_item_layout_in_list, null);
                }

                holder.firstOptionImage = (ImageView) view
                        .findViewById(R.id.firstImage);

                holder.secondOptionImage = (ImageView) view
                        .findViewById(R.id.secondImage);

                holder.firstText = (TextView) view.findViewById(R.id.firstText);

                holder.firstText.setTypeface(face);

                holder.secondText = (TextView) view
                        .findViewById(R.id.secondText);

                holder.secondText.setTypeface(face);

                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            if (listOfTaggedPicklesData.get(position)
                    .getFirst_option_image_url() != null) {

                if (listOfTaggedPicklesData.get(position)
                        .getFirst_option_image_url()
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    holder.firstOptionImage.setVisibility(View.GONE);
                } else {
                    holder.firstOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(
                            listOfTaggedPicklesData.get(position)
                                    .getFirst_option_image_url(),
                            holder.firstOptionImage);
                }

            }

            if (listOfTaggedPicklesData.get(position).getFirst_option_text() != null) {
                if (listOfTaggedPicklesData.get(position)
                        .getFirst_option_text().equals("")
                        || listOfTaggedPicklesData.get(position)
                        .getFirst_option_text().equals("null")) {
                    holder.firstText.setVisibility(View.GONE);

                } else {
                    holder.firstText.setVisibility(View.VISIBLE);
                    holder.firstText.setText(listOfTaggedPicklesData.get(
                            position).getFirst_option_text());
                }
            }

            if (listOfTaggedPicklesData.get(position)
                    .getSecond_option_image_url() != null) {
                if (listOfTaggedPicklesData.get(position)
                        .getSecond_option_image_url()
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    holder.secondOptionImage.setVisibility(View.GONE);
                } else {
                    holder.secondOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(
                            listOfTaggedPicklesData.get(position)
                                    .getSecond_option_image_url(),
                            holder.secondOptionImage);
                }
            }

            if (listOfTaggedPicklesData.get(position).getSecond_option_text() != null) {
                if (listOfTaggedPicklesData.get(position)
                        .getSecond_option_text().equals("")
                        || listOfTaggedPicklesData.get(position)
                        .getSecond_option_text().equals("null")) {
                    holder.secondText.setVisibility(View.GONE);
                } else {
                    holder.secondText.setVisibility(View.VISIBLE);
                    holder.secondText.setText(listOfTaggedPicklesData.get(
                            position).getSecond_option_text());
                }
            }

            return view;
        }
    }

    public class ViewHolder {
        MyPicklesUserData myPicklesUserData;
        TaggedPicklesUserData taggedPickleUserData;

        ImageView firstOptionImage;
        TextView firstText;
        ImageView secondOptionImage;
        TextView secondText;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editProfile:
                UserEditProfileFragment UserEditProfileFragment = new UserEditProfileFragment();
                FragmentTransaction UserEditProfileTransaction = getFragmentManager()
                        .beginTransaction();
                UserEditProfileTransaction.replace(R.id.content_frame,
                        UserEditProfileFragment);
                UserEditProfileTransaction.addToBackStack(null);
                UserEditProfileTransaction.commit();
                break;

            case R.id.mypickletext:
                showMyPickles.setVisibility(View.VISIBLE);
                showTaggedPickles.setVisibility(View.GONE);
                setMyPickles();
                break;
            case R.id.teggedpickletext:
                showMyPickles.setVisibility(View.GONE);
                showTaggedPickles.setVisibility(View.VISIBLE);
                setTaggedPickles();
                break;
            case R.id.allMyPicklesLayout:
                showMyPickles.setVisibility(View.VISIBLE);
                showTaggedPickles.setVisibility(View.GONE);

                setMyPickles();
                break;
            case R.id.allTaggedPickledLayout:
                showMyPickles.setVisibility(View.GONE);
                showTaggedPickles.setVisibility(View.VISIBLE);
                setTaggedPickles();
                break;

            case R.id.btnShowGridMyPickles:

                whichWay = "grid";
                MyPicklesAdapterNew myPicklesAdapter1 = new MyPicklesAdapterNew(
                        listOfMyPicklesData, whichWay);
                showMyPickles.setAdapter(myPicklesAdapter1);
                showMyPickles.setNumColumns(2);
                break;
            case R.id.btnShowListMyPickles:

                whichWay = "list";
                MyPicklesAdapterNew myPicklesAdapter2 = new MyPicklesAdapterNew(
                        listOfMyPicklesData, whichWay);
                showMyPickles.setAdapter(myPicklesAdapter2);
                showMyPickles.setNumColumns(1);
                break;
            case R.id.btnShowGridTaggedPickles:

                whichWay = "grid";
                TaggedAdapterNew taggedAdapterNew1 = new TaggedAdapterNew(
                        listOfTaggedPicklesData, whichWay);
                showTaggedPickles.setAdapter(taggedAdapterNew1);
                showTaggedPickles.setNumColumns(2);
                break;
            case R.id.btnShowListTaggedPickles:

                whichWay = "list";
                TaggedAdapterNew taggedAdapterNew2 = new TaggedAdapterNew(
                        listOfTaggedPicklesData, whichWay);
                showTaggedPickles.setAdapter(taggedAdapterNew2);
                showTaggedPickles.setNumColumns(1);
                break;
            case R.id.btnFollow:
                followUser("" + userId, HomeScreen.user_id,
                        HomeScreen.authentication_token, HomeScreen.key);

                break;
            case R.id.btnUnfollow:

                URL_UNFOLLOW_USER = Constant.PickleUrl_V1 + "followers/" + generatedId + ".json";

                UnFollowUser sendPostReqAsyncTask = new UnFollowUser();
                sendPostReqAsyncTask.execute();

//@PKDEC12 : Common Api call

                //NS20DEC
         //       initRetrofit();
//                new UnfollowApi(getActivity(), this).unfollow(HomeScreen.authentication_token, HomeScreen.key,
//                        SaveUtils.getUserID(getString(R.string.PreferenceFileName), getActivity(), SaveUtils.USER_ID, "0"),
//                        String.valueOf(generatedId), Constant.FOLLOWINGS, -1);
 /*               HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FOLLOW_USER);*/




                break;
            case R.id.counterPickelLayout:

                break;
            case R.id.counterFollowersLayout:
                if (followersCount == 0) {
                    //NS21DEC
                    String followersText = "No followers found!";
                    showNoFeedsDialog(followersText);
                } else {
                    FollowersFragment followersFragment = new FollowersFragment();
                    Bundle followersArgs = new Bundle();
                    followersArgs.putString("USER_NAME", userNameFrom);
                    followersFragment.setArguments(followersArgs);
                    FragmentTransaction followersTransaction = getFragmentManager()
                            .beginTransaction();
                    followersTransaction.replace(R.id.content_frame,
                            followersFragment);
                    followersTransaction.addToBackStack(null);
                    followersTransaction.commit();
                }
                break;
            case R.id.counterFollowingsLayout:
                if (followingsCount == 0) {
                    //NS21DEC
                    String followingText = "Not following any friends !";
                    showNoFeedsDialog(followingText);
                } else {
                    FollowingsFragments followingsFragment = new FollowingsFragments();
                    Bundle followingsArgs = new Bundle();
                    followingsArgs.putString("USER_NAME", userNameFrom);
                    followingsFragment.setArguments(followingsArgs);
                    FragmentTransaction followingsTransaction = getFragmentManager()
                            .beginTransaction();
                    followingsTransaction.replace(R.id.content_frame,
                            followingsFragment);
                    followingsTransaction.addToBackStack(null);
                    followingsTransaction.commit();
                }
                break;
            default:
                break;
        }
    }

    private void followUser(String followerUserId, String userId,
                            String authenticationToken, String key) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramFollowerUserId = params[0];
                String paramUserId = params[1];
                String paramAuthToken = params[2];
                String paramKey = params[3];

                URL_FOLLOW_USER = Constant.PickleUrl_V1+"followers/"
                        +"user_following"+ ".json";

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FOLLOW_USER);

                Log.e("@Create","Follower:"+URL_FOLLOW_USER);
                BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("follower_user_id", paramFollowerUserId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(followerUserIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        Log.e("@Create","Result:"+stringBuilder.toString());

                            try {
                                JSONObject json=new JSONObject(stringBuilder.toString());
                                if(json.getString("success").equals("true")){
                                    JSONObject follow_record = json.getJSONObject("follow_record");
                                    generatedId = follow_record.getString("id");
                                    return stringBuilder.toString();
                                }
                                else{
                                  return null;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("@Create","Result:"+result);
                if(result!=null){

                    followUnfollowlayout.setVisibility(View.VISIBLE);
                    btnFollow.setVisibility(View.GONE);
                    btnUnFollow.setVisibility(View.VISIBLE);
                    txtFollowersCounter.setText("" + (++followersCount));
                }

            }

        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(followerUserId, userId,
                authenticationToken, key);

    }

   class UnFollowUser extends AsyncTask<String, Void, String> {

       @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL_UNFOLLOW_USER);

            Log.e("@Create","Follower:"+URL_UNFOLLOW_USER);
            BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("Id", generatedId);
            BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", HomeScreen.user_id);
            BasicNameValuePair otherIdBasicNameValuePair = new BasicNameValuePair("other_id",userId+"");
            BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", HomeScreen.authentication_token);
            BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", HomeScreen.key);

            Log.e("@Create", "Follower req params:" + generatedId+" "+HomeScreen.user_id+" "+userId+" "+HomeScreen.authentication_token+" "+HomeScreen.key);

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(followerUserIdBasicNameValuePair);
            nameValuePairList.add(userIdBasicNameValuePair);
            nameValuePairList.add(otherIdBasicNameValuePair);
            nameValuePairList.add(authTokenBasicNameValuePair);
            nameValuePairList.add(keyBasicNameValuePair);

            try {
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                        nameValuePairList);

                httpPost.setEntity(urlEncodedFormEntity);

                try {
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    Log.e("@Create","Result:"+stringBuilder.toString());

                    try {
                        JSONObject json=new JSONObject(stringBuilder.toString());
                        if(json.getString("success").equals("true")){


                            return stringBuilder.toString();
                        }
                        else{
                            return null;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (ClientProtocolException cpe) {
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("@Create","Result:"+result);
            if(result!=null){

                followUnfollowlayout.setVisibility(View.VISIBLE);
                btnFollow.setVisibility(View.VISIBLE);
                btnUnFollow.setVisibility(View.GONE);
                txtFollowersCounter.setText("" + (--followersCount));
            }

        }
            // Creating service handler class instance




    }

    /*@PK28NOV added color filter changing code for 'imageShowGridTaggedPickes' & 'imageShowGridMyPickles' as on web*/
    private void setMyPickles() {
        btnMyPicklesLayout.setBackgroundColor(Color.parseColor("#f26f71"));
        btnTaggedPickledLayout.setBackgroundColor(Color.parseColor("#c0c0c0"));
        imageShowGridMyPickles.setColorFilter(Color.parseColor("#ffffff"));
        imageShowGridTaggedPickes.setColorFilter(Color.parseColor("#7F7F7F"));
    }

    /*@PK28NOV added color filter changing code for 'imageShowGridTaggedPickes' & 'imageShowGridMyPickles' as on web*/
    private void setTaggedPickles() {
        btnMyPicklesLayout.setBackgroundColor(Color.parseColor("#c0c0c0"));
        btnTaggedPickledLayout.setBackgroundColor(Color.parseColor("#f26f71"));
        imageShowGridTaggedPickes.setColorFilter(Color.parseColor("#ffffff"));
        imageShowGridMyPickles.setColorFilter(Color.parseColor("#7F7F7F"));
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setTextColor(Color.BLACK);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

}
/*
The response as on Dec 06, 2016 till 12:20 PM IST
For this response, the review tags were added
{
  "success": "true",
  "name": "Sanket Patil",
  "user_name": "Sanket Patil",
  "phone_no": null,
  "website": null,
  "designation": null,
  "bio": null,
  "private_info": {
    "email": "patilarun1588@gmail.com"
  },
  "gender": null,
  "posts_are_private": false,
  "user_id": 3,
  "is_follow": null,
  "follow_record": null,
  "profile_pic_url": "http://s3.amazonaws.com/pickel-test/profile_pictures/3/original/stringio.txt?1426052056",
  "pickles_count": 21,
  "followers_count": 0,
  "following_count": 1,
  "my_pickles": [
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1210,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1210/original/shared_picle_1210.jpg?1480945931",
          "pickle_name": "test",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T13:52:05Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1966,
          "first_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "first_option_lable": "",
          "first_option_text": "tets"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1967,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1967/original/pininterest_api.png?1480945924",
          "second_option_lable": "caption21",
          "second_option_text": ""
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1209,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1209/original/shared_picle_1209.jpg?1480945889",
          "pickle_name": "test",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T13:51:28Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1964,
          "first_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "first_option_lable": "",
          "first_option_text": "test1"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1965,
          "second_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "second_option_lable": "",
          "second_option_text": "test2"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1208,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1208/original/shared_picle_1208.jpg?1480945861",
          "pickle_name": "test pickle",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T13:50:48Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1962,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1962/original/gurgaon-2-contact-page_02.jpg?1480945847",
          "first_option_lable": "caption1",
          "first_option_text": ""
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1963,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1963/original/screen_one.jpeg?1480945847",
          "second_option_lable": "caption2",
          "second_option_text": ""
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1206,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1206/original/shared_picle_1206.jpg?1480944591",
          "pickle_name": "7g7g",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T11:50:56Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1960,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1960/original/Photo_20161027_175701.jpg?1480944574",
          "first_option_lable": "tydyfifi",
          "first_option_text": "null"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1961,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1961/original/1-WrTn0ooZERunZSh8w_PqyA.png?1480944577",
          "second_option_lable": "fhfhht9hy7h557",
          "second_option_text": "null"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1204,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1204/original/shared_picle_1204.jpg?1480937560",
          "pickle_name": "tower or bottle",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T10:04:38Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1956,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1956/original/Photo_20161027_175701.jpg?1480937542",
          "first_option_lable": "new tree image",
          "first_option_text": "null"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1957,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1957/original/DP_1476531839436.jpeg?1480937544",
          "second_option_lable": "what's this",
          "second_option_text": "null"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 1203,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1203/original/shared_picle_1203.jpg?1480932111",
          "pickle_name": "Left or Right ?",
          "user_id": 3,
          "pickle_created_at": "2016-12-05T09:52:21Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1954,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1954/original/images.jpg?1480931540",
          "first_option_lable": "This tower is in France",
          "first_option_text": "xbehe hdyduc.  hdgehfufudug"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1955,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1955/original/images_(1).jpg?1480931721",
          "second_option_lable": "null",
          "second_option_text": ""
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 931,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/931/original/931.gif?1436008258",
          "pickle_name": "hvuvuvuv",
          "user_id": 3,
          "pickle_created_at": "2015-07-02T05:43:10Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1478,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1478/original/1436007739554.jpg?1436008243",
          "first_option_lable": "xcc",
          "first_option_text": "null"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1479,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1479/original/1436007843368.jpg?1436008245",
          "second_option_lable": "ubb",
          "second_option_text": "null"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 733,
          "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/733/original/733.gif?1432791338",
          "pickle_name": "bsbsbbbxd",
          "user_id": 3,
          "pickle_created_at": "2015-05-28T05:35:30Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 1160,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1160/original/1432791240260.jpg?1432791329",
          "first_option_lable": "sbbdbd",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 1161,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1161/original/1432791279619.jpg?1432791330",
          "second_option_lable": "bdndndjsj",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 142,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "jdjd",
          "user_id": 3,
          "pickle_created_at": "2015-03-25T14:12:24Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 267,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/267/original/Screenshot_2015-01-17-12-34-58.png?1427292739",
          "first_option_lable": "ndjdj",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 268,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/268/original/1422533200892.jpg?1427292740",
          "second_option_lable": "jdjdj",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 141,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "ghh",
          "user_id": 3,
          "pickle_created_at": "2015-03-25T14:08:36Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 265,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/265/original/1422600752293.jpg?1427292511",
          "first_option_lable": "gshsh",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 266,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/266/original/Screenshot_2015-01-17-11-46-24.png?1427292515",
          "second_option_lable": "hjdjjd",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 140,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "ghffh",
          "user_id": 3,
          "pickle_created_at": "2015-03-25T14:02:14Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 263,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/263/original/1422529830888.jpg?1427292129",
          "first_option_lable": "gdhehdd",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 264,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/264/original/Screenshot_2015-01-17-12-34-58.png?1427292133",
          "second_option_lable": "hffhxh",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 136,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "gdhdhd",
          "user_id": 3,
          "pickle_created_at": "2015-03-25T13:37:14Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 255,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/255/original/1427290552904.jpg?1427290629",
          "first_option_lable": "hdhdj",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 256,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/256/original/1422598752428.jpg?1427290629",
          "second_option_lable": "hfhf",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 129,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "hfhfjdj",
          "user_id": 3,
          "pickle_created_at": "2015-03-25T12:39:13Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 241,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/241/original/1422598800698.jpg?1427287138",
          "first_option_lable": "dghhx",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 242,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/242/original/1422600342309.jpg?1427287145",
          "second_option_lable": "bxbdj",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 31,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "bees",
          "user_id": 3,
          "pickle_created_at": "2015-03-04T06:54:36Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 57,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/57/original/stringio.txt?1425452077",
          "first_option_lable": "A",
          "first_option_text": ""
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 58,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/58/original/open-uri20150304-16633-p72iq8?1425452082",
          "second_option_lable": "B",
          "second_option_text": ""
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 30,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "choose one",
          "user_id": 3,
          "pickle_created_at": "2015-01-23T07:26:47Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 55,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/55/original/open-uri20150123-4443-1rhx9wh?1421998015",
          "first_option_lable": "a ",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 56,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/56/original/stringio.txt?1421998029",
          "second_option_lable": "b",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 20,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "whatt",
          "user_id": 3,
          "pickle_created_at": "2015-01-22T12:04:34Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 39,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/39/original/Screenshot_2015-01-13-15-56-43.jpg?1421928273",
          "first_option_lable": "hjfjf",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 40,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/40/original/1418215366505.jpg?1421928274",
          "second_option_lable": "jjfjjf",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 19,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "rjjjfjrjr",
          "user_id": 3,
          "pickle_created_at": "2015-01-22T11:56:08Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 37,
          "first_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "first_option_lable": null,
          "first_option_text": "ururur"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 38,
          "second_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "second_option_lable": null,
          "second_option_text": "jtjjff"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 18,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "jjfjttjjtjjt",
          "user_id": 3,
          "pickle_created_at": "2015-01-22T11:17:33Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 35,
          "first_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "first_option_lable": null,
          "first_option_text": "thoughtfulnessdkkd"
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 36,
          "second_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
          "second_option_lable": null,
          "second_option_text": "iifktjtjtjtjjtjtj"
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 17,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "456",
          "user_id": 3,
          "pickle_created_at": "2015-01-22T11:11:58Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 33,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/33/original/1421963056096.jpg?1421925117",
          "first_option_lable": "bjjdj",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 34,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/34/original/1421963072556.jpg?1421925117",
          "second_option_lable": "jfjjffjf",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 16,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "what is best",
          "user_id": 3,
          "pickle_created_at": "2015-01-22T07:37:46Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 31,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/31/original/stringio.txt?1421912267",
          "first_option_lable": "a",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 32,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/32/original/stringio.txt?1421912272",
          "second_option_lable": "b",
          "second_option_text": null
        }
      }
    },
    {
      "pickle": {
        "pickle": {
          "pickle_deactive": false,
          "pickle_featured": false,
          "pickle_id": 2,
          "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
          "pickle_name": "which",
          "user_id": 3,
          "pickle_created_at": "2015-01-20T12:51:02Z"
        }
      },
      "option1": {
        "first_options": {
          "first_option_id": 3,
          "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/3/original/1421758221196.jpg?1421758261",
          "first_option_lable": "a",
          "first_option_text": null
        }
      },
      "option2": {
        "second_option": {
          "second_option_id": 4,
          "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/4/original/1421758229243.jpg?1421758262",
          "second_option_lable": "b",
          "second_option_text": null
        }
      }
    }
  ],
  "tagged_pickle_pickles": [
    {
      "pickle": {
        "pickle_deactive": false,
        "pickle_featured": false,
        "pickle_id": 13,
        "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
        "pickle_name": "bees",
        "user_id": 6,
        "pickle_created_at": "2015-01-22T06:20:02Z"
      },
      "first_options": {
        "first_option_id": 25,
        "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/25/original/stringio.txt?1421907602",
        "first_option_lable": "A",
        "first_option_text": ""
      },
      "second_option": {
        "second_option_id": 26,
        "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/26/original/open-uri20150122-4382-1tnow7l?1421907607",
        "second_option_lable": "B",
        "second_option_text": ""
      }
    },
    {
      "pickle": {
        "pickle_deactive": false,
        "pickle_featured": false,
        "pickle_id": 27,
        "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
        "pickle_name": "Text1 or Text2",
        "user_id": 8,
        "pickle_created_at": "2015-01-22T13:37:53Z"
      },
      "first_options": {
        "first_option_id": 49,
        "first_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
        "first_option_lable": "",
        "first_option_text": "Text1"
      },
      "second_option": {
        "second_option_id": 50,
        "second_option_image_url": "http://pickle.tingworks.in/assets/missing.jpg",
        "second_option_lable": "",
        "second_option_text": "Text2"
      }
    },
    {
      "pickle": {
        "pickle_deactive": false,
        "pickle_featured": false,
        "pickle_id": 29,
        "pickle_image": "http://pickle.tingworks.in/assets/missing.jpg",
        "pickle_name": "choose one",
        "user_id": 1,
        "pickle_created_at": "2015-01-23T07:20:32Z"
      },
      "first_options": {
        "first_option_id": 53,
        "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/53/original/IMG_20150113_170059.jpg?1421997627",
        "first_option_lable": "a ",
        "first_option_text": null
      },
      "second_option": {
        "second_option_id": 54,
        "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/54/original/1421756964491.jpg?1421997632",
        "second_option_lable": "b",
        "second_option_text": null
      }
    },
    {
      "pickle": {
        "pickle_deactive": false,
        "pickle_featured": false,
        "pickle_id": 1190,
        "pickle_image": "http://s3.amazonaws.com/pickel-test/questions/1190/original/shared_picle_1190.jpg?1480666896",
        "pickle_name": "what's next",
        "user_id": 67,
        "pickle_created_at": "2016-12-02T07:07:30Z"
      },
      "first_options": {
        "first_option_id": 1938,
        "first_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1938/original/Beautiful-Eiffel-Tower-Wallpapers.jpg?1480662448",
        "first_option_lable": "Eiffel Tower",
        "first_option_text": null
      },
      "second_option": {
        "second_option_id": 1939,
        "second_option_image_url": "http://s3.amazonaws.com/pickel-test/option_images/1939/original/images.jpg?1480662450",
        "second_option_lable": "Sheldon's Mind Tricks",
        "second_option_text": null
      }
    }
  ]
}

*/