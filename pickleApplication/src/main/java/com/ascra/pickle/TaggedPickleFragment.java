package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.data.FriendSearchData;
import com.ascra.data.PeopleDataFirstOption;
import com.ascra.data.PeopleDataSecondOption;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class TaggedPickleFragment extends Fragment implements
		OnClickListener, OnItemClickListener {

	View view;
	RelativeLayout layoutAddUser, layoutDefault;
	ImageView firstOptionImage, secondOptionImage;
	TextView first_text, second_text, txtText;
	ListView listOfFirstOptionTaggedUser, listOfSecondOptiontaggedUser;
	Button buttonDone;

	String PICKLE_ID, FIRST_OPTION_IMAGE, SECOND_OPTION_IMAGE, FIRST_OPTION_ID,
			SECOND_OPTION_ID, USER_ID, FIRST_TEXT, SECOND_TEXT;

	ImageLoader imageLoader;

	Dialog dialog;
	EditText etdSearch;
	Button btnClear;
	ListView listOfSearchData;
	TextView txtNoResult;
	ProgressBar inProcess;
	ImageView cancelImage;
	Button btnDone;

	private String URL_FEED = null;
	private String TAGGED_PEOPLE_URL = null;
	private String TAG_URL = null;
	private String DELETE_TAG_URL = null;
	String searchText;
	JSONArray jsonFriendSearchArray;
	FriendSearchAdapter friendSearchAdapter;
	ArrayList<FriendSearchData> mAllData = new ArrayList<FriendSearchData>();
	String where;
	public ViewHolder holder;
	public MyViewHolder myHolder;

	ArrayList<PeopleDataFirstOption> mPeopleData1 = new ArrayList<PeopleDataFirstOption>();
	ArrayList<PeopleDataSecondOption> mPeopleData2 = new ArrayList<PeopleDataSecondOption>();

	CustomeAdapterForFirstOPtion customeAdapterForFirstOPtion;
	CustomeAdapterForSecondOption customeAdapterForSecondOption;
	ProgressDialog ringProgressDialog, rpd;

	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	Tracker tracker;

	Typeface face;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.tagged_fragment, null);
		HomeScreen.whichFragment = "TAGGING";

		Bundle bundle = this.getArguments();
		PICKLE_ID = bundle.getString("PICKLE_ID");
		FIRST_OPTION_IMAGE = bundle.getString("FIRST_OPTION_IMAGE");
		SECOND_OPTION_IMAGE = bundle.getString("SECOND_OPTION_IMAGE");
		FIRST_OPTION_ID = bundle.getString("FIRST_OPTION_ID");
		SECOND_OPTION_ID = bundle.getString("SECOND_OPTION_ID");
		USER_ID = bundle.getString("USER_ID");
		FIRST_TEXT = bundle.getString("FIRST_TEXT");
		SECOND_TEXT = bundle.getString("SECOND_TEXT");

		TAGGED_PEOPLE_URL = Constant.PickleUrl + "tags/" + PICKLE_ID + ".json";
		rpd = ProgressDialog.show(getActivity(), "", "Loading tags ...", true);
		rpd.setCancelable(true);

		cd = new ConnectionDetector(getActivity());
		isInternetPresent = cd.isConnectingToInternet();

		if (isInternetPresent) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						new loadingAlreadyTaggedPeople().execute();
					} catch (Exception e) {
						e.printStackTrace();
					}

				}
			}).start();
		} else {
			String text = "No Internet Connection!";
			showDeleteDialog(text);
		}

		URL_FEED = Constant.PickleUrl + "users/search_all_users.json";

		TAG_URL = Constant.PickleUrl + "tags.json";

		face = Typeface.createFromAsset(getActivity().getAssets(),
				"fonts/sniglet-regular.ttf");

		setUpViews();
		setTracker();

		return view;
	}

	private void setTracker() {
		tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
				"UA-57240357-1");
		if (tracker != null) {
			tracker.set(Fields.SCREEN_NAME, "TAGGED PICKLE SCREEN");
			tracker.send(MapBuilder.createAppView().build());
		}
	}

	private void setUpViews() {

		imageLoader = new ImageLoader(getActivity().getApplicationContext());

		layoutAddUser = (RelativeLayout) view.findViewById(R.id.top);
		layoutDefault = (RelativeLayout) view.findViewById(R.id.taggingLayout);
		firstOptionImage = (ImageView) view.findViewById(R.id.firstImage);
		secondOptionImage = (ImageView) view.findViewById(R.id.secondImage);
		first_text = (TextView) view.findViewById(R.id.first_text);
		second_text = (TextView) view.findViewById(R.id.second_text);
		txtText = (TextView) view.findViewById(R.id.txtText);
		listOfFirstOptionTaggedUser = (ListView) view
				.findViewById(R.id.firstTagList);
		listOfSecondOptiontaggedUser = (ListView) view
				.findViewById(R.id.secondTagList);
		buttonDone = (Button) view.findViewById(R.id.done);

		if (FIRST_OPTION_IMAGE != null) {
			if (FIRST_OPTION_IMAGE
					.equals("http://54.251.33.194:8800/assets/missing.jpg")) {
				firstOptionImage.setVisibility(View.GONE);
			} else {
				firstOptionImage.setVisibility(View.VISIBLE);
				imageLoader.DisplayImage(FIRST_OPTION_IMAGE, firstOptionImage);
			}
		}

		if (SECOND_OPTION_IMAGE != null) {
			if (SECOND_OPTION_IMAGE
					.equals("http://54.251.33.194:8800/assets/missing.jpg")) {
				secondOptionImage.setVisibility(View.GONE);
			} else {
				secondOptionImage.setVisibility(View.VISIBLE);
				imageLoader
						.DisplayImage(SECOND_OPTION_IMAGE, secondOptionImage);
			}
		}

		if (FIRST_TEXT != null) {
			if (FIRST_TEXT.equals("") || FIRST_TEXT.equals("null")) {
				first_text.setVisibility(View.GONE);
			} else {
				first_text.setVisibility(View.VISIBLE);
				first_text.setText(FIRST_TEXT);
			}
		}

		if (SECOND_TEXT != null) {
			if (SECOND_TEXT.equals("") || SECOND_TEXT.equals("null")) {
				second_text.setVisibility(View.GONE);
			} else {
				second_text.setVisibility(View.VISIBLE);
				second_text.setText(SECOND_TEXT);
			}
		}

		first_text.setTypeface(face);
		second_text.setTypeface(face);
		txtText.setTypeface(face);
		buttonDone.setTypeface(face);

		firstOptionImage.setOnClickListener(this);
		secondOptionImage.setOnClickListener(this);
		first_text.setOnClickListener(this);
		second_text.setOnClickListener(this);
		buttonDone.setOnClickListener(this);

	}

	private class loadingAlreadyTaggedPeople extends
			AsyncTask<String, Void, String> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected String doInBackground(String... v) {
			ServiceHandler sh = new ServiceHandler();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("authentication_token",
					HomeScreen.authentication_token));
			params.add(new BasicNameValuePair("key", HomeScreen.key));

			String jsonStr = sh.makeServiceCall(TAGGED_PEOPLE_URL,
					ServiceHandler.GET, params);

			try {
				final JSONObject mainObject = new JSONObject(jsonStr);

				getActivity().runOnUiThread(new Runnable() {

					@Override
					public void run() {
						parseJSONAlreadyTaggedPeople(mainObject);
					}

				});

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			rpd.dismiss();
		}

	}

	private void parseJSONAlreadyTaggedPeople(JSONObject mainObject) {
		try {
			JSONObject objTaggedPeople = mainObject
					.getJSONObject("tagged_people");
			JSONArray arrayOfFirstOption = objTaggedPeople
					.getJSONArray("first_option");
			for (int noOfTagsFirstOption = 0; noOfTagsFirstOption < arrayOfFirstOption
					.length(); noOfTagsFirstOption++) {
				JSONObject objFirstOption = arrayOfFirstOption
						.getJSONObject(noOfTagsFirstOption);
				String creator_id = objFirstOption.getString("creator_id");
				String tagged_record_id = objFirstOption
						.getString("tagged_record_id");
				String option_id = objFirstOption.getString("option_id");
				String pickle_id = objFirstOption.getString("pickle_id");
				String tagged_user_name = objFirstOption
						.getString("tagged_user_name");
				String left = objFirstOption.getString("left");
				String top = objFirstOption.getString("top");

				PeopleDataFirstOption pData1 = new PeopleDataFirstOption(
						tagged_record_id, tagged_user_name, "", "", pickle_id,
						"", option_id, "", "");
				mPeopleData1.add(pData1);

				customeAdapterForFirstOPtion = new CustomeAdapterForFirstOPtion(
						mPeopleData1);
				listOfFirstOptionTaggedUser
						.setAdapter(customeAdapterForFirstOPtion);
				customeAdapterForFirstOPtion.notifyDataSetChanged();

			}

			JSONArray arrayOfSecondOption = objTaggedPeople
					.getJSONArray("second_option");
			for (int noOfTagsSecondOption = 0; noOfTagsSecondOption < arrayOfSecondOption
					.length(); noOfTagsSecondOption++) {
				JSONObject objSecondOption = arrayOfSecondOption
						.getJSONObject(noOfTagsSecondOption);
				String creator_id = objSecondOption.getString("creator_id");
				String tagged_record_id = objSecondOption
						.getString("tagged_record_id");
				String option_id = objSecondOption.getString("option_id");
				String pickle_id = objSecondOption.getString("pickle_id");
				String tagged_user_name = objSecondOption
						.getString("tagged_user_name");
				String left = objSecondOption.getString("left");
				String top = objSecondOption.getString("top");

				PeopleDataSecondOption pData2 = new PeopleDataSecondOption(
						tagged_record_id, tagged_user_name, "", "", pickle_id,
						"", option_id, "", "");
				mPeopleData2.add(pData2);

				customeAdapterForSecondOption = new CustomeAdapterForSecondOption(
						mPeopleData2);
				listOfSecondOptiontaggedUser
						.setAdapter(customeAdapterForSecondOption);
				customeAdapterForSecondOption.notifyDataSetChanged();

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.firstImage:
			where = "FIRST";
			showSearchFriendDialog(where);
			break;
		case R.id.secondImage:
			where = "SECOND";
			showSearchFriendDialog(where);
			break;
		case R.id.first_text:
			where = "FIRST";
			showSearchFriendDialog(where);
			break;
		case R.id.second_text:
			where = "SECOND";
			showSearchFriendDialog(where);
			break;
		case R.id.done:
			getFragmentManager().popBackStack();
			break;
		case R.id.btnClear:
			etdSearch.setText("");
			listOfSearchData.setVisibility(View.GONE);
			break;

		case R.id.cancelDialog:
			dialog.dismiss();
			break;
		default:
			break;
		}
	}

	private void showSearchFriendDialog(String where) {

		dialog = new Dialog(getActivity());
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.adding_people_dialog);
		dialog.setCanceledOnTouchOutside(false);

		etdSearch = (EditText) dialog.findViewById(R.id.edSearch);
		btnClear = (Button) dialog.findViewById(R.id.btnClear);
		listOfSearchData = (ListView) dialog.findViewById(R.id.mListView);
		txtNoResult = (TextView) dialog.findViewById(R.id.textview_list);
		inProcess = (ProgressBar) dialog.findViewById(R.id.search_ProgressBar);
		cancelImage = (ImageView) dialog.findViewById(R.id.cancelDialog);

		cancelImage.setOnClickListener(this);
		btnClear.setOnClickListener(this);

		etdSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				searchText = "";
				if (0 != etdSearch.getText().length()) {
					searchText = etdSearch.getText().toString();
					new GetFriendSearchData().execute();
				}
			}
		});

		dialog.show();

	}

	private class GetFriendSearchData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			inProcess.setVisibility(View.VISIBLE);
		}

		@Override
		protected Void doInBackground(Void... v) {
			ServiceHandler sh = new ServiceHandler();
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("q", searchText));
			params.add(new BasicNameValuePair("authentication_token",
					HomeScreen.authentication_token));
			params.add(new BasicNameValuePair("key", HomeScreen.key));
			String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET,
					params);
			Log.e("TaggedPickleFragment","Response : " + jsonStr);

			if (jsonStr != null) {
				try {
					jsonFriendSearchArray = new JSONArray(jsonStr);
					getActivity().runOnUiThread(new Runnable() {
						public void run() {
							parseJsonFeed(jsonFriendSearchArray);
						}
					});

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
			} else {
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try {
				setSearchResult(searchText);
				inProcess.setVisibility(View.GONE);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

	}

	public void parseJsonFeed(JSONArray array) {

		try {

			inProcess.setVisibility(View.GONE);
			mAllData.clear();
			if (array.length() == 0) {
				txtNoResult.setVisibility(View.VISIBLE);
			} else {
				txtNoResult.setVisibility(View.GONE);
				for (int i = 0; i < array.length(); i++) {
					JSONObject user = array.getJSONObject(i);

					String id = user.getString("id");
					String name = user.getString("name");
					String profilePic = user.getString("image");
					String userName = user.getString("username");
					String email = user.getString("email");
					String status = user.getString("status");

					FriendSearchData sm = new FriendSearchData(id, name,
							profilePic, userName, email, status);
					friendSearchAdapter.addItem(sm);
					mAllData.add(sm);
					listOfSearchData.setAdapter(friendSearchAdapter);
					friendSearchAdapter.notifyDataSetChanged();

				}

				listOfSearchData.setOnItemClickListener(this);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
	}

	public void setSearchResult(String str) {
		friendSearchAdapter = new FriendSearchAdapter(getActivity());

		for (FriendSearchData friendSearchData : mAllData) {
			if (friendSearchData.getName().toLowerCase()
					.contains(str.toLowerCase())) {
				friendSearchAdapter.addItem(friendSearchData);
			}
		}
		listOfSearchData.setAdapter(friendSearchAdapter);
	}

	public class FriendSearchAdapter extends BaseAdapter {

		private ArrayList<FriendSearchData> myListItems = new ArrayList<FriendSearchData>();
		private LayoutInflater myLayoutInflater;
		ImageLoader imageLoader;

		public FriendSearchAdapter(Activity activity) {
			myLayoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			imageLoader = new ImageLoader(activity.getApplicationContext());
		}

		public void addItem(FriendSearchData item) {
			myListItems.add(item);
			notifyDataSetChanged();
		}

		public int getCount() {
			return myListItems.size();
		}

		public FriendSearchData getItem(int position) {
			return myListItems.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View view, ViewGroup viewGroup) {
			ViewHolder holder;

			if (view == null) {
				holder = new ViewHolder();

				view = myLayoutInflater.inflate(R.layout.friend_search_item,
						null);
				holder.itemName = (TextView) view.findViewById(R.id.text);
				holder.profile_pic = (ImageView) view
						.findViewById(R.id.profile_pic);

				view.setTag(holder);
			} else {

				holder = (ViewHolder) view.getTag();
			}

			FriendSearchData stringItem = myListItems.get(position);
			if (stringItem != null) {
				if (holder.itemName != null) {
					holder.itemName.setText(stringItem.getName());

					imageLoader.DisplayImage(stringItem.getPic(),
							holder.profile_pic);
				}
			}

			return view;
		}

	}

	private class ViewHolder {

		TextView itemName;
		ImageView profile_pic;

	}

	private class MyViewHolder {

		TextView peopleName;
		ImageView deletePeople;

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view,
			final int position, long id) {

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

		final String TOP = "";
		final String LEFT = "";

		if (where.equals("FIRST")) {
			createTaggedPeople1(mAllData.get(position).getId(), PICKLE_ID,
					USER_ID, FIRST_OPTION_ID, TOP, LEFT,
					HomeScreen.authentication_token, HomeScreen.key);
		} else {
			createTaggedPeople2(mAllData.get(position).getId(), PICKLE_ID,
					USER_ID, SECOND_OPTION_ID, TOP, LEFT,
					HomeScreen.authentication_token, HomeScreen.key);
		}

	}

	private void createTaggedPeople1(String tagged_user_id, String question_id,
			String creator_id, String option_id, String top, String left,
			String authentication_token, String key) {

		class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

			@Override
			protected String doInBackground(String... params) {

				String paramTaggedUserId = params[0];
				String paramQuestionId = params[1];
				String paramCreatorId = params[2];
				String paramOptionId = params[3];
				String paramTop = params[4];
				String paramLeft = params[5];
				String paramAuthentication = params[6];
				String paramKey = params[7];

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(TAG_URL);

				BasicNameValuePair taggedUserIdBasicNameValuePair = new BasicNameValuePair(
						"tag[tagged_user_id]", paramTaggedUserId);
				BasicNameValuePair questionIdBasicNameValuePair = new BasicNameValuePair(
						"tag[question_id]", paramQuestionId);
				BasicNameValuePair creatorIdBasicNameValuePair = new BasicNameValuePair(
						"tag[creator_id]", paramCreatorId);
				BasicNameValuePair optionIdBasicNameValuePair = new BasicNameValuePair(
						"tag[option_id]", paramOptionId);
				BasicNameValuePair topBasicNameValuePair = new BasicNameValuePair(
						"tag[top]", paramTop);
				BasicNameValuePair leftBasicNameValuePair = new BasicNameValuePair(
						"tag[left]", paramLeft);
				BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair(
						"authentication_token", paramAuthentication);
				BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair(
						"key", paramKey);

				List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
				nameValuePairList.add(taggedUserIdBasicNameValuePair);
				nameValuePairList.add(questionIdBasicNameValuePair);
				nameValuePairList.add(creatorIdBasicNameValuePair);
				nameValuePairList.add(optionIdBasicNameValuePair);
				nameValuePairList.add(topBasicNameValuePair);
				nameValuePairList.add(leftBasicNameValuePair);
				nameValuePairList.add(authTokenBasicNameValuePair);
				nameValuePairList.add(keyBasicNameValuePair);

				try {
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
							nameValuePairList);

					httpPost.setEntity(urlEncodedFormEntity);

					try {
						HttpResponse httpResponse = httpClient
								.execute(httpPost);
						InputStream inputStream = httpResponse.getEntity()
								.getContent();
						InputStreamReader inputStreamReader = new InputStreamReader(
								inputStream);
						BufferedReader bufferedReader = new BufferedReader(
								inputStreamReader);
						StringBuilder stringBuilder = new StringBuilder();
						String bufferedStrChunk = null;

						while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
							stringBuilder.append(bufferedStrChunk);
						}
						return stringBuilder.toString();

					} catch (ClientProtocolException cpe) {
						cpe.printStackTrace();
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}

				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					JSONObject objResponse = new JSONObject(result);
					String tag_record_id = objResponse
							.getString("tag_record_id");
					String tag_user_name = objResponse
							.getString("tagged_user_user_name");
					String tagged_user_id = objResponse
							.getString("tagged_user_id");
					String tagged_user_email = objResponse
							.getString("tagged_user_email");
					String pickle_id = objResponse.getString("pickle_id");
					String pickle_name = objResponse.getString("pickle_name");
					String option_id = objResponse.getString("option_id");
					String option_image = objResponse.getString("option_image");
					String option_label = objResponse.getString("option_label");

					PeopleDataFirstOption pData1 = new PeopleDataFirstOption(
							tag_record_id, tag_user_name, tagged_user_id,
							tagged_user_email, pickle_id, pickle_name,
							option_id, option_image, option_label);
					mPeopleData1.add(pData1);

					customeAdapterForFirstOPtion = new CustomeAdapterForFirstOPtion(
							mPeopleData1);
					listOfFirstOptionTaggedUser
							.setAdapter(customeAdapterForFirstOPtion);
					customeAdapterForFirstOPtion.notifyDataSetChanged();

					dialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
		sendPostReqAsyncTask.execute(tagged_user_id, question_id, creator_id,
				option_id, top, left, authentication_token, key);

	}

	private void createTaggedPeople2(String tagged_user_id, String question_id,
			String creator_id, String option_id, String top, String left,
			String authentication_token, String key) {

		class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

			@Override
			protected String doInBackground(String... params) {

				String paramTaggedUserId = params[0];
				String paramQuestionId = params[1];
				String paramCreatorId = params[2];
				String paramOptionId = params[3];
				String paramTop = params[4];
				String paramLeft = params[5];
				String paramAuthentication = params[6];
				String paramKey = params[7];

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(TAG_URL);

				BasicNameValuePair taggedUserIdBasicNameValuePair = new BasicNameValuePair(
						"tag[tagged_user_id]", paramTaggedUserId);
				BasicNameValuePair questionIdBasicNameValuePair = new BasicNameValuePair(
						"tag[question_id]", paramQuestionId);
				BasicNameValuePair creatorIdBasicNameValuePair = new BasicNameValuePair(
						"tag[creator_id]", paramCreatorId);
				BasicNameValuePair optionIdBasicNameValuePair = new BasicNameValuePair(
						"tag[option_id]", paramOptionId);
				BasicNameValuePair topBasicNameValuePair = new BasicNameValuePair(
						"tag[top]", paramTop);
				BasicNameValuePair leftBasicNameValuePair = new BasicNameValuePair(
						"tag[left]", paramLeft);
				BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair(
						"authentication_token", paramAuthentication);
				BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair(
						"key", paramKey);

				List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
				nameValuePairList.add(taggedUserIdBasicNameValuePair);
				nameValuePairList.add(questionIdBasicNameValuePair);
				nameValuePairList.add(creatorIdBasicNameValuePair);
				nameValuePairList.add(optionIdBasicNameValuePair);
				nameValuePairList.add(topBasicNameValuePair);
				nameValuePairList.add(leftBasicNameValuePair);
				nameValuePairList.add(authTokenBasicNameValuePair);
				nameValuePairList.add(keyBasicNameValuePair);

				try {
					UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
							nameValuePairList);

					httpPost.setEntity(urlEncodedFormEntity);

					try {
						HttpResponse httpResponse = httpClient
								.execute(httpPost);

						InputStream inputStream = httpResponse.getEntity()
								.getContent();
						InputStreamReader inputStreamReader = new InputStreamReader(
								inputStream);
						BufferedReader bufferedReader = new BufferedReader(
								inputStreamReader);
						StringBuilder stringBuilder = new StringBuilder();
						String bufferedStrChunk = null;

						while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
							stringBuilder.append(bufferedStrChunk);
						}
						return stringBuilder.toString();

					} catch (ClientProtocolException cpe) {
						cpe.printStackTrace();
					} catch (IOException ioe) {
						ioe.printStackTrace();
					}

				} catch (UnsupportedEncodingException uee) {
					uee.printStackTrace();
				}

				return null;
			}

			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				try {
					JSONObject objResponse = new JSONObject(result);
					String tag_record_id = objResponse
							.getString("tag_record_id");
					String tag_user_name = objResponse
							.getString("tagged_user_user_name");
					String tagged_user_id = objResponse
							.getString("tagged_user_id");
					String tagged_user_email = objResponse
							.getString("tagged_user_email");
					String pickle_id = objResponse.getString("pickle_id");
					String pickle_name = objResponse.getString("pickle_name");
					String option_id = objResponse.getString("option_id");
					String option_image = objResponse.getString("option_image");
					String option_label = objResponse.getString("option_label");

					PeopleDataSecondOption pData2 = new PeopleDataSecondOption(
							tag_record_id, tag_user_name, tagged_user_id,
							tagged_user_email, pickle_id, pickle_name,
							option_id, option_image, option_label);
					mPeopleData2.add(pData2);

					customeAdapterForSecondOption = new CustomeAdapterForSecondOption(
							mPeopleData2);
					listOfSecondOptiontaggedUser
							.setAdapter(customeAdapterForSecondOption);
					customeAdapterForSecondOption.notifyDataSetChanged();

					dialog.dismiss();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
		sendPostReqAsyncTask.execute(tagged_user_id, question_id, creator_id,
				option_id, top, left, authentication_token, key);

	}

	public class CustomeAdapterForFirstOPtion extends BaseAdapter {
		LayoutInflater inflater;
		ArrayList<PeopleDataFirstOption> pData1 = new ArrayList<PeopleDataFirstOption>();

		public CustomeAdapterForFirstOPtion(
				ArrayList<PeopleDataFirstOption> mPeopleData1) {
			this.pData1 = mPeopleData1;
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return pData1.size();
		}

		@Override
		public Object getItem(int position) {
			return pData1.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			MyViewHolder myHolder;
			if (view == null) {
				myHolder = new MyViewHolder();

				view = inflater.inflate(R.layout.tagged_people_item, null);
				//
				myHolder.peopleName = (TextView) view
						.findViewById(R.id.people_name);

				myHolder.peopleName.setTypeface(face);

				if (pData1.get(position).getTagged_user_name() != null) {

					if (pData1.get(position).getTagged_user_name()
							.contains("-")) {
						String[] parts = pData1.get(position)
								.getTagged_user_name().split("-");
						String part1 = parts[0];
						String part2 = parts[1];

						myHolder.peopleName.setText(part1 + " " + part2);

					} else {
						myHolder.peopleName.setText(pData1.get(position)
								.getTagged_user_name());
					}
				}

				myHolder.deletePeople = (ImageView) view
						.findViewById(R.id.delete);
				myHolder.deletePeople.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						DELETE_TAG_URL = Constant.PickleUrl + "tags/"
								+ pData1.get(position).getTags_id() + ".json";

						new DeleteTag1().execute();

						mPeopleData1.remove(position);
						customeAdapterForSecondOption.notifyDataSetChanged();
					}
				});

				view.setTag(holder);
			} else {
				myHolder = (MyViewHolder) view.getTag();
			}

			return view;
		}

		private class DeleteTag1 extends AsyncTask<Void, Void, Void> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... v) {
				// Creating service handler class instance
				ServiceHandler sh = new ServiceHandler();

				List<NameValuePair> params = new ArrayList<NameValuePair>();

				try {
					params.add(new BasicNameValuePair("authentication_token",
							HomeScreen.authentication_token));
					params.add(new BasicNameValuePair("key", HomeScreen.key));

				} catch (Exception e) {
					e.printStackTrace();
				}
				// Making a request to url and getting response
				String jsonStr = sh.makeServiceCall(DELETE_TAG_URL,
						ServiceHandler.DELETE, params);

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);

			}

		}

	}

	public class CustomeAdapterForSecondOption extends BaseAdapter {
		LayoutInflater inflater;
		ArrayList<PeopleDataSecondOption> pData2 = new ArrayList<PeopleDataSecondOption>();

		public CustomeAdapterForSecondOption(
				ArrayList<PeopleDataSecondOption> mPeopleData2) {
			this.pData2 = mPeopleData2;
			inflater = LayoutInflater.from(getActivity());
		}

		@Override
		public int getCount() {
			return pData2.size();
		}

		@Override
		public Object getItem(int position) {
			return pData2.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			MyViewHolder myHolder;
			if (view == null) {
				myHolder = new MyViewHolder();

				view = inflater.inflate(R.layout.tagged_people_item, null);
				//
				myHolder.peopleName = (TextView) view
						.findViewById(R.id.people_name);

				myHolder.peopleName.setTypeface(face);

				if (pData2.get(position).getTagged_user_name() != null) {

					if (pData2.get(position).getTagged_user_name()
							.contains("-")) {
						String[] parts = pData2.get(position)
								.getTagged_user_name().split("-");
						String part1 = parts[0];
						String part2 = parts[1];

						myHolder.peopleName.setText(part1 + " " + part2);

					} else {
						myHolder.peopleName.setText(pData2.get(position)
								.getTagged_user_name());
					}

				}

				myHolder.deletePeople = (ImageView) view
						.findViewById(R.id.delete);
				myHolder.deletePeople.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						DELETE_TAG_URL = Constant.PickleUrl + "tags/"
								+ pData2.get(position).getTags_id() + ".json";

						new DeleteTag2().execute();

						mPeopleData2.remove(position);
						customeAdapterForSecondOption.notifyDataSetChanged();
					}
				});

				view.setTag(holder);
			} else {
				myHolder = (MyViewHolder) view.getTag();
			}

			return view;
		}

		private class DeleteTag2 extends AsyncTask<Void, Void, Void> {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Void doInBackground(Void... v) {
				// Creating service handler class instance
				ServiceHandler sh = new ServiceHandler();

				List<NameValuePair> params = new ArrayList<NameValuePair>();

				try {
					params.add(new BasicNameValuePair("authentication_token",
							HomeScreen.authentication_token));
					params.add(new BasicNameValuePair("key", HomeScreen.key));

				} catch (Exception e) {
					e.printStackTrace();
				}
				// Making a request to url and getting response
				String jsonStr = sh.makeServiceCall(DELETE_TAG_URL,
						ServiceHandler.DELETE, params);

				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);

			}

		}

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void showDeleteDialog(String text) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setCancelable(true);

		TextView msg = new TextView(getActivity());
		msg.setText(text);
		msg.setPadding(10, 35, 10, 10);
		msg.setGravity(Gravity.CENTER);
		msg.setTextSize(15);

		InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

		builder.setView(msg);

		builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog_interface, int which) {

			}
		});

		AlertDialog alert = builder.create();

		alert.show();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(getActivity()).activityStart(getActivity()); 
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(getActivity()).activityStop(getActivity()); 
	}

}
