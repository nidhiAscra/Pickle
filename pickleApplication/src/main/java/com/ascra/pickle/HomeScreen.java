package com.ascra.pickle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.SessionManager;

import java.util.HashMap;

public class HomeScreen extends AppCompatActivity implements
        OnItemClickListener, OnClickListener {

    ImageView menuButton, pickleBundle,title_text;
    DrawerLayout dLayout;
    ListView dList;
    String[] allOption = {"a", "b", "c", "d", "e", "f"};
    boolean isOpen = false;
    private android.support.v4.app.Fragment mContent;
    static String whichFragment = null;
    Bundle intent;
    static String authentication_token;
    static String key;
    static String user_id;
    static String user_name;

    static String session_name;
    static String session_email;

    SessionManager session;

    ImageView btnPickleFeeds, btnFeaturedPickle, btnCreatePickle,
            btnPickleNotification, btnPickleProfile, btnBundlePickleOne,
            btnBundlePickleTwo;

    SharedPreferences sp;

    String whichWayLogin;
    LinearLayout bottom_strip;
    Bundle savedInstanceState;

    String messageText;
    Typeface face;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pickle_home_screen);

//        HardwareInfo.print(HomeScreen.this);

        face = Typeface.createFromAsset(getAssets(),
                "fonts/sniglet-regular.ttf");

        setUpViews();

        this.savedInstanceState = savedInstanceState;

        session = new SessionManager(getApplicationContext());
        session.checkLogin();
        HashMap<String, String> user = session.getUserDetails();
        session_name = user.get(SessionManager.KEY_NAME);
        session_email = user.get(SessionManager.KEY_EMAIL);

        authentication_token = SaveUtils.getAuthKeyFromPref(getResources().getString(R.string.PreferenceFileName), this, SaveUtils.AUTH, null);
        key = SaveUtils.getKeyFromPref(getResources().getString(R.string.PreferenceFileName), this, SaveUtils.KEY, null);
        user_name = SaveUtils.getUserFromPref(getResources().getString(R.string.PreferenceFileName), this, SaveUtils.USER, null);

        user_id = SaveUtils.getUserID(getResources().getString(R.string.PreferenceFileName), this, SaveUtils.USER_ID, null);
        if (savedInstanceState != null)
            mContent = getSupportFragmentManager().getFragment(
                    savedInstanceState, "mContent");

        if (mContent == null) {
            mContent = new FeedsFragment();
            whichFragment = "PICKLEFEEDS";
            setFeedsIcon();
        }
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();

    }

    //@Override
//protected void onResume() {
//      HomeScreen.this.getActionBar().show();
//	super.onResume();
//}
    private void setUpViews() {
        ActionBar mActionBar = getSupportActionBar();

        LayoutInflater mInflater = LayoutInflater.from(this);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        View mCustomView = mInflater.inflate(R.layout.pickle_custom_actionbar,
                null);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
// @PK30NOV action bar color
        mActionBar.setBackgroundDrawable(new ColorDrawable(0xffF08080));

        menuButton = (ImageView) mCustomView.findViewById(R.id.homeButtonaction);
        pickleBundle = (ImageView) mCustomView.findViewById(R.id.pickleBundleacttion);
        title_text=(ImageView)mCustomView.findViewById(R.id.title_text);
        title_text.setOnClickListener(this);
        pickleBundle.setOnClickListener(this);
        menuButton.setOnClickListener(this);
        // searchButton.setOnClickListener(this);
        btnPickleFeeds = (ImageView) findViewById(R.id.pickleFeeds);
        btnFeaturedPickle = (ImageView) findViewById(R.id.featuredPickle);
        btnBundlePickleOne = (ImageView) findViewById(R.id.bundlePickle_one);
        btnCreatePickle = (ImageView) findViewById(R.id.createPickle);
        btnPickleNotification = (ImageView) findViewById(R.id.pickleNotification);
        btnPickleProfile = (ImageView) findViewById(R.id.pickleProfile);
        bottom_strip = (LinearLayout) findViewById(R.id.bottom_strip);
        whichWayLogin = SaveUtils.getUserLoginFrom(
                getResources().getString(R.string.PreferenceFileName), this,
                "WHICH_LOGIN", null);

        Log.e("whichWayLogin", whichWayLogin);

        if (whichWayLogin.equals("BRAND")) {
            btnFeaturedPickle.setVisibility(View.GONE);
            btnBundlePickleOne.setVisibility(View.VISIBLE);
            //	pickleBundle.setVisibility(View.GONE);
        } else {
            btnFeaturedPickle.setVisibility(View.VISIBLE);
            btnBundlePickleOne.setVisibility(View.GONE);
            //	pickleBundle.setVisibility(View.VISIBLE);
        }

        btnPickleFeeds.setOnClickListener(this);
        btnFeaturedPickle.setOnClickListener(this);
        btnBundlePickleOne.setOnClickListener(this);
        btnCreatePickle.setOnClickListener(this);
        btnPickleNotification.setOnClickListener(this);
        btnPickleProfile.setOnClickListener(this);

        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        dList.setItemsCanFocus(true);
        dList.setAdapter(new AllDrawerMenus());

        dList.setOnItemClickListener(this);
        this.enable_bottom_strip();
    }

    public void switchContent(android.support.v4.app.Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment).commit();
    }

    @Override
    public void onBackPressed() {

        if (whichFragment.equalsIgnoreCase("PICKLEFEEDS")) {
            showExitDialog();
        } else {
            super.onBackPressed();
            if (whichFragment.equalsIgnoreCase("PICKLEFEEDS")) {

            }
        }
    }

    class AllDrawerMenus extends BaseAdapter {

        private static final int FIRST_INDEX = 0;
        private static final int OTHER_INDEX = 1;

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return FIRST_INDEX;
            } else {

                return OTHER_INDEX;
            }
        }

        @Override
        public int getCount() {
            return allOption.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {

                LayoutInflater inflator = HomeScreen.this.getLayoutInflater();
                if (getItemViewType(position) == FIRST_INDEX) {
                    convertView = inflator.inflate(
                            R.layout.pickle_searchfriend_layout, null);
                } else if (getItemViewType(position) == OTHER_INDEX) {
                    convertView = inflator.inflate(R.layout.pickle_drawer_item,
                            null);
                }

                holder = new ViewHolder();
                if (getItemViewType(position) == FIRST_INDEX) {
                    holder.etdFriendSearch = (TextView) convertView
                            .findViewById(R.id.etdSearch);
                } else if (getItemViewType(position) == OTHER_INDEX) {
                    holder.txtDrawerItem = (TextView) convertView
                            .findViewById(R.id.txtDrawerItem);
                }

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (position == 0) {
                holder.etdFriendSearch.setTypeface(face);
                holder.etdFriendSearch.setText("Friend Search");
            }
            if (position == 1) {
                holder.txtDrawerItem.setTypeface(face);
                holder.txtDrawerItem.setText("Notification");
            } else if (position == 2) {
                holder.txtDrawerItem.setTypeface(face);
                holder.txtDrawerItem.setText("Report a Problem");
            } else if (position == 3) {
                holder.txtDrawerItem.setTypeface(face);
                holder.txtDrawerItem.setText("Help");
            } else if (position == 4) {
                holder.txtDrawerItem.setTypeface(face);
                holder.txtDrawerItem.setText("Privacy Policy");
            } else if (position == 5) {
                holder.txtDrawerItem.setTypeface(face);
                holder.txtDrawerItem.setText("Log Out");
            }
            return convertView;

        }

    }

    private class ViewHolder {
        public TextView txtDrawerItem;
        public TextView etdFriendSearch;
    }

    public void disable_bottom_strip() {
        bottom_strip.setVisibility(View.GONE);

    }

    public void enable_bottom_strip() {
        bottom_strip.setVisibility(View.VISIBLE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.homeButtonaction:
                if (isOpen == true) {
                    dLayout.closeDrawers();
                    isOpen = false;
                } else {
                    dLayout.openDrawer(Gravity.LEFT);
                    isOpen = true;
                }
                break;

            case R.id.pickleBundleacttion:
                BundlePickleFragment bundlePickleFragment = new BundlePickleFragment();
                FragmentTransaction bundlePickleTransaction = getSupportFragmentManager()
                        .beginTransaction();
                bundlePickleTransaction.replace(R.id.content_frame,
                        bundlePickleFragment);
                bundlePickleTransaction.addToBackStack(null);
                bundlePickleTransaction.commit();
                // setBundlePickleIcon();
                dLayout.closeDrawers();
                break;
            case R.id.pickleFeeds:
                FeedsFragment pickleFeedsFragment = new FeedsFragment();
                FragmentTransaction pickleFeedsTransaction = getSupportFragmentManager()
                        .beginTransaction();
                pickleFeedsTransaction.replace(R.id.content_frame,
                        pickleFeedsFragment);
                pickleFeedsTransaction.addToBackStack(null);
                pickleFeedsTransaction.commit();
                setFeedsIcon();
                dLayout.closeDrawers();
                break;
            case R.id.title_text:
                FeedsFragment pickleFeedsFragmentTitle = new FeedsFragment();
                FragmentTransaction pickleFeedsTransactionTitle = getSupportFragmentManager()
                        .beginTransaction();
                pickleFeedsTransactionTitle.replace(R.id.content_frame,
                        pickleFeedsFragmentTitle);
                pickleFeedsTransactionTitle.addToBackStack(null);
                pickleFeedsTransactionTitle.commit();
                setFeedsIcon();
                dLayout.closeDrawers();
                break;
            case R.id.featuredPickle:
                FeaturedPickleFragment featuredPickleFragment = new FeaturedPickleFragment();
                FragmentTransaction featuredPickleTransaction = getSupportFragmentManager()
                        .beginTransaction();
                featuredPickleTransaction.replace(R.id.content_frame,
                        featuredPickleFragment);
                featuredPickleTransaction.addToBackStack(null);
                featuredPickleTransaction.commit();
                setFeaturedPickleIcon();
                dLayout.closeDrawers();
                break;

            case R.id.bundlePickle_one:
                BundlePickleFragment bundlePickleFragmentOne = new BundlePickleFragment();
                FragmentTransaction bundlePickleTransactionOne = getSupportFragmentManager()
                        .beginTransaction();
                bundlePickleTransactionOne.replace(R.id.content_frame,
                        bundlePickleFragmentOne);
                bundlePickleTransactionOne.addToBackStack(null);
                bundlePickleTransactionOne.commit();
                setBundlePickleIcon();
                dLayout.closeDrawers();
                break;

            case R.id.createPickle:
//			startActivity(new Intent(HomeScreen.this,
//					CreatePickleFragmentActivity.class));
                CreatePickleFragmentActivity CreatePickleFragmentone = new CreatePickleFragmentActivity();
                FragmentTransaction CreatePickleTransaction = getSupportFragmentManager()
                        .beginTransaction();
                CreatePickleTransaction.replace(R.id.content_frame,
                        CreatePickleFragmentone);
                CreatePickleTransaction.addToBackStack(null);
                CreatePickleTransaction.commit();
                setCreatepickleicon();
                dLayout.closeDrawers();

                break;

            case R.id.pickleNotification:

                NotificationFragment notificationFragment = new NotificationFragment();
                FragmentTransaction notificationTransaction = getSupportFragmentManager().beginTransaction();
                notificationTransaction.replace(R.id.content_frame, notificationFragment);
                notificationTransaction.addToBackStack(null);
                notificationTransaction.commit();
                setNotificationIcon();
                dLayout.closeDrawers();

                break;

            case R.id.pickleProfile:

                UserProfileFragment userProfileFragment = new UserProfileFragment();
                Bundle args = new Bundle();
                args.putString("USER_NAME", HomeScreen.session_name);
                args.putString("FROM", "HOME");
                args.putString("GENERATED_ID", "");
                userProfileFragment.setArguments(args);
                FragmentTransaction userProfileTransaction = getSupportFragmentManager()
                        .beginTransaction();
                userProfileTransaction.replace(R.id.content_frame,
                        userProfileFragment);
                userProfileTransaction.addToBackStack(null);
                userProfileTransaction.commit();
                setProfileIcon();
                dLayout.closeDrawers();

                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        switch (position) {
            case 0:
                SearchFriendFragment searchFriendFragment = new SearchFriendFragment();
                FragmentTransaction searchFriendTransaction = getSupportFragmentManager()
                        .beginTransaction();
                searchFriendTransaction.replace(R.id.content_frame,
                        searchFriendFragment);
                searchFriendTransaction.addToBackStack(null);
                searchFriendTransaction.commit();
                dLayout.closeDrawers();
                break;
            case 1:
                NotificationFragment notificationFragment = new NotificationFragment();
                FragmentTransaction notificationTransaction = getSupportFragmentManager()
                        .beginTransaction();
                notificationTransaction.replace(R.id.content_frame,
                        notificationFragment);
                notificationTransaction.addToBackStack(null);
                notificationTransaction.commit();
                dLayout.closeDrawers();
                break;
            case 2:
                messageText = "Pending !";
                showErrorDialog(messageText);
                break;
            case 3:
                messageText = "Pending !";
                showErrorDialog(messageText);
                break;
            case 4:
                messageText = "Pending !";
                showErrorDialog(messageText);
                break;
            case 5:
                showLogOutDialog();
                break;

            default:
                break;
        }
    }

    private void showLogOutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);
        builder.setCancelable(true);

        TextView msg = new TextView(HomeScreen.this);
        // msg.setTypeface(face);
        msg.setText("Do you want to LogOut?");
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Splash.str_option = "3";

                session.logoutUser();
                SaveUtils.saveLoginSessionPref(Constant.LoginSession,
                        HomeScreen.this, "LOGIN", false);
                startActivity(new Intent(HomeScreen.this, MainActivity.class));
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);
        builder.setCancelable(true);

        TextView msg = new TextView(HomeScreen.this);
        // msg.setTypeface(face);
        msg.setText("Do you want to Exit?");
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showErrorDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeScreen.this);
        builder.setCancelable(true);

        TextView msg = new TextView(HomeScreen.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();

        alert.show();
    }

    private void setFeedsIcon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_color);
        btnFeaturedPickle.setImageResource(R.drawable.featured_black);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle_balck);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_black);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_black);
    }

    private void setBundlePickleIcon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_black);
        btnFeaturedPickle.setImageResource(R.drawable.featured_black);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_black);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_black);
    }

    private void setFeaturedPickleIcon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_black);
        btnFeaturedPickle.setImageResource(R.drawable.featured_color);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle_balck);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_black);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_black);
    }

    private void setNotificationIcon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_black);
        btnFeaturedPickle.setImageResource(R.drawable.featured_black);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle_balck);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_color);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_black);
    }

    //@PKDEC02 : Made public from private as will bec called from FeedsFragment Header Click
    public void setProfileIcon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_black);
        btnFeaturedPickle.setImageResource(R.drawable.featured_black);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle_balck);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_black);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_color);
    }

    private void setCreatepickleicon() {
        btnPickleFeeds.setImageResource(R.drawable.feed_black);
        btnFeaturedPickle.setImageResource(R.drawable.featured_black);
        btnBundlePickleOne.setImageResource(R.drawable.picklebundle_balck);
        btnCreatePickle.setImageResource(R.drawable.makepickle);
        btnPickleNotification.setImageResource(R.drawable.pickle_notification_black);
        btnPickleProfile.setImageResource(R.drawable.pickleprofile_black);
    }

    public void showBottomStrip(boolean val) {
        bottom_strip.setVisibility(val ? View.VISIBLE : View.GONE);
    }

    public void showActionBar(boolean val) {
        try {
            if (val) {
                getSupportActionBar().show();
            } else {
                getSupportActionBar().hide();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
