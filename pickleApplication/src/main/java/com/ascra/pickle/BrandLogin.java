package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.SessionManager;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.Tracker;

public class BrandLogin extends AppCompatActivity implements
        OnClickListener {

    EditText etdPutEmailId, etdPutPassword;
    RelativeLayout btnLogin;
    TextView txtSignUp, txtNewBrand, copyright, txtlogin;
    FrameLayout inProcess;
    Intent intent;
    String stringUserName, stringPassword;

    private String URL_FEED_ONE = Constant.PickleUrl + "tokens/get_key";
    private String URL_FEED_TWO = Constant.PickleUrl + "tokens";

    String authentication_token = null;
    String key = null;
    Integer user_id = 0;
    String user_name = null;

    //@PKDEC01 User/Brand Name : Used to show UserName when commenting in BundlePickles Comments
    String brandOrUserName = null;

    SessionManager session;

    Dialog ringProgressDialog;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Tracker tracker;

    Typeface face;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.pickle_login_layout);
        session = new SessionManager(getApplicationContext());

        cd = new ConnectionDetector(BrandLogin.this);
        isInternetPresent = cd.isConnectingToInternet();

        Splash.str_option = "2";

        face = Typeface.createFromAsset(getAssets(),
                "fonts/sniglet-regular.ttf");

        setUpViews();
        setTracker();

    }

    private void setUpViews() {
        etdPutEmailId = (EditText) findViewById(R.id.etdPutEmail);
        etdPutPassword = (EditText) findViewById(R.id.etdPutPassword);
        btnLogin = (RelativeLayout) findViewById(R.id.btnLogin);
        txtSignUp = (TextView) findViewById(R.id.txtSignUp);
        txtNewBrand = (TextView) findViewById(R.id.txtNewBrand);
        copyright = (TextView) findViewById(R.id.copyright);
        txtlogin = (TextView) findViewById(R.id.txtlogin);
        inProcess = (FrameLayout) findViewById(R.id.inProcess);
        btnLogin.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);

        txtlogin.setTypeface(face);
        txtSignUp.setTypeface(face);
        txtNewBrand.setTypeface(face);
        copyright.setTypeface(face);
        etdPutEmailId.setTypeface(face);
        etdPutPassword.setTypeface(face);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(BrandLogin.this).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "LOGIN SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:

                if (isInternetPresent) {

                    if (!isValid(etdPutEmailId.getText().toString().trim())) {

                        String text = "Please enter correct email id !";
                        showErrorDialog(text);

                    } else if (etdPutPassword.getText().toString().trim().length() < 8) {
                        String text = "Please enter correct password !";
                        showErrorDialog(text);
                    } else {
                        SaveUtils.saveUserNameToPref(
                                getResources().getString(
                                        R.string.PreferenceFileName),
                                BrandLogin.this, "USERNAME", etdPutEmailId
                                        .getText().toString().trim());
                        SaveUtils.savePasswordToPref(
                                getResources().getString(
                                        R.string.PreferenceFileName),
                                BrandLogin.this, "PASSWORD", etdPutPassword
                                        .getText().toString().trim());
                        ringProgressDialog=new Dialog(BrandLogin.this);
                        ringProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        ringProgressDialog.setContentView(R.layout.dialog_progress);
                        TextView msg = (TextView) ringProgressDialog.findViewById(R.id.msg);
                        msg.setText("Login in please wait....");
                        ringProgressDialog.setCanceledOnTouchOutside(false);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    userSignIn(etdPutEmailId.getText().toString()
                                            .trim(), etdPutPassword.getText()
                                            .toString().trim());

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }

                } else {
                    String text = "No Internet Connection!";
                    showNoFeedsDialog(text);
                }

                break;
            case R.id.txtSignUp:
                intent = new Intent(BrandLogin.this, BrandRegister.class);
                startActivity(intent);
                finish();
                break;
            default:
                break;
        }
    }

    private void userSignIn(String userEmail, String userPassword) {

        class SendPostReqAsyncTaskOne extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramUserEmail = params[0];
                String paramUserPassword = params[1];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FEED_ONE);

                BasicNameValuePair emailBasicNameValuePair = new BasicNameValuePair(
                        "email", paramUserEmail);
                BasicNameValuePair passwordBasicNameValuePair = new BasicNameValuePair(
                        "password", paramUserPassword);

                //@PK30NOV regId, deviceToken
                BasicNameValuePair regId = new BasicNameValuePair(
                        "registration_id", session.getRegID() == null
                        ? SaveUtils.getGcmRegistrationId(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, SaveUtils.GcmRegistraionId, "")
                        : session.getRegID());
                BasicNameValuePair deviceToken = new BasicNameValuePair(
                        "device_token", session.getGcmToken() == null
                        ? SaveUtils.getGcmDeviceToken(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, SaveUtils.GcmDeviceToken, "")
                        : session.getGcmToken());

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(emailBasicNameValuePair);
                nameValuePairList.add(passwordBasicNameValuePair);

                //@PK30NOV regId, deviceToken
                nameValuePairList.add(regId);
                nameValuePairList.add(deviceToken);
                for (NameValuePair a : nameValuePairList) {
                    Log.e("NameValue Login", "Pair : " + a.getName() + " = " + a.getValue());
                }

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                sendToken(result);

            }
        }
        SendPostReqAsyncTaskOne sendPostReqAsyncTask = new SendPostReqAsyncTaskOne();
        sendPostReqAsyncTask.execute(userEmail, userPassword);

    }

    private void sendToken(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);

            if (jsonObject.has("message")) {
                String message = jsonObject.getString("message");
                if (message.equals("Invalid email")) {
                    String text = "Please enter correct email id !";
                    showErrorDialog(text);
                } else {
                    String text = "Please enter correct password !";
                    showErrorDialog(text);
                }
                ringProgressDialog.dismiss();
            }

            String secretKey = jsonObject.getString("secret_key");
            key = jsonObject.getString("key");

            LoginViaToken(secretKey, key);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void LoginViaToken(final String secretKey, final String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramSecretKey = params[0];
                String paramKey = params[1];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FEED_TWO);

                BasicNameValuePair emailBasicNameValuePair = new BasicNameValuePair(
                        "secret_key", paramSecretKey);
                BasicNameValuePair passwordBasicNameValuePair = new BasicNameValuePair(
                        "key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(emailBasicNameValuePair);
                nameValuePairList.add(passwordBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);

                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                ringProgressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    authentication_token = jsonObject
                            .getString("authentication_token");
                    user_id = jsonObject.getInt("user_id");
                    user_name = jsonObject.getString("username");

                    //@PKDEC01
                    brandOrUserName = jsonObject.getString("name");

                    SaveUtils.saveUserID(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, SaveUtils.USER_ID, String.valueOf(user_id));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                session.createLoginSession(user_name, etdPutEmailId.getText().toString().trim());

                intent = new Intent(BrandLogin.this, HomeScreen.class);
                startActivity(intent);

                SaveUtils.saveAuthKeyToPref(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, "AUTH", authentication_token);
                SaveUtils.saveKeyToPref(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, SaveUtils.KEY, key);
                SaveUtils.saveUserToPref(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, "USER", user_name);

                SaveUtils.saveLoginSessionPref(Constant.LoginSession, BrandLogin.this, "LOGIN", true);

                SaveUtils.saveUserLoginFrom(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, "WHICH_LOGIN", "BRAND");

                //@PKDEC01
                SaveUtils.saveBrandOrUserName(getResources().getString(R.string.PreferenceFileName), BrandLogin.this, SaveUtils.BrandOrUserName, brandOrUserName);

                finish();
            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(secretKey, key);

    }

    @Override
    protected void onResume() {
        super.onResume();
        stringUserName = SaveUtils
                .getUserNameFromPref(
                        getResources().getString(R.string.PreferenceFileName),
                        this, "USERNAME", null);
        stringPassword = SaveUtils
                .getPasswordFromPref(
                        getResources().getString(R.string.PreferenceFileName),
                        this, "PASSWORD", null);

        etdPutEmailId.setText(stringUserName);
        etdPutPassword.setText(stringPassword);
    }

    private void showErrorDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BrandLogin.this);
        builder.setCancelable(true);

        TextView msg = new TextView(BrandLogin.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                etdPutEmailId.setFocusable(true);
                etdPutPassword.setFocusable(true);
            }
        });

        AlertDialog alert = builder.create();

        alert.show();
    }

    public static boolean isValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(BrandLogin.this);
        builder.setCancelable(true);

        TextView msg = new TextView(BrandLogin.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

}
