package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.data.AllFeedsModel;
import com.ascra.data.ListOfBundleData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ExtendedViewPager;
import com.ascra.utils.ServiceHandler;
import com.ascra.utils.TouchImageView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BundlePickleFragment extends Fragment implements
        OnItemClickListener {

    View view;
    ListView listOfBundledPickles;
    private String URL_FEED = null;
    ImageLoader imageLoader;
    ProgressBar inProgress;


    ArrayList<ListOfBundleData> listOfbundlePickles = new ArrayList<ListOfBundleData>();
    BundlePicklesAdapter bundlePicklesAdapter;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;


    Dialog rpd;

    Tracker tracker;

    Context context;
    private TextView msg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.bundle_pickle_fragment, null);
        context=getActivity();
        //@PK30NOV
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HomeScreen.whichFragment = "BUNDLE-PICKLE";

        setUpViews();

        setTracker();

        listOfbundlePickles.clear();

        URL_FEED = Constant.PickleUrl + "bundle-pickels.json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            rpd=new Dialog(getActivity());
            rpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            rpd.setContentView(R.layout.dialog_progress);
            msg = (TextView) rpd.findViewById(R.id.msg);msg.setText("Loading bundle pickle");
            rpd.setCanceledOnTouchOutside(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        new loadingBundledPicklesData().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();

        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }

        return view;
    }

    private void setUpViews() {
        listOfBundledPickles = (ListView) view.findViewById(R.id.bundlePicklesList);
        inProgress = (ProgressBar) view.findViewById(R.id.inProgress);

        listOfBundledPickles.setOnItemClickListener(this);

        //@PKDEC06 Adding header to listview
        CommonMethods.addHeaderToListview(getActivity(), listOfBundledPickles, getString(R.string.bundledPickleHeader), R.drawable.picklebundle);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "BUNDLE PICKLE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private class loadingBundledPicklesData extends
            AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET, params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProgress.setVisibility(View.GONE);
            rpd.dismiss();
        }

    }

    private void parseJSONUserProfile(JSONObject objMain) {

        try {
            JSONArray arrayOfBundles = objMain.getJSONArray("bundle_pickles");
            for (int bundleArrayElement = 0; bundleArrayElement < arrayOfBundles.length(); bundleArrayElement++) {
                JSONObject objSub = arrayOfBundles.getJSONObject(bundleArrayElement);
                int id = objSub.getInt("id");
                String cover_pic_url = objSub.getString("cover_pic_url");
                String name = objSub.getString("name");
                String seo_name = objSub.getString("seo_name");
                boolean deactive = objSub.getBoolean("deactive");

                ListOfBundleData lstOfBundleData = new ListOfBundleData(id, cover_pic_url, name, seo_name, deactive);
                listOfbundlePickles.add(lstOfBundleData);

                bundlePicklesAdapter = new BundlePicklesAdapter(getActivity(), listOfbundlePickles);

                listOfBundledPickles.setAdapter(bundlePicklesAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class BundlePicklesAdapter extends BaseAdapter {

        ArrayList<ListOfBundleData> listOfBundlePicklesData = new ArrayList<ListOfBundleData>();
        Context mContext;

        public BundlePicklesAdapter(Context mContext,
                                    ArrayList<ListOfBundleData> listOfBundlePicklesData) {
            this.listOfBundlePicklesData = listOfBundlePicklesData;
            this.mContext = mContext;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return listOfBundlePicklesData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfBundlePicklesData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            ImageView bundlePickleImage;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.bundle_pickle_item, null);
                holder = new ViewHolder();
                holder.bundlePickleImage = (ImageView) convertView.findViewById(R.id.bundlePickleImage);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            imageLoader.DisplayImage(listOfBundlePicklesData.get(position).getCover_pic_url(), holder.bundlePickleImage);

            return convertView;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        position--;
        ShareBundlePickleFragment shareBundlePickleFragment = new ShareBundlePickleFragment();
        Bundle args = new Bundle();
        args.putString("SEO_NAME", listOfbundlePickles.get(position).getSeo_name());
        args.putInt("ID", listOfbundlePickles.get(position).getId());
        shareBundlePickleFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, shareBundlePickleFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }



}