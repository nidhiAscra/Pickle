package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.CommonApis.UnfollowApi;
import com.ascra.data.FollowersData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.interfaces.UnfollowResponse;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class FollowersFragment extends Fragment implements
        OnClickListener, UnfollowResponse {

    View view;
    ListView listOfFollowers;
    ProgressBar inProcess;
    TextView btnBack;

    private String URL_FEED = null;

    ArrayList<FollowersData> listOfFollowersData = new ArrayList<FollowersData>();

    FollowingsAdapter followersAdapter;

    ImageLoader imageLoader;

    String userNameFrom;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    String generated_id;
    private String URL_UNFOLLOW_USER = null;
    private String URL_FOLLOW_USER = Constant.PickleUrl + "followers.json";

    com.ascra.pickle.FollowersFragment.FollowingsAdapter.ViewHolder holder;

    RelativeLayout buttonlayout;

    Tracker tracker;


    Typeface face;

    ImageView back;//@PK30NOV
    //@PKDEC12
    private UnfollowApi unfollowApi;

    //NS22DEC

    private Dialog rpd;
    private TextView msg;
    Boolean isCurrentUser=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.followers_fragment, null);
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");


        //@NS21DEC
        Bundle bundle =getArguments();
        userNameFrom = bundle.getString("USER_NAME","null");
        Log.e("@Create"," u_names  "+userNameFrom+" current "+HomeScreen.user_name);
        isCurrentUser=userNameFrom.equals(HomeScreen.user_name);
        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");


        setUpViews();
        setTracker();

        listOfFollowersData.clear();

        URL_FEED = Constant.PickleUrl + userNameFrom.replace(" ", "%20")
                + "/followers.json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            try {
                new loadingUserFollowingsData().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }
        //@PKDEC12
        unfollowApi = new UnfollowApi(getActivity(), this);
        return view;
    }

    private void setUpViews() {
        listOfFollowers = (ListView) view.findViewById(R.id.followersList);
        inProcess = (ProgressBar) view.findViewById(R.id.inProcess);
        buttonlayout = (RelativeLayout) view.findViewById(R.id.buttonlayout);
        back = (ImageView) view.findViewById(R.id.btnBackpress);
// @PK30NOV commented
//		btnBack = (TextView) view.findViewById(R.id.btnBack);
//		btnBack.setTypeface(face);
        buttonlayout.setOnClickListener(this);
        back.setOnClickListener(this);
        rpd=new Dialog(getActivity());
        rpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rpd.setContentView(R.layout.dialog_progress);
        msg = (TextView) rpd.findViewById(R.id.msg);
        rpd.setCanceledOnTouchOutside(false);
        CommonMethods.addHeaderToListview(getActivity(), listOfFollowers, getString(R.string.followersHeader), R.drawable.followers_icon);

    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "FRIENDS FOLLOWERS SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    @Override
    public void handleResponse(Call<JSONObject> call, Response<JSONObject> response, int position) {
        Log.e("handleResponse", this.getClass().getSimpleName() + " FollowersFragment -" + position + "- " + response);
        holder.addFollowing.setVisibility(View.VISIBLE);
        holder.unFollowing.setVisibility(View.GONE);
        listOfFollowersData.get(position).setUser_follow(false);
        followersAdapter.notifyDataSetChanged();
    }

    @Override
    public void handleError(Call<JSONObject> call, Throwable t, int position) {
        Log.e("handleError", this.getClass().getSimpleName() + " FollowersFragment -" + position + "- " + t);
    }

    private class loadingUserFollowingsData extends
            AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET,
                    params);

            try {
                Log.e("@Create","Response:"+jsonStr);
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    private void parseJSONUserProfile(JSONObject objMain) {
        inProcess.setVisibility(View.GONE);
        try {
            JSONArray followingsArray = null;
            followingsArray = objMain.getJSONArray("followers");
int follower_id = 0;
            for (int followers = 0; followers < followingsArray.length(); followers++) {

                JSONObject objFollowings = followingsArray
                        .getJSONObject(followers);


                int user_id = objFollowings.getInt("id");
                String following_email = objFollowings.getString("email");
                String following_username = objFollowings.getString("username");
                String following_profile_pic = objFollowings.getString("profile_pic");
                String following_gender = objFollowings.getString("gender");
                String following_phone_no = objFollowings.getString("phone_no");
                String following_provider = objFollowings.getString("provider");
                String following_name = objFollowings.getString("name");
                boolean is_me = objFollowings.getBoolean("is_me");
                boolean user_follow = objFollowings.getBoolean("user_follow");

                if (objFollowings.has("is_follow")) {

                    try {
                        //@PKDEC06 - if condition of <objFollowings.has("is_follow") ...> added to avoid any JSONException
                        if (objFollowings.has("is_follow") && objFollowings.get("is_follow") instanceof JSONObject) {
                            JSONObject objIsFollow = objFollowings.getJSONObject("is_follow");
                            //@PKDEC06 - the || condition in below if condition changed to && as it may produce NullPointerException
                            if (objIsFollow != null && objIsFollow.length() > 0) {
                                String generatedId = objIsFollow.getString("id");
                                follower_id = Integer.parseInt(generatedId);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                FollowersData followersData = new FollowersData(follower_id,
                        following_email, following_username,
                        following_profile_pic, following_gender,
                        following_phone_no, following_provider, following_name,
                        is_me, user_follow,user_id);

                listOfFollowersData.add(followersData);

            }
            //@PKDEC06 - moved out of the above for loop
            followersAdapter = new FollowingsAdapter(getActivity(), listOfFollowersData);
            listOfFollowers.setAdapter(followersAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class FollowingsAdapter extends BaseAdapter {

        ArrayList<FollowersData> listOfFollowersData = new ArrayList<FollowersData>();
        Context mContext;

        public FollowingsAdapter(Context mContext,
                                 ArrayList<FollowersData> listOfFollowersData) {
            this.listOfFollowersData = listOfFollowersData;
            this.mContext = mContext;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return listOfFollowersData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfFollowersData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            ImageView profilePic;
            TextView userName;
            TextView userEmail;

            ImageView addFollowing;
            ImageView unFollowing;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.followers_item_layout,
                        null);
                holder = new ViewHolder();
                holder.userName = (TextView) convertView
                        .findViewById(R.id.name);
                holder.userName.setTypeface(face);
                holder.userEmail = (TextView) convertView
                        .findViewById(R.id.email);
                holder.userEmail.setTypeface(face);
                holder.profilePic = (ImageView) convertView
                        .findViewById(R.id.profile_pic);

                holder.addFollowing = (ImageView) convertView
                        .findViewById(R.id.addFollowing);
                holder.unFollowing = (ImageView) convertView
                        .findViewById(R.id.unfollowing);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            //NS22DEC
            if(isCurrentUser) {
                if (listOfFollowersData.get(position).isUser_follow()
                        && !listOfFollowersData.get(position).isIs_me()) {
                    holder.unFollowing.setVisibility(View.VISIBLE);
                    holder.addFollowing.setVisibility(View.GONE);
                } else if (!listOfFollowersData.get(position).isUser_follow()
                        && listOfFollowersData.get(position).isIs_me()) {
                    holder.unFollowing.setVisibility(View.GONE);
                    holder.addFollowing.setVisibility(View.GONE);
                } else {
                    holder.unFollowing.setVisibility(View.GONE);
                    holder.addFollowing.setVisibility(View.VISIBLE);
                }
            }
            else if(listOfFollowersData.get(position).isIs_me()) {
                holder.unFollowing.setVisibility(View.VISIBLE);
                holder.addFollowing.setVisibility(View.GONE);
            }
            else
            {
                holder.unFollowing.setVisibility(View.GONE);
                holder.addFollowing.setVisibility(View.GONE);
            }
            holder.userName.setText(listOfFollowersData.get(position)
                    .getfollowers_name());
            holder.userEmail.setText(listOfFollowersData.get(position)
                    .getfollowers_email());

            imageLoader.DisplayImage(listOfFollowersData.get(position)
                    .getfollowers_profile_pic(), holder.profilePic);

            holder.userName.setOnClickListener(new OnClickListener() {

                public void onClick(View v) {
                    if (listOfFollowersData.get(position).isIs_me()) {

                    } else {
                        UserProfileFragment profileFragment = new UserProfileFragment();
                        Bundle args = new Bundle();
                        args.putString("USER_NAME",
                                listOfFollowersData.get(position)
                                        .getfollowers_username());
                        args.putString("FROM", "FOLLOWERS");
                        args.putString("GENERATED_ID", "");
                        profileFragment.setArguments(args);
                        FragmentTransaction profileTransaction = getFragmentManager()
                                .beginTransaction();
                        profileTransaction.replace(R.id.content_frame,
                                profileFragment);
                        profileTransaction.addToBackStack(null);
                        profileTransaction.commit();
                    }
                }
            });

            holder.addFollowing.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    holder.unFollowing.setVisibility(View.VISIBLE);
                    holder.addFollowing.setVisibility(View.GONE);
                    followersAdapter.notifyDataSetChanged();

                    followUser("" + listOfFollowersData.get(position).getUser_id(), HomeScreen.user_id, HomeScreen.authentication_token, HomeScreen.key,position);

                }
            });

            holder.unFollowing.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

//                    holder.addFollowing.setVisibility(View.VISIBLE);
//                    holder.unFollowing.setVisibility(View.GONE);
//                    followersAdapter.notifyDataSetChanged();

                    URL_UNFOLLOW_USER = Constant.PickleUrl_V1
                            + "followers/" + listOfFollowersData.get(position).getfollowers_id() + ".json";

                    //         URL_UNFOLLOW_USER = Constant.PickleUrl_V1 + "followers/" + HomeScreen.user_id + ".json";
                    unfollowUser(""+listOfFollowersData.get(position).getfollowers_id(),listOfFollowersData.get(position).getUser_id(),position);

                  /*  //@PKDEC12 : Common Api call
                    unfollowApi.unfollow(HomeScreen.authentication_token, HomeScreen.key,
                            SaveUtils.getUserID(getString(R.string.PreferenceFileName), getActivity(), SaveUtils.USER_ID, "0"),
                            String.valueOf(listOfFollowersData.get(position).getfollowers_id()), Constant.FOLLOWERS, position);
*/
//
//                    Log.e("handleResponse", this.getClass().getSimpleName() + " FollowingsFragment -" + position + "- " + response);
//                    new unFollowUser(position).execute();

                }
            });

            return convertView;
        }

    }

    private void followUser(String followerUserId, String userId,
                            String authenticationToken, String key, final int position) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //NS21DEC
                msg.setText("Submitting changes");
                rpd.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String paramFollowerUserId = params[0];
                String paramUserId = params[1];
                String paramAuthToken = params[2];
                String paramKey = params[3];

                URL_FOLLOW_USER = Constant.PickleUrl_V1+"followers/"
                        +"user_following"+ ".json";

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FOLLOW_USER);

                Log.e("@Create","Follower:"+URL_FOLLOW_USER);
                BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("follower_user_id", paramFollowerUserId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(followerUserIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        Log.e("@Create","Result:"+stringBuilder.toString());

                        try {
                            JSONObject json=new JSONObject(stringBuilder.toString());
                            if(json.getString("success").equals("true")){
                                JSONObject follow_record = json.getJSONObject("follow_record");

                                //NS21DEC

                                generated_id = follow_record.getString("id");
                                return stringBuilder.toString();
                            }
                            else{
                                return null;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("@Create","Result:"+result);
                if(result!=null){
                    holder.addFollowing.setVisibility(View.VISIBLE);
                    holder.unFollowing.setVisibility(View.GONE);
                    listOfFollowersData.get(Integer.parseInt(String.valueOf(position))).setUser_follow(true);
                    listOfFollowersData.get(Integer.parseInt(String.valueOf(position))).setfollowers_id(Integer.parseInt(generated_id));
                    followersAdapter.notifyDataSetChanged();
                 /*   followUnfollowlayout.setVisibility(View.VISIBLE);
                    btnFollow.setVisibility(View.GONE);
                    btnUnFollow.setVisibility(View.VISIBLE);
                    txtFollowersCounter.setText("" + (++followersCount));*/
                }
                //NS21DEC

                rpd.dismiss();

            }

        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(followerUserId, userId,
                authenticationToken, key, String.valueOf(position));

    }

    private void unfollowUser(final String followerUserId, final int userId, final int position){
        class UnFollowUser extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //NS21DEC
                msg.setText("Submitting changes");
                rpd.show();
            }

            @Override
            protected String doInBackground(String... strings) {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_UNFOLLOW_USER);

                Log.e("@Create", "Follower:" + URL_UNFOLLOW_USER);

                BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("Id", followerUserId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", HomeScreen.user_id);
                BasicNameValuePair otherIdBasicNameValuePair = new BasicNameValuePair("other_id", userId + "");
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", HomeScreen.authentication_token);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", HomeScreen.key);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(followerUserIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(otherIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                Log.e("@Create", "Follower req params:" + followerUserId+" "+HomeScreen.user_id+" "+userId+" "+HomeScreen.authentication_token+" "+HomeScreen.key);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        Log.e("@Create", "Result:" + stringBuilder.toString());

                        try {
                            JSONObject json = new JSONObject(stringBuilder.toString());
                            if (json.getString("success").equals("true")) {


                                return stringBuilder.toString();
                            } else {
                                return null;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("@Create", "Result:" + result);
                if (result != null) {
                   // listOfFollowersData.remove(Integer.parseInt(String.valueOf(position)));
                    listOfFollowersData.get(position).setUser_follow(false);
                    listOfFollowersData.get(position).setIs_me(false);
                    followersAdapter.notifyDataSetChanged();

//                followUnfollowlayout.setVisibility(View.VISIBLE);
//                btnFollow.setVisibility(View.VISIBLE);
//                btnUnFollow.setVisibility(View.GONE);
//                txtFollowersCounter.setText("" + (--followersCount));

                }
                //NS21DEC

                rpd.dismiss();
            }
            // Creating service handler class instance


        }
        UnFollowUser unFollowUser = new UnFollowUser();
        unFollowUser.execute(followerUserId, String.valueOf(position));

    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBackpress: //@PK30NOV this particular case added
            case R.id.buttonlayout:
                getFragmentManager().popBackStack();
                break;

            default:
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }
}
