package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.data.AllFeedsModel;
import com.ascra.data.ShareBundlePickleData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ExtendedViewPager;
import com.ascra.utils.ServiceHandler;
import com.ascra.utils.TouchImageView;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

//@PKDEC05 /votes/ changed to votes/ as Constant url already contains / at the end. [UNVOTED_URL]
public class ShareBundlePickleFragment extends Fragment {

    View view;
    String seoName;
    int id;
    ProgressBar inProgress;
    ListView listOfBundledPickles;
//    TextView txtName, txtDescription;

    private String URL_FEED = null;
    private String SHAREBUNDLE_URL = null;
    private String VOTE_URL = Constant.PickleUrl_V1 + "votes.json";
    private String UNVOTED_URL = null;

    ArrayList<ShareBundlePickleData> listOfShareBundlePickleData = new ArrayList<ShareBundlePickleData>();
    ImageLoader imageLoader;
    Dialog dialog;

    boolean isFirstVoted = false;
    boolean isSecondVoted = false;
    long count_one = 0;
    long count_two = 0;
    String share_pickle_id = "";

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Dialog rpd;

    Tracker tracker;

    Typeface face;
    TextView msg;
    MyBudleCustomAdapter myBundleCustomAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.share_bundle_pickle_fragment, null);
        //@PK30NOV
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = this.getArguments();

        id = bundle.getInt("ID");
        seoName = bundle.getString("SEO_NAME");

        listOfShareBundlePickleData.clear();

        setUpViews();
        setTracker();

        URL_FEED = Constant.PickleUrl + "bundles/" + id + "-" + seoName
                + ".json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            rpd=new Dialog(getActivity());
            rpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            rpd.setContentView(R.layout.dialog_progress);
            msg = (TextView) rpd.findViewById(R.id.msg);msg.setText("Loading bundle pickle");
            rpd.setCanceledOnTouchOutside(false);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        new loadingBundledData().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }).start();

        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        return view;
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "PROFILE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void setUpViews() {
        // @PKDEC06 - Adding header to listview
        // Commented since added in header_share_bundle_pickle.xml layout

//        txtName = (TextView) view.findViewById(R.id.txtName);
//        txtDescription = (TextView) view.findViewById(R.id.txtDescription);
        listOfBundledPickles = (ListView) view.findViewById(R.id.bundlePicklesList);
        inProgress = (ProgressBar) view.findViewById(R.id.inProgress);

//        txtName.setTypeface(face);
//        txtDescription.setTypeface(face);
        addHeaderToListview();
    }

    private class loadingBundledData extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET,
                    params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProgress.setVisibility(View.GONE);
            rpd.dismiss();
        }

    }

    private void parseJSONUserProfile(JSONObject mainObject) {
        try {
            JSONObject objBundle = mainObject.getJSONObject("bundle");
            String bundle_name = objBundle.getString("bundle_name");
            String bundle_description = objBundle
                    .getString("bundle_description");

            JSONArray arrayOfPickles = objBundle.getJSONArray("pickles");
            for (int pickleArrayElement = 0; pickleArrayElement < arrayOfPickles
                    .length(); pickleArrayElement++) {
                JSONObject objPickles = arrayOfPickles
                        .getJSONObject(pickleArrayElement);

                JSONObject objPickle = objPickles.getJSONObject("pickle");
                boolean strPDeactive = objPickle.getBoolean("pickle_deactive");
                boolean strPFeatured = objPickle.getBoolean("pickle_featured");
                String strPId = objPickle.getString("pickle_id");
                String strPImage = objPickle.getString("pickle_image");
                String strPName = objPickle.getString("pickle_name");
                String strPUserId = objPickle.getString("user_id");
                String pickle_created_at = objPickle
                        .getString("pickle_created_at");
                boolean is_shared = objPickle.getBoolean("is_shared");

                JSONObject objFirstOption = objPickles
                        .getJSONObject("first_option");
                long strFId = objFirstOption.getLong("first_option_id");
                String strFOptionImageUrl = objFirstOption
                        .getString("first_option_image_url");
                String strFOptionLable = objFirstOption
                        .getString("first_option_lable");
                String strFirstOptionText = objFirstOption
                        .getString("first_option_text");
                long strFQuestionId = objFirstOption
                        .getLong("first_option_question_id");
                long strFirstOption = objFirstOption
                        .getLong("first_option_total_votes");
                String first_option_voted_id = objFirstOption
                        .getString("first_option_voted_id");
                boolean first_option_is_voted = objFirstOption
                        .getBoolean("first_option_is_voted");

                JSONObject objSecondOption = objPickles
                        .getJSONObject("second_option");

                long strSId = objSecondOption.getLong("second_option_id");
                String strSOptionImageUrl = objSecondOption
                        .getString("second_option_image_url");
                String strSOptionLable = objSecondOption
                        .getString("second_option_lable");
                String strSecondOptionText = objSecondOption
                        .getString("second_option_text");
                long strSQuestionId = objSecondOption
                        .getLong("second_option_pickle_id");
                long strSecondOption = objSecondOption
                        .getLong("second_option_total_votes");
                String second_option_voted_id = objSecondOption
                        .getString("second_option_voted_id");
                boolean second_option_is_voted = objSecondOption
                        .getBoolean("second_option_is_voted");

                ShareBundlePickleData shareBundlePickleData = new ShareBundlePickleData(
                        strPDeactive, strPFeatured, strPId, strPImage,
                        strPName, strPUserId, pickle_created_at, is_shared,
                        strFId, strFOptionImageUrl, strFOptionLable,
                        strFQuestionId, strFirstOptionText, strFirstOption,
                        first_option_voted_id, first_option_is_voted, strSId,
                        strSOptionImageUrl, strSOptionLable, strSQuestionId,
                        strSecondOptionText, strSecondOption,
                        second_option_voted_id, second_option_is_voted);

                listOfShareBundlePickleData.add(shareBundlePickleData);
                myBundleCustomAdapter = new MyBudleCustomAdapter(
                        getActivity(), listOfShareBundlePickleData);
                listOfBundledPickles.setAdapter(myBundleCustomAdapter);

            }

//            txtName.setText("Bundle " + bundle_name);
//            txtDescription.setText(bundle_description);
            //@PKDEC06 - set bundle name and desc in header.
            setBundleDetailsInHeader(bundle_name, bundle_description);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyBudleCustomAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<ShareBundlePickleData> listOfShareBundlePickleData = new ArrayList<ShareBundlePickleData>();

        public MyBudleCustomAdapter(Context mContext,
                                    ArrayList<ShareBundlePickleData> listOfShareBundlePickleData) {
            this.mContext = mContext;
            this.listOfShareBundlePickleData = listOfShareBundlePickleData;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return listOfShareBundlePickleData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfShareBundlePickleData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {


            View v = convertView;
            final FeedsViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.share_bundle_item, null);
                viewHolder = new FeedsViewHolder(v);
                v.setTag(viewHolder);
            } else {
                viewHolder = (FeedsViewHolder) v.getTag();
            }

            viewHolder.firstOptionImage.setTag(position);
            viewHolder.firstText.setTag(position);
            viewHolder.secondOptionImage.setTag(position);
            viewHolder.SecondText.setTag(position);



            if (listOfShareBundlePickleData.get(position).getStrPName() != null) {
                viewHolder.txtPickleQuestion.setText(listOfShareBundlePickleData.get(position).getStrPName());
            }

            if (listOfShareBundlePickleData.get(position)
                    .getStrFOptionImageUrl() != null) {
                if (listOfShareBundlePickleData.get(position).getStrFOptionImageUrl().equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    viewHolder.firstOptionImage.setVisibility(View.GONE);

                } else {
                    viewHolder.firstOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(listOfShareBundlePickleData.get(position).getStrFOptionImageUrl(), viewHolder.firstOptionImage);
                }

            }

            if (listOfShareBundlePickleData.get(position).getStrFOptionLable() != null) {

                if (listOfShareBundlePickleData.get(position).getStrFOptionLable().equals("") || listOfShareBundlePickleData.get(position).getStrFOptionLable().equals("null")) {
                    viewHolder.txtFirstOptionName.setVisibility(View.GONE);
                } else {
                    viewHolder.txtFirstOptionName.setVisibility(View.VISIBLE);
                    viewHolder.txtFirstOptionName.setText(listOfShareBundlePickleData.get(position).getStrFOptionLable());
                }

            }

            if (listOfShareBundlePickleData.get(position)
                    .getStrFirstOptionText() != null) {
                if (listOfShareBundlePickleData.get(position).getStrFirstOptionText().equals("") || listOfShareBundlePickleData.get(position).getStrFirstOptionText().equals("null")) {
                    viewHolder.firstText.setVisibility(View.GONE);
                } else {
                    viewHolder.firstText.setVisibility(View.VISIBLE);
                    viewHolder.firstText.setText(listOfShareBundlePickleData.get(position).getStrFirstOptionText());
                }

            }

            viewHolder.firstOptionVoteCount.setText("" + listOfShareBundlePickleData.get(position).getStrFirstOption());

            if (listOfShareBundlePickleData.get(position)
                    .getStrSOptionImageUrl() != null) {

                if (listOfShareBundlePickleData.get(position)
                        .getStrSOptionImageUrl()
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    viewHolder.secondOptionImage.setVisibility(View.GONE);

                } else {
                    viewHolder.secondOptionImage.setVisibility(View.VISIBLE);
                    imageLoader.DisplayImage(
                            listOfShareBundlePickleData.get(position)
                                    .getStrSOptionImageUrl(),
                            viewHolder.secondOptionImage);
                }

            }

            if (listOfShareBundlePickleData.get(position).getStrSOptionLable() != null) {
                if (listOfShareBundlePickleData.get(position)
                        .getStrSOptionLable().equals("")
                        || listOfShareBundlePickleData.get(position)
                        .getStrSOptionLable().equals("null")) {
                    viewHolder.txtSecondOptionName.setVisibility(View.GONE);
                } else {
                    viewHolder.txtSecondOptionName.setVisibility(View.VISIBLE);
                    viewHolder.txtSecondOptionName
                            .setText(listOfShareBundlePickleData.get(position)
                                    .getStrSOptionLable());
                }
            }

            if (listOfShareBundlePickleData.get(position)
                    .getStrSecondOptionText() != null) {

                if (listOfShareBundlePickleData.get(position)
                        .getStrSecondOptionText().equals("")
                        || listOfShareBundlePickleData.get(position)
                        .getStrSecondOptionText().equals("null")) {
                    viewHolder.SecondText.setVisibility(View.GONE);
                } else {
                    viewHolder.SecondText.setVisibility(View.VISIBLE);
                    viewHolder.SecondText.setText(listOfShareBundlePickleData
                            .get(position).getStrSecondOptionText());
                }

            }

            viewHolder.secondOptionVoteCount.setText(""
                    + listOfShareBundlePickleData.get(position)
                    .getStrSecondOption());

            if (listOfShareBundlePickleData.get(position)
                    .isFirst_option_is_voted()
                    && !listOfShareBundlePickleData.get(position)
                    .isSecond_option_is_voted()) {
                viewHolder.firstView.setBackgroundColor(Color
                        .parseColor("#F08080"));
                viewHolder.firstOptionVoteCount.setTextColor(Color
                        .parseColor("#F08080"));
                viewHolder.firstOptionVoteImage
                        .setImageResource(R.drawable.vote_color);
                viewHolder.firstOptionVoteText.setTextColor(Color
                        .parseColor("#F08080"));

                viewHolder.secondView.setBackgroundColor(Color
                        .parseColor("#11000000"));
                viewHolder.secondOptionVoteCount.setTextColor(Color
                        .parseColor("#000000"));
                viewHolder.secondOptionVoteImage
                        .setImageResource(R.drawable.vote_black);
                viewHolder.secondOptionVoteText.setTextColor(Color
                        .parseColor("#000000"));

                listOfShareBundlePickleData.get(position)
                        .setFirst_option_is_voted(true);
                listOfShareBundlePickleData.get(position)
                        .setSecond_option_is_voted(false);

            } else if (!listOfShareBundlePickleData.get(position)
                    .isFirst_option_is_voted()
                    && listOfShareBundlePickleData.get(position)
                    .isSecond_option_is_voted()) {

                viewHolder.firstView.setBackgroundColor(Color
                        .parseColor("#11000000"));
                viewHolder.firstOptionVoteCount.setTextColor(Color
                        .parseColor("#000000"));
                viewHolder.firstOptionVoteImage
                        .setImageResource(R.drawable.vote_black);
                viewHolder.firstOptionVoteText.setTextColor(Color
                        .parseColor("#000000"));

                viewHolder.secondView.setBackgroundColor(Color
                        .parseColor("#F08080"));
                viewHolder.secondOptionVoteCount.setTextColor(Color
                        .parseColor("#F08080"));
                viewHolder.secondOptionVoteImage
                        .setImageResource(R.drawable.vote_color);
                viewHolder.secondOptionVoteText.setTextColor(Color
                        .parseColor("#F08080"));

                listOfShareBundlePickleData.get(position)
                        .setFirst_option_is_voted(false);
                listOfShareBundlePickleData.get(position)
                        .setSecond_option_is_voted(true);

            } else {
                viewHolder.firstView.setBackgroundColor(Color
                        .parseColor("#11000000"));
                viewHolder.firstOptionVoteCount.setTextColor(Color
                        .parseColor("#000000"));
                viewHolder.firstOptionVoteImage
                        .setImageResource(R.drawable.vote_black);
                viewHolder.firstOptionVoteText.setTextColor(Color
                        .parseColor("#000000"));

                viewHolder.secondView.setBackgroundColor(Color
                        .parseColor("#11000000"));
                viewHolder.secondOptionVoteCount.setTextColor(Color
                        .parseColor("#000000"));
                viewHolder.secondOptionVoteImage
                        .setImageResource(R.drawable.vote_black);
                viewHolder.secondOptionVoteText.setTextColor(Color
                        .parseColor("#000000"));

                listOfShareBundlePickleData.get(position)
                        .setFirst_option_is_voted(false);
                listOfShareBundlePickleData.get(position)
                        .setSecond_option_is_voted(false);
            }

            viewHolder.firstOptionImage
                    .setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            new FullScreenImage().setDialog(listOfShareBundlePickleData.get((Integer) v.getTag()),0);
                        }
                    });
            viewHolder.secondOptionImage
                    .setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            new FullScreenImage().setDialog(listOfShareBundlePickleData.get((Integer) v.getTag()),1);
                        }
                    });
            viewHolder.firstText
                    .setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                             new FullScreenImage().setDialog(listOfShareBundlePickleData.get((Integer) v.getTag()),0);
                        }
                    });
            viewHolder.SecondText
                    .setOnClickListener(new OnClickListener() {
                        public void onClick(View v) {
                            new FullScreenImage().setDialog(listOfShareBundlePickleData.get((Integer) v.getTag()),1);
                        }
                    });
            viewHolder.firstOptionVoteImage
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if (listOfShareBundlePickleData.get(position)
                                    .isFirst_option_is_voted() == false
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == false) {

                                viewHolder.firstView.setBackgroundColor(Color
                                        .parseColor("#F08080"));
                                viewHolder.firstOptionVoteCount
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                viewHolder.firstOptionVoteImage
                                        .setImageResource(R.drawable.vote_color);
                                viewHolder.firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#F08080"));

                                listOfShareBundlePickleData.get(position)
                                        .setFirst_option_is_voted(true);
                                listOfShareBundlePickleData.get(position)
                                        .setSecond_option_is_voted(false);


                                long count_one = listOfShareBundlePickleData.get(position)
                                        .getStrFirstOption();
                                count_one = count_one + 1;
                                viewHolder.firstOptionVoteCount.setText(""
                                        + (count_one + listOfShareBundlePickleData
                                        .get(position)
                                        .getStrFirstOption()));
                                listOfShareBundlePickleData.get(position).setStrFirstOption(count_one);

                                pickleVote(listOfShareBundlePickleData.get(
                                        position),true);

                            } else if (listOfShareBundlePickleData
                                    .get(position).isFirst_option_is_voted() == true
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == false) {

                                viewHolder.firstView.setBackgroundColor(Color
                                        .parseColor("#11000000"));
                                viewHolder.firstOptionVoteCount
                                        .setTextColor(Color
                                                .parseColor("#000000"));
                                viewHolder.firstOptionVoteImage
                                        .setImageResource(R.drawable.vote_black);
                                viewHolder.firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#000000"));

                                listOfShareBundlePickleData.get(position)
                                        .setFirst_option_is_voted(false);
                                listOfShareBundlePickleData.get(position)
                                        .setSecond_option_is_voted(false);

                                long count_one = listOfShareBundlePickleData.get(position)
                                        .getStrFirstOption();
                                count_one = count_one - 1;
                                viewHolder.firstOptionVoteCount.setText(""
                                        + (count_one + listOfShareBundlePickleData
                                        .get(position)
                                        .getStrFirstOption()));
                                listOfShareBundlePickleData.get(position).setStrFirstOption(count_one);
                                notifyDataSetChanged();
                                UNVOTED_URL = Constant.PickleUrl
                                        + "votes/"
                                        + listOfShareBundlePickleData.get(position)
                                        .getFirst_option_voted_id()
                                        + ".json";

                                new UnVoted().execute();

                            } else if (listOfShareBundlePickleData
                                    .get(position).isFirst_option_is_voted() == false
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == true) {
                            }
                        }
                    });
            viewHolder.secondOptionVoteImage
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if (listOfShareBundlePickleData.get(position)
                                    .isFirst_option_is_voted() == false
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == false) {

                                viewHolder.secondView.setBackgroundColor(Color
                                        .parseColor("#F08080"));
                                viewHolder.secondOptionVoteCount
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                viewHolder.secondOptionVoteImage
                                        .setImageResource(R.drawable.vote_color);
                                viewHolder.secondOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#F08080"));

                                listOfShareBundlePickleData.get(position)
                                        .setFirst_option_is_voted(false);
                                listOfShareBundlePickleData.get(position)
                                        .setSecond_option_is_voted(true);

                                count_two = count_two + 1;
                                viewHolder.secondOptionVoteCount
                                        .setText(""
                                                + (count_two + listOfShareBundlePickleData
                                                .get(position)
                                                .getStrSecondOption()));

                                pickleVote(listOfShareBundlePickleData.get(
                                        position),true);

                            } else if (listOfShareBundlePickleData
                                    .get(position).isFirst_option_is_voted() == false
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == true) {

                                viewHolder.secondView.setBackgroundColor(Color
                                        .parseColor("#11000000"));
                                viewHolder.secondOptionVoteCount
                                        .setTextColor(Color
                                                .parseColor("#000000"));
                                viewHolder.secondOptionVoteImage
                                        .setImageResource(R.drawable.vote_black);
                                viewHolder.secondOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#000000"));

                                listOfShareBundlePickleData.get(position)
                                        .setFirst_option_is_voted(false);
                                listOfShareBundlePickleData.get(position)
                                        .setSecond_option_is_voted(false);

                                count_two = count_two - 1;
                                viewHolder.secondOptionVoteCount
                                        .setText(""
                                                + (count_two + listOfShareBundlePickleData
                                                .get(position)
                                                .getStrSecondOption()));

                                UNVOTED_URL = Constant.PickleUrl
                                        + "votes/"
                                        + listOfShareBundlePickleData.get(
                                        position)
                                        .getSecond_option_voted_id()
                                        + ".json";

                                new UnVoted().execute();

                            } else if (listOfShareBundlePickleData
                                    .get(position).isFirst_option_is_voted() == true
                                    && listOfShareBundlePickleData
                                    .get(position)
                                    .isSecond_option_is_voted() == false) {
                            }
                        }
                    });

            if (listOfShareBundlePickleData.get(position).isIs_shared()) {
                viewHolder.popUp.setText("shared");
            }
            class FullScreenImage {

                Dialog dialog;
                ShareBundlePickleData model;
                RelativeLayout layout_first,layout_second;
                ImageView secondVoteImage,firstVoteImage;
                TextView txt_title,txtTitle_first,txtTitle_second,secondVoteCount,firstVoteCount,firstOptionVoteText,secondOptionVoteText;

                public void setDialog(ShareBundlePickleData allFeedsModel,int position) {

                    this.model=allFeedsModel;

                    dialog = new Dialog(getActivity(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.setContentView(R.layout.image_fullscreen);

                    ExtendedViewPager mViewPager = (ExtendedViewPager)dialog.findViewById(R.id.view_pager);
                    mViewPager.setAdapter(new TouchImageAdapter(model));
                    mViewPager.setCurrentItem(position);

                    txt_title=(TextView)dialog.findViewById(R.id.txtTitle);
                    txtTitle_first=(TextView)dialog.findViewById(R.id.txtTitle_first);
                    txtTitle_second=(TextView)dialog.findViewById(R.id.txtTitle_second);
                    secondVoteCount=(TextView)dialog.findViewById(R.id.secondVoteCount);
                    firstVoteCount=(TextView)dialog.findViewById(R.id.firstVoteCount);
                    layout_first=(RelativeLayout)dialog.findViewById(R.id.layout_one) ;
                    layout_second=(RelativeLayout)dialog.findViewById(R.id.layout_second) ;
                    secondVoteImage=(ImageView)dialog.findViewById(R.id.seconVoteImage);
                    firstVoteImage=(ImageView)dialog.findViewById(R.id.firstVoteImage);
                    firstOptionVoteText=(TextView)dialog.findViewById(R.id.firstVoteText);
                    secondOptionVoteText=(TextView)dialog.findViewById(R.id.secondVoteText);

                    txtTitle_first.setText(!model.getStrFOptionLable().equals("null")?model.getStrFOptionLable():"");
                    txtTitle_second.setText(!model.getStrSOptionLable().equals("null")?model.getStrSOptionLable():"");


                    txt_title.setText(model.getStrPName());
                    if(position==0) {
                        txtTitle_first.setVisibility(View.VISIBLE);
                        txtTitle_second.setVisibility(View.GONE);
                        layout_second.setVisibility(View.GONE);
                        layout_first.setVisibility(View.VISIBLE);
                    }else{
                        layout_first.setVisibility(View.GONE);
                        layout_second.setVisibility(View.VISIBLE);
                        txtTitle_first.setVisibility(View.GONE);
                        txtTitle_second.setVisibility(View.VISIBLE);
                    }

                    firstVoteCount.setText(model.getStrFirstOption()+"");
                    secondVoteCount.setText(model.getStrSecondOption()+"");

                    if (model.isFirst_option_is_voted()
                            && !model
                            .isSecond_option_is_voted()) {

                        firstVoteCount.setTextColor(Color.parseColor("#F08080"));
                        firstVoteImage.setImageResource(R.drawable.vote_color);
                        firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                        secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                        secondVoteImage.setImageResource(R.drawable.vote_black);
                        secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                        model.setFirst_option_is_voted(true);
                        model.setSecond_option_is_voted(false);

                    } else if (!model.isFirst_option_is_voted() && model.isSecond_option_is_voted()) {

                        firstVoteCount.setTextColor(Color.parseColor("#ffffff"));
                        firstVoteImage.setImageResource(R.drawable.vote_black);
                        firstOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                        secondVoteCount.setTextColor(Color.parseColor("#F08080"));
                        secondVoteImage.setImageResource(R.drawable.vote_color);
                        secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                        model.setFirst_option_is_voted(false);
                        model.setSecond_option_is_voted(true);
                    }

                    firstVoteImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            if (model
                                    .isFirst_option_is_voted() == false
                                    && model
                                    .isSecond_option_is_voted() == false) {

                                firstVoteCount
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                firstVoteImage
                                        .setImageResource(R.drawable.vote_color);
                                firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#F08080"));

                                model.setFirst_option_is_voted(true);
                                model
                                        .setSecond_option_is_voted(false);
                                long count_one = model
                                        .getStrFirstOption();
                                count_one = count_one + 1;
                                model.setStrFirstOption(count_one);
                                firstVoteCount.setText(""
                                        + (model
                                        .getStrFirstOption()));

                                pickleVote(model,true);
                                notifyDataSetChanged();



                            } else if (model
                                    .isFirst_option_is_voted() == true
                                    && model
                                    .isSecond_option_is_voted() == false) {


                                firstVoteCount
                                        .setTextColor(Color
                                                .parseColor("#FFFFFF"));
                                firstVoteImage
                                        .setImageResource(R.drawable.vote_black);
                                firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#FFFFFF"));

                                model
                                        .setFirst_option_is_voted(false);
                                model
                                        .setSecond_option_is_voted(false);
                                long count_one = model
                                        .getStrFirstOption();
                                count_one = count_one - 1;
                                model.setStrFirstOption(count_one);
                                firstVoteCount.setText("" + model.getStrFirstOption());

                                Log.e("@Create"," model.getFirst_option_voted_id():"+model.getStrFirstOption()+"----"+ model.getFirst_option_voted_id());

                                UNVOTED_URL = Constant.PickleUrl
                                        + "votes/"
                                        + model.getFirst_option_voted_id()
                                        + ".json";

                                new UnVoted().execute();
                                notifyDataSetChanged();
                            } else if (model
                                    .isFirst_option_is_voted() == false
                                    && model
                                    .isSecond_option_is_voted() == true) {
                                Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    secondVoteImage
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    if (model.isFirst_option_is_voted() == false
                                            && model
                                            .isSecond_option_is_voted() == false) {


                                        secondVoteCount
                                                .setTextColor(Color
                                                        .parseColor("#F08080"));
                                        secondVoteImage
                                                .setImageResource(R.drawable.vote_color);
                                        secondOptionVoteText
                                                .setTextColor(Color
                                                        .parseColor("#F08080"));
                                        model.setFirst_option_is_voted(false);
                                        model.setSecond_option_is_voted(true);
                                        long count_two = model.getStrSecondOption();
                                        count_two = count_two + 1;
                                        model.setStrSecondOption(count_two);
                                        secondVoteCount.setText("" + (model.getStrSecondOption()));
                                        pickleVote(model,false);
                                        notifyDataSetChanged();
                                    } else if (model.isFirst_option_is_voted() == false && model.isSecond_option_is_voted() == true) {


                                        secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                                        secondVoteImage.setImageResource(R.drawable.vote_black);
                                        secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                                        model
                                                .setFirst_option_is_voted(false);
                                        model
                                                .setSecond_option_is_voted(false);
                                        long count_two = model
                                                .getStrSecondOption();
                                        count_two = count_two - 1;
                                        model.setStrSecondOption(count_two);
                                        secondVoteCount.setText(""+model.getStrSecondOption());
                                        Log.e("@Create"," model.getSecond_option_voted_id():"+model.getStrSecondOption()+"---"+ model.getSecond_option_voted_id());

                                        UNVOTED_URL = Constant.PickleUrl + "votes/" + model.getSecond_option_voted_id() + ".json";

                                        new UnVoted().execute();
                                        notifyDataSetChanged();

                                    } else if (model.isFirst_option_is_voted() == true && model.isSecond_option_is_voted() == false) {
                                        Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            if(position==0){
                                txtTitle_first.setVisibility(View.VISIBLE);
                                txtTitle_second.setVisibility(View.GONE);
                                layout_first.setVisibility(View.VISIBLE);
                                layout_second.setVisibility(View.GONE);
                            }else{
                                txtTitle_second.setVisibility(View.VISIBLE);
                                txtTitle_first.setVisibility(View.GONE);
                                layout_second.setVisibility(View.VISIBLE);
                                layout_first.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                    dialog.show();
                }
                class TouchImageAdapter extends PagerAdapter {

                    ShareBundlePickleData innerModel;

                    public TouchImageAdapter(ShareBundlePickleData model) {
                        this.innerModel=model;
                    }

                    @Override
                    public int getCount() {
                        return 2;
                    }

                    @Override
                    public View instantiateItem(ViewGroup container, int position) {
                        if (position == 0) {

                            if (innerModel.getStrFirstOptionText().equals("")||innerModel.getStrSecondOptionText().equals("null")) {


                                TouchImageView img = new TouchImageView(container.getContext());
                                Picasso.with(mContext).load(innerModel.getStrFOptionImageUrl()).into(img);
                                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                return img;

                            } else {
                                TextView text = new TextView(container.getContext());
                                text.setText(innerModel.getStrFirstOptionText());
                                text.setGravity(Gravity.CENTER);
                                container.addView(text);
                                return text;
                            }
                        } else if (position == 1) {



                            if (innerModel.getStrSecondOptionText().equals("")||innerModel.getStrSecondOptionText().equals("null")) {

                                TouchImageView img = new TouchImageView(container.getContext());
                                Picasso.with(mContext).load(innerModel.getStrSOptionImageUrl()).into(img);
                                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                return img;
                            } else {

                                TextView text = new TextView(container.getContext());
                                text.setText(innerModel.getStrSecondOptionText());
                                text.setGravity(Gravity.CENTER);
                                container.addView(text);
                                return text;
                            }

                        }
                        return null;
                    }

                    @Override
                    public void destroyItem(ViewGroup container, int position, Object object) {
                        container.removeView((View) object);
                    }

                    @Override
                    public boolean isViewFromObject(View view, Object object) {
                        return view == object;
                    }

                }
            }
            viewHolder.popUp.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {

                    if (!listOfShareBundlePickleData.get(position).isIs_shared()) {
                        SHAREBUNDLE_URL = Constant.PickleUrl + "share_pickels.json";

                        sharePickle(listOfShareBundlePickleData.get(position).getStrPId(), HomeScreen.authentication_token, HomeScreen.key);
                    } else {
                        String notShareText = "Pickle Already Shared !";
                        showNoFeedsDialog(notShareText);
                    }
                }
            });

            viewHolder.btnComment.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    BundleCommentFragment bundleCommentFragment = new BundleCommentFragment();
                    Bundle args = new Bundle();

                    args.putString("USER_ID", "" + HomeScreen.user_id);
                    args.putString("PICKLE_ID", ""
                            + listOfShareBundlePickleData.get(position)
                            .getStrPId());

                    bundleCommentFragment.setArguments(args);
                    FragmentTransaction bundleCommentTransaction = getFragmentManager()
                            .beginTransaction();
                    bundleCommentTransaction.replace(R.id.content_frame,
                            bundleCommentFragment);
                    bundleCommentTransaction.addToBackStack(null);
                    bundleCommentTransaction.commit();
                }
            });

            viewHolder.firstOptionVoteText
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            VotingFragment votingfragment = new VotingFragment();
                            Bundle showArgs = new Bundle();
                            showArgs.putString("OPTION_ID", ""
                                    + listOfShareBundlePickleData.get(position)
                                    .getStrFId());
                            votingfragment.setArguments(showArgs);
                            FragmentTransaction showTransaction = getFragmentManager()
                                    .beginTransaction();
                            showTransaction.replace(R.id.content_frame,
                                    votingfragment);
                            showTransaction.addToBackStack(null);
                            showTransaction.commit();

                        }

                    });

            viewHolder.secondOptionVoteText
                    .setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            VotingFragment votingfragment = new VotingFragment();
                            Bundle showArgs = new Bundle();
                            showArgs.putString("OPTION_ID", ""
                                    + listOfShareBundlePickleData.get(position)
                                    .getStrSId());
                            votingfragment.setArguments(showArgs);
                            FragmentTransaction showTransaction = getFragmentManager()
                                    .beginTransaction();
                            showTransaction.replace(R.id.content_frame,
                                    votingfragment);
                            showTransaction.addToBackStack(null);
                            showTransaction.commit();

                        }
                    });

            return v;
        }

        class FeedsViewHolder {
            TextView txtPickleQuestion;
            ImageView firstOptionImage;
            TextView txtFirstOptionName;
            ImageView firstOptionVoteImage;
            TextView firstOptionVoteCount;
            TextView firstOptionVoteText;
            TextView firstText;
            ImageView secondOptionImage;
            TextView txtSecondOptionName;
            ImageView secondOptionVoteImage;
            TextView secondOptionVoteCount;
            TextView secondOptionVoteText;
            TextView SecondText;
            ImageView btnComment;
            TextView popUp;
            RelativeLayout firstView;
            RelativeLayout secondView;


            public FeedsViewHolder(View base) {

                txtPickleQuestion = (TextView) base
                        .findViewById(R.id.pickleQuestion);
                txtPickleQuestion.setTypeface(face);
                firstOptionImage = (ImageView) base
                        .findViewById(R.id.firstOptionImageUrl);
                txtFirstOptionName = (TextView) base
                        .findViewById(R.id.firstOptionName);
                txtFirstOptionName.setTypeface(face);
                firstOptionVoteImage = (ImageView) base
                        .findViewById(R.id.firstVoteImage);
                firstOptionVoteCount = (TextView) base
                        .findViewById(R.id.firstVoteCount);
                firstOptionVoteText = (TextView) base
                        .findViewById(R.id.firstVoteText);
                firstOptionVoteText.setTypeface(face);
                firstText = (TextView) base.findViewById(R.id.firstText);
                firstText.setTypeface(face);
                secondOptionImage = (ImageView) base
                        .findViewById(R.id.secondOptionImageUrl);
                txtSecondOptionName = (TextView) base
                        .findViewById(R.id.secondOptionName);
                txtSecondOptionName.setTypeface(face);
                secondOptionVoteImage = (ImageView) base
                        .findViewById(R.id.secondVoteImage);
                secondOptionVoteCount = (TextView) base
                        .findViewById(R.id.secondVoteCount);
                secondOptionVoteText = (TextView) base
                        .findViewById(R.id.secondVoteText);
                secondOptionVoteText.setTypeface(face);
                SecondText = (TextView) base.findViewById(R.id.SecondText);
                SecondText.setTypeface(face);
                btnComment = (ImageView) base.findViewById(R.id.btnComment);
                popUp = (TextView) base.findViewById(R.id.popUp);

                firstView = (RelativeLayout) base.findViewById(R.id.firstView);
                secondView = (RelativeLayout) base
                        .findViewById(R.id.secondView);
            }
        }

    }
    class FullScreenImage {

        Dialog dialog;
        ShareBundlePickleData model;
        RelativeLayout layout_first,layout_second;
        ImageView secondVoteImage,firstVoteImage;
        TextView txt_title,txtTitle_first,txtTitle_second,secondVoteCount,firstVoteCount,firstOptionVoteText,secondOptionVoteText;

        public void setDialog(ShareBundlePickleData allFeedsModel, int position) {

            this.model=allFeedsModel;

            dialog = new Dialog(getActivity(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialog.setContentView(R.layout.image_fullscreen);

            ExtendedViewPager mViewPager = (ExtendedViewPager)dialog.findViewById(R.id.view_pager);
            mViewPager.setAdapter(new TouchImageAdapter(model));
            mViewPager.setCurrentItem(position);

            txt_title=(TextView)dialog.findViewById(R.id.txtTitle);
            txtTitle_first=(TextView)dialog.findViewById(R.id.txtTitle_first);
            txtTitle_second=(TextView)dialog.findViewById(R.id.txtTitle_second);
            secondVoteCount=(TextView)dialog.findViewById(R.id.secondVoteCount);
            firstVoteCount=(TextView)dialog.findViewById(R.id.firstVoteCount);
            layout_first=(RelativeLayout)dialog.findViewById(R.id.layout_one) ;
            layout_second=(RelativeLayout)dialog.findViewById(R.id.layout_second) ;
            secondVoteImage=(ImageView)dialog.findViewById(R.id.seconVoteImage);
            firstVoteImage=(ImageView)dialog.findViewById(R.id.firstVoteImage);
            firstOptionVoteText=(TextView)dialog.findViewById(R.id.firstVoteText);
            secondOptionVoteText=(TextView)dialog.findViewById(R.id.secondVoteText);

            txtTitle_first.setText(!model.getStrFOptionLable().equals("null")?model.getStrFOptionLable():"");
            txtTitle_second.setText(!model.getStrSOptionLable().equals("null")?model.getStrSOptionLable():"");


            txt_title.setText(model.getStrPName());
            if(position==0) {
                txtTitle_first.setVisibility(View.VISIBLE);
                txtTitle_second.setVisibility(View.GONE);
                layout_second.setVisibility(View.GONE);
                layout_first.setVisibility(View.VISIBLE);
            }else{
                layout_first.setVisibility(View.GONE);
                layout_second.setVisibility(View.VISIBLE);
                txtTitle_first.setVisibility(View.GONE);
                txtTitle_second.setVisibility(View.VISIBLE);
            }

            firstVoteCount.setText(model.getStrFirstOption()+"");
            secondVoteCount.setText(model.getStrSecondOption()+"");

            if (model.isFirst_option_is_voted()
                    && !model
                    .isSecond_option_is_voted()) {

                firstVoteCount.setTextColor(Color.parseColor("#F08080"));
                firstVoteImage.setImageResource(R.drawable.vote_color);
                firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                secondVoteImage.setImageResource(R.drawable.vote_black);
                secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                model.setFirst_option_is_voted(true);
                model.setSecond_option_is_voted(false);

            } else if (!model.isFirst_option_is_voted() && model.isSecond_option_is_voted()) {

                firstVoteCount.setTextColor(Color.parseColor("#ffffff"));
                firstVoteImage.setImageResource(R.drawable.vote_black);
                firstOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                secondVoteCount.setTextColor(Color.parseColor("#F08080"));
                secondVoteImage.setImageResource(R.drawable.vote_color);
                secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                model.setFirst_option_is_voted(false);
                model.setSecond_option_is_voted(true);
            }

            firstVoteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if (model
                            .isFirst_option_is_voted() == false
                            && model
                            .isSecond_option_is_voted() == false) {

                        firstVoteCount
                                .setTextColor(Color
                                        .parseColor("#F08080"));
                        firstVoteImage
                                .setImageResource(R.drawable.vote_color);
                        firstOptionVoteText
                                .setTextColor(Color
                                        .parseColor("#F08080"));

                        model.setFirst_option_is_voted(true);
                        model
                                .setSecond_option_is_voted(false);
                        long count_one = model
                                .getStrFirstOption();
                        count_one = count_one + 1;
                        model.setStrFirstOption(count_one);
                        firstVoteCount.setText(""
                                + (model
                                .getStrFirstOption()));

                        pickleVote(model,true);
                        myBundleCustomAdapter.notifyDataSetChanged();



                    } else if (model
                            .isFirst_option_is_voted() == true
                            && model
                            .isSecond_option_is_voted() == false) {


                        firstVoteCount
                                .setTextColor(Color
                                        .parseColor("#FFFFFF"));
                        firstVoteImage
                                .setImageResource(R.drawable.vote_black);
                        firstOptionVoteText
                                .setTextColor(Color
                                        .parseColor("#FFFFFF"));

                        model
                                .setFirst_option_is_voted(false);
                        model
                                .setSecond_option_is_voted(false);
                        long count_one = model
                                .getStrFirstOption();
                        count_one = count_one - 1;
                        model.setStrFirstOption(count_one);
                        firstVoteCount.setText("" + model.getStrFirstOption());

                        Log.e("@Create"," model.getFirst_option_voted_id():"+model.getStrFirstOption()+"----"+ model.getFirst_option_voted_id());

                        UNVOTED_URL = Constant.PickleUrl
                                + "votes/"
                                + model.getFirst_option_voted_id()
                                + ".json";

                        new UnVoted().execute();
                        myBundleCustomAdapter.notifyDataSetChanged();
                    } else if (model
                            .isFirst_option_is_voted() == false
                            && model
                            .isSecond_option_is_voted() == true) {
                        Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                    }
                }
            });
            secondVoteImage
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            if (model.isFirst_option_is_voted() == false
                                    && model
                                    .isSecond_option_is_voted() == false) {


                                secondVoteCount
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                secondVoteImage
                                        .setImageResource(R.drawable.vote_color);
                                secondOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                model.setFirst_option_is_voted(false);
                                model.setSecond_option_is_voted(true);
                                long count_two = model.getStrSecondOption();
                                count_two = count_two + 1;
                                model.setStrSecondOption(count_two);
                                secondVoteCount.setText("" + (model.getStrSecondOption()));
                                pickleVote(model,false);
                                myBundleCustomAdapter.notifyDataSetChanged();
                            } else if (model.isFirst_option_is_voted() == false && model.isSecond_option_is_voted() == true) {


                                secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                                secondVoteImage.setImageResource(R.drawable.vote_black);
                                secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                                model
                                        .setFirst_option_is_voted(false);
                                model
                                        .setSecond_option_is_voted(false);
                                long count_two = model
                                        .getStrSecondOption();
                                count_two = count_two - 1;
                                model.setStrSecondOption(count_two);
                                secondVoteCount.setText(""+model.getStrSecondOption());
                                Log.e("@Create"," model.getSecond_option_voted_id():"+model.getStrSecondOption()+"---"+ model.getSecond_option_voted_id());

                                UNVOTED_URL = Constant.PickleUrl + "votes/" + model.getSecond_option_voted_id() + ".json";

                                new UnVoted().execute();
                                myBundleCustomAdapter.notifyDataSetChanged();

                            } else if (model.isFirst_option_is_voted() == true && model.isSecond_option_is_voted() == false) {
                                Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if(position==0){
                        txtTitle_first.setVisibility(View.VISIBLE);
                        txtTitle_second.setVisibility(View.GONE);
                        layout_first.setVisibility(View.VISIBLE);
                        layout_second.setVisibility(View.GONE);
                    }else{
                        txtTitle_second.setVisibility(View.VISIBLE);
                        txtTitle_first.setVisibility(View.GONE);
                        layout_second.setVisibility(View.VISIBLE);
                        layout_first.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            dialog.show();
        }
        class TouchImageAdapter extends PagerAdapter {

            ShareBundlePickleData innerModel;

            public TouchImageAdapter(ShareBundlePickleData model) {
                this.innerModel=model;
            }

            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public View instantiateItem(ViewGroup container, int position) {
                if (position == 0) {

                    if (innerModel.getStrFirstOptionText().equals("")||innerModel.getStrFirstOptionText().equals("null")) {


                        TouchImageView img = new TouchImageView(container.getContext());
                        Picasso.with(getActivity()).load(innerModel.getStrFOptionImageUrl()).into(img);
                        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        return img;

                    } else {
                        TextView text = new TextView(container.getContext());
                        text.setText(innerModel.getStrFirstOptionText());
                        text.setGravity(Gravity.CENTER);
                        container.addView(text);
                        return text;
                    }
                } else if (position == 1) {



                    if (innerModel.getStrSecondOptionText().equals("")||innerModel.getStrSecondOptionText().equals("null")) {

                        TouchImageView img = new TouchImageView(container.getContext());
                        Picasso.with(getActivity()).load(innerModel.getStrSOptionImageUrl()).into(img);
                        container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        return img;
                    } else {

                        TextView text = new TextView(container.getContext());
                        text.setText(innerModel.getStrSecondOptionText());
                        text.setGravity(Gravity.CENTER);
                        container.addView(text);
                        return text;
                    }

                }
                return null;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

        }
    }

    private void pickleVote(final ShareBundlePickleData model, final Boolean isFirst) {
       /* final String[] _id = {"1264"};//TODO This should be dynamic as per the _id[0] below.*/
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            String id="";


            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                msg.setText("Submitting your vote");
                rpd.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String paramOptionId = params[0];
                String paramQuestionId = params[1];
                String paramSharePickleId = params[2];



                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(VOTE_URL);
                BasicNameValuePair optionIdBasicNameValuePair = new BasicNameValuePair("option_id", paramOptionId);
                BasicNameValuePair questionIdBasicNameValuePair = new BasicNameValuePair("question_id", paramQuestionId);
                BasicNameValuePair sharePickleIdBasicNameValuePair = new BasicNameValuePair("share_pickle_id", paramSharePickleId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", HomeScreen.user_id);
                BasicNameValuePair auth  = new BasicNameValuePair("authentication_token", HomeScreen.authentication_token);
                BasicNameValuePair key = new BasicNameValuePair("key", HomeScreen.key);


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(optionIdBasicNameValuePair);
                nameValuePairList.add(questionIdBasicNameValuePair);
                nameValuePairList.add(sharePickleIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(auth);
                nameValuePairList.add(key);


                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        String strJson = stringBuilder.toString();
                        Log.e("@Create JSON", strJson);
                        try {
                            JSONObject JsonVote = new JSONObject(strJson);
                            JSONObject JsonItem = JsonVote.getJSONObject("vote");
                            return JsonItem.getString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            return null;
                        }

                        // return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(result!=null){
                    id =result;
                    if(isFirst)
                        model.setFirst_option_voted_id(id);
                    else
                        model.setSecond_option_voted_id(id);

                }
                myBundleCustomAdapter.notifyDataSetChanged();
                rpd.dismiss();
            }


        }


        new SendPostReqAsyncTask().execute(String.valueOf(model.getStrFId()), model.getStrPId(),share_pickle_id);


    }
    private class UnVoted extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try {
                params.add(new BasicNameValuePair("authentication_token",
                        HomeScreen.authentication_token));
                params.add(new BasicNameValuePair("key", HomeScreen.key));

            } catch (Exception e) {
                e.printStackTrace();
            }
            String jsonStr = sh.makeServiceCall(UNVOTED_URL,
                    ServiceHandler.DELETE, params);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    private void sharePickle(String pickleId, String authentication_token,
                             String key) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramPikcleId = params[0];
                String paramAuthenticationToken = params[1];
                String paramKey = params[2];
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(SHAREBUNDLE_URL);

                BasicNameValuePair pickleIdBasicNameValuePair = new BasicNameValuePair(
                        "question_id", paramPikcleId);

                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair(
                        "authentication_token", paramAuthenticationToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair(
                        "key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(pickleIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }

                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                String text = "Share pickle successfully";
                showDialog(text);
            }

        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(pickleId, authentication_token, key);

    }

    private void showDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                FeedsFragment pickleFeedsFragment = new FeedsFragment();
                FragmentTransaction pickleFeedsTransaction = getFragmentManager()
                        .beginTransaction();
                pickleFeedsTransaction.replace(R.id.content_frame,
                        pickleFeedsFragment);
                pickleFeedsTransaction.addToBackStack(null);
                pickleFeedsTransaction.commit();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

    TextView bundleName, bundleDescription;

    public void addHeaderToListview() {
        LayoutInflater myinflater = getActivity().getLayoutInflater();
        ViewGroup myHeader = (ViewGroup) myinflater.inflate(R.layout.header_share_bundle_pickle_fragment, listOfBundledPickles, false);
        TextView headerTitle = (TextView) myHeader.findViewById(R.id.headerTitle);
        bundleName = (TextView) myHeader.findViewById(R.id.bundleName);
        bundleDescription = (TextView) myHeader.findViewById(R.id.bundleDescription);
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/sniglet-regular.ttf");
        headerTitle.setTypeface(typeface);
        listOfBundledPickles.addHeaderView(myHeader, null, false);
    }

    public void setBundleDetailsInHeader(String bundleNameText, String bundleDescriptionText) {
        bundleName.setText(bundleNameText);
        bundleDescription.setText(bundleDescriptionText);
    }




}
