package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
/*
import com.ascra.CommonApis.UnfollowApi;*/
import com.ascra.data.FollowingData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.interfaces.UnfollowResponse;
import com.ascra.pickle.FollowingsFragments.FollowingsAdapter.ViewHolder;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class FollowingsFragments extends Fragment implements UnfollowResponse {

    private View view;
    private ListView listOfFollowings;
    private ProgressBar inProcess;
    private TextView btnBack;

   // private UnfollowApi unfollowApi;//@PKDEC12
    private ArrayList<FollowingData> listOfFollowingData = new ArrayList<FollowingData>();
    private FollowingsAdapter followingsAdapter;
    private ImageLoader imageLoader;
    private String userNameFrom;
    private String generated_id;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ViewHolder holder = null;
    private RelativeLayout buttonLayout;
    private Tracker tracker;

    private Typeface face;
    //@PKDEC01
    private ImageView btnBackpress;
    private String URL_FEED = null;
    private String URL_FOLLOW_USER = Constant.PickleUrl_V1 + "followers.json";
    private String URL_UNFOLLOW_USER = null;

    //NS21DEC
    Boolean isCurrentUser=false;

    private Dialog rpd;
    private TextView msg;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.followings_fragment, null);

        //@PKDEC01
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //@PKDEC01
        btnBackpress = (ImageView) view.findViewById(R.id.btnBackpress);
        btnBackpress.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStack();
            }
        });


        //@NS21DEC
        Bundle bundle =getArguments();
        userNameFrom = bundle.getString("USER_NAME","null");
        Log.e("@Create"," u_names  "+userNameFrom+" current "+HomeScreen.user_name);
        isCurrentUser=userNameFrom.equals(HomeScreen.user_name);
        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");


        setUpViews();
        setTracker();

        listOfFollowingData.clear();

    URL_FEED = Constant.PickleUrl + userNameFrom.replace(" ", "%20")
               + "/followings.json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            try {
                new loadingUserFollowingsData().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }

        //@PKDEC12
    //    unfollowApi = new UnfollowApi(getActivity(), this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "FRIENDS FOLLOWINGS SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void setUpViews() {
        rpd=new Dialog(getActivity());
        rpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rpd.setContentView(R.layout.dialog_progress);
        msg = (TextView) rpd.findViewById(R.id.msg);
        rpd.setCanceledOnTouchOutside(false);
        listOfFollowings = (ListView) view.findViewById(R.id.followingsList);
        inProcess = (ProgressBar) view.findViewById(R.id.inProcess);
        // listOfFollowings.setOnItemClickListener(this);
        buttonLayout = (RelativeLayout) view.findViewById(R.id.buttonlayout);
        btnBack = (TextView) view.findViewById(R.id.btnBack);
        btnBack.setTypeface(face);

        buttonLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        CommonMethods.addHeaderToListview(getActivity(), listOfFollowings, getString(R.string.followingsHeader), R.drawable.followers_icon);
    }

    @Override
    public void handleResponse(Call<JSONObject> call, Response<JSONObject> response, int position) {
        Log.e("handleResponse", this.getClass().getSimpleName() + " FollowingsFragment -" + position + "- " + response);
        holder.addFollowing.setVisibility(View.VISIBLE);
        holder.unFollowing.setVisibility(View.GONE);
        listOfFollowingData.get(position).setUser_follow(false);
        followingsAdapter.notifyDataSetChanged();
    }

    @Override
    public void handleError(Call<JSONObject> call, Throwable t, int position) {
        Log.e("handleError", this.getClass().getSimpleName() + " FollowingsFragment -" + position + "- " + t);
    }

    private void parseJSONUserProfile(JSONObject objMain) {
        inProcess.setVisibility(View.GONE);
        try {

            FollowingData followingData;
            JSONArray followingsArray = null;
            JSONObject objFollowings;
            boolean user_follow;
            boolean is_me;
            followingsArray = objMain.getJSONArray("followings");
            for (int followings = 0; followings < followingsArray.length(); followings++) {
                objFollowings = followingsArray.getJSONObject(followings);
                //NS21DEC : User id for unfollow user
                int userid=objFollowings.getInt("id");
                String following_email = objFollowings.getString("email");
                String following_username = objFollowings.getString("username");
                String following_profile_pic = objFollowings.getString("profile_pic");
                String following_gender = objFollowings.getString("gender");
                String following_phone_no = objFollowings.getString("phone_no");
                String following_provider = objFollowings.getString("provider");
                String following_name = objFollowings.getString("name");
                user_follow = objFollowings.getBoolean("user_follow");
                is_me = objFollowings.getBoolean("is_me");
                int following_id = 0;
                if (objFollowings.has("is_follow")) {
                    try {
                        //@PKDEC06 - if condition of <objFollowings.has("is_follow") ...> added to avoid any JSONException
                        if (objFollowings.has("is_follow") && objFollowings.get("is_follow") instanceof JSONObject) {
                            JSONObject objIsFollow = objFollowings.getJSONObject("is_follow");

                            //@PKDEC06 - the || condition in below if condition changed to && as it may produce NullPointerException
                            if (objIsFollow != null && objIsFollow.length() > 0) {
                                String generatedId = objIsFollow.getString("id");
                                following_id = Integer.parseInt(generatedId);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Log.e("@Follow", "..................................................................");
                Log.e("@Follow", "Following Id................ " + following_id);
                Log.e("@Follow", "Following Email............. " + following_email);
                Log.e("@Follow", "Following User name......... " + following_username);
                Log.e("@Follow", "Following Dp................ " + following_profile_pic);
                Log.e("@Follow", "Following gender............ " + following_gender);
                Log.e("@Follow", "Following phone............. " + following_phone_no);
                Log.e("@Follow", "Following provider.......... " + following_provider);
                Log.e("@Follow", "Following name.............. " + following_name);
                Log.e("@Follow", "ObjFollowings............... " + objFollowings);
                Log.e("@Follow", "User follow................. " + user_follow);
                Log.e("@Follow", "Me ?........................ " + is_me);

                followingData = new FollowingData(following_id, following_email, following_username, following_profile_pic, following_gender,
                        following_phone_no, following_provider, following_name, objFollowings, user_follow, is_me,userid);

                listOfFollowingData.add(followingData);

            }

            //@PKDEC06 - moved out of the above for loop
            followingsAdapter = new FollowingsAdapter(getActivity(), listOfFollowingData);
            listOfFollowings.setAdapter(followingsAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//NS21DEC : new method is there
  /*  private void followUser(String followerUserId, String userId, String authenticationToken, String key, final int position) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramFollowerUserId = params[0];
                String paramUserId = params[1];
                String paramAuthToken = params[2];
                String paramKey = params[3];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FOLLOW_USER);
                BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("follow[follower_user_id]", paramFollowerUserId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("follow[user_id]", paramUserId);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(followerUserIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);
                Log.e("@Create", "FOLLOW USER " + URL_FOLLOW_USER);
                Log.e("@Create", "FOLLOW USER PARAMS " + nameValuePairList);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (Exception ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("RESULT :: ", "" + result);
                Log.e("@Follow", "FOLLOW USER RESP " + result);
                // rpd.dismiss();
                holder.unFollowing.setVisibility(View.VISIBLE);
                holder.addFollowing.setVisibility(View.GONE);
//                holder.unFollowing.setVisibility(View.VISIBLE);
//                holder.addFollowing.setVisibility(View.GONE);
                listOfFollowingData.get(position).setUser_follow(true);
                followingsAdapter.notifyDataSetChanged();
            }

        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(followerUserId, userId, authenticationToken, key);

    }
*/
    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class loadingUserFollowingsData extends
            AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET,
                    params);

            Log.e("@Create","Response::::::"+jsonStr);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    public class FollowingsAdapter extends BaseAdapter {

        ArrayList<FollowingData> listOfFollowingData = new ArrayList<FollowingData>();
        Context mContext;

        public FollowingsAdapter(Context mContext, ArrayList<FollowingData> listOfFollowingData) {
            this.listOfFollowingData = listOfFollowingData;
            this.mContext = mContext;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return listOfFollowingData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfFollowingData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.followings_item_layout, null);
                holder = new ViewHolder();
                holder.userName = (TextView) convertView.findViewById(R.id.name);
                holder.userName.setTypeface(face);
                holder.userEmail = (TextView) convertView.findViewById(R.id.email);
                holder.userEmail.setTypeface(face);
                holder.profilePic = (ImageView) convertView.findViewById(R.id.profile_pic);
                holder.addFollowing = (ImageView) convertView.findViewById(R.id.addFollowing);
                holder.unFollowing = (ImageView) convertView.findViewById(R.id.unfollowing);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.userName.setText(listOfFollowingData.get(position).getFollowing_name());
            holder.userEmail.setText(listOfFollowingData.get(position).getFollowing_email());

            imageLoader.DisplayImage(listOfFollowingData.get(position).getFollowing_profile_pic(), holder.profilePic);

            Log.e("@Create ","iscurrent "+isCurrentUser);
            //NS21DEC
if(isCurrentUser) {
    if (listOfFollowingData.get(position).isUser_follow() && !listOfFollowingData.get(position).isIs_me()) {
        holder.unFollowing.setVisibility(View.VISIBLE);
        holder.addFollowing.setVisibility(View.GONE);
    } else if (!listOfFollowingData.get(position).isUser_follow() && listOfFollowingData.get(position).isIs_me()) {
        holder.unFollowing.setVisibility(View.GONE);
        holder.addFollowing.setVisibility(View.GONE);
    } else {
        holder.unFollowing.setVisibility(View.GONE);
        holder.addFollowing.setVisibility(View.VISIBLE);
    }

}
else{
    holder.unFollowing.setVisibility(View.GONE);
    holder.addFollowing.setVisibility(View.GONE);
            }
            holder.userName.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (listOfFollowingData.get(position).isIs_me()) {

                    } else {
                        UserProfileFragment profileFragment = new UserProfileFragment();
                        Bundle args = new Bundle();
                        args.putString("USER_NAME", listOfFollowingData.get(position).getFollowing_username());
                        args.putString("FROM", "FOLLOWING");
                        args.putString("GENERATED_ID", generated_id);
                        profileFragment.setArguments(args);
                        FragmentTransaction profileTransaction = getFragmentManager().beginTransaction();
                        profileTransaction.replace(R.id.content_frame, profileFragment);
                        profileTransaction.addToBackStack(null);
                        profileTransaction.commit();
                    }
                }
            });

            holder.addFollowing.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

//                    holder.unFollowing.setVisibility(View.VISIBLE);
//                    holder.addFollowing.setVisibility(View.GONE);
//                    followingsAdapter.notifyDataSetChanged();
                    Log.e("@Follow", "Add - " + position + " - " + listOfFollowingData.get(position).getFollowing_id());
               /*     followUser("" + userId, HomeScreen.user_id,
                            HomeScreen.authentication_token, HomeScreen.key);*/
                    followUser("" + listOfFollowingData.get(position).getUserid(), HomeScreen.user_id, HomeScreen.authentication_token, HomeScreen.key,position);

                }
            });

            holder.unFollowing.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

//                    holder.addFollowing.setVisibility(View.VISIBLE);
//                    holder.unFollowing.setVisibility(View.GONE);
//                    followingsAdapter.notifyDataSetChanged();
//                    JSONObject follow_record = listOfFollowingData.get(position).getJSONObject("follow_record");
//                    generatedId = follow_record.getString("id");

//NS21DEC
            URL_UNFOLLOW_USER = Constant.PickleUrl_V1
          + "followers/" + listOfFollowingData.get(position).getFollowing_id() + ".json";

               //         URL_UNFOLLOW_USER = Constant.PickleUrl_V1 + "followers/" + HomeScreen.user_id + ".json";
                        unfollowUser(""+listOfFollowingData.get(position).getFollowing_id(),listOfFollowingData.get(position).getUserid(),position);



                    //@PKDEC12 : Common Api call
                /*    unfollowApi.unfollow(HomeScreen.authentication_token, HomeScreen.key,
                            SaveUtils.getUserID(getString(R.string.PreferenceFileName), getActivity(), SaveUtils.USER_ID, "0"),
                            String.valueOf(listOfFollowingData.get(position).getFollowing_id()), Constant.FOLLOWINGS, position);
*/
//                    new unFollowUser(position).execute();

                }
            });

            return convertView;
        }

        public class ViewHolder {
            ImageView profilePic;
            TextView userName;
            TextView userEmail;
            ImageView addFollowing;
            ImageView unFollowing;
        }

    }

    private void followUser(String followerUserId, String userId,
                            String authenticationToken, String key, final int position) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                //NS21DEC
                msg.setText("Submitting changes");
                rpd.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String paramFollowerUserId = params[0];
                String paramUserId = params[1];
                String paramAuthToken = params[2];
                String paramKey = params[3];

                URL_FOLLOW_USER = Constant.PickleUrl_V1+"followers/"
                        +"user_following"+ ".json";

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FOLLOW_USER);

                Log.e("@Create","Follower:"+URL_FOLLOW_USER);
                BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("follower_user_id", paramFollowerUserId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", paramUserId);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(followerUserIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        Log.e("@Create","Result:"+stringBuilder.toString());

                        try {
                            JSONObject json=new JSONObject(stringBuilder.toString());
                            if(json.getString("success").equals("true")){
                                JSONObject follow_record = json.getJSONObject("follow_record");

                                //NS21DEC
                                generated_id = follow_record.getString("id");
                                return stringBuilder.toString();
                            }
                            else{
                                return null;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                Log.e("@Create","Result:"+result);
                if(result!=null){
                    holder.addFollowing.setVisibility(View.VISIBLE);
                    holder.unFollowing.setVisibility(View.GONE);
                    listOfFollowingData.get(Integer.parseInt(String.valueOf(position))).setUser_follow(false);
                    followingsAdapter.notifyDataSetChanged();
                 /*   followUnfollowlayout.setVisibility(View.VISIBLE);
                    btnFollow.setVisibility(View.GONE);
                    btnUnFollow.setVisibility(View.VISIBLE);
                    txtFollowersCounter.setText("" + (++followersCount));*/
                }
                //NS21DEC

                rpd.dismiss();

            }

        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(followerUserId, userId,
                authenticationToken, key, String.valueOf(position));

    }
    private void unfollowUser(final String followerUserId, final int userId, final int position){
    class UnFollowUser extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //NS21DEC
            msg.setText("Submitting changes");
            rpd.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL_UNFOLLOW_USER);

            Log.e("@Create", "Follower:" + URL_UNFOLLOW_USER);

            BasicNameValuePair followerUserIdBasicNameValuePair = new BasicNameValuePair("Id", followerUserId);
            BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", HomeScreen.user_id);
            BasicNameValuePair otherIdBasicNameValuePair = new BasicNameValuePair("other_id", userId + "");
            BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", HomeScreen.authentication_token);
            BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", HomeScreen.key);

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            nameValuePairList.add(followerUserIdBasicNameValuePair);
            nameValuePairList.add(userIdBasicNameValuePair);
            nameValuePairList.add(otherIdBasicNameValuePair);
            nameValuePairList.add(authTokenBasicNameValuePair);
            nameValuePairList.add(keyBasicNameValuePair);

            Log.e("@Create", "Follower req params:" + followerUserId+" "+HomeScreen.user_id+" "+userId+" "+HomeScreen.authentication_token+" "+HomeScreen.key);

            try {
                UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                        nameValuePairList);

                httpPost.setEntity(urlEncodedFormEntity);

                try {
                    HttpResponse httpResponse = httpClient.execute(httpPost);
                    InputStream inputStream = httpResponse.getEntity().getContent();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    StringBuilder stringBuilder = new StringBuilder();
                    String bufferedStrChunk = null;

                    while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                        stringBuilder.append(bufferedStrChunk);
                    }
                    Log.e("@Create", "Result:" + stringBuilder.toString());

                    try {
                        JSONObject json = new JSONObject(stringBuilder.toString());
                        if (json.getString("success").equals("true")) {


                            return stringBuilder.toString();
                        } else {
                            return null;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } catch (ClientProtocolException cpe) {
                    cpe.printStackTrace();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.e("@Create", "Result:" + result);
            if (result != null) {
                listOfFollowingData.remove(Integer.parseInt(String.valueOf(position)));
                followingsAdapter.notifyDataSetChanged();


//                followUnfollowlayout.setVisibility(View.VISIBLE);
//                btnFollow.setVisibility(View.VISIBLE);
//                btnUnFollow.setVisibility(View.GONE);
//                txtFollowersCounter.setText("" + (--followersCount));
            }
            //NS21DEC

            rpd.dismiss();
        }
        // Creating service handler class instance


    }
        UnFollowUser unFollowUser = new UnFollowUser();
        unFollowUser.execute(followerUserId, String.valueOf(position));

    }
}
