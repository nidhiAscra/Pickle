package com.ascra.pickle;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CreatePickleFragmentActivity extends Fragment implements
        OnClickListener {

    private static View view;
    // add quesion?.
    EditText etdAddQuestion;
    ImageView btnAddQuestion;
    // main view;
    LinearLayout addView;
    // add firstView.
    LinearLayout addFirstPickleView;
    RelativeLayout addViewOne, addImageOne, addTextOne;
    EditText enterFirstText;
    TextView setEnterFirstText;
    ImageView btnCancelFirstPickleView, btnDoneFirstPickleView;
    RelativeLayout addFirstImageWithCaption;
    ImageView firstImage;
    EditText firstCaption;

    // add secondView.
    LinearLayout addSecondPickleView;
    RelativeLayout addViewTwo, addImageTwo, addTextTwo;
    EditText enterSecondText;
    TextView setEnterSecondText;
    ImageView btnCancelSecondPickleView, btnDoneSecondPickleView;
    RelativeLayout addSecondImageWithCaption;
    ImageView secondImage;
    EditText secondCaption;

    // Share Button
    Button btnShareOnPickle;

    FrameLayout inProcess;

    boolean happen = false;
    boolean whenCreatePickle = false;
    Dialog dialog;

    static String whichOne = null;
    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;

    private String URL_FEED = Constant.PickleUrl + "questions.json";
    Bitmap photo1, photo2;
    Uri selectedImageUri1, selectedImageUri2, mImageCaptureUri;
    HttpEntity resEntity;
    String tempPath1, tempPath2;

    ProgressDialog barProgressDialog;
    Handler updateBarHandler;

    String stringFirstText;
    String stringSecondText;

    ProgressDialog ringProgressDialog;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    Bundle savedInstanceState;
    private android.support.v4.app.Fragment mContent;

    Tracker tracker;

    Typeface face;

    //@PKNOV29 - changed the temppath1, temppath2, firstcaption, secondcaption, firstscreentet & secondscreentext on click of text/imageUpload in create pickle screen
// This was because if image is changed to text, tempPath1 is not null and if..else conditions go wrong.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.new_pickle_create, container, false);
//				view = inflater.inflate(R.layout.nearby_fragment, container, false);
        } catch (InflateException e) {
                /* map is already there, just return view as it is */
        }

        HomeScreen.whichFragment = "CREATEPICKLE";
        this.savedInstanceState = savedInstanceState;
        updateBarHandler = new Handler();

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        setUpViews();
        setTracker();
        return view;

//		return container;

    }

    private void setUpViews() {
//		ActionBar mActionBar = getSupportActionBar();
//		mActionBar.setDisplayShowHomeEnabled(false);
//		mActionBar.setDisplayShowTitleEnabled(false);
//		LayoutInflater mInflater = LayoutInflater.from(this);
//		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//		View mCustomView = mInflater.inflate(R.layout.pickle_custom_actionbar,
//				null);
//		ImageView menuButton = (ImageView) mCustomView
//				.findViewById(R.id.homeButton);
//		ImageView pickleBundle = (ImageView) mCustomView
//				.findViewById(R.id.pickleBundle);
//		menuButton.setVisibility(View.GONE);
//		pickleBundle.setVisibility(View.GONE);
//		mActionBar.setCustomView(mCustomView);
//		mActionBar.setDisplayShowCustomEnabled(true);

        // MainView;
        addView = (LinearLayout) view.findViewById(R.id.addView);
        // addquestion;
        etdAddQuestion = (EditText) view.findViewById(R.id.editQuestion);
        etdAddQuestion.setTypeface(face);
        // etdAddQuestion.setEnabled(false);
        btnAddQuestion = (ImageView) view.findViewById(R.id.btnAddQuestion);
        // first_View;
        addViewOne = (RelativeLayout) view.findViewById(R.id.addViewOne);
        // addViewOne.setClickable(false);
        addFirstPickleView = (LinearLayout) view.findViewById(R.id.addFirstPickleView);
        addImageOne = (RelativeLayout) view.findViewById(R.id.addImageOne);
        addTextOne = (RelativeLayout) view.findViewById(R.id.addTextOne);
        enterFirstText = (EditText) view.findViewById(R.id.enterFirstText);
        enterFirstText.setTypeface(face);
        setEnterFirstText = (TextView) view.findViewById(R.id.setEnterFirstText);
        setEnterFirstText.setTypeface(face);
        btnCancelFirstPickleView = (ImageView) view.findViewById(R.id.btnCancelFirstPickleView);
        btnDoneFirstPickleView = (ImageView) view.findViewById(R.id.btnDoneFirstPickleView);
        addFirstImageWithCaption = (RelativeLayout) view.findViewById(R.id.addFirstImageWithCaption);
        firstImage = (ImageView) view.findViewById(R.id.FirstImage);
        firstCaption = (EditText) view.findViewById(R.id.etdCaptionFirst);
        firstCaption.setTypeface(face);
        // SecondView;
        addViewTwo = (RelativeLayout) view.findViewById(R.id.addViewTwo);
        addSecondPickleView = (LinearLayout) view.findViewById(R.id.addSecondPickleView);
        addImageTwo = (RelativeLayout) view.findViewById(R.id.addImageTwo);
        addTextTwo = (RelativeLayout) view.findViewById(R.id.addTextTwo);
        enterSecondText = (EditText) view.findViewById(R.id.enterSecondText);
        enterSecondText.setTypeface(face);
        setEnterSecondText = (TextView) view.findViewById(R.id.setEnterSecondText);
        setEnterSecondText.setTypeface(face);
        btnCancelSecondPickleView = (ImageView) view.findViewById(R.id.btnCancelSecondPickleView);
        btnDoneSecondPickleView = (ImageView) view.findViewById(R.id.btnDoneSecondPickleView);
        addSecondImageWithCaption = (RelativeLayout) view.findViewById(R.id.addSecondImageWithCaption);
        secondImage = (ImageView) view.findViewById(R.id.SecondImage);
        secondCaption = (EditText) view.findViewById(R.id.etdCaptionSecond);
        secondCaption.setTypeface(face);
        // shareButton;
        btnShareOnPickle = (Button) view.findViewById(R.id.btnShareOnPickle);
        // btnShareOnPickle.setEnabled(false);

        // inProcess = (FrameLayout) findViewById(R.id.inProcess);

        btnAddQuestion.setOnClickListener(this);
        addImageOne.setOnClickListener(this);
        addTextOne.setOnClickListener(this);
        btnCancelFirstPickleView.setOnClickListener(this);
        btnDoneFirstPickleView.setOnClickListener(this);

        addImageTwo.setOnClickListener(this);
        addTextTwo.setOnClickListener(this);
        btnShareOnPickle.setOnClickListener(this);
        btnCancelSecondPickleView.setOnClickListener(this);
        btnDoneSecondPickleView.setOnClickListener(this);

        //@PKDEC08
        etdAddQuestion.requestFocus();
        CommonMethods.showKeyboardForcefully(getActivity(), etdAddQuestion);

//        InputMethodManager imm = (InputMethodManager) ((HomeScreen) getActivity()).getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
//        imm.showSoftInput(etdAddQuestion, InputMethodManager.SHOW_IMPLICIT);

        //@PKDEC06 - request focus to question as soon as the CreatePickle is opened
        etdAddQuestion.requestFocus();
        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.makePickleHeader, R.drawable.make_pickle_icon);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "CREATE PICKLE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddQuestion:
                addView.animate().alpha(1).setDuration(500).start();
                if (!etdAddQuestion.getText().toString().trim().isEmpty()) {//@PKDEC07
                    if (!happen) {
                        etdAddQuestion.setEnabled(true);
                        etdAddQuestion.requestFocus();
                        // btnAddQuestion.setImageResource(R.drawable.editfeatured_color);
                        btnAddQuestion.setColorFilter(Color.parseColor("#AEAEAE"));//question in editable mode
                        happen = true;
                    } else {
                        CommonMethods.closeKeyboard(getActivity());
                        etdAddQuestion.setEnabled(false);
                        btnAddQuestion.setImageResource(R.drawable.editfeatured_color);//@PKDEC06 - Actual = editfeatured
                        //                    btnAddQuestion.setColorFilter(ContextCompat.getColor(getActivity(), R.color.pink));
                        btnAddQuestion.setColorFilter(android.R.color.transparent);//question in non-editable mode
                        if (etdAddQuestion.getText().toString().length() > 0) {
                            // addViewOne.setClickable(true);
                            // addViewOne.setBackgroundColor(Color.parseColor("#f26f71"));
                        }
                        happen = false;

                        //@PKDEC07
                        if (addFirstPickleView.getVisibility() == View.VISIBLE) {
//                            addViewOne.setScaleX(1);
//                            addViewOne.setScaleY(1);
                            addViewOne.animate().alpha(0.5f)//.scaleX(0.5f).scaleY(0.5f).translationY(-5).translationX(-5)
                                    .setDuration(500)
                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animator) {
                                            addViewOne.animate().alpha(1)//.scaleX(1).scaleY(1).translationY(0).translationX(0)
                                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                                    .setDuration(500)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animator) {

                                        }
                                    }).start();
                        } else if (addSecondPickleView.getVisibility() == View.VISIBLE) {
                            addViewTwo.animate().alpha(0.5f).setDuration(500)
                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animator) {
                                            addViewTwo.animate().alpha(1)
                                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                                    .setDuration(500)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animator) {

                                        }
                                    }).start();
                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Add Your Question !", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.addImageOne:
                whichOne = "IMAGE_ONE";
                //@PKNOV29
                stringFirstText = null;
//                stringSecondText = null;
                //@PKDEC07
//                firstCaption.requestFocus();
//                CommonMethods.showKeyboard(getActivity(), firstCaption);
                //----------------------
                showDialog();
                break;
            case R.id.addTextOne:
                addFirstPickleView.setVisibility(View.GONE);
                enterFirstText.setVisibility(View.VISIBLE);
                enterFirstText.setEnabled(true);
                enterFirstText.requestFocus();
                //@PKDEC07
                CommonMethods.showKeyboardForcefully(getActivity(), enterFirstText);

                //@PKDEC06
                btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                // btnDoneFirstPickleView.setVisibility(View.VISIBLE);
                //@PKNOV29
                tempPath1 = null;
//                tempPath2 = null;
                firstCaption.setText("");
//                secondCaption.setText("");
                //----------------------
                // String message1 = "Pending !";
                // showNoFeedsDialog(message1);
                break;

            case R.id.btnDoneFirstPickleView:
                String firstString = enterFirstText.getText().toString();
                setEnterFirstText.setVisibility(View.VISIBLE);
                setEnterFirstText.setText(firstString);
                btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                btnDoneFirstPickleView.setVisibility(View.GONE);
                enterFirstText.setVisibility(View.GONE);
                // addViewTwo.setBackgroundColor(Color.parseColor("#f26f71"));
                // addViewTwo.setEnabled(true);
                break;
            case R.id.btnCancelFirstPickleView:
                enterFirstText.setVisibility(View.GONE);
                addFirstPickleView.setVisibility(View.VISIBLE);
                btnCancelFirstPickleView.setVisibility(View.GONE);
                setEnterFirstText.setVisibility(View.GONE);
                enterFirstText.setText("");
                setEnterFirstText.setText("");
                addFirstImageWithCaption.setVisibility(View.GONE);
                addViewOne.setBackgroundColor(Color.parseColor("#c0c0c0"));

                //@PKDEC07
                tempPath1 = null;
                firstCaption.setText("");

                break;
            case R.id.addImageTwo:
                //@PKNOV29
//                stringFirstText = null;
                stringSecondText = null;
                //----------------------
                whichOne = "IMAGE_TWO";
                showDialog();
                break;
            case R.id.addTextTwo:
                addSecondPickleView.setVisibility(View.GONE);
                enterSecondText.setVisibility(View.VISIBLE);
                enterSecondText.setEnabled(true);
                enterSecondText.requestFocus();

                //@PKDEC07
                CommonMethods.showKeyboardForcefully(getActivity(), enterSecondText);
                //@PKDEC06
                btnCancelSecondPickleView.setVisibility(View.VISIBLE);
                // btnDoneSecondPickleView.setVisibility(View.VISIBLE);

                // String message2 = "Pending !";
                // showNoFeedsDialog(message2);
                //@PKNOV29
                tempPath2 = null;
//                tempPath2 = null;
                secondCaption.setText("");
//                secondCaption.setText("");
                //----------------------
                break;

            case R.id.btnDoneSecondPickleView:
                String SecondString = enterFirstText.getText().toString();
                setEnterSecondText.setVisibility(View.VISIBLE);
                setEnterSecondText.setText(SecondString);
                btnCancelSecondPickleView.setVisibility(View.VISIBLE);
                btnDoneSecondPickleView.setVisibility(View.GONE);
                enterSecondText.setVisibility(View.GONE);
                // addViewTwo.setBackgroundColor(Color.parseColor("#f26f71"));
                // addViewTwo.setEnabled(true);
                break;
            case R.id.btnCancelSecondPickleView:
                enterSecondText.setVisibility(View.GONE);
                addSecondPickleView.setVisibility(View.VISIBLE);
                btnCancelSecondPickleView.setVisibility(View.GONE);
                setEnterSecondText.setVisibility(View.GONE);
                enterSecondText.setText("");
                setEnterSecondText.setText("");
                addSecondImageWithCaption.setVisibility(View.GONE);
                addViewTwo.setBackgroundColor(Color.parseColor("#c0c0c0"));

                //@PKDEC07
                tempPath2 = null;
                secondCaption.setText("");
                break;
            case R.id.btnShareOnPickle:

                stringFirstText = enterFirstText.getText().toString();
                stringSecondText = enterSecondText.getText().toString();

                if (isInternetPresent) {

                    if (etdAddQuestion.getText().toString().length() < 1) {
                        String text = "Add Your Question !";
                        showNoFeedsDialog(text);
                    } else if (tempPath1 == null && stringFirstText.length() < 1) {
                        String text = "Add first image or text !";
                        showNoFeedsDialog(text);
                    } else if (tempPath2 == null && stringSecondText.length() < 1) {
                        String text = "Add second image or text !";
                        showNoFeedsDialog(text);
                    } else if (tempPath1 != null
                            && firstCaption.getText().toString().length() < 1
                            && stringFirstText.length() < 1) {
                        String text = "Add caption for first image  !";
                        showNoFeedsDialog(text);
                    } else if (tempPath2 != null
                            && secondCaption.getText().toString().length() < 1
                            && stringSecondText.length() < 1) {
                        String text = "Add caption for second image  !";
                        showNoFeedsDialog(text);
                    } else if (tempPath1 == null && tempPath2 == null
                            && stringFirstText.length() > 0
                            && stringSecondText.length() > 0) {

                        ringProgressDialog = ProgressDialog.show(
                                getActivity(), "",
                                "Creating Pickle ...", true);
                        ringProgressDialog.setCancelable(true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    addPickleWithTexts(HomeScreen.user_id,
                                            etdAddQuestion.getText().toString(),
                                            stringFirstText, stringSecondText,
                                            HomeScreen.authentication_token,
                                            HomeScreen.key);

                                } catch (Exception e) {

                                }
                            }
                        }).start();

                    } else if (tempPath1 != null && tempPath2 != null
                            && stringFirstText.length() < 1
                            && stringSecondText.length() < 1) {
                        ringProgressDialog = ProgressDialog.show(
                                getActivity(), "",
                                "Creating Pickle ...", true);
                        ringProgressDialog.setCancelable(true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    addPickleWithImages(HomeScreen.user_id,
                                            HomeScreen.authentication_token,
                                            HomeScreen.key, etdAddQuestion
                                                    .getText().toString(),
                                            tempPath1, firstCaption.getText()
                                                    .toString(), tempPath2,
                                            secondCaption.getText().toString());
                                } catch (Exception e) {

                                }
                            }
                        }).start();
                    } else if (tempPath1 != null && tempPath2 == null
                            && stringFirstText.length() < 1
                            && stringSecondText.length() > 0) {

                        ringProgressDialog = ProgressDialog.show(
                                getActivity(), "",
                                "Creating Pickle ...", true);
                        ringProgressDialog.setCancelable(true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    addPickleWithImageFirstAndTextSecond(
                                            HomeScreen.user_id, etdAddQuestion
                                                    .getText().toString(),
                                            tempPath1, firstCaption.getText()
                                                    .toString(), stringSecondText,
                                            HomeScreen.authentication_token,
                                            HomeScreen.key);
                                } catch (Exception e) {

                                }
                            }
                        }).start();
                    } else if (tempPath1 == null && tempPath2 != null
                            && stringFirstText.length() > 0
                            && stringSecondText.length() < 1) {
                        ringProgressDialog = ProgressDialog.show(
                                getActivity(), "",
                                "Creating Pickle ...", true);
                        ringProgressDialog.setCancelable(true);
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    addPickleWithImageSecondAndTextFirst(
                                            HomeScreen.user_id, etdAddQuestion
                                                    .getText().toString(),
                                            stringFirstText, tempPath2,
                                            secondCaption.getText().toString(),
                                            HomeScreen.authentication_token,
                                            HomeScreen.key);
                                } catch (Exception e) {

                                }
                            }
                        }).start();
                    }
                } else {
                    String messageNoInternet = "No Internet Connection!";
                    showNoFeedsDialog(messageNoInternet);
                }

                break;

            case R.id.open_Camera:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = new File(Environment.getExternalStorageDirectory(),
                        "tmp_avatar_" + String.valueOf(System.currentTimeMillis())
                                + ".jpg");
                mImageCaptureUri = Uri.fromFile(file);
                // cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                // mImageCaptureUri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                dialog.dismiss();
                break;
            case R.id.open_Gallery:
                Intent galleryIntent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(galleryIntent, "Select File"),
                        GALLERY_REQUEST);

                dialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void showDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_camera_gallery);
        dialog.setCanceledOnTouchOutside(false);

        dialog.findViewById(R.id.open_Camera).setOnClickListener(this);
        dialog.findViewById(R.id.open_Gallery).setOnClickListener(this);

        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                if (data != null) {
                    if (whichOne.equals("IMAGE_ONE")) {

                        photo1 = (Bitmap) data.getExtras().get("data");
                        firstImage.setImageBitmap(photo1);
                        addFirstPickleView.setVisibility(View.GONE);
                        addFirstImageWithCaption.setVisibility(View.VISIBLE);
                        btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                        addViewOne.setBackgroundColor(Color.parseColor("#F0F0F0"));

                        selectedImageUri1 = getImageUri(getActivity(), photo1);
                        tempPath1 = getPath(selectedImageUri1, getActivity());
                        firstCaption.requestFocus();
                        //@PKDEC07
                        CommonMethods.showKeyboardForcefully(getActivity(), firstCaption);
                    } else {
                        photo2 = (Bitmap) data.getExtras().get("data");
                        secondImage.setImageBitmap(photo2);
                        addSecondPickleView.setVisibility(View.GONE);
                        addSecondImageWithCaption.setVisibility(View.VISIBLE);
                        btnCancelSecondPickleView.setVisibility(View.VISIBLE);
                        addViewTwo.setBackgroundColor(Color.parseColor("#F0F0F0"));

                        selectedImageUri2 = getImageUri(getActivity(), photo2);
                        tempPath2 = getPath(selectedImageUri2, getActivity());
                        secondCaption.requestFocus();
                        //@PKDEC07
                        CommonMethods.showKeyboardForcefully(getActivity(), secondCaption);
                    }
                }

            } else if (requestCode == GALLERY_REQUEST) {

                if (whichOne.equals("IMAGE_ONE")) {
                    selectedImageUri1 = data.getData();
                    tempPath1 = getPath(selectedImageUri1, getActivity());
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                    photo1 = BitmapFactory.decodeFile(tempPath1, btmapOptions);
                    firstImage.setImageBitmap(photo1);
                    addFirstPickleView.setVisibility(View.GONE);
                    addFirstImageWithCaption.setVisibility(View.VISIBLE);
                    btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                    addViewOne.setBackgroundColor(Color.parseColor("#F0F0F0"));
                    firstCaption.requestFocus();
                    //@PKDEC07
                    CommonMethods.showKeyboardForcefully(((HomeScreen) getActivity()), firstCaption);
                    firstCaption.requestFocus();
                } else {
                    selectedImageUri2 = data.getData();
                    tempPath2 = getPath(selectedImageUri2, getActivity());
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                    photo2 = BitmapFactory.decodeFile(tempPath2, btmapOptions);
                    secondImage.setImageBitmap(photo2);
                    addSecondPickleView.setVisibility(View.GONE);
                    addSecondImageWithCaption.setVisibility(View.VISIBLE);
                    btnCancelSecondPickleView.setVisibility(View.VISIBLE);
                    addViewTwo.setBackgroundColor(Color.parseColor("#F0F0F0"));
                    secondCaption.requestFocus();
                    //@PKDEC07

                    CommonMethods.showKeyboardForcefully(getActivity(), secondCaption);
                }
            }
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri, Activity context) {
        String[] projection = {MediaColumns.DATA};
        Cursor cursor = context.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void addPickleWithImages(String user_id,
                                     final String authentication_token, final String key,
                                     String strQuetion, String path1, String strFirstCaption,
                                     String path2, String strSecondCaption) {

        File firstFile = new File(path1);
        File secondFile = new File(path2);

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(URL_FEED);
            FileBody image1 = new FileBody(firstFile);
            FileBody image2 = new FileBody(secondFile);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", image1);
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][1][option_image]", image2);
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[status]", new StringBody("Approved"));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void parseResponse(String response_str) {
        Log.e("response_str", "" + response_str);
        try {
            JSONObject objMain = new JSONObject(response_str);
            JSONObject objPickle = objMain.getJSONObject("pickle");
            String pickle_id = objPickle.getString("id");
            String pickle_name = objPickle.getString("name");

            ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
            Bundle args = new Bundle();
            args.putString("PICKLE_ID", pickle_id);
            args.putString("PICKLE_NAME", pickle_name);
            args.putString("FROM", "CREATE");

            ShowPickleFragmentobj.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
            transaction.addToBackStack(null);
            transaction.commit();

//			Intent showPickleIntent = new Intent(getActivity(), ShowPickleFragment.class);
//			showPickleIntent.putExtra("PICKLE_ID", pickle_id);
//			showPickleIntent.putExtra("PICKLE_NAME", pickle_name);
//			showPickleIntent.putExtra("FROM", "CREATE");
//			startActivity(showPickleIntent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addPickleWithImageFirstAndTextSecond(String user_id,
                                                      String strQuetion, String path1, String strFirstCaption,
                                                      String stringSecondText, final String authentication_token,
                                                      final String key) {

        File firstFile = new File(path1);

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(URL_FEED);
            FileBody image1 = new FileBody(firstFile);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", image1);
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][1][option_text]", new StringBody(stringSecondText));
            reqEntity.addPart("question[status]", new StringBody("Approved"));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void addPickleWithImageSecondAndTextFirst(String user_id,
                                                      String strQuetion, String stringFirstText, String path2,
                                                      String strSecondCaption, final String authentication_token,
                                                      final String key) {

        File secondFile = new File(path2);

        try {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(URL_FEED);
            FileBody image2 = new FileBody(secondFile);
            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_text]", new StringBody(stringFirstText));
            reqEntity.addPart("question[options_attributes][1][option_image]", image2);
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[status]", new StringBody("Approved"));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void addPickleWithTexts(String user_id, String strQuetion,
                                    String stringFirstText, String stringSecondText,
                                    final String authentication_token, final String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramUserId = params[0];
                String paramQuestion = params[1];
                String paramFirstText = params[2];
                String paramSecondText = params[3];
                String paramAuthToken = params[4];
                String paramKey = params[5];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FEED);

                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("question[user_id]", paramUserId);
                BasicNameValuePair questionBasicNameValuePair = new BasicNameValuePair("question[question]", paramQuestion);
                BasicNameValuePair firstTextBasicNameValuePair = new BasicNameValuePair("question[options_attributes][0][option_text]", paramFirstText);
                BasicNameValuePair secondTextBasicNameValuePair = new BasicNameValuePair("question[options_attributes][1][option_text]", paramSecondText);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(questionBasicNameValuePair);
                nameValuePairList.add(firstTextBasicNameValuePair);
                nameValuePairList.add(secondTextBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);

                    Log.e("nameValuePairList", "" + nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                ringProgressDialog.dismiss();
                parseResponse(result);
            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(user_id, strQuetion, stringFirstText,
                stringSecondText, authentication_token, key);

    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

}
