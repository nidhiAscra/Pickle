package com.ascra.pickle;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ascra.data.Fb_friendlistAdapter;
import com.ascra.data.Fb_model;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Fb_friendlist extends AlertDialog {
    Context context;
    String uid, qid;
    int option_id;
    loadingfblist loading_list;
    GridView fb_list;
    String jsonStr;
    ArrayList<Fb_model> fb_friend_list;
    Fb_friendlistAdapter friendlistadapter;
    String one_or_two;
    String friend_name_to_pass, friend_image_to_pass, id_user;
    passselected pass;
    ProgressBar pbar;

    public Fb_friendlist(Context context, int option_id, String qid,
                         String id_one, passselected pass, String one_or_two) {
        super(context);
        this.context = context;
        this.option_id = option_id;
        this.uid = id_one;
        this.qid = qid;
        this.pass = pass;
        this.one_or_two = one_or_two;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fb_friendlist);
        fb_list = (GridView) findViewById(R.id.friendlist);
        pbar = (ProgressBar) findViewById(R.id.fb_progress);
        fb_friend_list = new ArrayList<Fb_model>();
        loading_list = new loadingfblist();
        loading_list.execute();

        fb_list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                friend_name_to_pass = fb_friend_list.get(position).getFriend_name();
                friend_image_to_pass = fb_friend_list.get(position).getUrl();
                //	id_user = fb_friend_list.get(position).getFrind_id();
                //pass.setvalue(friend_image_to_pass, friend_name_to_pass, id_user,one_or_two,option_id);
                pass.setvalue(friend_image_to_pass, friend_name_to_pass, one_or_two, "" + option_id);
                dismiss();
            }
        });

    }

    public interface passselected {

        public void setvalue(String a, String b, String c, String d);

    }

    class loadingfblist extends AsyncTask<Void, Void, Void> {

        String url_fblist = "";

        public loadingfblist() {
            super();
            //url_fblist="https://graph.facebook.com/100001457625947/friends?access_token=CAAGic0KB4ncBAFj7sYdegrtluMjfzse3vd2QDfw3v3aF11UojMVTkLXhnQz3dkrrgRJbUH3WLB82fssZBLkUysduXZAYHbYUz4ykgj8beLWtTmZBveV8Vy7wPwyQ2WuMiowJwQQRdPqMiXcvkZAo6PLgIKbJ9EonkZAvcZC3uqpdm9bSsWZBEgL55iReAerljfUzRFyScs4D8kuDECkZBczP&limit=5000
            url_fblist = Constant.PickleUrl + "fetch_fb_friends.json?"
                    + "id=" + one_or_two + "&option_index=" + option_id + "&uid="
                    + uid;
            Log.e("fblist:", url_fblist);
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pbar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();
            params1.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params1.add(new BasicNameValuePair("key", HomeScreen.key));

            jsonStr = sh.makeServiceCall(url_fblist, ServiceHandler.GET, params1);

            if (jsonStr != null) {
                try {
                    Log.e("fb_list", "" + jsonStr.toString());
                    JSONObject fb_object = new JSONObject(jsonStr);
                    JSONArray fb_array = fb_object.getJSONArray("fb_friends");

                    for (int i = 0; i < fb_array.length(); i++) {
                        Fb_model fb_model = new Fb_model();
                        JSONObject actor = fb_array.getJSONObject(i);
                        String url = actor.getString("fb_image_url");
                        String name = actor.getString("friend_name");
                        //String id = actor.getString("friend_id");
                        fb_model.setFriend_name(name);
                        fb_model.setUrl(url);
                        //fb_model.setFrind_id(id);
                        fb_friend_list.add(fb_model);

                    }

                    Log.e("list size:", "" + fb_friend_list.size());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            super.onPostExecute(result);
//@PKDEC09 -----------------------------------------------------------------------------------------
            //noinspection ConstantConditions
            if (fb_friend_list == null || (fb_friend_list != null && fb_friend_list.isEmpty())) {
                dismiss();
                Toast.makeText(getContext(), "You don't have any friends!", Toast.LENGTH_SHORT).show();
            } else {
// -------------------------------------------------------------------------------------------------
                friendlistadapter = new Fb_friendlistAdapter(context, fb_friend_list);
                fb_list.setAdapter(friendlistadapter);
                pbar.setVisibility(View.GONE);
            }
        }

    }

}
