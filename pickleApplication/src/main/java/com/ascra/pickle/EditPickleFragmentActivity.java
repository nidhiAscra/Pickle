package com.ascra.pickle;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.imageloader.ImageLoader;
import com.ascra.pickle.Fb_friendlist.passselected;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditPickleFragmentActivity extends Fragment implements OnClickListener {

    private static final int CAMERA_REQUEST = 1;
    private static final int GALLERY_REQUEST = 2;
    static String whichOne = null;
    View view;
    // add quesion?.
    EditText etdAddQuestion;
    ImageView btnAddQuestion;
    // main view;
    LinearLayout addView;
    // add firstView.
    String image_url1, user_name1, one_Or_two;
    LinearLayout addFirstPickleView;
    RelativeLayout addViewOne, addImageOne, addTextOne;
    EditText enterFirstText;
    TextView setEnterFirstText;
    ImageView btnCancelFirstPickleView, btnDoneFirstPickleView;
    RelativeLayout addFirstImageWithCaption;
    ImageView firstImage;
    EditText firstCaption;
    String load_fb1, load_fb2;
    // add secondView.
    LinearLayout addSecondPickleView;
    RelativeLayout addViewTwo, addImageTwo, addTextTwo;
    EditText enterSecondText;
    TextView setEnterSecondText;
    ImageView btnCancelSecondPickleView, btnDoneSecondPickleView;
    RelativeLayout addSecondImageWithCaption;
    ImageView secondImage;
    EditText secondCaption;
    // Share Button
    Button btnShareOnPickle;
    FrameLayout inProcess;
    boolean happen = false;
    boolean whenCreatePickle = false;
    Dialog dialog;
    Bitmap photo1, photo2;
    Uri selectedImageUri1, selectedImageUri2, mImageCaptureUri;
    HttpEntity resEntity;
    String tempPath1, tempPath2;
    String edittempPath1, edittempPath2;
    Uri editselectedImageUri1, editselectedImageUri2;
    ProgressDialog barProgressDialog;
    Handler updateBarHandler;
    ImageView editFirst, editSecond;
    String pickleEditQuestion, editQuestion, editFirstImage, editSecondImage,
            editFirstText, editSecondText, editFirstLable, editSecondLabel,
            edit_pickle_id, edit_user_id, opid_one, opid_two;
    String user_one_id, user_two_id, user_one_name, user_two_name, user_one_url, user_two_url;
    String AUTH, KEY;
    String editPickleId, editPickleName, user_id;
    ImageLoader imageLoader;
    byte[] friend_image1, friend_image2;
    ArrayList<String> imagesList = new ArrayList<String>();
    ArrayList<String> labelList = new ArrayList<String>();
    ArrayList<String> textList = new ArrayList<String>();
    ArrayList<String> idsList = new ArrayList<String>();
    ProgressDialog ringProgressDialog1, ringProgressDialog2;
    Uri myUri;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    String jsonStr1;
    Tracker tracker;
    JSONObject mainObject;
    Typeface face;
    ImageView back;
    String id_one, id_two, flag;
    View v;
    boolean isfeatured, fetured_pickle;
    private String URL;
    private String UPDATE_URL_FEED;
    private String URL_FEED = Constant.PickleUrl + "questions.json";

    // @PKDEC09
    private boolean fbDataExists = false;

    private void setUpViews() {

//        try {
//            ActionBar mActionBar = getActivity().getActionBar();
//            mActionBar.hide();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//		mActionBar.setDisplayShowHomeEnabled(false);
//		mActionBar.setDisplayShowTitleEnabled(false);
//
//		LayoutInflater mInflater = LayoutInflater.from(getActivity());
//		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//		View mCustomView = mInflater.inflate(R.layout.pickle_custom_actionbar,
//				null);
//		ImageView menuButton = (ImageView) mCustomView
//				.findViewById(R.id.homeButtonaction);
//		ImageView pickleBundle = (ImageView) mCustomView
//				.findViewById(R.id.pickleBundleacttion);
//	    menuButton.setVisibility(View.GONE);
//		pickleBundle.setVisibility(View.GONE);
//		mActionBar.setCustomView(mCustomView);
//		mActionBar.setDisplayShowCustomEnabled(true);

        // MainView;
        addView = (LinearLayout) v.findViewById(R.id.addView);
        // addquestion;
        etdAddQuestion = (EditText) v.findViewById(R.id.editQuestion);
        // etdAddQuestion.setEnabled(false);
        etdAddQuestion.setTypeface(face);
        btnAddQuestion = (ImageView) v.findViewById(R.id.btnAddQuestion);
        // first_View;
        addViewOne = (RelativeLayout) v.findViewById(R.id.addViewOne);
        // addViewOne.setClickable(false);
        addFirstPickleView = (LinearLayout) v.findViewById(R.id.addFirstPickleView);
        addImageOne = (RelativeLayout) v.findViewById(R.id.addImageOne);
        addTextOne = (RelativeLayout) v.findViewById(R.id.addTextOne);
        enterFirstText = (EditText) v.findViewById(R.id.enterFirstText);
        enterFirstText.setTypeface(face);
        setEnterFirstText = (TextView) v.findViewById(R.id.setEnterFirstText);
        setEnterFirstText.setTypeface(face);
        btnCancelFirstPickleView = (ImageView) v.findViewById(R.id.btnCancelFirstPickleView);
        btnDoneFirstPickleView = (ImageView) v.findViewById(R.id.btnDoneFirstPickleView);
        addFirstImageWithCaption = (RelativeLayout) v.findViewById(R.id.addFirstImageWithCaption);
        firstImage = (ImageView) v.findViewById(R.id.FirstImage);
        firstCaption = (EditText) v.findViewById(R.id.etdCaptionFirst);
        firstCaption.setTypeface(face);
        // SecondView;
        addViewTwo = (RelativeLayout) v.findViewById(R.id.addViewTwo);
        addSecondPickleView = (LinearLayout) v.findViewById(R.id.addSecondPickleView);
        addImageTwo = (RelativeLayout) v.findViewById(R.id.addImageTwo);
        addTextTwo = (RelativeLayout) v.findViewById(R.id.addTextTwo);
        enterSecondText = (EditText) v.findViewById(R.id.enterSecondText);
        enterSecondText.setTypeface(face);
        setEnterSecondText = (TextView) v.findViewById(R.id.setEnterSecondText);
        setEnterSecondText.setTypeface(face);
        btnCancelSecondPickleView = (ImageView) v.findViewById(R.id.btnCancelSecondPickleView);
        btnDoneSecondPickleView = (ImageView) v.findViewById(R.id.btnDoneSecondPickleView);
        addSecondImageWithCaption = (RelativeLayout) v.findViewById(R.id.addSecondImageWithCaption);
        secondImage = (ImageView) v.findViewById(R.id.SecondImage);
        secondCaption = (EditText) v.findViewById(R.id.etdCaptionSecond);
        secondCaption.setTypeface(face);
        // shareButton;
        btnShareOnPickle = (Button) v.findViewById(R.id.btnShareOnPickle);

        btnShareOnPickle.setTypeface(face);

        editFirst = (ImageView) v.findViewById(R.id.editFirst);
        editSecond = (ImageView) v.findViewById(R.id.editSecond);

        editQuestion = etdAddQuestion.getText().toString();
        editFirstLable = firstCaption.getText().toString();
        editSecondLabel = secondCaption.getText().toString();

        btnAddQuestion.setOnClickListener(this);
        addImageOne.setOnClickListener(this);
        addTextOne.setOnClickListener(this);
        btnCancelFirstPickleView.setOnClickListener(this);
        btnDoneFirstPickleView.setOnClickListener(this);

        addImageTwo.setOnClickListener(this);
        addTextTwo.setOnClickListener(this);
        btnShareOnPickle.setOnClickListener(this);

        editFirst.setOnClickListener(this);
        editSecond.setOnClickListener(this);

        editFirstLable = firstCaption.getText().toString();
        editSecondLabel = firstCaption.getText().toString();

        CommonMethods.setLogoTitleToHeaderView(getActivity(), v, R.string.editPickleHeader, R.drawable.make_pickle_icon);
    }
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		// 
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.new_edit_pickle);
//		// friend_imageview1=(ImageView)v.findViewById(R.id.userone);

    // friend_imageview2=(ImageView)v.findViewById(R.id.usertwo);

//		HomeScreen.whichFragment = "CREATEPICKLE";
//		updateBarHandler = new Handler();
//		imageLoader = new ImageLoader(getActivity().getApplicationContext());
//
//		Bundle extras = getIntent().getExtras();
//		try {
//
//			if (extras != null) {
//				editPickleId = extras.getString("PICKLE_ID");
//				editPickleName = extras.getString("PICKLE_NAME");
//				AUTH = extras.getString("AUTH");
//				KEY = extras.getString("KEY");
//				user_id = extras.getString("USERID");
//				user_one_id = extras.getString("USER_ONE_ID");
//				user_one_name = extras.getString("USER_ONE_NAME");
//
//				user_two_id = extras.getString("USER_TWO_ID");
//				user_two_name = extras.getString("USER_TWO_NAME");
//				flag = extras.getString("FLAG");
//
//				// intent.putExtra("USER_ONE_ID",listOfFeaturedPickleData.get(position).getOption_one_fb_id());
//				// intent.putExtra("USER_ONE_NAME",listOfFeaturedPickleData.get(position).getOption_one_name()
//				// );
//				//
//				// intent.putExtra("USER_TWO_ID",
//				// listOfFeaturedPickleData.get(position).getOption_two_fb_id());
//				// intent.putExtra("USER_TWO_NAME",listOfFeaturedPickleData.get(position).getOption_one_name()
//				// );
//				// friend_image1=extras.getByteArray("FRIEND_IMAGE1");
//				// Bitmap bitmap1 = BitmapFactory.decodeByteArray(friend_image1
//				// , 0, friend_image1.length);
//				// friend_imageview1.setImageBitmap(bitmap1);
//
//				/*
//				 * friend_image2=extras.getByteArray("FRIEND_IMAGE2"); Bitmap
//				 * bitmap2 = BitmapFactory.decodeByteArray(friend_image2 , 0,
//				 * friend_image2.length);
//				 * friend_imageview2.setImageBitmap(bitmap2);
//				 */
//
//			}
//
//		} catch (NullPointerException e) {
//			// 
//			e.printStackTrace();
//		}
//
//		URL = Constant.PickleUrl + "questions/" + editPickleId + "-"
//				+ editPickleName.replace(" ", "-") + "/edit.json";
//		// Log.e("amol",""+URL);
//
//		cd = new ConnectionDetector(getActivity());
//		isInternetPresent = cd.isConnectingToInternet();
//
//		if (flag.equals("0")) {
//			if (isInternetPresent) {
//				ringProgressDialog1 = ProgressDialog.show(
//						getActivity(), "",
//						"Loading Pickle ...", true);
//				ringProgressDialog1.setCancelable(true);
//				runOnUiThread(new Runnable() {
//
//					@Override
//					public void run() {
//						// 
//						new createpickle(user_one_id, user_one_name,
//								user_two_id, user_two_name, edit_pickle_id)
//								.execute();
//					}
//				});
//
//			} else {
//				String text = "No Internet Connection!";
//				showNoFeedsDialog(text);
//			}
//		} else {
//			new Thread(new Runnable() {
//				@Override
//				public void run() {
//					try {
//						new load_pickle_data().execute();
//					} catch (Exception e) {
//
//					}
//				}
//			}).start();
//		}
//		face = Typeface.createFromAsset(getAssets(),
//				"fonts/sniglet-regular.ttf");
//
//		setUpViews();
//		setTracker();
//	}

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "CREATE PICKLE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void load_user1_data(final String image_url, final String name,
                                 final String one_or_two, String option) {

        try {

            String option_image_url = "" + image_url;
            String option_text = "" + name;
            String option_label = "" + name;

            Log.e("passed url:", image_url);
            Log.e("passed url:", name);
            //	Log.e("passed url:", id);
            imagesList.add(option_image_url);
            labelList.add(option_label);
            textList.add(option_text);
            //idsList.add(id);

            if (option.equals("0")) {
                addFirstImageWithCaption.setVisibility(View.VISIBLE);
                addFirstPickleView.setVisibility(View.GONE);
                // imageLoader.DisplayImage(imagesList.get(0), firstImage);
                firstCaption.setText(name);
                Picasso.with(getActivity()).load(image_url).into(firstImage);

                //@PKDEC07
                firstCaption.setSelection(name.trim().length());
            } else {
                if (option.equals("1")) {
                    addSecondImageWithCaption.setVisibility(View.VISIBLE);
                    addSecondPickleView.setVisibility(View.GONE);
                    secondCaption.setText(name);
                    Picasso.with(getActivity()).load(image_url).into(secondImage);

                    //@PKDEC07
                    secondCaption.setSelection(name.trim().length());
                }
            }
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // 

                    Update_User_Profile user_profile = new Update_User_Profile(
                            image_url, name, one_or_two);
                    user_profile.execute();
                }
            });

        } catch (Exception e) {
            // 
        }

    }

    private void parseJSONUserProfile(JSONObject mainObject) {
        try {
            Log.e("EditPickle", "Parse response = " + mainObject);
            JSONArray optionArray = mainObject.getJSONArray("options");
            for (int optionItem = 0; optionItem < optionArray.length(); optionItem++) {
                JSONObject objOptions = optionArray.getJSONObject(optionItem);
                String option_image_url = objOptions.getString("option_image_url");
                String option_text = objOptions.getString("option_text");
                String option_label = objOptions.getString("option_label");
                String id = objOptions.getString("id");

                imagesList.add(option_image_url);
                labelList.add(option_label);
                textList.add(option_text);
                idsList.add(id);
            }

            JSONObject objpickle = mainObject.getJSONObject("pickle");
            editQuestion = objpickle.getString("name");
            edit_user_id = objpickle.getString("user_id");
            edit_pickle_id = objpickle.getString("id");

            if (editQuestion != null) {
                etdAddQuestion.setText(editQuestion);
                //@PKDEC07
                etdAddQuestion.setSelection(editQuestion.trim().length());
                pickleEditQuestion = null;
            }

            if (idsList.get(0) != null) {
                opid_one = idsList.get(0);
            }

            if (idsList.get(1) != null) {
                opid_two = idsList.get(1);
            }
            if (imagesList.get(0) != null) {
                addFirstImageWithCaption.setVisibility(View.VISIBLE);
                addFirstPickleView.setVisibility(View.GONE);
                //imageLoader.DisplayImage(imagesList.get(0), firstImage);
                Picasso.with(getActivity())
                        .load(imagesList.get(0))
                        .into(firstImage);
                firstCaption.setText(labelList.get(0));
                //@PKDEC07
                firstCaption.setSelection(labelList.get(0).trim().length());

                tempPath1 = "http://54.251.33.194:8800/assets/missing.jpg";
                editFirstLable = labelList.get(0);

            }

            if (imagesList.get(1) != null) {
                addSecondImageWithCaption.setVisibility(View.VISIBLE);
                addSecondPickleView.setVisibility(View.GONE);
                //imageLoader.DisplayImage(imagesList.get(1), secondImage);

                Picasso.with(getActivity()).load(imagesList.get(1)).into(secondImage);

                secondCaption.setText(labelList.get(1));
                //@PKDEC07
                secondCaption.setSelection(labelList.get(1).trim().length());

                tempPath2 = "http://54.251.33.194:8800/assets/missing.jpg";
                editSecondLabel = labelList.get(1);
            }

            Log.e("textList.get(0)", "" + textList.get(0));
            Log.e("textList.get(1)", "" + textList.get(1));

            if (textList.get(0) != null) {
                if (textList.get(0).equals("") || textList.get(0).equals("null")) {
                    // addFirstImageWithCaption.setVisibility(View.GONE);
                    addFirstPickleView.setVisibility(View.GONE);
                } else {
                    addFirstImageWithCaption.setVisibility(View.GONE);
                    addFirstPickleView.setVisibility(View.GONE);
                    enterFirstText.setVisibility(View.VISIBLE);
                    enterFirstText.setText(textList.get(0));
                    //@PKDEC07
                    enterFirstText.setSelection(textList.get(0).trim().length());
                }
            }

            if (textList.get(1) != null) {
                if (textList.get(1).equals("") || textList.get(1).equals("null")) {
                    // addSecondImageWithCaption.setVisibility(View.GONE);
                    addSecondPickleView.setVisibility(View.GONE);
                } else {
                    addSecondImageWithCaption.setVisibility(View.GONE);
                    addSecondPickleView.setVisibility(View.GONE);
                    enterSecondText.setVisibility(View.VISIBLE);
                    enterSecondText.setText(textList.get(1));
                    //@PKDEC07
                    enterSecondText.setSelection(textList.get(1).trim().length());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updatePickleWithOldImageWithNewText(String user_id,
                                                     String strQuetion, String path1, String strFirstCaption,
                                                     String strFirstText, final String opId1, String path2,
                                                     String strSecondCaption, String strSecondText, String opId2,
                                                     String status, final String authentication_token, final String key) {

        try {
            Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleWithOldImageWithNewText ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", path1.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path1) : new FileBody(new File(path1)));
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(getValueFor(strFirstCaption)));
            reqEntity.addPart("question[options_attributes][0][option_text]", new StringBody(getValueFor(strFirstText)));
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_image]", path2.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path2) : new FileBody(new File(path2)));
            //@PKDEC07 - changed, Actual - new StringBody("null")); - on ROR req
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(getValueFor(strSecondCaption)));
            reqEntity.addPart("question[options_attributes][1][option_text]", new StringBody(getValueFor(strSecondText)));
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + path1);
            Log.e("EditPickle", "question[options_attributes][0][option_label] ===== " + strFirstCaption);
            Log.e("EditPickle", "question[options_attributes][0][option_text] ====== " + strFirstText);
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + path2);
            Log.e("EditPickle", "question[options_attributes][1][option_label] ===== " + strSecondCaption);
            Log.e("EditPickle", "question[options_attributes][1][option_text] ====== " + strSecondText);
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
            ex.printStackTrace();
        }

    }

    private void updatePickleWithText(String user_id, String strQuetion,
                                      String strFirstText, final String opId1, String strSecondText,
                                      String opId2, String status, final String authentication_token,
                                      final String key) {

        try {
            Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleWithText ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_text]", new StringBody(strFirstText));
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_text]", new StringBody(strSecondText));
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + strFirstText);
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + strSecondText);
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
        }

    }

    private void updatePickleNew(String user_id, String strQuetion,
                                 String path1, String strFirstCaption, String strFirstText,
                                 final String opId1, String path2, String strSecondCaption,
                                 String strSecondText, String opId2, String status,
                                 final String authentication_token, final String key) {
        Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleNew ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        File firstFile = null;
        File secondFile = null;

        firstFile = new File(path1);
        secondFile = new File(path2);

        try {

            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);
            FileBody image1 = null;
            FileBody image2 = null;
            image1 = new FileBody(firstFile);
            image2 = new FileBody(secondFile);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", image1);
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][0][option_text]", new StringBody(""));//@PKDEC07 - changed, Actual - new StringBody("null")); - on ROR req
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_image]", image2);
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[options_attributes][1][option_text]", new StringBody(""));//@PKDEC07 - changed, Actual - new StringBody("null")); - on ROR req
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + firstFile);
            Log.e("EditPickle", "question[options_attributes][0][option_label] ===== " + strFirstCaption);
            Log.e("EditPickle", "question[options_attributes][0][option_text] ====== " + "null");
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + secondFile);
            Log.e("EditPickle", "question[options_attributes][1][option_label] ===== " + strSecondCaption);
            Log.e("EditPickle", "question[options_attributes][1][option_text] ====== " + "null");
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
        }

    }

    private void updatePickleEditFirstImage(String user_id, String strQuetion,
                                            String path1, String strFirstCaption, String strFirstText,
                                            final String opId1, String path2, String strSecondCaption,
                                            String opId2, String status, final String authentication_token,
                                            final String key) {
        Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleEditFirstImage ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        File firstFile = null;
        firstFile = new File(path1);
        try {

            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);
            FileBody image1 = null;
            image1 = new FileBody(firstFile);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", image1);
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][0][option_text]", new StringBody(""));//@PKDEC07 - changed, Actual - new StringBody("null")); - on ROR req
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_image]", path2.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path2) : new FileBody(new File(path2)));
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + firstFile);
            Log.e("EditPickle", "question[options_attributes][0][option_label] ===== " + strFirstCaption);
            Log.e("EditPickle", "question[options_attributes][0][option_text] ====== " + "null");
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + path2);
            Log.e("EditPickle", "question[options_attributes][1][option_label] ===== " + strSecondCaption);
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
        }

    }

    private void updatePickleEditSecondImage(String user_id, String strQuetion,
                                             String path1, String strFirstCaption, final String opId1,
                                             String path2, String strSecondCaption, String strSecondOption,
                                             String opId2, String status, final String authentication_token,
                                             final String key) {
        Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleEditSecondImage ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        File secondFile = null;
        secondFile = new File(path2);

        try {

            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);
            FileBody image2 = null;
            image2 = new FileBody(secondFile);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", path1.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path1) : new FileBody(new File(path1)));
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_image]", image2);
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[options_attributes][1][option_text]", new StringBody(""));//@PKDEC07 - changed, Actual - new StringBody("null")); - on ROR req
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + path1);
            Log.e("EditPickle", "question[options_attributes][0][option_label] ===== " + strFirstCaption);
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + secondFile);
            Log.e("EditPickle", "question[options_attributes][1][option_label] ===== " + strSecondCaption);
            Log.e("EditPickle", "question[options_attributes][1][option_text] ====== " + "null");
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
        }

    }

    private void updatePickleEdit(String user_id, String strQuetion,
                                  String path1, String strFirstCaption, final String opId1,
                                  String path2, String strSecondCaption, String opId2, String status,
                                  final String authentication_token, final String key) {

        try {
            Log.e("EditPickle", "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ updatePickleEdit ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            HttpClient client = new DefaultHttpClient();
            HttpPut post = new HttpPut(UPDATE_URL_FEED);

            MultipartEntity reqEntity = new MultipartEntity();

            reqEntity.addPart("question[question]", new StringBody(strQuetion));
            reqEntity.addPart("question[options_attributes][0][option_image]", path1.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path1) : new FileBody(new File(path1)));
            reqEntity.addPart("question[options_attributes][0][option_label]", new StringBody(strFirstCaption));
            reqEntity.addPart("question[options_attributes][0][id]", new StringBody(opId1));
            reqEntity.addPart("question[options_attributes][1][option_image]", path2.equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                    ? new StringBody(path2) : new FileBody(new File(path2)));
            reqEntity.addPart("question[options_attributes][1][option_label]", new StringBody(strSecondCaption));
            reqEntity.addPart("question[options_attributes][1][id]", new StringBody(opId2));
            reqEntity.addPart("question[status]", new StringBody(status));
            reqEntity.addPart("question[user_id]", new StringBody(user_id));
            reqEntity.addPart("authentication_token", new StringBody(authentication_token));
            reqEntity.addPart("key", new StringBody(key));

            Log.e("EditPickle", "UPDATE_URL_FEED ==================================================== " + UPDATE_URL_FEED);
            Log.e("EditPickle", "question[question] ================================ " + strQuetion);
            Log.e("EditPickle", "question[options_attributes][0][option_image] ===== " + path1);
            Log.e("EditPickle", "question[options_attributes][0][option_label] ===== " + strFirstCaption);
            Log.e("EditPickle", "question[options_attributes][0][id] =============== " + opId1);
            Log.e("EditPickle", "question[options_attributes][1][option_image] ===== " + path2);
            Log.e("EditPickle", "question[options_attributes][1][option_label] ===== " + strSecondCaption);
            Log.e("EditPickle", "question[options_attributes][1][id] =============== " + opId2);
            Log.e("EditPickle", "question[status] ================================== " + status);
            Log.e("EditPickle", "question[user_id] ================================= " + user_id);
            Log.e("EditPickle", "authentication_token ============================== " + authentication_token);
            Log.e("EditPickle", "key =============================================== " + key);

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post);
            resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);

            ringProgressDialog2.dismiss();
            parseResponse(response_str);

        } catch (Exception ex) {
            String text = "Something wrong can't update pickle !";
            exitDialog(text);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnAddQuestion:

                addView.animate().alpha(1).setDuration(500).start();
                if (!etdAddQuestion.getText().toString().trim().isEmpty()) {//@PKDEC09
                    if (!happen) {
                        etdAddQuestion.setEnabled(true);
                        etdAddQuestion.requestFocus();
                        // btnAddQuestion.setImageResource(R.drawable.editfeatured_color);
                        btnAddQuestion.setColorFilter(Color.parseColor("#AEAEAE"));//@PKDEC09 - question in editable mode
                        happen = true;
                    } else {
                        etdAddQuestion.setEnabled(false);
                        btnAddQuestion.setImageResource(R.drawable.editfeatured_color);//@PKDEC09 - Actual = editfeatured
                        if (etdAddQuestion.getText().toString().length() > 0) {
                            // addViewOne.setClickable(true);
                            // addViewOne.setBackgroundColor(Color.parseColor("#f26f71"));
                        }
                        happen = false;

                        //@PKDEC09
                        if (addFirstPickleView.getVisibility() == View.VISIBLE) {
//                            addViewOne.setScaleX(1);
//                            addViewOne.setScaleY(1);
                            addViewOne.animate().alpha(0.5f)//.scaleX(0.5f).scaleY(0.5f).translationY(-5).translationX(-5)
                                    .setDuration(500)
                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animator) {
                                        }
                                        @Override
                                        public void onAnimationEnd(Animator animator) {
                                            addViewOne.animate().alpha(1)//.scaleX(1).scaleY(1).translationY(0).translationX(0)
                                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                                    .setDuration(500)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animator) {

                                        }
                                    }).start();
                        } else if (addSecondPickleView.getVisibility() == View.VISIBLE) {
                            addViewTwo.animate().alpha(0.5f).setDuration(500)
                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                    .setListener(new Animator.AnimatorListener() {
                                        @Override
                                        public void onAnimationStart(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationEnd(Animator animator) {
                                            addViewTwo.animate().alpha(1)
                                                    .setInterpolator(new AccelerateDecelerateInterpolator())
                                                    .setDuration(500)
                                                    .start();
                                        }

                                        @Override
                                        public void onAnimationCancel(Animator animator) {

                                        }

                                        @Override
                                        public void onAnimationRepeat(Animator animator) {

                                        }
                                    }).start();
                        }

                    }
                } else {
                    Toast.makeText(getActivity(), "Add Your Question !", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.addImageOne:
                //@PKDEC09
                enterFirstText.setText("");
                whichOne = "IMAGE_ONE";
                showDialog();
                break;
            case R.id.addTextOne:
                addFirstPickleView.setVisibility(View.GONE);
                enterFirstText.setVisibility(View.VISIBLE);
                enterFirstText.setEnabled(true);
                enterFirstText.requestFocus();

                //@PKDEC09
                firstCaption.setText("");
                editFirstLable = "";
                tempPath1 = null;

                // btnDoneFirstPickleView.setVisibility(View.VISIBLE);

                // String message1 = "Pending !";
                // showNoFeedsDialog(message1);
                break;

            case R.id.btnDoneFirstPickleView:
                String firstString = enterFirstText.getText().toString();
                setEnterFirstText.setVisibility(View.VISIBLE);
                setEnterFirstText.setText(firstString);
                btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                btnDoneFirstPickleView.setVisibility(View.GONE);
                enterFirstText.setVisibility(View.GONE);
                // addViewTwo.setBackgroundColor(Color.parseColor("#f26f71"));
                // addViewTwo.setEnabled(true);
                break;
            case R.id.btnCancelFirstPickleView:
                enterFirstText.setVisibility(View.GONE);
                addFirstPickleView.setVisibility(View.VISIBLE);
                btnCancelFirstPickleView.setVisibility(View.GONE);
                setEnterFirstText.setVisibility(View.GONE);
                enterFirstText.setText("");
                setEnterFirstText.setText("");
                addFirstImageWithCaption.setVisibility(View.GONE);
                addViewOne.setBackgroundColor(Color.parseColor("#c0c0c0"));
                break;
            case R.id.addImageTwo:
                whichOne = "IMAGE_TWO";
                showDialog();

                //@PKDEC09
                enterSecondText.setText("");
                break;
            case R.id.addTextTwo:
                addSecondPickleView.setVisibility(View.GONE);
                enterSecondText.setVisibility(View.VISIBLE);
                enterSecondText.setEnabled(true);
                enterSecondText.requestFocus();

                //@PKDEC09
                secondCaption.setText("");
                editSecondLabel = "";
                tempPath2 = null;

                // btnDoneSecondPickleView.setVisibility(View.VISIBLE);

                // String message2 = "Pending !";
                // showNoFeedsDialog(message2);
                break;

            case R.id.btnDoneSecondPickleView:
                String SecondString = enterFirstText.getText().toString();
                setEnterSecondText.setVisibility(View.VISIBLE);
                setEnterSecondText.setText(SecondString);
                //btnCancelSecondPickleView.setVisibility(View.VISIBLE); // @PKDEC07 commented as we will not be showing xdelete icon
                btnDoneSecondPickleView.setVisibility(View.GONE);
                enterSecondText.setVisibility(View.GONE);
                // addViewTwo.setBackgroundColor(Color.parseColor("#f26f71"));
                // addViewTwo.setEnabled(true);
                break;
            case R.id.btnCancelSecondPickleView:
                enterSecondText.setVisibility(View.GONE);
                addSecondPickleView.setVisibility(View.VISIBLE);
                btnCancelSecondPickleView.setVisibility(View.GONE);
                setEnterSecondText.setVisibility(View.GONE);
                enterSecondText.setText("");
                setEnterSecondText.setText("");
                addSecondImageWithCaption.setVisibility(View.GONE);
                addViewTwo.setBackgroundColor(Color.parseColor("#c0c0c0"));
                break;
            case R.id.btnShareOnPickle:

                pickleEditQuestion = etdAddQuestion.getText().toString();
                editFirstLable = firstCaption.getText().toString();
                editSecondLabel = secondCaption.getText().toString();

                UPDATE_URL_FEED = Constant.PickleUrl + "questions/" + edit_pickle_id + ".json";

                final String stringStatus = "Approved";
                Log.e("EditPickle", "pickle edit qs  = " + pickleEditQuestion);
                Log.e("EditPickle", "tempPath1       = " + tempPath1);
                Log.e("EditPickle", "tempPath2       = " + tempPath2);
                Log.e("EditPickle", "enterFirstText  = " + enterFirstText.getText());
                Log.e("EditPickle", "enterSecondText = " + enterSecondText.getText());
                Log.e("EditPickle", "editFirstLable  = " + editFirstLable);
                Log.e("EditPickle", "editSecondLabel = " + editSecondLabel);
                if (pickleEditQuestion.length() < 1) {
                    String text = "Add Your Question !";
                    showNoFeedsDialog(text);
                } else if (tempPath1 == null
                        && enterFirstText.getText().toString().length() < 1) {
                    String text = "Add first image or text !";
                    showNoFeedsDialog(text);
                } else if (tempPath2 == null
                        && enterSecondText.getText().toString().length() < 1) {
                    String text = "Add second image or text !";
                    showNoFeedsDialog(text);
                } else if (tempPath1 != null && editFirstLable.length() < 1
                        && enterFirstText.getText().toString().length() < 1) {
                    String text = "Add caption for first image  !";
                    showNoFeedsDialog(text);
                } else if (tempPath2 != null && editSecondLabel.length() < 1
                        && enterSecondText.getText().toString().length() < 1) {
                    String text = "Add caption for second image  !";
                    showNoFeedsDialog(text);
                } else if (tempPath1 != null && tempPath2 != null
                        && enterFirstText.getText().toString().length() < 1
                        && enterSecondText.getText().toString().length() < 1) {
                    Log.e("EditPickle", "condition 1");
                    ringProgressDialog2 = ProgressDialog.show(
                            getActivity(), "",
                            "Updating Pickle ...", true);
                    ringProgressDialog2.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                if (tempPath1
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")
                                        && tempPath2
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {

                                    updatePickleEdit(edit_user_id,
                                            pickleEditQuestion, tempPath1,
                                            editFirstLable, opid_one, tempPath2,
                                            editSecondLabel, opid_two,
                                            stringStatus, AUTH, KEY);
                                } else if (!tempPath1
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")
                                        && tempPath2
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {

                                    updatePickleEditFirstImage(edit_user_id,
                                            pickleEditQuestion, tempPath1,
                                            editFirstLable, enterFirstText
                                                    .getText().toString(),
                                            opid_one, tempPath2, editSecondLabel,
                                            opid_two, stringStatus, AUTH, KEY);
                                } else if (tempPath1
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")
                                        && !tempPath2
                                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {

                                    updatePickleEditSecondImage(edit_user_id,
                                            pickleEditQuestion, tempPath1,
                                            editFirstLable, opid_one, tempPath2,
                                            editSecondLabel, enterSecondText
                                                    .getText().toString(),
                                            opid_two, stringStatus, AUTH, KEY);
                                } else {

                                    updatePickleNew(edit_user_id,
                                            pickleEditQuestion, tempPath1,
                                            editFirstLable, enterFirstText
                                                    .getText().toString(),
                                            opid_one, tempPath2, editSecondLabel,
                                            enterSecondText.getText().toString(),
                                            opid_two, stringStatus, AUTH, KEY);
                                }
                            } catch (Exception e) {
                                Log.e("EditPickle", "Error " + e);
                            }
                        }
                    }).start();

                } else if (tempPath1 != null
                        && tempPath1
                        .equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                        && tempPath2 != null
                        && tempPath2
                        .equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg")
                        && enterFirstText.getText().toString().length() > 1
                        && enterSecondText.getText().toString().length() > 1) {
                    Log.e("EditPickle", "condition 2");
                    ringProgressDialog2 = ProgressDialog.show(
                            getActivity(), "",
                            "Updating Pickle ...", true);
                    ringProgressDialog2.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                updatePickleWithText(edit_user_id,
                                        pickleEditQuestion, enterFirstText
                                                .getText().toString(), opid_one,
                                        enterSecondText.getText().toString(),
                                        opid_two, stringStatus, AUTH, KEY);
                            } catch (Exception e) {

                            }
                        }
                    }).start();
                } else if ((tempPath1 != null || tempPath1
                        .equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg"))
                        && (tempPath2 != null || tempPath2
                        .equalsIgnoreCase("http://54.251.33.194:8800/assets/missing.jpg"))
                        && enterFirstText.getText().toString().length() > 1
                        && enterSecondText.getText().toString().length() > 1) {

                    Log.e("EditPickle", "condition 3");
                    ringProgressDialog2 = ProgressDialog.show(
                            getActivity(), "",
                            "Updating Pickle ...", true);
                    ringProgressDialog2.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                updatePickleWithText(edit_user_id,
                                        pickleEditQuestion, enterFirstText
                                                .getText().toString(), opid_one,
                                        enterSecondText.getText().toString(),
                                        opid_two, stringStatus, AUTH, KEY);
                            } catch (Exception e) {

                            }
                        }
                    }).start();

                } else if ((tempPath1 != null && (tempPath1
                        .equals("http://54.251.33.194:8800/assets/missing.jpg") || !tempPath1
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")))
                        && enterSecondText.getText().toString().length() > 0) {
                    Log.e("EditPickle", "condition 4");
                    ringProgressDialog2 = ProgressDialog.show(
                            getActivity(), "",
                            "Updating Pickle ...", true);
                    ringProgressDialog2.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                updatePickleWithOldImageWithNewText(edit_user_id,
                                        pickleEditQuestion, tempPath1,
                                        editFirstLable, enterFirstText.getText()
                                                .toString(), opid_one, tempPath2,
                                        editSecondLabel, enterSecondText.getText()
                                                .toString(), opid_two,
                                        stringStatus, AUTH, KEY);
                            } catch (Exception e) {

                            }
                        }
                    }).start();
                } else if (enterFirstText.getText().toString().length() > 0
                        && (tempPath2 != null && (tempPath2
                        .equals("http://54.251.33.194:8800/assets/missing.jpg") || !tempPath2
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")))) {
                    Log.e("EditPickle", "condition 5");
                    ringProgressDialog2 = ProgressDialog.show(
                            getActivity(), "",
                            "Updating Pickle ...", true);
                    ringProgressDialog2.setCancelable(true);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                updatePickleWithOldImageWithNewText(edit_user_id,
                                        pickleEditQuestion, tempPath1,
                                        editFirstLable, enterFirstText.getText()
                                                .toString(), opid_one, tempPath2,
                                        editSecondLabel, enterSecondText.getText()
                                                .toString(), opid_two,
                                        stringStatus, AUTH, KEY);
                            } catch (Exception e) {

                            }
                        }
                    }).start();
                }

                break;

            case R.id.open_Camera:
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File file = new File(Environment.getExternalStorageDirectory(),
                        "tmp_avatar_" + String.valueOf(System.currentTimeMillis())
                                + ".jpg");
                mImageCaptureUri = Uri.fromFile(file);
                // cameraIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                // mImageCaptureUri);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                dialog.dismiss();
                break;
            case R.id.open_Gallery:
                Intent galleryIntent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                galleryIntent.setType("image/*");
                startActivityForResult(
                        Intent.createChooser(galleryIntent, "Select File"),
                        GALLERY_REQUEST);

                dialog.dismiss();
                break;

            case R.id.editFirst:
                enterFirstText.setVisibility(View.GONE);
                addFirstPickleView.setVisibility(View.VISIBLE);
                btnCancelFirstPickleView.setVisibility(View.GONE);
                setEnterFirstText.setVisibility(View.GONE);
                enterFirstText.setText("");
                setEnterFirstText.setText("");
                addFirstImageWithCaption.setVisibility(View.GONE);
                addViewOne.setBackgroundColor(Color.parseColor("#c0c0c0"));

                /*
                * @PKDEC09 - if (flag.equals("0") || fetured_pickle) - already exists.
                * createpickle(...) is not called if there are no fb friends returned after the api call, so
                * jsonStr1 will be null
                * */
                if ((flag.equals("0") || fetured_pickle) && jsonStr1 != null) {
                    String qid1 = "";
                    try {
                        mainObject = new JSONObject(jsonStr1);
                        qid1 = mainObject.getString("question_id");
                        Log.e("qid", "" + qid1);
                    } catch (JSONException e) {
                        // 
                        e.printStackTrace();
                    } catch (Exception e) {
                        //Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                    Edit_user edit_user_obj = new Edit_user();
                    Fb_friendlist fbdialog1 = new Fb_friendlist(getActivity(), 0, qid1, user_id, edit_user_obj, id_one);
                    fbdialog1.show();
                }
                enterFirstText.setVisibility(View.GONE);
                addFirstPickleView.setVisibility(View.VISIBLE);
                btnCancelFirstPickleView.setVisibility(View.GONE);
                setEnterFirstText.setVisibility(View.GONE);
                enterFirstText.setText("");
                setEnterFirstText.setText("");
                addFirstImageWithCaption.setVisibility(View.GONE);
                addViewOne.setBackgroundColor(Color.parseColor("#c0c0c0"));
                break;

//				Edit_user edit_user_obj = new Edit_user();
//				Fb_friendlist fbdialog1 = new Fb_friendlist(
//						getActivity(), 0, editPickleId, user_id,
//						edit_user_obj,id_one);
//				fbdialog1.show();
//			}
//			break;
            case R.id.editSecond:
                enterSecondText.setVisibility(View.GONE);
                addSecondPickleView.setVisibility(View.VISIBLE);
                btnCancelSecondPickleView.setVisibility(View.GONE);
                setEnterSecondText.setVisibility(View.GONE);
                enterSecondText.setText("");
                setEnterSecondText.setText("");
                addSecondImageWithCaption.setVisibility(View.GONE);
                addViewTwo.setBackgroundColor(Color.parseColor("#c0c0c0"));
/*
                * @PKDEC09 - if (flag.equals("0") || fetured_pickle) - already exists.
                * createpickle(...) is not called if there are no fb friends returned after the api call, so
                * jsonStr1 will be null
                * */
                if ((flag.equals("0") || fetured_pickle) && jsonStr1 != null) {

                    String qid2 = "";
                    try {
                        mainObject = new JSONObject(jsonStr1);
                        qid2 = mainObject.getString("question_id");
                        Log.e("qid", "" + qid2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Edit_user edit_user_obj1 = new Edit_user();
                    Fb_friendlist fbdialog2 = new Fb_friendlist(
                            getActivity(), 1, qid2, user_id,
                            edit_user_obj1, id_two);
                    fbdialog2.show();
                }

                enterSecondText.setVisibility(View.GONE);
                addSecondPickleView.setVisibility(View.VISIBLE);
                btnCancelSecondPickleView.setVisibility(View.GONE);
                setEnterSecondText.setVisibility(View.GONE);
                enterSecondText.setText("");
                setEnterSecondText.setText("");
                addSecondImageWithCaption.setVisibility(View.GONE);
                addViewTwo.setBackgroundColor(Color.parseColor("#c0c0c0"));
                break;

//			else{
//				Edit_user edit_user_obj1 = new Edit_user();
//				Fb_friendlist fbdialog2 = new Fb_friendlist(
//						getActivity(), 1, editPickleId, user_id,
//						edit_user_obj1,id_two);
//				fbdialog2.show();
//			}
//			break;
            default:
                break;
        }
    }

    private void parseResponse(String response_str) {
        try {
            Log.e("EditPickle", "Respones = " + response_str);
            JSONObject objMain = new JSONObject(response_str);
            JSONObject objPickle = objMain.getJSONObject("pickle");
            String pickle_id = objPickle.getString("id");
            String pickle_name = objPickle.getString("name");

//			if(flag.equalsIgnoreCase("0")){
//				Intent inte1 = new Intent(
//						getActivity(), HomeScreen.class);
//				startActivity(inte1);
//			}
//			else{
//			Intent showPickleIntent = new Intent(
//					getActivity(), ShowPickleFragment.class);
//			showPickleIntent.putExtra("PICKLE_ID", pickle_id);
//			showPickleIntent.putExtra("PICKLE_NAME", pickle_name);
//			showPickleIntent.putExtra("FROM", "EDIT");
//			startActivity(showPickleIntent);
//			getActivity().finish();
//			}

            ShowPickleFragment ShowPickleFragmentobj = new ShowPickleFragment();
            Bundle args = new Bundle();
            args.putString("PICKLE_ID", pickle_id);
            args.putString("PICKLE_NAME", pickle_name);
            args.putString("FROM", "EDIT");
            //args.putString("edited photo", );

            ShowPickleFragmentobj.setArguments(args);
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();
            transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
            transaction.addToBackStack(null);
            transaction.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            // 
            Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }

    private void showDialog() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_choose_camera_gallery);
        dialog.setCanceledOnTouchOutside(false);
        dialog.findViewById(R.id.open_Camera).setOnClickListener(this);
        dialog.findViewById(R.id.open_Gallery).setOnClickListener(this);

        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                if (data != null) {
                    if (whichOne.equals("IMAGE_ONE")) {

                        photo1 = (Bitmap) data.getExtras().get("data");
                        firstImage.setImageBitmap(photo1);
                        addFirstPickleView.setVisibility(View.GONE);
                        addFirstImageWithCaption.setVisibility(View.VISIBLE);
                        btnCancelFirstPickleView.setVisibility(View.GONE);
                        addViewOne.setBackgroundColor(Color
                                .parseColor("#F0F0F0"));
                        firstCaption.requestFocus();
                        selectedImageUri1 = getImageUri(
                                getActivity(), photo1);
                        tempPath1 = getPath(selectedImageUri1,
                                getActivity());

                    } else {
                        photo2 = (Bitmap) data.getExtras().get("data");
                        secondImage.setImageBitmap(photo2);
                        addSecondPickleView.setVisibility(View.GONE);
                        addSecondImageWithCaption.setVisibility(View.VISIBLE);
                        btnCancelSecondPickleView.setVisibility(View.GONE);
                        addViewTwo.setBackgroundColor(Color
                                .parseColor("#F0F0F0"));
                        secondCaption.requestFocus();
                        selectedImageUri2 = getImageUri(
                                getActivity(), photo2);
                        tempPath2 = getPath(selectedImageUri2,
                                getActivity());

                    }
                }

            } else if (requestCode == GALLERY_REQUEST) {

                if (whichOne.equals("IMAGE_ONE")) {
                    selectedImageUri1 = data.getData();
                    tempPath1 = getPath(selectedImageUri1,
                            getActivity());
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                    photo1 = BitmapFactory.decodeFile(tempPath1, btmapOptions);
                    firstImage.setImageBitmap(photo1);
                    addFirstPickleView.setVisibility(View.GONE);
                    addFirstImageWithCaption.setVisibility(View.VISIBLE);
                    btnCancelFirstPickleView.setVisibility(View.VISIBLE);
                    addViewOne.setBackgroundColor(Color.parseColor("#F0F0F0"));
                    firstCaption.requestFocus();
                } else {
                    selectedImageUri2 = data.getData();
                    tempPath2 = getPath(selectedImageUri2,
                            getActivity());
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                    photo2 = BitmapFactory.decodeFile(tempPath2, btmapOptions);
                    secondImage.setImageBitmap(photo2);
                    addSecondPickleView.setVisibility(View.GONE);
                    addSecondImageWithCaption.setVisibility(View.VISIBLE);
                    //btnCancelSecondPickleView.setVisibility(View.VISIBLE); // @PKDEC07 commented as we will not be showing xdelete icon
                    addViewTwo.setBackgroundColor(Color.parseColor("#F0F0F0"));
                    secondCaption.requestFocus();
                }
            }
        }
    }

    // ImageView friend_imageview1,friend_imageview2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // 
        v = inflater.inflate(R.layout.new_edit_pickle, null);
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HomeScreen.whichFragment = "CREATEPICKLE";
        updateBarHandler = new Handler();
        imageLoader = new ImageLoader(getActivity().getApplicationContext());
        try {//@PK30NOV
            back = (ImageView) v.findViewById(R.id.btnBackpress);
            back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    getFragmentManager().popBackStack();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        Bundle bundle = this.getArguments();

        try {
            if (bundle != null) {
                editPickleId = bundle.getString("PICKLE_ID");
                editPickleName = bundle.getString("PICKLE_NAME");
                AUTH = bundle.getString("AUTH");
                KEY = bundle.getString("KEY");
                user_id = bundle.getString("USERID");
                user_one_id = bundle.getString("USER_ONE_ID");
                user_one_name = bundle.getString("USER_ONE_NAME");
                user_one_url = bundle.getString("USER_ONE_IMG_URL");
                user_two_url = bundle.getString("USER_TWO_IMG_URL");
                user_two_id = bundle.getString("USER_TWO_ID");
                user_two_name = bundle.getString("USER_TWO_NAME");
                fetured_pickle = bundle.getBoolean("is_featured");
                flag = bundle.getString("FLAG");

                Log.e("EditPickle", "OC bundle editPickleId    = " + editPickleId);
                Log.e("EditPickle", "OC bundle editPickleName  = " + editPickleName);
                Log.e("EditPickle", "OC bundle AUTH  =========== " + AUTH);
                Log.e("EditPickle", "OC bundle KEY   =========== " + KEY);
                Log.e("EditPickle", "OC bundle user_id ========= " + user_id);
                Log.e("EditPickle", "OC bundle user_one_id     = " + user_one_id);
                Log.e("EditPickle", "OC bundle user_one_name   = " + user_one_name);
                Log.e("EditPickle", "OC bundle user_one_url    = " + user_one_url);
                Log.e("EditPickle", "OC bundle user_two_url    = " + user_two_url);
                Log.e("EditPickle", "OC bundle user_two_id     = " + user_two_id);
                Log.e("EditPickle", "OC bundle user_two_name   = " + user_two_name);
                Log.e("EditPickle", "OC bundle fetured_pickle  = " + fetured_pickle);
                Log.e("EditPickle", "OC bundle flag ============ " + flag);

                // intent.putExtra("USER_ONE_ID",listOfFeaturedPickleData.get(position).getOption_one_fb_id());
                // intent.putExtra("USER_ONE_NAME",listOfFeaturedPickleData.get(position).getOption_one_name()
                // );
                //
                // intent.putExtra("USER_TWO_ID",
                // listOfFeaturedPickleData.get(position).getOption_two_fb_id());
                // intent.putExtra("USER_TWO_NAME",listOfFeaturedPickleData.get(position).getOption_one_name()
                // );
                // friend_image1=extras.getByteArray("FRIEND_IMAGE1");
                // Bitmap bitmap1 = BitmapFactory.decodeByteArray(friend_image1
                // , 0, friend_image1.length);
                // friend_imageview1.setImageBitmap(bitmap1);

				/*
                 * friend_image2=extras.getByteArray("FRIEND_IMAGE2"); Bitmap
				 * bitmap2 = BitmapFactory.decodeByteArray(friend_image2 , 0,
				 * friend_image2.length);
				 * friend_imageview2.setImageBitmap(bitmap2);
				 */

            }

        } catch (NullPointerException e) {
            // 
            e.printStackTrace();
        }

        setUpViews();
        setTracker();

//		URL = Constant.PickleUrl + "questions/" + editPickleId + "-"
//				+ editPickleName.replace(" ", "-") + "/edit.json";

        URL = Constant.PickleUrl + "questions/" + editPickleId + "/edit.json";

        // Log.e("amol",""+URL);

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (flag.equals("0")) {
            if (isInternetPresent) {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        // @PKDEC09 - if..else.. condition added, new createpickle(...) already exists

                        if (checkNull(user_one_id) && checkNull(user_one_name) && checkNull(user_one_url) && checkNull(user_two_id)
                                && checkNull(user_two_name) && checkNull(user_two_url) && checkNull(edit_pickle_id)) {
                            fbDataExists = false;
                        } else {
                            fbDataExists = true;

                            //@PKDEC09 - moved inside the runOnUiThread, originally immediately after if(isInternetPresent)
                            ringProgressDialog1 = ProgressDialog.show(getActivity(), "", "Loading Pickle ...", true);
                            ringProgressDialog1.setCancelable(true);//@PKDEC09 - Actual - setCancelable(true);
                            new createpickle(user_one_id, user_one_name, user_one_url, user_two_id, user_two_name, user_two_url, edit_pickle_id).execute();
                        }
                    }
                });

            } else {
                String text = "No Internet Connection!";
                showNoFeedsDialog(text);
            }
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        new load_pickle_data().execute();
                    } catch (Exception e) {

                    }
                }
            }).start();
        }
        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri, Activity context) {
        String[] projection = {MediaColumns.DATA};
        Cursor cursor = context.managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void exitDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setCancelable(true);//@PKDEC09 - Actual - setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getActivity(),
                        HomeScreen.class));
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setCancelable(true);//@PKDEC09 - Actual - setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @SuppressWarnings("deprecation")
    private class createpickle extends AsyncTask<Void, Void, Void> {
        String create_pickle_url = Constant.PickleUrl + "featured_pickels.json";
        String paramIdOne, paramNameOne, paramUrlOne, paramIdTwo, paramNameTwo, paramUrlTwo, paramPId,
                uid;
        String qid;

        @SuppressWarnings("unused")
        public createpickle(String id_one, String name_one, String url_one, String id_two,
                            String name_two, String url_two, String pId) {
            this.paramIdOne = id_one;
            this.paramNameOne = name_one;
            this.paramUrlOne = url_one;
            this.paramIdTwo = id_two;
            this.paramNameTwo = name_two;
            this.paramUrlTwo = url_two;
            this.paramPId = editPickleId;
            this.uid = user_id;
            Log.e("EditPickle", "Create pickle called");
        }

        @Override
        protected Void doInBackground(Void... params) {

            // paramIdOne = params[0];
            // paramNameOne = params[1];
            // paramIdTwo = params[2];
            // paramNameTwo = params[3];
            // paramPId = params[4];
            // paramShare = params[5];
            // paramAuthToken = params[6];
            // paramKey = params[7];

            HttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(create_pickle_url);

//			BasicNameValuePair idOneBasicNameValuePair = new BasicNameValuePair(
//					"[featured_pickel][friends][][id]", paramIdOne);

            BasicNameValuePair nameOneBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][name]", paramNameOne);
            BasicNameValuePair urlOneBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][picture][data][url]", paramUrlOne);

//			BasicNameValuePair idTwoBasicNameValuePair = new BasicNameValuePair(
//					"[featured_pickel][friends][][id]", paramIdTwo);
            BasicNameValuePair nameTwoBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][name]", paramNameTwo);
            BasicNameValuePair urltwoBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][picture][data][url]", paramUrlTwo);
            BasicNameValuePair pIdBasicNameValuePair = new BasicNameValuePair("[featured_pickel][question_id]", paramPId);
//			 BasicNameValuePair shareBasicNameValuePair = new
//			 BasicNameValuePair(
//			 "[featured_pickel][share]", "true");
            // BasicNameValuePair authTokenBasicNameValuePair = new
            // BasicNameValuePair(
            // "authentication_token", paramAuthToken);
            // BasicNameValuePair keyBasicNameValuePair = new
            // BasicNameValuePair(
            // "key", paramKey);

            BasicNameValuePair user_id = new BasicNameValuePair("uid", HomeScreen.user_id);

            List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
            //	nameValuePairList.add(idOneBasicNameValuePair);

            nameValuePairList.add(nameOneBasicNameValuePair);
            nameValuePairList.add(urlOneBasicNameValuePair);
            //	nameValuePairList.add(idTwoBasicNameValuePair);

            nameValuePairList.add(nameTwoBasicNameValuePair);
            nameValuePairList.add(urltwoBasicNameValuePair);

            nameValuePairList.add(pIdBasicNameValuePair);
            //nameValuePairList.add(shareBasicNameValuePair);
            // nameValuePairList.add(authTokenBasicNameValuePair);
            // nameValuePairList.add(keyBasicNameValuePair);
            nameValuePairList.add(user_id);

            ServiceHandler sh = new ServiceHandler();
            jsonStr1 = sh.makeServiceCall(create_pickle_url, ServiceHandler.POST, nameValuePairList);
            if (jsonStr1 != null) {

                try {
                    mainObject = new JSONObject(jsonStr1);
                    qid = mainObject.getString("question_id");

                    Log.e("qid", "" + qid);

                    //@PK30NOV
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new loadingEditPickleData(qid).execute();
                        }
                    });
                } catch (JSONException e) {
                    // 
                    e.printStackTrace();
                }

            }
            // try {
            // UrlEncodedFormEntity urlEncodedFormEntity = new
            // UrlEncodedFormEntity(
            // nameValuePairList);
            // Log.e("create_param",""+nameValuePairList);
            //
            // httpPost.setEntity(urlEncodedFormEntity);
            //
            // try {
            // HttpResponse httpResponse = httpClient4
            // .execute(httpPost);
            // Log.e("response:",""+httpResponse.toString());
            //
            // InputStream inputStream = httpResponse.getEntity()
            // .getContent();
            // InputStreamReader inputStreamReader = new InputStreamReader(
            // inputStream);
            // BufferedReader bufferedReader = new BufferedReader(
            // inputStreamReader);
            // StringBuilder stringBuilder = new StringBuilder();
            // String bufferedStrChunk = null;
            //
            // while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
            // stringBuilder.append(bufferedStrChunk);
            // }
            // //return stringBuilder;
            //
            // } catch (ClientProtocolException cpe) {
            // cpe.printStackTrace();
            // } catch (IOException ioe) {
            // ioe.printStackTrace();
            // }
            //
            // } catch (UnsupportedEncodingException uee) {
            // uee.printStackTrace();
            // }

            return null;
        }

        @Override
        protected void onPreExecute() {
            // 
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void result) {
            // 
            super.onPostExecute(result);

        }

    }

    private class load_pickle_data extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();
            params1.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params1.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URL, ServiceHandler.GET,
                    params1);

            if (jsonStr != null) {
                try {
                    Log.e("user_info", "" + jsonStr.toString());

                    final JSONObject obj = new JSONObject(jsonStr);
                    JSONArray user_array = obj.getJSONArray("options");
                    id_one = user_array.getJSONObject(0).getString("id");
                    id_two = user_array.getJSONObject(1).getString("id");
                    load_fb1 = id_one;
                    load_fb2 = id_two;
                    Log.e("idone", "" + id_one);
                    Log.e("idtwo", "" + id_two);
                    //
                    getActivity()
                            .runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    try {
                                        if (obj.has("status") && obj.get("status").equals("Failure")) {
                                            String text = "Something wrong can't edit getActivity() pickle!!";
                                            exitDialog(text);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e1) {
                    // 
                    e1.printStackTrace();
                }
            } else {
                String text = "Couldn't get any data from the url!!";
                exitDialog(text);
            }

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

    }

    private class loadingEditPickleData extends AsyncTask<String, Void, Void> {

        int question_id;
        String load_pickle_url;

        public loadingEditPickleData(String qid) {
            // 
            this.question_id = (Integer.parseInt(qid));
            Log.e("loading q", "" + question_id);
            load_pickle_url =
                    // '"+title+"'
                    Constant.PickleUrl + "/questions/" + question_id + "/edit.json";
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(load_pickle_url,
                    ServiceHandler.GET, params);

            if (jsonStr != null) {
                try {
                    Log.e("user_info", "" + jsonStr.toString());

                    final JSONObject obj = new JSONObject(jsonStr);
                    JSONArray user_array = obj.getJSONArray("options");
                    id_one = user_array.getJSONObject(0).getString("id");
                    id_two = user_array.getJSONObject(1).getString("id");
                    Log.e("idone", "" + id_one);
                    Log.e("idtwo", "" + id_two);

                    // getActivity()
                    // .runOnUiThread(new Runnable() {
                    //
                    // @Override
                    // public void run() {
                    // try {
                    // if (obj.get("status").equals("Failure")) {
                    // String text = "Something wrong can't edit getActivity() pickle!!";
                    // exitDialog(text);
                    // }
                    // } catch (JSONException e) {
                    // e.printStackTrace();
                    // }
                    // }
                    // });

                } catch (NullPointerException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    // 
                    e.printStackTrace();
                }
            } else {
                String text = "Couldn't get any data from the url!!";
                exitDialog(text);
            }

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONUserProfile(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ringProgressDialog1.dismiss();
        }

    }

    public class Update_User_Profile extends AsyncTask<Void, Void, Void> {

        String update_user_url;

        public Update_User_Profile(String image_url, String name, String one_or_two) {
            // 
            image_url1 = image_url.toString();
            user_name1 = name.replaceAll("\\s", "");
            one_Or_two = one_or_two;
            update_user_url = Constant.PickleUrl + "update_option.json";
            Log.e("update_url", "" + update_user_url);
        }

        @Override
        protected void onPreExecute() {
            // 
            super.onPreExecute();
        }

        @SuppressLint("LongLogTag")
        @Override
        protected Void doInBackground(Void... params) {
            // 

            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> param = new ArrayList<NameValuePair>();
            param.add(new BasicNameValuePair("image_url", image_url1));
            param.add(new BasicNameValuePair("friend_name", user_name1));
            param.add(new BasicNameValuePair("id", one_Or_two));
            param.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            param.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(update_user_url,
                    ServiceHandler.GET, param);
            if (jsonStr != null) {
                Log.e("updated suceess response", "" + jsonStr);

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // 
            super.onPostExecute(result);
        }

    }

    public class Edit_user implements passselected {

        @Override
        public void setvalue(String image, String name, String one_or_two, String option) {
            // 

            load_user1_data(image, name, one_or_two, option);

        }

    }

    private String getValueFor(String val) {
        return val == null ? "" : val;
    }

    /**
     * PK
     * Checks the string is null or not
     *
     * @return true : if string is null or empty
     * @since 09 December 2016 @ 12:38 PM
     */
    private boolean checkNull(String val) {

        return (val == null || (val != null && (val.equalsIgnoreCase("null") || val.trim().isEmpty()))) ? true : false;

    }
}
