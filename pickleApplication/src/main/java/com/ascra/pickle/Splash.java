package com.ascra.pickle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Window;
import android.widget.TextView;

import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.SessionManager;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

public class Splash extends AppCompatActivity {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    SessionManager session;
    static String str_option;
    String whichWayLogin;
    public static final String SENDER_ID = "264226440806";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();

        Splash.str_option = "1";

        setContentView(R.layout.pickle_splash);
        cd = new ConnectionDetector(Splash.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(this);
        whichWayLogin = SaveUtils.getUserLoginFrom(
                getResources().getString(R.string.PreferenceFileName), this,
                "WHICH_LOGIN", null);

        if (isInternetPresent) {

            Thread thread = new Thread() {
                public void run() {
                    try {
                        Thread.sleep(3000);
                        doThis();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };

            thread.start();
        } else {
            showExitDialog();
        }

    }

    private void doThis() {

        if (SaveUtils.getLoginSessionFromPref(Constant.LoginSession,
                Splash.this, "LOGIN")) {
            GetGcmRegistrationId();
            Intent newintent = new Intent(Splash.this, HomeScreen.class);
            startActivity(newintent);
            finish();
        } else {
           try {
               GetGcmRegistrationId();
           }catch (Exception e){
               e.printStackTrace();
           }
            Intent newintent = new Intent(Splash.this,
                    MainActivity.class);

            startActivity(newintent);
            finish();
        }

    }

    private void GetGcmRegistrationId() {

        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);

        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);

        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);

        Log.e("regid", regId);
        GCMRegistrar.register(this, SENDER_ID);

        session.createRegID(regId);

        //@PK30NOV
        SaveUtils.saveGcmRegistrationId(getResources().getString(R.string.PreferenceFileName), Splash.this, SaveUtils.GcmRegistraionId, regId);
        //noinspection unchecked
        new AsynGetGCMId().execute();

    }

    private void showExitDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
        builder.setCancelable(true);

        TextView msg = new TextView(Splash.this);
        msg.setText("No Internet Connection");
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    class AsynGetGCMId extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Object doInBackground(Object[] params) {

            try {
                InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
                String gcm_token = instanceID.getToken(SENDER_ID, GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Log.e("=======GCM ID======", gcm_token);
                new SessionManager(Splash.this).setGcmToken(gcm_token);

                //@PK30NOV
                SaveUtils.saveGcmDeviceToken(getResources().getString(R.string.PreferenceFileName), Splash.this, SaveUtils.GcmDeviceToken, gcm_token);

            } catch (Exception | IllegalAccessError e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {

        }
    }
}
