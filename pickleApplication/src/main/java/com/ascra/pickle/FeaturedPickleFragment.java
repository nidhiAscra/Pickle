package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ascra.data.AllFeedsModel;
import com.ascra.data.FeaturedPickleData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class FeaturedPickleFragment extends Fragment {

    View view;
    ListView listOfFeaturedPickles;
    private String URL_FEED = null;
    private String SHAREBUNDLE_URL = null;
    ImageLoader imageLoader;
    ProgressBar inProgress;

    ArrayList<FeaturedPickleData> listOfFeaturedPickleData = new ArrayList<FeaturedPickleData>();

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    Tracker tracker;

    String facebookUId;
    String facebookAccessToken;

    Typeface face;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.featured_pickle_fragment, null);
        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HomeScreen.whichFragment = "FEATURED-PICKLE";

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");
//		((HomeScreen)getActivity()).enable_bottom_strip();
        setUpViews();
        setTracker();

        facebookUId = SaveUtils.getUserFromPref(
                getResources().getString(R.string.PreferenceFileName),
                getActivity(), "FACEBOOK_UID", null);

        facebookAccessToken = SaveUtils.getUserFromPref(getResources()
                        .getString(R.string.PreferenceFileName), getActivity(),
                "FACEBOOK_ACCESS_TOKEN", null);

        //@PKDEC08 Api changed
//        URL_FEED = Constant.PickleUrl + "featured_pickels.json";
//        URL_FEED = Constant.PickleUrl + "api/v1/featured_pickles.json";
        URL_FEED = Constant.PickleUrl + "api/v1/featured_pickles.json?uid=" + facebookUId + "&access_token=" + facebookAccessToken;

        //Working - Uday's acc api
//        URL_FEED = "http://pickle.tingworks.in/api/v1/featured_pickles.json?uid=687382558030448&access_token=EAAGic0KB4ncBAFSO3ZBBZBZAG9ZBwF78zFnD3O8rKnjxbIRU6pnltqKqVPS9rtNCuLcZAEJVYJSkcWpsFKRrONUFWSWzr1Ar3OykiZC98urlgTsm1IWq8SH9gVqpZCgvZA3qfUJXjrq1j20MbvbOGXDPdZBVuKOeFtA0ZD";

        //+"uid="+facebookUId+"&access_token="+facebookAccessToken;

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            try {
                new loadingFeaturedPicklesData().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String text = "No Internet Connection!";
            showExitDialog(text);
        }

        return view;
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(
                "UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "FEATURED PICKLE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    @Override
    public void onResume() {
//		((SherlockFragmentActivity) getActivity()).getSupportActionBar().show();

        super.onResume();
    }

    private void setUpViews() {
        listOfFeaturedPickles = (ListView) view.findViewById(R.id.featuredPicklesList);
        listOfFeaturedPickleData.clear();
        inProgress = (ProgressBar) view.findViewById(R.id.inProgress);

        //@PKDEC06
        CommonMethods.addHeaderToListview(getActivity(), listOfFeaturedPickles, getString(R.string.featuredPickleHeader), R.drawable.featured_color);
//        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.featuredPickleHeader, R.drawable.featured_color);
    }

    private class loadingFeaturedPicklesData extends
            AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
//			params.add(new BasicNameValuePair("authentication_token",
//					HomeScreen.authentication_token));
//			params.add(new BasicNameValuePair("key", HomeScreen.key));
//            params.add(new BasicNameValuePair("uid", facebookUId));
//            params.add(new BasicNameValuePair("access_token",
//                    facebookAccessToken));

            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET, params);
            Log.e("Featured url:", "" + URL_FEED + "\tfacebookUId " + facebookUId + "\tfacebookAccessToken " + facebookAccessToken);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Featured", "Response = " + mainObject);
                        parseJSONFeaturedPickle(mainObject);
                    }
                });

            } catch (JSONException e) {
                // e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String text = "Featured pickle's not found !";
                        showExitDialog(text);
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProgress.setVisibility(View.GONE);
        }

    }

    private void parseJSONFeaturedPickle(JSONObject mainObject) {
        try {
            JSONArray arrayOfFeaturedPickle = mainObject.getJSONArray("featured_pickels");
            for (int featuredItems = 0; featuredItems < arrayOfFeaturedPickle.length(); featuredItems++) {
                JSONObject objFeaturedPickleMain = arrayOfFeaturedPickle.getJSONObject(featuredItems);
                JSONObject objFeaturedPickleSub = objFeaturedPickleMain.getJSONObject("featured_pickel");
                String featured_id = objFeaturedPickleSub.getString("id");
                String featured_name = objFeaturedPickleSub.getString("name");
                String featured_user_id = objFeaturedPickleSub.getString("user_id");
                String featured_bundle_id = objFeaturedPickleSub.getString("bundle_id");
                String featured_featured = objFeaturedPickleSub.getString("featured");
                String featured_status = objFeaturedPickleSub.getString("status");
                String featured_deactive = objFeaturedPickleSub.getString("deactive");
                String featured_image = objFeaturedPickleSub.getString("image");
                String featured_is_featured = objFeaturedPickleSub.getString("is_featured");
                JSONArray optionsArray = objFeaturedPickleSub.getJSONArray("options");

                String option_one_option_image = null;
                String option_one_name = null;
                String option_one_fb_id = null;

                String option_two_option_image = null;
                String option_two_name = null;
                String option_two_fb_id = null;

                if (optionsArray.length() == 0) {

                } else if (optionsArray.length() <= 2) {
                    JSONObject objOptionOne = optionsArray.getJSONObject(0);
                    option_one_option_image = objOptionOne.getString("option_image");
                    option_one_name = objOptionOne.getString("name");
                    option_one_fb_id = objOptionOne.getString("user_fb_id");
                    JSONObject objOptionTwo = optionsArray.getJSONObject(1);
                    option_two_option_image = objOptionTwo.getString("option_image");
                    option_two_name = objOptionTwo.getString("name");
                    option_two_fb_id = objOptionTwo.getString("user_fb_id");

                }
                //

                FeaturedPickleData featuredPickleData = new FeaturedPickleData(
                        featured_id, featured_name, featured_user_id,
                        featured_bundle_id, featured_featured, featured_status,
                        featured_deactive, featured_image,
                        featured_is_featured, option_one_option_image,
                        option_one_name, option_one_fb_id,
                        option_two_option_image, option_two_name,
                        option_two_fb_id);

                listOfFeaturedPickleData.add(featuredPickleData);
            }

            MyFeaturedPicklesCustomAdapter myFeaturedPicklesCustomAdapter = new MyFeaturedPicklesCustomAdapter(
                    getActivity(), listOfFeaturedPickleData);
            listOfFeaturedPickles.setAdapter(myFeaturedPicklesCustomAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyFeaturedPicklesCustomAdapter extends BaseAdapter {

        Context mContext;
        ArrayList<FeaturedPickleData> listOfFeaturedPickleData = new ArrayList<FeaturedPickleData>();
        AllFeedsModel allFeedsModel;

        public MyFeaturedPicklesCustomAdapter(Context mContext,
                                              ArrayList<FeaturedPickleData> listOfFeaturedPickleData) {
            this.mContext = mContext;
            this.listOfFeaturedPickleData = listOfFeaturedPickleData;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return listOfFeaturedPickleData.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfFeaturedPickleData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;
            final FeedsViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.featured_pickle_item, null);
                viewHolder = new FeedsViewHolder(v);
                v.setTag(viewHolder);
            } else {
                viewHolder = (FeedsViewHolder) v.getTag();
            }

            if (listOfFeaturedPickleData.get(position).getFeatured_name() != null) {
                viewHolder.txtPickleQuestion.setText(listOfFeaturedPickleData.get(position).getFeatured_name());
            }

            if (listOfFeaturedPickleData.get(position).getOption_one_option_image() != null)
                if (listOfFeaturedPickleData.get(position).getOption_one_option_image().equals("http://pickle.tingworks.in/assets/missing.jpg"))
                    viewHolder.firstOptionImage.setVisibility(View.GONE);
                else {
                    viewHolder.firstOptionImage.setVisibility(View.VISIBLE);
//					imageLoader.DisplayImage( listOfFeaturedPickleData.get(position) .getOption_one_option_image(), viewHolder.firstOptionImage);
                    Picasso.with(mContext).load(listOfFeaturedPickleData.get(position).getOption_one_option_image()).into(viewHolder.firstOptionImage);
                }

            if (listOfFeaturedPickleData.get(position).getOption_one_name() != null) {
                if (listOfFeaturedPickleData.get(position).getOption_one_name().equals("") || listOfFeaturedPickleData.get(position).getOption_one_name().equals("null"))
                    viewHolder.txtFirstOptionName.setVisibility(View.GONE);
                else {
                    viewHolder.txtFirstOptionName.setVisibility(View.VISIBLE);
                    viewHolder.txtFirstOptionName.setText(listOfFeaturedPickleData.get(position).getOption_one_name());
                }
            }

            if (listOfFeaturedPickleData.get(position).getOption_two_option_image() != null) {
                if (listOfFeaturedPickleData.get(position).getOption_two_option_image().equals("http://pickle.tingworks.in/assets/missing.jpg")) {
                    viewHolder.secondOptionImage.setVisibility(View.GONE);
                } else {
                    viewHolder.secondOptionImage.setVisibility(View.VISIBLE);
//					imageLoader.DisplayImage( listOfFeaturedPickleData.get(position) .getOption_two_option_image(), viewHolder.secondOptionImage);
                    Picasso.with(mContext).load(listOfFeaturedPickleData.get(position).getOption_two_option_image()).into(viewHolder.secondOptionImage);

                }
            }

            if (listOfFeaturedPickleData.get(position).getOption_two_name() != null) {
                if (listOfFeaturedPickleData.get(position).getOption_two_name().equals("") || listOfFeaturedPickleData.get(position).getOption_two_name().equals("null"))
                    viewHolder.txtSecondOptionName.setVisibility(View.GONE);
                else {
                    viewHolder.txtSecondOptionName.setVisibility(View.VISIBLE);
                    viewHolder.txtSecondOptionName.setText(listOfFeaturedPickleData.get(position).getOption_two_name());
                }
            }

            viewHolder.moreComment.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
//					viewHolder.firstOptionImage.setDrawingCacheEnabled(true);
//					viewHolder.secondOptionImage.setDrawingCacheEnabled(true);
//					Bitmap bitmap1 = Bitmap.createBitmap(viewHolder.firstOptionImage.getDrawingCache());
//					Bitmap bitmap2 = Bitmap.createBitmap(viewHolder.secondOptionImage.getDrawingCache());
//					
//					ByteArrayOutputStream bs1 = new ByteArrayOutputStream();
//					bitmap1.compress(Bitmap.CompressFormat.PNG, 50, bs1);
//					
//					ByteArrayOutputStream bs2 = new ByteArrayOutputStream();
//					bitmap2.compress(Bitmap.CompressFormat.PNG, 50, bs2);
//		            

//					Log.e("EDIT", "EDIT");
//					Intent intent = new Intent(getActivity(),
//							EditPickleFragmentActivity.class);
//					intent.putExtra("PICKLE_ID",
//							listOfFeaturedPickleData.get(position)
//									.getFeatured_id());
//				//	intent.putExtra("FRIEND_IMAGE1",bs1.toByteArray());
//				//	intent.putExtra("FRIEND_IMAGE2",bs2.toByteArray());
//					
//					intent.putExtra("PICKLE_NAME", listOfFeaturedPickleData
//							.get(position).getFeatured_name());
//					intent.putExtra("AUTH", HomeScreen.authentication_token);
//					intent.putExtra("KEY", HomeScreen.key);
//					intent.putExtra("USERID", HomeScreen.user_id);
//					
//					intent.putExtra("USER_ONE_ID",listOfFeaturedPickleData.get(position).getOption_one_fb_id());
//					intent.putExtra("USER_ONE_NAME",listOfFeaturedPickleData.get(position).getOption_one_name() );
//
//					intent.putExtra("USER_TWO_ID", listOfFeaturedPickleData.get(position).getOption_two_fb_id());
//					intent.putExtra("USER_TWO_NAME",listOfFeaturedPickleData.get(position).getOption_two_name() );
//					intent.putExtra("FLAG","0");
//					startActivity(intent);

                    EditPickleFragmentActivity ShowPickleFragmentobj = new EditPickleFragmentActivity();
                    Bundle args = new Bundle();
                    args.putString("PICKLE_ID", listOfFeaturedPickleData.get(position).getFeatured_id());
                    args.putString("PICKLE_NAME", listOfFeaturedPickleData.get(position).getFeatured_name());
                    args.putString("AUTH", HomeScreen.authentication_token);
                    args.putString("KEY", HomeScreen.key);
                    args.putString("USERID", HomeScreen.user_id);
                    args.putString("USER_ONE_IMG_URL", listOfFeaturedPickleData.get(position).getOption_one_option_image());
                    args.putString("USER_ONE_ID", listOfFeaturedPickleData.get(position).getOption_one_fb_id());
                    args.putString("USER_ONE_NAME", listOfFeaturedPickleData.get(position).getOption_one_name());
                    args.putString("USER_TWO_IMG_URL", listOfFeaturedPickleData.get(position).getOption_two_option_image());
                    args.putString("USER_TWO_ID", listOfFeaturedPickleData.get(position).getOption_two_fb_id());
                    args.putString("USER_TWO_NAME", listOfFeaturedPickleData.get(position).getOption_two_name());
                    args.putString("FLAG", "0");
                    ShowPickleFragmentobj.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });

            viewHolder.popUp.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Log.e("SHARE", "SHARE");

                    if (listOfFeaturedPickleData.get(position).getOption_one_option_image() != null
                            || listOfFeaturedPickleData.get(position).getOption_one_name() != null
                            || listOfFeaturedPickleData.get(position).getOption_two_option_image() != null
                            || listOfFeaturedPickleData.get(position).getOption_two_name() != null) {
                        if (listOfFeaturedPickleData.get(position).getOption_one_option_image()
                                .equals("http://pickle.tingworks.in/assets/missing.jpg")
                                || listOfFeaturedPickleData.get(position).getOption_one_name().equals("null")
                                || listOfFeaturedPickleData.get(position).getOption_one_name().equals(" ")
                                || listOfFeaturedPickleData.get(position).getOption_two_option_image().equals("http://pickle.tingworks.in/assets/missing.jpg")
                                || listOfFeaturedPickleData.get(position).getOption_two_name().equals("null")
                                || listOfFeaturedPickleData.get(position).getOption_two_name().equals(" ")) {

                            String text = "Can't share this pikcle";
                            showExitDialog(text);
                        } else {

                            SHAREBUNDLE_URL = Constant.PickleUrl + "featured_pickels.json";
                            // new sharePickle().execute();

                            final ProgressDialog ringProgressDialog = ProgressDialog.show(getActivity(), "", "Sharing Pickle ...", true);
                            ringProgressDialog.setCancelable(true);
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        String share = "true";

                                        sharePickle(
                                                listOfFeaturedPickleData.get(position).getOption_one_name(),
                                                listOfFeaturedPickleData.get(position).getOption_one_option_image(),
                                                listOfFeaturedPickleData.get(position).getOption_two_name(),
                                                listOfFeaturedPickleData.get(position).getOption_two_option_image(),
                                                listOfFeaturedPickleData.get(position).getFeatured_id(),
                                                share,
                                                HomeScreen.authentication_token,
                                                HomeScreen.key,
                                                HomeScreen.user_id);

                                    } catch (Exception e) {

                                    }
                                    // ringProgressDialog.dismiss();
                                }
                            }).start();

                        }
                    }

                }
            });

            return v;
        }

        class FeedsViewHolder {

            TextView txtPickleQuestion;
            ImageView firstOptionImage;
            TextView txtFirstOptionName;
            ImageView secondOptionImage;
            TextView txtSecondOptionName;
            TextView moreComment;
            TextView popUp;

            public FeedsViewHolder(View base) {

                txtPickleQuestion = (TextView) base.findViewById(R.id.pickleQuestion);
                txtPickleQuestion.setTypeface(face);
                firstOptionImage = (ImageView) base.findViewById(R.id.firstOptionImageUrl);
                txtFirstOptionName = (TextView) base.findViewById(R.id.firstOptionName);
                txtFirstOptionName.setTypeface(face);
                secondOptionImage = (ImageView) base.findViewById(R.id.secondOptionImageUrl);
                txtSecondOptionName = (TextView) base.findViewById(R.id.secondOptionName);
                txtSecondOptionName.setTypeface(face);
                moreComment = (TextView) base.findViewById(R.id.moreComment);
                moreComment.setTypeface(face);
                popUp = (TextView) base.findViewById(R.id.popUp);
                popUp.setTypeface(face);

            }
        }

    }

    private void sharePickle(String name_one, String img_one,
                             String name_two, String img_two, String pId, String share,
                             String authentication_token, String key, String uid) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramNameOne = params[0];
                String paramNameIMG = params[1];
                String paramNameTwo = params[2];
                String paramNameIMGtwo = params[3];
                String paramPId = params[4];
                String paramShare = params[5];
                String paramAuthToken = params[6];
                String paramKey = params[7];
                String paramuid = params[8];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(SHAREBUNDLE_URL);

                BasicNameValuePair nameOneBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][name]", paramNameOne);
                BasicNameValuePair IMGOneBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][picture][data][url]", paramNameIMG);
                BasicNameValuePair nameTwoBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][name]", paramNameTwo);
                BasicNameValuePair IMGTwoBasicNameValuePair = new BasicNameValuePair("[featured_pickel][friends][][picture][data][url]", paramNameIMGtwo);
                BasicNameValuePair pIdBasicNameValuePair = new BasicNameValuePair("[featured_pickel][question_id]", paramPId);
                BasicNameValuePair shareBasicNameValuePair = new BasicNameValuePair("[featured_pickel][share]", paramShare);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);
                BasicNameValuePair user_id = new BasicNameValuePair("uid", paramuid);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();

                nameValuePairList.add(nameOneBasicNameValuePair);
                nameValuePairList.add(IMGOneBasicNameValuePair);
                nameValuePairList.add(nameTwoBasicNameValuePair);
                nameValuePairList.add(IMGTwoBasicNameValuePair);
                nameValuePairList.add(pIdBasicNameValuePair);
                nameValuePairList.add(shareBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);
                nameValuePairList.add(user_id);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                    Log.e("param", "" + nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        Log.e("response:", "" + httpResponse);

                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (Exception ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                } catch (Exception ioe) {
                    ioe.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                String text = "Pickle Share successfully!";
                showShareDialog(text);
            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(name_one, img_one, name_two, img_two, pId,
                share, authentication_token, key, uid);

    }

    private void showShareDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getActivity(), HomeScreen.class));
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showExitDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }
}
