package com.ascra.pickle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class CommentFragment extends Fragment implements
        OnClickListener {

    View view;
    ListView listOfComments;
    EditText enterComment;
    ImageView commentButton;
    TextView btnBack;

    String COMMENT_ARRAY, USER_ID, PICKLE_ID, USER_NAME, SHARE_PICKLE_ID;
    boolean MYPICKLE;

    final ArrayList<String> listOfCommentContent = new ArrayList<String>();
    final ArrayList<String> listOfCommentUser = new ArrayList<String>();
    final ArrayList<String> listOfCommentIds = new ArrayList<String>();

    //@PK30NOV
    final ArrayList<String> listOfUserIds = new ArrayList<String>();

    JSONArray arrayOfComments;
    private CustomCommentAdapter customCommentAdapter;

    private String COMMENT_URL = Constant.PickleUrl + "comments.json";
    private String COMMENT_DELETE_URL = null;

    String content = null;
    String commentable_type = null;
    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    RelativeLayout buttonlayout;
    ProgressDialog rpd;
    Typeface face;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.comment_dialog, null);

        //@PK30NOV
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {//@PK30NOV
            ImageView back = (ImageView) view.findViewById(R.id.btnBackpress);
            back.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    getFragmentManager().popBackStack();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        HomeScreen.whichFragment = "COMMENT_FRAGMENT";

        Bundle bundle = this.getArguments();
        COMMENT_ARRAY = bundle.getString("COMMENT_ARRAY");
        USER_ID = bundle.getString("USER_ID");
        PICKLE_ID = bundle.getString("PICKLE_ID");
        USER_NAME = bundle.getString("USER_NAME");
        SHARE_PICKLE_ID = bundle.getString("SHARE_PICKLE_ID");
        MYPICKLE = bundle.getBoolean("MYPICKLE");

        try {
            arrayOfComments = new JSONArray(COMMENT_ARRAY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");

        setUpViews();

        return view;
    }

    private void setUpViews() {

        listOfComments = (ListView) view.findViewById(R.id.allComments);
        enterComment = (EditText) view.findViewById(R.id.enterComment);
        commentButton = (ImageView) view.findViewById(R.id.commentbutton);
        btnBack = (TextView) view.findViewById(R.id.btnBack);
        btnBack.setTypeface(face);
        enterComment.setTypeface(face);

        buttonlayout = (RelativeLayout) view.findViewById(R.id.buttonlayout);

        //@PKDEC06 Adding header to listview
        CommonMethods.addHeaderToListview(getActivity(), listOfComments, getString(R.string.commentsHeader), R.drawable.comments);

        for (int i = 0; i < arrayOfComments.length(); i++) {
            try {
//                Gson g = new Gson();
//                CommentArrayModel vc = g.fromJson(String.valueOf(arrayOfComments.getJSONObject(i)), CommentArrayModel.class);
//                String commentContent, String commentCreatedAt, Integer commentId, Object commentPickleId, String commentUserName, String commentUserProfilePic, String commentUserUserName, Integer userId)
                listOfCommentContent.add(arrayOfComments.getJSONObject(i).getString("comment_content"));
                listOfCommentUser.add(arrayOfComments.getJSONObject(i).getString("comment_user_name"));
                listOfCommentIds.add(arrayOfComments.getJSONObject(i).getString("comment_id"));
                listOfUserIds.add(arrayOfComments.getJSONObject(i).has("user_id") ? arrayOfComments.getJSONObject(i).getString("user_id") : "0");

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        customCommentAdapter = new CustomCommentAdapter(getActivity(),
                listOfCommentContent, listOfCommentUser,
                listOfCommentIds, listOfUserIds);

        listOfComments.setAdapter(customCommentAdapter);
        commentButton.setOnClickListener(this);

        buttonlayout.setOnClickListener(this);
    }

    public class CustomCommentAdapter extends BaseAdapter {
        Context mContext;
        ArrayList<String> listOfUser = new ArrayList<String>();
        ArrayList<String> listOfContent = new ArrayList<String>();
        ArrayList<String> listOfIds = new ArrayList<String>();
        ArrayList<String> listOfUserIds = new ArrayList<String>();
        private LayoutInflater myLayoutInflater;

        //		@PK30NOV 5th param listOfUserIds added to differentiate between users and other users' comments so as to
//		prevent showing x (delete) icon
        public CustomCommentAdapter(Context mContext,
                                    ArrayList<String> listOfContent, ArrayList<String> listOfUser,
                                    ArrayList<String> listOfIds, ArrayList<String> listOfUserIds) {
            this.mContext = mContext;
            this.listOfContent = listOfContent;
            this.listOfUser = listOfUser;
            this.listOfIds = listOfIds;
            this.listOfUserIds = listOfUserIds;
            myLayoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            for (String id : listOfIds) {
                Log.e("@DELETE init", "Id -- " + id);
            }
        }

        @Override
        public int getCount() {
            return listOfContent.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfContent.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            View v = convertView;
            CompleteListViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.comment_view_with_delete, null);
                viewHolder = new CompleteListViewHolder(v);
                v.setTag(viewHolder);
            } else {
                viewHolder = (CompleteListViewHolder) v.getTag();
            }
            viewHolder.mUserName.setTypeface(face);
            viewHolder.mUserName.setText(listOfUser.get(position));
            viewHolder.mContent.setTypeface(face);
            viewHolder.mContent.setText(listOfContent.get(position));
            try {
                Log.e("COMMENT", "Position " + position + " "
                        + listOfUserIds.get(position) + " " + SaveUtils.getUserID(getString(R.string.PreferenceFileName), mContext, SaveUtils.USER_ID, "0"));
                Log.e("COMMENT@", "Position " + position + " "
                        + listOfIds.get(position) + " " + SaveUtils.getUserID(getString(R.string.PreferenceFileName), mContext, SaveUtils.USER_ID, "0"));
            } catch (Exception e) {

            }

            //@PKDEC01 : To hide the delete 'x' icon when the comment belongs to other user
            try {
                viewHolder.mDelete.setVisibility(listOfUserIds.get(position).equalsIgnoreCase(SaveUtils.getUserID(getString(R.string.PreferenceFileName), mContext, SaveUtils.USER_ID, "0"))
                        ? View.VISIBLE : View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            viewHolder.mDelete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Log.e("@DELETE @CREATE 1", "Comment Id = " + listOfIds.get(position));
                    COMMENT_DELETE_URL = Constant.PickleUrl + "comments/"
                            + listOfIds.get(position) + ".json";
                    Log.e("@DELETE @CREATE 2", "Comment Id = " + listOfIds.get(position));
                    if (isInternetPresent) {

                        new DeleteComment().execute();

                        listOfContent.remove(position);

                        ////@PKDEC05 The following should also be removed(offcourse)
                        listOfUser.remove(position);
                        listOfIds.remove(position);
                        listOfUserIds.remove(position);

                        customCommentAdapter.notifyDataSetChanged();
                    } else {
                        String text = "No Internet Connection!";
                        showNoFeedsDialog(text);
                    }

                }

            });

            return v;
        }

        class CompleteListViewHolder {
            public TextView mUserName;
            public TextView mContent;
            public ImageView mDelete;

            public CompleteListViewHolder(View base) {
                mUserName = (TextView) base.findViewById(R.id.commentUserName);
                mContent = (TextView) base.findViewById(R.id.commentContent);
                mDelete = (ImageView) base.findViewById(R.id.commentDelete);
            }
        }

        private class DeleteComment extends AsyncTask<Void, Void, Void> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(Void... v) {
                // Creating service handler class instance
                ServiceHandler sh = new ServiceHandler();

                List<NameValuePair> params = new ArrayList<NameValuePair>();

                try {
                    params.add(new BasicNameValuePair("authentication_token",
                            HomeScreen.authentication_token));
                    params.add(new BasicNameValuePair("key", HomeScreen.key));

                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Making a request to url and getting response
                String jsonStr = sh.makeServiceCall(COMMENT_DELETE_URL,
                        ServiceHandler.DELETE, params);

                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }

        }

    }

    private void commentCreation(final String content, String user_id,
                                 final String commentable_id, String commentable_type,
                                 String authentication_token, String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                try {
                    String paramContent = params[0];
                    String paramUserId = params[1];
                    String paramCommentId = params[2];
                    String paramCommentType = params[3];
                    String paramAythToken = params[4];
                    String paramKey = params[5];

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(COMMENT_URL);
                    Log.e("REQUEST SEND", "REQ-----");

                    BasicNameValuePair contentBasicNameValuePair = new BasicNameValuePair("comment[content]", paramContent);
                    BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("comment[user_id]", paramUserId);
                    BasicNameValuePair commentIdBasicNameValuePair = new BasicNameValuePair("comment[commentable_id]", paramCommentId);
                    BasicNameValuePair commentTypeBasicNameValuePair = new BasicNameValuePair("comment[commentable_type]", paramCommentType);
                    BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAythToken);
                    BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                    List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                    nameValuePairList.add(contentBasicNameValuePair);
                    nameValuePairList.add(userIdBasicNameValuePair);
                    nameValuePairList.add(commentIdBasicNameValuePair);
                    nameValuePairList.add(commentTypeBasicNameValuePair);
                    nameValuePairList.add(authTokenBasicNameValuePair);
                    nameValuePairList.add(keyBasicNameValuePair);

                    try {
                        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                        httpPost.setEntity(urlEncodedFormEntity);
                        try {
                            HttpResponse httpResponse = httpClient.execute(httpPost);

                            InputStream inputStream = httpResponse.getEntity().getContent();
                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            StringBuilder stringBuilder = new StringBuilder();
                            String bufferedStrChunk = null;

                            while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                                stringBuilder.append(bufferedStrChunk);
                            }
                            return stringBuilder.toString();

                        } catch (ClientProtocolException cpe) {
                            cpe.printStackTrace();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }

                    } catch (UnsupportedEncodingException uee) {
                        uee.printStackTrace();
                    } catch (NullPointerException ne) {
                        ne.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                try {
                    JSONObject objRes = new JSONObject(result);
                    JSONObject objCom = objRes.getJSONObject("comment");
                    String content = objCom.getString("content");
                    String id = objCom.getString("commentable_id");

                    //@PKDEC01
                    String user_id = objCom.getString("user_id");

                    listOfCommentUser.add(USER_NAME);
                    listOfCommentContent.add(content);
                    listOfCommentIds.add(id);

                    //@PKDEC01
                    listOfUserIds.add(user_id);

                    customCommentAdapter = new CustomCommentAdapter(
                            getActivity(), listOfCommentContent,
                            listOfCommentUser, listOfCommentIds, listOfUserIds);
                    listOfComments.setAdapter(customCommentAdapter);
                    customCommentAdapter.notifyDataSetChanged();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(content, user_id, commentable_id,
                commentable_type, authentication_token, key);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.commentbutton:

                if (isInternetPresent) {
                    Log.e("COMMENTTYPE", SHARE_PICKLE_ID + "");
                    content = enterComment.getText().toString();
                    if (!SHARE_PICKLE_ID.equals("null")) {
                        commentable_type = "BundlePickleComment";

                        commentCreationOnShareBundle(content, "" + USER_ID,
                                SHARE_PICKLE_ID, commentable_type,
                                HomeScreen.authentication_token, HomeScreen.key);

                    } else {
                        commentable_type = "Question";
                        Log.e("COMMENTTYPE e", SHARE_PICKLE_ID + "");
                        commentCreation(content, "" + USER_ID, PICKLE_ID,
                                commentable_type, HomeScreen.authentication_token,
                                HomeScreen.key);
                    }

                    enterComment.setText("");

                    InputMethodManager imm = (InputMethodManager) getActivity()
                            .getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                } else {
                    String text = "No Internet Connection!";
                    showNoFeedsDialog(text);
                }

                break;

            case R.id.buttonlayout:
                getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }

    private void commentCreationOnShareBundle(final String content, String user_id, final String commentable_id, String commentable_type, String authentication_token, String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                try {
                    String paramContent = params[0];
                    String paramUserId = params[1];
                    String paramCommentId = params[2];
                    String paramCommentType = params[3];
                    String paramAythToken = params[4];
                    String paramKey = params[5];

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(COMMENT_URL);
                    Log.e("REQUEST SEND", "REQ-----");

                    BasicNameValuePair contentBasicNameValuePair = new BasicNameValuePair("comment[content]", paramContent);
                    BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("comment[user_id]", paramUserId);
                    BasicNameValuePair commentIdBasicNameValuePair = new BasicNameValuePair("comment[commentable_id]", paramCommentId);
                    BasicNameValuePair commentTypeBasicNameValuePair = new BasicNameValuePair("comment[commentable_type]", paramCommentType);
                    BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAythToken);
                    BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);

                    List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                    nameValuePairList.add(contentBasicNameValuePair);
                    nameValuePairList.add(userIdBasicNameValuePair);
                    nameValuePairList.add(commentIdBasicNameValuePair);
                    nameValuePairList.add(commentTypeBasicNameValuePair);
                    nameValuePairList.add(authTokenBasicNameValuePair);
                    nameValuePairList.add(keyBasicNameValuePair);

                    try {
                        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(nameValuePairList);
                        httpPost.setEntity(urlEncodedFormEntity);

                        try {
                            HttpResponse httpResponse = httpClient.execute(httpPost);

                            InputStream inputStream = httpResponse.getEntity().getContent();
                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            StringBuilder stringBuilder = new StringBuilder();
                            String bufferedStrChunk = null;

                            while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                                stringBuilder.append(bufferedStrChunk);
                            }
                            return stringBuilder.toString();

                        } catch (Exception ioe) {
                            ioe.printStackTrace();
                        }

                    } catch (Exception ne) {
                        ne.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                try {
                    JSONObject objRes = new JSONObject(result);
                    JSONObject objCom = objRes.getJSONObject("comment");
                    String content = objCom.getString("content");
                    String id = objCom.getString("commentable_id");
                    //@PKDEC01
                    String user_id = objCom.getString("user_id");

                    listOfCommentUser.add(USER_NAME);
                    listOfCommentContent.add(content);
                    listOfCommentIds.add(id);
                    //@PKDEC01
                    listOfUserIds.add(user_id);
                    customCommentAdapter = new CustomCommentAdapter(getActivity(), listOfCommentContent, listOfCommentUser, listOfCommentIds, listOfUserIds);
                    listOfComments.setAdapter(customCommentAdapter);
                    customCommentAdapter.notifyDataSetChanged();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }

            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(content, user_id, commentable_id,
                commentable_type, authentication_token, key);
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }
}
