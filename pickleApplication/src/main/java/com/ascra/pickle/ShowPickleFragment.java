package com.ascra.pickle;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.data.ShowPickleData;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

//@PKDEC03 UNVITED_URL changed to votes/ from /votes/ as the base url already has / at the end
public class ShowPickleFragment extends Fragment implements
        OnClickListener {

    View view;

    ImageView firstImage, secondImage, fbImage, twitterImage, post,
            firstVoteCountImage, secondVoteCountImage;
    TextView txtEdit, txtFirstVoteCount, txtSecondVoteCount, firstLabel,
            secondLabel, firstText, secondText, firstVoteText, secondVoteText;
    TextView pickleQuestion;

    RelativeLayout firstViewLayout, secondViewLayout;

    String pickleId, pickleName, from;
    String url_share;
    String URL, URLSTITCH;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    ArrayList<String> imagesList = new ArrayList<String>();
    ArrayList<String> labelList = new ArrayList<String>();
    ArrayList<String> textList = new ArrayList<String>();
    ArrayList<String> idsList = new ArrayList<String>();

    ImageLoader imageLoader;

    String editQuestion, editFirstImage, editSecondImage, editFirstText,
            editSecondText, editFirstLable, editSecondLabel, edit_pickle_id,
            edit_user_id, opid_one, opid_two;

    ArrayList<ShowPickleData> arrayListOfPickledata = new ArrayList<ShowPickleData>();

    ShowPickleData showPickleData;

    String firstOptionid = null;
    String secondOptionId = null;
    String pickle_id = null;
    String share_pickle_id = "";
    String pickleUser_id = null;
    String strFOptionImageUrl;
    boolean first_option_is_voted;
    boolean second_option_is_voted, fetured_pickle;
    String second_option_voted_id;
    String first_option_voted_id;
    long strSecondOption;
    long strFirstOption;
    long count_one;
    long count_two;

    private String VOTE_URL = Constant.PickleUrl_V1 + "votes.json";
    private String UNVOTED_URL = null;
    ImageView back;
    ProgressDialog rpd;
    ProgressBar inProcess;
    Tracker tracker;

    RelativeLayout buttonBackLayout;

    TextView btnBack;
    Typeface face;
    String one_id, one_name, two_id, two_name;

//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		 getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//		    getActionBar().hide();
//		setContentView(R.layout.show_pickle_fragment);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            view = inflater.inflate(R.layout.show_pickle_fragment, container, false);
//				view = inflater.inflate(R.layout.nearby_fragment, container, false);
        } catch (InflateException e) {
                /* map is already there, just return view as it is */
        }
        inProcess = (ProgressBar) view.findViewById(R.id.inProcess);
        try {
            back = (ImageView) view.findViewById(R.id.btnBackpress);
            back.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (from.equalsIgnoreCase("CREATE")
                            || from.equalsIgnoreCase("EDIT")) {
                        startActivity(new Intent(getActivity(),
                                HomeScreen.class));
                    } else {
                        getFragmentManager().popBackStack();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        HomeScreen.whichFragment = "SHOWING";

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");
        Bundle bundle = this.getArguments();

//		args.putString("USER_ONE_ID",""+listOfMyPicklesData.get(position).getFirst_option_id());
//		args.putString("USER_ONE_NAME",listOfMyPicklesData.get(position).getFirst_option_lable());
//		args.putString("USER_TWO_ID", ""+listOfMyPicklesData.get(position).getSecond_option_id());
//		args.putString("USER_TWO_NAME",listOfMyPicklesData.get(position).getSecond_option_lable());
//		args.putString("FLAG","1");

        if (bundle != null) {
            pickleId = bundle.getString("PICKLE_ID");
            pickleName = bundle.getString("PICKLE_NAME");
            from = bundle.getString("FROM");
            one_id = bundle.getString("USER_ONE_ID");
            one_name = bundle.getString("USER_ONE_NAME");
            two_id = bundle.getString("USER_TWO_ID");
            two_name = bundle.getString("USER_TWO_NAME");
            fetured_pickle = bundle.getBoolean("is_featured");

            Log.e("generated pickel id:", "" + pickleId);
            Log.e("URl to share", Constant.PickleUrl + "questions/" + pickleId);
            url_share = Constant.PickleUrl + "questions/" + pickleId;
            //URLSTITCH = Constant.PickleUrl +"update_share_pickle.json";
            URLSTITCH = Constant.PickleUrl + "update_share_pickle.json";
        }

        String picklename = pickleName.replace(" ", "-");
        picklename = picklename.replace("?", "");

//NS
  URL = Constant.PickleUrl + "questions/" + pickleId +".json";

       // URL = Constant.PickleUrl + "questions/" + pickleId + "-"
               // + picklename + ".json";

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            rpd = ProgressDialog.show(getActivity(), "",
                    "Loading Pickle ...", true);
            rpd.setCancelable(true);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        new loadingPickleData().execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        } else {
            String text = "No Internet Connection!";
            showNoFeedsDialog(text);
        }

        setUpViews();
        setTracker();

        if (from.equalsIgnoreCase("FOLLOWERS")
                || from.equalsIgnoreCase("SEARCH")
                || from.equalsIgnoreCase("FOLLOWING")
                || from.equalsIgnoreCase("NOTIFICATION")) {
            txtEdit.setVisibility(View.GONE);
        } else if (from.equalsIgnoreCase("EDIT")
                || from.equalsIgnoreCase("CREATE")) {
            txtEdit.setVisibility(View.VISIBLE);
        }
        return view;

    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "SHOW PICKLE SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void setUpViews() {

        imageLoader = new ImageLoader(
                getActivity().getApplicationContext());

//		btnBack = (TextView) view.findViewById(R.id.btnBack);
//		btnBack.setTypeface(face);
        firstViewLayout = (RelativeLayout) view.findViewById(R.id.firstView);
        secondViewLayout = (RelativeLayout) view.findViewById(R.id.secondView);
        pickleQuestion = (TextView) view.findViewById(R.id.pickleQuestion);
        pickleQuestion.setTypeface(face);
        firstImage = (ImageView) view.findViewById(R.id.firstOptionImageUrl);
        secondImage = (ImageView) view.findViewById(R.id.secondOptionImageUrl);
        firstLabel = (TextView) view.findViewById(R.id.firstOptionName);
        firstLabel.setTypeface(face);
        secondLabel = (TextView) view.findViewById(R.id.secondOptionName);
        secondLabel.setTypeface(face);
        firstText = (TextView) view.findViewById(R.id.firstText);
        firstText.setTypeface(face);
        secondText = (TextView) view.findViewById(R.id.secondText);
        secondText.setTypeface(face);
        txtFirstVoteCount = (TextView) view.findViewById(R.id.firstVoteCount);
        txtSecondVoteCount = (TextView) view.findViewById(R.id.secondVoteCount);
        firstVoteText = (TextView) view.findViewById(R.id.firstVoteText);
        firstVoteText.setTypeface(face);
        secondVoteText = (TextView) view.findViewById(R.id.secondVoteText);
        secondVoteText.setTypeface(face);
        firstVoteCountImage = (ImageView) view.findViewById(R.id.firstVoteImage);
        secondVoteCountImage = (ImageView) view.findViewById(R.id.secondVoteImage);
        txtEdit = (TextView) view.findViewById(R.id.editPickle);
        txtEdit.setTypeface(face);
        fbImage = (ImageView) view.findViewById(R.id.postOnFb);
        twitterImage = (ImageView) view.findViewById(R.id.postOnTwitter);
        post = (ImageView) view.findViewById(R.id.post);
        //	buttonBackLayout = (RelativeLayout) view.findViewById(R.id.buttonlayout);

        txtEdit.setOnClickListener(this);
        fbImage.setOnClickListener(this);
        twitterImage.setOnClickListener(this);
        firstImage.setOnClickListener(this);
        secondImage.setOnClickListener(this);
        firstText.setOnClickListener(this);
        secondText.setOnClickListener(this);
        firstVoteCountImage.setOnClickListener(this);
        secondVoteCountImage.setOnClickListener(this);
        post.setOnClickListener(this);
        //	buttonBackLayout.setOnClickListener(this);
        //@PKDEC06
        CommonMethods.setLogoTitleToHeaderView(getActivity(), view, R.string.showPicklesHeader, R.drawable.feed_color);
    }

    private class loadingPickleData extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh
                    .makeServiceCall(URL, ServiceHandler.GET, params);
            Log.e("show frag", "" + jsonStr + "" + URL + "" + HomeScreen.key + "" + HomeScreen.authentication_token);
            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        parseJSONPickle(mainObject);
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            rpd.dismiss();
        }

    }

    private void parseJSONPickle(JSONObject mainObject) {
        try {
            JSONObject objPickle = mainObject.getJSONObject("pickle");
            boolean strPDeactive = objPickle.getBoolean("pickle_deactive");
            boolean strPFeatured = objPickle.getBoolean("pickle_featured");
            String strPId = objPickle.getString("pickle_id");
            String strPImage = objPickle.getString("pickle_image");
            String strPName = objPickle.getString("pickle_name");
            String strPUserId;
            if (objPickle.has("pickle_user_id")) {
                strPUserId = objPickle.getString("pickle_user_id");
            } else {
                strPUserId = objPickle.getString("user_id");
            }
            String pickle_created_at = objPickle.getString("pickle_created_at");

            JSONObject objFirstOption = objPickle.getJSONObject("first_option");
            long strFId = objFirstOption.getLong("first_option_id");
            strFOptionImageUrl = objFirstOption
                    .getString("first_option_image_url");
            Log.e("Image url:", "" + strFOptionImageUrl);
            String strFOptionLable = objFirstOption
                    .getString("first_option_lable");
            long strFQuestionId = objFirstOption
                    .getLong("first_option_question_id");
            strFirstOption = objFirstOption.getLong("first_option_total_votes");
            String first_option_text = objFirstOption
                    .getString("first_option_text");
            first_option_voted_id = objFirstOption
                    .getString("first_option_voted_id");
            first_option_is_voted = objFirstOption
                    .getBoolean("first_option_is_voted");

            JSONObject objSecondOption = objPickle
                    .getJSONObject("second_option");
            long strSId = objSecondOption.getLong("second_option_id");
            String strSOptionImageUrl = objSecondOption
                    .getString("second_option_image_url");
            String strSOptionLable = objSecondOption
                    .getString("second_option_lable");
            long strSQuestionId = objSecondOption
                    .getLong("second_option_pickle_id");
            strSecondOption = objSecondOption
                    .getLong("second_option_total_votes");
            String second_option_text = objSecondOption
                    .getString("second_option_text");
            second_option_voted_id = objSecondOption
                    .getString("second_option_voted_id");
            second_option_is_voted = objSecondOption
                    .getBoolean("second_option_is_voted");

            ShowPickleData showPickleData = new ShowPickleData(strPDeactive,
                    strPFeatured, strPId, strPImage, strPName, strPUserId,
                    pickle_created_at, strFId, strFOptionImageUrl,
                    strFOptionLable, strFQuestionId, strFirstOption,
                    first_option_text, first_option_voted_id,
                    first_option_is_voted, strSId, strSOptionImageUrl,
                    strSOptionLable, strSQuestionId, strSecondOption,
                    second_option_text, second_option_voted_id,
                    second_option_is_voted);

            arrayListOfPickledata.add(showPickleData);

            firstOptionid = "" + strFQuestionId;
            secondOptionId = "" + strSQuestionId;
            pickle_id = strPId;
            pickleUser_id = strPUserId;

            if (strPName != null) {
                pickleQuestion.setText(strPName);
            }

            if (strFOptionLable != null) {

                if (strFOptionLable.equals("")
                        || strFOptionLable.equals("null")) {
                    firstLabel.setVisibility(View.GONE);
                } else {
                    firstLabel.setVisibility(View.VISIBLE);
                    firstLabel.setText(strFOptionLable);
                }

            }

            if (first_option_text != null) {
                if (first_option_text.equals("")
                        || first_option_text.equals("null")) {
                    firstText.setVisibility(View.GONE);
                } else {
                    firstText.setVisibility(View.VISIBLE);
                    firstText.setText(first_option_text);
                }

            }

            txtFirstVoteCount.setText("" + strFirstOption);

            if (strFOptionImageUrl != null) {
                if (strFOptionImageUrl
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    firstImage.setVisibility(View.GONE);

                } else {
                    firstImage.setVisibility(View.VISIBLE);
                    //imageLoader.DisplayImage(strFOptionImageUrl, firstImage);
                    Picasso.with(getActivity())
                            .load(strFOptionImageUrl)
                            .into(firstImage);
                }

            }

            if (strSOptionLable != null) {

                if (strSOptionLable.equals("")
                        || strSOptionLable.equals("null")) {
                    secondLabel.setVisibility(View.GONE);
                } else {
                    secondLabel.setVisibility(View.VISIBLE);
                    secondLabel.setText(strSOptionLable);
                }

            }

            if (second_option_text != null) {
                if (second_option_text.equals("")
                        || second_option_text.equals("null")) {
                    secondText.setVisibility(View.GONE);
                } else {
                    secondText.setVisibility(View.VISIBLE);
                    secondText.setText(second_option_text);
                }
            }

            txtSecondVoteCount.setText("" + strSecondOption);

            if (strSOptionImageUrl != null) {

                if (strSOptionImageUrl
                        .equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                    secondImage.setVisibility(View.GONE);

                } else {
                    secondImage.setVisibility(View.VISIBLE);
                    //imageLoader.DisplayImage(strSOptionImageUrl, secondImage);
                    Picasso.with(getActivity())
                            .load(strSOptionImageUrl)
                            .into(secondImage);
                }

            }

            if (first_option_is_voted && !second_option_is_voted) {
                firstViewLayout.setBackgroundColor(Color.parseColor("#F08080"));
                txtFirstVoteCount.setTextColor(Color.parseColor("#F08080"));
                firstVoteCountImage.setImageResource(R.drawable.vote_color);
                firstVoteText.setTextColor(Color.parseColor("#F08080"));

                secondViewLayout.setBackgroundColor(Color
                        .parseColor("#110000"));
                txtSecondVoteCount.setTextColor(Color.parseColor("#000000"));
                secondVoteCountImage.setImageResource(R.drawable.vote_black);
                secondVoteText.setTextColor(Color.parseColor("#000000"));

                first_option_is_voted = true;
                second_option_is_voted = false;

            } else if (!first_option_is_voted && second_option_is_voted) {

                firstViewLayout.setBackgroundColor(Color
                        .parseColor("#11000000"));
                txtFirstVoteCount.setTextColor(Color.parseColor("#000000"));
                firstVoteCountImage.setImageResource(R.drawable.vote_black);
                firstVoteText.setTextColor(Color.parseColor("#000000"));

                secondViewLayout
                        .setBackgroundColor(Color.parseColor("#F08080"));
                txtSecondVoteCount.setTextColor(Color.parseColor("#F08080"));
                secondVoteCountImage.setImageResource(R.drawable.vote_color);
                secondVoteText.setTextColor(Color.parseColor("#F08080"));

                first_option_is_voted = false;
                second_option_is_voted = true;

            } else {
                firstViewLayout.setBackgroundColor(Color
                        .parseColor("#11000000"));
                txtFirstVoteCount.setTextColor(Color.parseColor("#000000"));
                firstVoteCountImage.setImageResource(R.drawable.vote_black);
                firstVoteText.setTextColor(Color.parseColor("#000000"));

                secondViewLayout.setBackgroundColor(Color
                        .parseColor("#110000"));
                txtSecondVoteCount.setTextColor(Color.parseColor("#000000"));
                secondVoteCountImage.setImageResource(R.drawable.vote_black);
                secondVoteText.setTextColor(Color.parseColor("#000000"));

                first_option_is_voted = false;
                second_option_is_voted = false;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.editPickle:
//			Intent intent = new Intent(getActivity(),
//					EditPickleFragmentActivity.class);
//			intent.putExtra("PICKLE_ID", pickleId);
//			intent.putExtra("PICKLE_NAME", pickleName);
//			intent.putExtra("AUTH", HomeScreen.authentication_token);
//			intent.putExtra("KEY", HomeScreen.key);
//			intent.putExtra("USERID", HomeScreen.user_id);
//			startActivity(intent);

                //	args.putString("USER_ONE_ID",""+listOfMyPicklesData.get(position).getFirst_option_id());
//			args.putString("USER_ONE_NAME",listOfMyPicklesData.get(position).getFirst_option_lable());
//			args.putString("USER_TWO_ID", ""+listOfMyPicklesData.get(position).getSecond_option_id());
//			args.putString("USER_TWO_NAME",listOfMyPicklesData.get(position).getSecond_option_lable());
//			args.putString("FLAG","1");

                EditPickleFragmentActivity ShowPickleFragmentobj = new EditPickleFragmentActivity();
                Bundle args = new Bundle();
                args.putString("PICKLE_ID", pickleId);
                args.putString("PICKLE_NAME", pickleName.replace("?", ""));
                args.putString("AUTH", HomeScreen.authentication_token);
                args.putString("KEY", HomeScreen.key);
                args.putString("USERID", HomeScreen.user_id);

                args.putString("USER_ONE_ID", "100001058387150");
                args.putString("USER_ONE_NAME", one_name);
                args.putBoolean("is_featured", fetured_pickle);
                args.putString("USER_TWO_ID", "100001058387150");
                args.putString("USER_TWO_NAME", two_name);

                args.putString("FLAG", "1");
                ShowPickleFragmentobj.setArguments(args);
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                transaction.addToBackStack(null);
                transaction.commit();

                break;

            case R.id.post:

//		      
                break;
            case R.id.postOnFb:

                new Stitchimg().execute();

//			Intent shareIntent1 = new Intent(android.content.Intent.ACTION_SEND);
//			shareIntent1.setType("text/plain");
//			shareIntent1.putExtra(android.content.Intent.EXTRA_TEXT, ""+Constant.PickleUrl + "questions/" + pickleId);
//			
//			Log.e("pickleshare", ""+Constant.PickleUrl + "questions/" + pickleId);
//			PackageManager pm1 = v.getContext().getPackageManager();
//			List<ResolveInfo> activityList1 = pm1.queryIntentActivities(shareIntent1, 0);
//			for (final ResolveInfo app : activityList1) {
//			    if ((app.activityInfo.name).contains("facebook")) {
//			        final ActivityInfo activity = app.activityInfo;
//			        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
//			        shareIntent1.addCategory(Intent.CATEGORY_LAUNCHER);
//			        shareIntent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//			        shareIntent1.setComponent(name);
//			        v.getContext().startActivity(shareIntent1);
//			    }
//			}
                break;

            case R.id.postOnTwitter:

                break;
            case R.id.firstText:
                firstOptionMethod();
                break;
            case R.id.secondText:
                secondOptionMethod();
                break;
            case R.id.firstOptionImageUrl:
                firstOptionMethod();
                break;
            case R.id.secondOptionImageUrl:
                secondOptionMethod();
                break;
            case R.id.firstVoteImage:
                firstOptionMethod();
                break;
            case R.id.secondVoteImage:
                secondOptionMethod();
                break;
            case R.id.buttonlayout:

                if (from.equalsIgnoreCase("CREATE")
                        || from.equalsIgnoreCase("EDIT")) {
                    startActivity(new Intent(getActivity(),
                            HomeScreen.class));
                } else {
                    getFragmentManager().popBackStack();
                }

                break;
            default:
                break;
        }
    }

    void shareTextUrl(String pid) {

        try {
            Intent intent1 = new Intent();
            intent1.setClassName("com.facebook.katana",
                    "com.facebook.katana.activity.composer.ImplicitShareIntentHandler");
            intent1.setAction("android.intent.action.SEND");
            intent1.setType("text/plain");
            intent1.putExtra("android.intent.extra.TEXT", "" + Constant.PickleUrl + "questions/" + pickleId);
            startActivity(intent1);
        } catch (Exception e) {
            // If we failed (not native FB app installed), try share through
            // SEND
            Intent intent = new Intent(Intent.ACTION_SEND);
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u="
                    + "" + Constant.PickleUrl + "questions/" + pickleId;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
            startActivity(intent);
        }
    }

    private class Stitchimg extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            inProcess.setVisibility(View.VISIBLE);
            super.onPreExecute();

        }

        @Override
        protected Void doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();
            List<NameValuePair> params1 = new ArrayList<NameValuePair>();
            params1.add(new BasicNameValuePair("q_id", pickle_id));
            params1.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params1.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(URLSTITCH,
                    ServiceHandler.GET, params1);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            mainObject.getString("success").equalsIgnoreCase("true");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                });

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            inProcess.setVisibility(View.GONE);

//				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
//				shareIntent.setType("text/plain");
//				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, ""+Constant.PickleUrl + "questions/" + pickleId);
//				
//				Log.e("pickleshare", ""+Constant.PickleUrl + "questions/" + pickleId);
//				PackageManager pm = getActivity().getPackageManager();
//				List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
//				for (final ResolveInfo app : activityList) {
//				    if ((app.activityInfo.name).contains("facebook")) {
//				        final ActivityInfo activity = app.activityInfo;
//				        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
//				        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
//				        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//				        shareIntent.setComponent(name);
//				        getActivity().startActivity(shareIntent);
//				 
//				    }
//				}

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL");
            i.putExtra(android.content.Intent.EXTRA_TEXT, "" + Constant.PickleUrl + "questions/" + pickleId);
            startActivity(Intent.createChooser(i, "Share URL"));

        }

    }

//		  try
//		  {
//		      List<Intent> targetedShareIntents = new ArrayList<Intent>();
//		      Intent share = new Intent(android.content.Intent.ACTION_SEND);
//		      share.setType("image/jpeg");
//		      List<ResolveInfo> resInfo =getActivity(). getPackageManager().queryIntentActivities(share, 0);
//		      if (!resInfo.isEmpty()){
//		          for (ResolveInfo info : resInfo) {
//		              Intent targetedShare = new Intent(android.content.Intent.ACTION_SEND);
//		              targetedShare.setType("image/jpeg"); // put here your mime type
//		              if (info.activityInfo.packageName.toLowerCase().contains("facebook") || info.activityInfo.name.toLowerCase().contains("facebook")) {
//		                  targetedShare.putExtra(Intent.EXTRA_SUBJECT, "Sample Photo");
//		                  targetedShare.putExtra(Intent.EXTRA_TEXT,"This photo is created by App Name");
//		                  targetedShare.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(""+Constant.PickleUrl + "questions/" + pickleId)) );
//		                  targetedShare.setPackage(info.activityInfo.packageName);
//		                  targetedShareIntents.add(targetedShare);
//		              }
//		          }
//		          Intent chooserIntent = Intent.createChooser(targetedShareIntents.remove(0), "Select app to share");
//		          chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetedShareIntents.toArray(new Parcelable[]{}));
//		          startActivity(chooserIntent);
//		      }
//		  }
//		  catch(Exception e){
//		      Log.v("VM","Exception while sending image on" + "facebook" + " "+  e.getMessage());
//		  }
//		Intent share = new Intent(android.content.Intent.ACTION_SEND);
//		share.setType("text/plain");
//		//share.setType("text/*");
//		//share.setType("image/*");
//		share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
//
//		// Add data to the intent, the receiving app will decide
//		// what to do with it.
//		share.putExtra(Intent.EXTRA_SUBJECT, "Share Using Pickle");
//		share.putExtra(Intent.EXTRA_STREAM, ""+Constant.PickleUrl + "questions/" + pickleId);
//		share.putExtra(Intent.EXTRA_TEXT, ""+Constant.PickleUrl + "questions/" + pickleId);
//
//		startActivity(Intent.createChooser(share, "Share link!"));

    private void firstOptionMethod() {

        if (first_option_is_voted == false && second_option_is_voted == false) {

            firstViewLayout.setBackgroundColor(Color.parseColor("#F08080"));
            txtFirstVoteCount.setTextColor(Color.parseColor("#F08080"));
            firstVoteCountImage.setImageResource(R.drawable.vote_color);
            firstVoteText.setTextColor(Color.parseColor("#F08080"));

            first_option_is_voted = true;
            second_option_is_voted = false;

            count_one = count_one + 1;
            txtFirstVoteCount.setText("" + (count_one + strFirstOption));

            try {
                pickleVote(firstOptionid, pickle_id, share_pickle_id,
                        pickleUser_id, HomeScreen.authentication_token,
                        HomeScreen.key,1);// ,1 added by NS
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (first_option_is_voted == true
                && second_option_is_voted == false) {

            firstViewLayout.setBackgroundColor(Color.parseColor("#11000000"));
            txtFirstVoteCount.setTextColor(Color.parseColor("#000000"));
            firstVoteCountImage.setImageResource(R.drawable.vote_black);
            firstVoteText.setTextColor(Color.parseColor("#000000"));

            first_option_is_voted = false;
            second_option_is_voted = false;

            count_one = count_one - 1;
            txtFirstVoteCount.setText("" + (count_one + strFirstOption));

            UNVOTED_URL = Constant.PickleUrl + "votes/"
                    + first_option_voted_id + ".json";

            try {
                new UnVoted().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (first_option_is_voted == false
                && second_option_is_voted == true) {
        }

    }

    private void secondOptionMethod() {

        if (first_option_is_voted == false && second_option_is_voted == false) {

            secondViewLayout.setBackgroundColor(Color.parseColor("#F08080"));
            txtSecondVoteCount.setTextColor(Color.parseColor("#F08080"));
            secondVoteCountImage.setImageResource(R.drawable.vote_color);
            secondVoteText.setTextColor(Color.parseColor("#F08080"));

            first_option_is_voted = false;
            second_option_is_voted = true;

            count_two = count_two + 1;
            txtSecondVoteCount.setText("" + (count_two + strSecondOption));

            try {
                 pickleVote(secondOptionId, pickle_id, share_pickle_id,
                        pickleUser_id, HomeScreen.authentication_token,
                        HomeScreen.key,2);// ,2 added by NS
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (first_option_is_voted == false
                && second_option_is_voted == true) {

            secondViewLayout.setBackgroundColor(Color.parseColor("#11000000"));
            txtSecondVoteCount.setTextColor(Color.parseColor("#000000"));
            secondVoteCountImage.setImageResource(R.drawable.vote_black);
            secondVoteText.setTextColor(Color.parseColor("#000000"));

            first_option_is_voted = false;
            second_option_is_voted = false;

            count_two = count_two - 1;
            txtSecondVoteCount.setText("" + (count_two + strSecondOption));

            UNVOTED_URL = Constant.PickleUrl + "votes/"
                    + second_option_voted_id + ".json";

            try {
                new UnVoted().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (first_option_is_voted == true
                && second_option_is_voted == false) {
        }

    }

//NS
    private void pickleVote(String option_id, String question_id,
                            String share_pickle_id, final String user_id,
                            String authentication_token, String key, final int id) {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramOptionId = params[0];
                String paramQuestionId = params[1];
                String paramSharePickleId = params[2];
                String paramUserId = params[3];
                String paramAuthenticationToken = params[4];
                String paramKey = params[5];
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(VOTE_URL);
                Log.e("REQUEST SEND", "REQ-----");


                BasicNameValuePair questionIdBasicNameValuePair = new BasicNameValuePair(
                        "question_id", paramQuestionId);
                BasicNameValuePair optionIdBasicNameValuePair = new BasicNameValuePair(
                        "option_id", paramOptionId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair(
                        "user_id", paramUserId);
                BasicNameValuePair sharePickleIdBasicNameValuePair = new BasicNameValuePair(
                            "share_pickle_id", paramSharePickleId);

              BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair(
                       "authentication_token", paramAuthenticationToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair(
                        "key", paramKey);
            //    HttpPost httpPost = new HttpPost(VOTE_URL);


                Log.e("@Create VOTE_URL ",VOTE_URL);
                Log.e("@Create","user id--"+user_id);



                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(optionIdBasicNameValuePair);
                nameValuePairList.add(questionIdBasicNameValuePair);
                nameValuePairList.add(sharePickleIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
//NS
 parseJSONVote(stringBuilder);
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }


            private void parseJSONVote(StringBuilder stringBuilder) {
                String strJson=stringBuilder.toString();
                Log.e("@Create JSON",strJson);
                try {
                    JSONObject JsonVote=new JSONObject(strJson);
                   JSONObject JsonItem=JsonVote.getJSONObject("vote");
                    if(id==1){
                        first_option_voted_id =JsonItem.getString("id");
                    }else{
                        second_option_voted_id =JsonItem.getString("id");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
            }

        }
        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute(option_id, question_id, share_pickle_id,
                user_id, authentication_token, key);

    }

    private class UnVoted extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try {
                params.add(new BasicNameValuePair("authentication_token",
                        HomeScreen.authentication_token));
                params.add(new BasicNameValuePair("key", HomeScreen.key));

            } catch (Exception e) {
                e.printStackTrace();
            }
            String jsonStr = sh.makeServiceCall(UNVOTED_URL,
                    ServiceHandler.DELETE, params);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

}
