package com.ascra.pickle;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.NetworkOnMainThreadException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ascra.data.AllFeedsModel;
import com.ascra.data.Model;
import com.ascra.data.Vote;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.ExtendedViewPager;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.ServiceHandler;
import com.ascra.utils.TouchImageView;
import com.example.nsdl.ShaGenerater;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.Indicator;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

//@PKDEC05 /votes/ changed to votes/ as Constant url already contains / at the end. [UNVOTED_URL]
public class FeedsFragment extends Fragment {

    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    JSONArray auditsArray;
    JSONArray arrayOfComments;
    View view;
    StickyListHeadersListView listOfPickleFeeds;
    int tot_count,tot_records;
    LinearLayout linlay;
    AVLoadingIndicatorView inProgress;
    String user_name;
    ArrayList<AllFeedsModel> arrayListOfFeeddata;
    AllFeedsModel allFeedModel, allFeedModel2;
    ArrayList<Vote> voteData = new ArrayList<Vote>();
    Bitmap bitmap;
    URL url;
    ImageLoader imageLoader;
    Dialog dialog, reportdialog;
    String share_pickle_id = "";
    int page_count = 1;
    HttpEntity resEntity;
    MyFeedsCustomAdapter myFeedsCustomAdapter;
    String questionId;
    String reportText;
    String report_content;
    Dialog  rpd;
    HashMap<String, String> report_data = new HashMap<String, String>();
    String firstOptionId = null;
    String secondOptionId = null;
    ProgressBar btnLoadMore;
    Tracker tracker;
    int getviewPos;
    Typeface face;
    ActionBar actionbar;
    String strUEmail, strUIs_brand, strUName, strUProfilePicUrl, strUserName, strPId, strPImage, strPName, strPUserId, pickle_created_at, generatedSharePickleId="", strFOptionImageUrl, strFOptionLable,
            first_option_text, first_option_voted_id, strSOptionImageUrl, strSOptionLable, second_option_text, second_option_voted_id;
    boolean strPDeactive, strPFeatured, isPickleShared, is_my_pickle, first_option_is_voted, second_option_is_voted;
    long strFQuestionId, strFirstOption, strSQuestionId, strSId, strFId, strSecondOption;
    int strUId;
    //@PK28NOV : user_id for identifying the comment author in comments so as to show the delete icon accordingly
    String user_id;
    private String URL_FEED = null;
    private String COMMENT_URL = null;
    private String COMMENT_DELETE_URL = null;
    private String SHAREBUNDLE_URL = null;
    private String UPDATE_REPORTLIST_URL = Constant.PickleUrl
            + "report_pickles.json";
    private String VOTE_URL = Constant.PickleUrl_V1 + "votes.json";
    private String UNVOTED_URL = null;
    private String REPORT_URL = Constant.PickleUrl + "report_pickles.json";
    private String REPORT_LIST_URL = Constant.PickleUrl + "report_pickles.json";
    private String DELETE_SHARE_PICKLE_URL = null;
    private String REPORT_SHOW;
    private LayoutInflater inflater;
    TextView msg;
    //@NS9DEC :
    int lastLoadPosition=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pickle_feeds, null);

        try {
            ((HomeScreen) getActivity()).showActionBar(true);
            ((HomeScreen) getActivity()).showBottomStrip(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        HomeScreen.whichFragment = "PICKLEFEEDS";
//		actionbar=getActivity().getActionBar();
//        LayoutInflater mInflater = LayoutInflater.from(getActivity());
//
//	 		View mCustomView = mInflater.inflate(R.layout.pickle_custom_actionbar, null);
//	 		
//	 		
//	 		
//	 		actionbar.setCustomView(mCustomView);
//	 		actionbar.setDisplayShowCustomEnabled(true);

//arrayListOfFeeddata.clear();

        user_name = SaveUtils.getUserFromPref(getResources().getString(R.string.PreferenceFileName), getActivity(), SaveUtils.USER, null);

        //@NS9DEC : Url package changed
        if(HomeScreen.session_name!=null) {
            URL_FEED = Constant.PickleUrl_V1 + "feeds/"
                    + HomeScreen.session_name.replace(" ", "%20") + ".json";
        }

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        setUpViews();
        if (isInternetPresent) {
            inProgress.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

//						if (Model.getInstatnce().getArrayListOfFeeddata().size() > 0 && Model.getInstatnce().getNewcomments()==false) {
//							
//							allFeedModel = new AllFeedsModel(auditsArray, strUId,
//									strUEmail, strUIs_brand, strUName,
//									strUProfilePicUrl, strUserName, strPDeactive,
//									strPFeatured, strPId, strPImage, strPName,
//									strPUserId, pickle_created_at, isPickleShared,
//									generatedSharePickleId, is_my_pickle, strFId,
//									strFOptionImageUrl, strFOptionLable,
//									strFQuestionId, strFirstOption,
//									first_option_text, first_option_voted_id,
//									first_option_is_voted, strSId,
//									strSOptionImageUrl, strSOptionLable,
//									strSQuestionId, strSecondOption,
//									second_option_text, second_option_voted_id,
//									second_option_is_voted, arrayOfComments);
//
//							Model.getInstatnce().getArrayListOfFeeddata().add(allFeedModel);
//							myFeedsCustomAdapter = new MyFeedsCustomAdapter(
//									getActivity(), Model.getInstatnce().getArrayListOfFeeddata());
//						    listOfPickleFeeds.setAdapter(myFeedsCustomAdapter);
//
//						} else 
//						{
                        page_count = 1;
                        new loadingAllFeeds().execute();

//						}

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//					try {
//					//	new loadingReportPickleShow().execute();
//					} catch (Exception e) {
//
//					}
                }
            }).start();

        } else {
            String text = "No internet connection!";
            showNoFeedsDialog(text);

            inProgress.hide();
        }

        setTracker();

        face = Typeface.createFromAsset(getActivity().getAssets(),
                "fonts/sniglet-regular.ttf");
        btnLoadMore = new ProgressBar(getActivity());



/*
        btnLoadMore.setBackgroundColor(Color
                .parseColor("#F08080"));
*/

        btnLoadMore.setVisibility(View.GONE);
        // Adding Load More button to lisview at bottom
        listOfPickleFeeds.addFooterView(btnLoadMore);
//@NS9DEC :
        listOfPickleFeeds.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

                if(lastLoadPosition!=(i+i1))
                {
                    if ((i + i1) >= arrayListOfFeeddata.size() && arrayListOfFeeddata.size() < tot_records) {
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent && page_count <= tot_count) {
                            btnLoadMore.setVisibility(View.VISIBLE);
                            new loadMoreListView().execute();
                            lastLoadPosition=i+i1;
                        } else {
                            return;
                        }
                    }
                }

            }
        });
//		listOfPickleFeeds.setOnScrollListener(new OnScrollListener() {
//
//		        @Override
//		        public void onScroll(AbsListView view, int firstVisibleItem,
//		                int visibleItemCount, int totalItemCount) {
//		            }
//		        int mPosition=0;
//		        int mOffset=0;
//
//		        @Override
//		        public void onScrollStateChanged(AbsListView view, int scrollState) {
//		             int position = listOfPickleFeeds.getFirstVisiblePosition();
//		               View v = listOfPickleFeeds.getChildAt(0);
//		                int offset = (v == null) ? 0 : v.getTop();
//
//		                if (mPosition < position || (mPosition == position && mOffset < offset)){
//		                     // Scrolled up 
//		                	Animation animFadeOut = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
//		                   
//		                    linlay.setAnimation(animFadeOut);
//		                	//if necessary then call:
//		                    linlay.setVisibility(View.GONE);
//
//		                } else {
//		                     // Scrolled down
//		                	 Animation animFadeIn = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
//		                	  linlay.setAnimation(animFadeIn);
//		                	  linlay.setVisibility(View.VISIBLE);
//
//		                }
//		        }
//		    }); 

//        LayoutInflater myinflater = getActivity().getLayoutInflater();
//        ViewGroup myHeader = (ViewGroup)myinflater.inflate(R.layout.header_common, listOfPickleFeeds, false);
//        listOfPickleFeeds.addHeaderView(myHeader, null, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(getActivity()).activityStart(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(getActivity()).activityStop(getActivity());
    }

    private void setUpViews() {
        listOfPickleFeeds = (StickyListHeadersListView) view.findViewById(R.id.lstPickleFeeds);
        inProgress = (AVLoadingIndicatorView) view.findViewById(R.id.inProgress);
        linlay = (LinearLayout) view.findViewById(R.id.titleLayout);
        //NS changed dialog layout every rpd been changed
        rpd=new Dialog(getActivity());
        rpd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rpd.setContentView(R.layout.dialog_progress);
        msg = (TextView) rpd.findViewById(R.id.msg);
        rpd.setCanceledOnTouchOutside(false);
    }

    private void setTracker() {
        tracker = GoogleAnalytics.getInstance(getActivity()).getTracker("UA-57240357-1");
        if (tracker != null) {
            tracker.set(Fields.SCREEN_NAME, "USER FEEDS SCREEN");
            tracker.send(MapBuilder.createAppView().build());
        }
    }

    private void parseJSONFEED(JSONObject mainObject) {


        try {
            Log.e("@Create","mainobject");
            auditsArray = null;
            //@PKDEC05 : Added coz the above <auditsArray = null;> wasn't working most of the time. Double check!
            if (auditsArray != null) {
                auditsArray = null;
            }
//            Log.e("FeedsResponse", mainObject + "");
//			if (mainObject.getJSONArray("audits").equals(null)
//					|| mainObject.getJSONArray("audits").length() > 0) {
//
//			} else {
//				String text = "No Feeds Available!";
//				showNoFeedsDialog(text);
//			}
            if (!mainObject.has("audits")) {
                //@PK30NOV this condition
                //@NS9DEC :
                btnLoadMore.setVisibility(View.GONE);
            } else if (mainObject.getJSONArray("audits").length() == 0 && page_count == 1) {
                //@NS9DEC :
                btnLoadMore.setVisibility(View.GONE);
            } else if (mainObject.getJSONArray("audits").length() == 0) {
                //@NS9DEC :
                btnLoadMore.setVisibility(View.GONE);
            }
            if (mainObject.has("audits")) {
//@NS9DEC : pagination
                tot_records=mainObject.getInt("total_count");
                tot_count = (int) (Math.ceil((tot_records) * 1.0 / 5.0));//@NS9DEC :


                auditsArray = mainObject.getJSONArray("audits");
                //@PK30NOV this 'if' condition added. Contents were already +nt.
                Log.e("@Create", "arrayListOfFeeddata " + (arrayListOfFeeddata != null ? arrayListOfFeeddata.size() : "NA") + "\tPage = " + page_count);
                if (arrayListOfFeeddata == null) {
                    arrayListOfFeeddata = new ArrayList<AllFeedsModel>();
                    arrayListOfFeeddata.add(0, null);
                }
                //@PKDEC05 : else part
//                else if (page_count == 1) {
//                    arrayListOfFeeddata.clear();
//                }

                try {
                    for (int i = 0; i < auditsArray.length(); i++) {
                        //@PK30NOV the following for is original one
//                    for (int i = 0; i < auditsArray.length(); i++) {

// @PK30NOV  This is because the first element received in audits array is empty json array sometimes.
                        if (auditsArray.get(i) instanceof JSONObject) {
                            JSONObject subMain = auditsArray.getJSONObject(i);

                            if (subMain != null || subMain.length() > 0) {

                                JSONObject objUser = subMain.getJSONObject("user");
                                strUId = objUser.getInt("user_id");

                                strUEmail = objUser.getString("user_email");
                                strUIs_brand = objUser.getString("user_is_brand");
                                strUName = objUser.getString("user_name");
                                strUProfilePicUrl = objUser.getString("user_profile_pic_url");
                                strUserName = objUser.getString("user_username");
                                JSONObject objPickle = subMain.getJSONObject("pickle");
                                strPDeactive = objPickle.getBoolean("pickle_deactive");
                                strPFeatured = objPickle.getBoolean("is_featured");
                                strPId = objPickle.getString("pickle_id");

                                questionId = strPId;

                                strPImage = objPickle.getString("pickle_image");
                                strPName = objPickle.getString("pickle_name");

                                if (objPickle.has("pickle_user_id")) {
                                    strPUserId = objPickle.getString("pickle_user_id");
                                } else {
                                    strPUserId = objPickle.getString("user_id");
                                }
                                if (objPickle.has("pickle_status")) {

                                }
                                pickle_created_at = objPickle.getString("pickle_created_at");
                                isPickleShared = objPickle.getBoolean("is_shared");
                                generatedSharePickleId = "";
                                if (objPickle.has("share_pickle_id")) {
                                    generatedSharePickleId = objPickle.getString("share_pickle_id");
                                }

                                is_my_pickle = objPickle.getBoolean("is_my_pickle");
                                JSONObject objFirstOption = subMain.getJSONObject("first_option");
                                strFId = objFirstOption.getLong("first_option_id");
                                strFOptionImageUrl = objFirstOption.getString("first_option_image_url");
                                strFOptionLable = objFirstOption.getString("first_option_lable");
                                strFQuestionId = objFirstOption.getLong("first_option_question_id");
                                strFirstOption = objFirstOption.getLong("first_option_total_votes");
                                first_option_text = objFirstOption.getString("first_option_text");
                                first_option_voted_id = objFirstOption.getString("first_option_voted_id");
                                first_option_is_voted = objFirstOption.getBoolean("first_option_is_voted");
                                JSONObject objSecondOption = subMain.getJSONObject("second_option");
                                strSId = objSecondOption.getLong("second_option_id");
                                strSOptionImageUrl = objSecondOption.getString("second_option_image_url");
                                strSOptionLable = objSecondOption.getString("second_option_lable");
                                strSQuestionId = objSecondOption.getLong("second_option_pickle_id");
                                strSecondOption = objSecondOption.getLong("second_option_total_votes");
                                second_option_text = objSecondOption.getString("second_option_text");
                                second_option_voted_id = objSecondOption.getString("second_option_voted_id");
                                second_option_is_voted = objSecondOption.getBoolean("second_option_is_voted");
                                arrayOfComments = subMain.getJSONArray("comments");

                                allFeedModel = new AllFeedsModel(auditsArray, strUId,
                                        strUEmail, strUIs_brand, strUName,
                                        strUProfilePicUrl, strUserName, strPDeactive,
                                        strPFeatured, strPId, strPImage, strPName,
                                        strPUserId, pickle_created_at, isPickleShared,
                                        generatedSharePickleId, is_my_pickle, strFId,
                                        strFOptionImageUrl, strFOptionLable,
                                        strFQuestionId, strFirstOption,
                                        first_option_text, first_option_voted_id,
                                        first_option_is_voted, strSId,
                                        strSOptionImageUrl, strSOptionLable,
                                        strSQuestionId, strSecondOption,
                                        second_option_text, second_option_voted_id,
                                        second_option_is_voted, arrayOfComments);

                                arrayListOfFeeddata.add(allFeedModel);

                            }
                            Log.e("@Create", "arrayListOfFeeddata " + (arrayListOfFeeddata != null ? arrayListOfFeeddata.size() : "NA") + "\tPage = " + page_count);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            //@NS9DEC :set page count
            if (page_count < tot_count) {
                page_count += 1;

            } else {
                btnLoadMore.setVisibility(View.GONE);
            }
//@NS9DEC :


            Model.getInstatnce().setArrayListOfFeeddata(arrayListOfFeeddata);
            //@NS9DEC : Remove condition
    /*        btnLoadMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // Starting a new async task
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        new loadMoreListView().execute();
//						Toast.makeText(getActivity(),
//								"in load more", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(),
                                "No Internet Connection	You don't have internet connection.", Toast.LENGTH_LONG).show();
                    }
                }
            });
*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pickleVote(final AllFeedsModel model, final Boolean isFirst) {
       /* final String[] _id = {"1264"};//TODO This should be dynamic as per the _id[0] below.*/
         class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

             String id="";
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                msg.setText("Submitting your vote");
                rpd.show();
            }

            @Override
            protected String doInBackground(String... params) {

                String paramOptionId = params[0];
                String paramQuestionId = params[1];
                String paramSharePickleId = params[2];



                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(VOTE_URL);
                BasicNameValuePair optionIdBasicNameValuePair = new BasicNameValuePair("option_id", paramOptionId);
                BasicNameValuePair questionIdBasicNameValuePair = new BasicNameValuePair("question_id", paramQuestionId);
                BasicNameValuePair sharePickleIdBasicNameValuePair = new BasicNameValuePair("share_pickle_id", paramSharePickleId);
                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair("user_id", HomeScreen.user_id);
                BasicNameValuePair auth  = new BasicNameValuePair("authentication_token", HomeScreen.authentication_token);
                BasicNameValuePair key = new BasicNameValuePair("key", HomeScreen.key);


                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(optionIdBasicNameValuePair);
                nameValuePairList.add(questionIdBasicNameValuePair);
                nameValuePairList.add(sharePickleIdBasicNameValuePair);
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(auth);
                nameValuePairList.add(key);


                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient.execute(httpPost);
                        InputStream inputStream = httpResponse.getEntity().getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;
                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        String strJson = stringBuilder.toString();
                        Log.e("@Create JSON", strJson);
                        try {
                            JSONObject JsonVote = new JSONObject(strJson);
                            JSONObject JsonItem = JsonVote.getJSONObject("vote");
                            return JsonItem.getString("id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                            return null;
                        }

                       // return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(result!=null){
                    id =result;
                    if(isFirst)
                        model.setFirst_option_voted_id(id);
                    else
                        model.setSecond_option_voted_id(id);

                }
               myFeedsCustomAdapter.notifyDataSetChanged();
                rpd.dismiss();
            }


         }


        new SendPostReqAsyncTask().execute(String.valueOf(model.getStrFId2()), model.getStrPId2(),share_pickle_id);


    }

    private void reportDailog(final String pickelId, final String userId,
                              int position) {
        reportdialog = new Dialog(getActivity());
        reportdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reportdialog.setContentView(R.layout.report_dialog);

        RelativeLayout op1 = (RelativeLayout) reportdialog.findViewById(R.id.firstLay);
        RelativeLayout op2 = (RelativeLayout) reportdialog.findViewById(R.id.secondLay);
        RelativeLayout op3 = (RelativeLayout) reportdialog.findViewById(R.id.thirdLay);
        RelativeLayout op4 = (RelativeLayout) reportdialog.findViewById(R.id.fourLay);
        Button btnSubmit = (Button) reportdialog.findViewById(R.id.btnsubmit);
        final TextView t1 = (TextView) reportdialog.findViewById(R.id.imgFirst);
        final TextView t2 = (TextView) reportdialog
                .findViewById(R.id.imgSecond);
        final TextView t3 = (TextView) reportdialog.findViewById(R.id.imgThird);
        final TextView t4 = (TextView) reportdialog.findViewById(R.id.imgFour);
        t1.setTypeface(face);
        t2.setTypeface(face);
        t3.setTypeface(face);
        t4.setTypeface(face);

        position = position + 1;

        if (report_data.get(pickelId) != null) {
            if (report_data.get(pickelId).contains("dont like")) {
                t1.setBackgroundResource(R.drawable.ovel_selected);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_shape);
            } else if (report_data.get(pickelId).contains("spam")) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_selected);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_shape);
            } else if (report_data.get(pickelId).contains("risky")) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_selected);
                t4.setBackgroundResource(R.drawable.ovel_shape);
            } else if (report_data.get(pickelId)
                    .contains("shouldn�t on pickle")) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_selected);
            } else {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_shape);
            }
        }

        op1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                t1.setBackgroundResource(R.drawable.ovel_selected);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_shape);
                reportText = "dont like";
            }
        });
        op2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_selected);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_shape);
                reportText = "spam";
            }
        });
        op3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_selected);
                t4.setBackgroundResource(R.drawable.ovel_shape);
                reportText = "risky";
            }
        });
        op4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                t1.setBackgroundResource(R.drawable.ovel_shape);
                t2.setBackgroundResource(R.drawable.ovel_shape);
                t3.setBackgroundResource(R.drawable.ovel_shape);
                t4.setBackgroundResource(R.drawable.ovel_selected);
                reportText = "shouldn�t on pickle";
            }
        });

        btnSubmit.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                rpd = ProgressDialog.show(getActivity(), "",
                        "Please wait reporting pickle ...", true);
                rpd.setCancelable(false);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            if (reportText != null) {
                                reportPickle(userId, reportText, pickelId,
                                        HomeScreen.authentication_token,
                                        HomeScreen.key);
                            } else {
                                reportText = "";
                                reportPickle(userId, reportText, pickelId,
                                        HomeScreen.authentication_token,
                                        HomeScreen.key);
                            }

                        } catch (Exception e) {

                        }
                    }
                }).start();

            }
        });

        reportdialog.show();
    }

    private void reportPickle(String userId, String reportText,
                              String pickelId, String authentication_token, String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramUserId = params[0];
                String paramReportText = params[1];
                String paramPickleId = params[2];
                String paramAuthToken = params[3];
                String paramKey = params[4];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(REPORT_URL);

                BasicNameValuePair userIdBasicNameValuePair = new BasicNameValuePair(
                        "report_pickle[user_id]", paramUserId);
                BasicNameValuePair reportTextBasicNameValuePair = new BasicNameValuePair(
                        "report_pickle[content]", paramReportText);
                BasicNameValuePair pickleIdBasicNameValuePair = new BasicNameValuePair(
                        "report_pickle[question_id]", paramPickleId);
                BasicNameValuePair AuthBasicNameValuePair = new BasicNameValuePair(
                        "authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair(
                        "key", paramKey);

                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(userIdBasicNameValuePair);
                nameValuePairList.add(reportTextBasicNameValuePair);
                nameValuePairList.add(pickleIdBasicNameValuePair);
                nameValuePairList.add(AuthBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);

                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                try {
                    final JSONObject mainObject = new JSONObject(result);

                    JSONObject reportObj = mainObject
                            .getJSONObject("report_pickle");
                    String created_report_content = reportObj
                            .getString("content");
                    String created_at = reportObj.getString("created_at");
                    String created_id = reportObj.getString("id");
                    String created_question_id = reportObj
                            .getString("question_id");
                    String updated_at = reportObj.getString("updated_at");
                    String user_id = reportObj.getString("user_id");

                    Log.e("created_id", "" + created_id);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String text = "Report successfully!";
                showShareDialog(text);

            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(userId, reportText, pickelId,
                authentication_token, key);

    }

    private void sharePickle(String pId, String authentication_token, String key) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramPId = params[0];
                String paramAuthToken = params[1];
                String paramKey = params[2];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(SHAREBUNDLE_URL);

                BasicNameValuePair pIdBasicNameValuePair = new BasicNameValuePair("question_id", paramPId);
                BasicNameValuePair authTokenBasicNameValuePair = new BasicNameValuePair("authentication_token", paramAuthToken);
                BasicNameValuePair keyBasicNameValuePair = new BasicNameValuePair("key", paramKey);
                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(pIdBasicNameValuePair);
                nameValuePairList.add(authTokenBasicNameValuePair);
                nameValuePairList.add(keyBasicNameValuePair);

                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);

                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);
                        }
                        return stringBuilder.toString();

                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                String text = "Pickle Share successfully!";
                showShareDialog(text);
            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(pId, authentication_token, key);

    }

    private void showShareDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(getActivity(), HomeScreen.class));
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),AlertDialog.THEME_HOLO_LIGHT);
        builder.setCancelable(true);

        TextView msg = new TextView(getActivity());
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setTextColor(Color.BLACK);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    private class loadingAllFeeds extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));
            params.add(new BasicNameValuePair("username", "" + HomeScreen.user_name));
            params.add(new BasicNameValuePair("page", "" + page_count));
          //  String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET, params); //@PKDEC12 :- This is original one.
            //@PKDEC12 : new one
  String jsonStr = sh.makeServiceCall(Constant.PickleUrl_V1 + "feeds/" + HomeScreen.session_name.replace(" ", "%20") + ".json", ServiceHandler.GET, params);

            try {

                final JSONObject mainObject = new JSONObject(jsonStr);

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        //@PKDEC05 : Executes for initial data(or the first pg).
                        // So clear all data in arrayListOfFeeddata, if present.
                        arrayListOfFeeddata = new ArrayList<AllFeedsModel>();
                        arrayListOfFeeddata.add(0, null);
                        parseJSONFEED(mainObject);
                    }

                });

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            myFeedsCustomAdapter = new MyFeedsCustomAdapter(getActivity(), arrayListOfFeeddata);
            listOfPickleFeeds.setAdapter(myFeedsCustomAdapter);
            inProgress.hide();

        }

    }

 /*   private class loadingReportPickleShow extends
            AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("authentication_token",
                    HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(REPORT_LIST_URL,
                    ServiceHandler.GET, params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);
                JSONArray arrayReport = mainObject
                        .getJSONArray("report_pickles");
                for (int r = 0; r < arrayReport.length(); r++) {
                    JSONObject reportObj = arrayReport.getJSONObject(r);
                    String load_report_content = reportObj.getString("content");
                    String created_at = reportObj.getString("created_at");
                    String load_id = reportObj.getString("id");
                    String load_question_id = reportObj
                            .getString("question_id");
                    String updated_at = reportObj.getString("updated_at");
                    String user_id = reportObj.getString("user_id");

                    report_data.put(load_question_id, load_report_content);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }

    }*/

    private class loadMoreListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            //inProgress.setVisibility(View.VISIBLE);
        }

        protected Void doInBackground(Void... unused) {

            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
//@NS9DEC :
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));
            params.add(new BasicNameValuePair("page", "" + page_count));
            params.add(new BasicNameValuePair("user_id", HomeScreen.user_id));
            String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.GET, params);
Log.e("@Create jsonStr",jsonStr);
            //@NS9DEC :
            // Making a request to url and getting response
            // String jsonStr = sh.makeServiceCall(URL_FEED, ServiceHandler.POST, params);

            if (jsonStr != null) {
                try {
                    final JSONObject mainObject = new JSONObject(jsonStr);

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            parseJSONFEED(mainObject);
                        }

                    });

                } catch (JSONException je) {
                    je.printStackTrace();
                } catch (NetworkOnMainThreadException nme) {
                    nme.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Couldn't get any data from the url");
            }

            return null;
        }

        protected void onPostExecute(Void unused) {
            //@NS9DEC :
            btnLoadMore.setVisibility(View.GONE);
            myFeedsCustomAdapter.notifyDataSetChanged();



        }
    }

    public class MyFeedsCustomAdapter extends BaseAdapter implements StickyListHeadersAdapter {

        Context mContext;
        ArrayList<AllFeedsModel> arrayListOfFeeddata = new ArrayList<AllFeedsModel>();
        AllFeedsModel allFeedsModel;
        private Animation animation;
        private int lastPosition = 0;

        public MyFeedsCustomAdapter(Context mContext, ArrayList<AllFeedsModel> arrayListOfFeeddata) {
            this.mContext = mContext;
            this.arrayListOfFeeddata = arrayListOfFeeddata;
            //this.allFeedsModel = allFeedsModel;
            imageLoader = new ImageLoader(mContext);
        }

        @Override
        public int getCount() {
            return arrayListOfFeeddata.size();
        }

        @Override
        public Object getItem(int position) {
            return arrayListOfFeeddata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {

            return arrayListOfFeeddata.get(position) == null ? 0 : 1;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final FeedsViewHolder viewHolder;
            final MainHeaderViewHolder mainHeaderViewHolder;
            Animation myFadeInAnimation = AnimationUtils.loadAnimation(mContext, R.anim.image_animation);
            if (getItemViewType(position) == 0) {
                LayoutInflater li;
                View v;
//                v = convertView;
//                if (convertView == null) {
                li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.header_common, null);
                TextView headerTitle = (TextView) v.findViewById(R.id.headerTitle);
                Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/sniglet-regular.ttf");
                headerTitle.setTypeface(typeface);
                mainHeaderViewHolder = new MainHeaderViewHolder(v);
                v.setTag(mainHeaderViewHolder);
//                } else {
//                    mainHeaderViewHolder = (MainHeaderViewHolder) v.getTag();
//                }
                return v;
            } else {
                LayoutInflater li;
                View v;
//                v = convertView;
//                if (convertView == null) {

                li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.feed_item, null);
                viewHolder = new FeedsViewHolder(v);
//  @PKDEC12 : up_from_bottom animation code.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                if (position > lastPosition) {
//                    animation = AnimationUtils.loadAnimation(mContext, R.anim.up_from_bottom);
//                    viewHolder.ll_main.startAnimation(animation);
//                    lastPosition = position;
//                }
//  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                v.setTag(viewHolder);

//                } else {
//                    viewHolder = (FeedsViewHolder) v.getTag();
//                }

                Log.e("FeedsFragment", "Item view type = " + getItemViewType(position));
                setDataInRow(position, viewHolder);
                return v;
            }

        }

        private void setDataInRow(final int position, final FeedsViewHolder viewHolder) {
            viewHolder.firstOptionVoteText.setTypeface(face);
            viewHolder.secondOptionVoteText.setTypeface(face);

//NS
            viewHolder.firstOptionImage.setTag(position);viewHolder.secondOptionImage.setTag(position);
            viewHolder.firstText.setTag(position);viewHolder.SecondText.setTag(position);
            try {

//				if (arrayListOfFeeddata.get(position).getStrUProfilePicUrl2() != null) {
//
////					imageLoader.DisplayImage(arrayListOfFeeddata.get(position)
////							.getStrUProfilePicUrl2(),
////							viewHolder.userprofileImage);
//					Picasso.with(mContext)
//					.load(arrayListOfFeeddata.get(position)
//							.getStrUProfilePicUrl2())
//					.into(viewHolder.userprofileImage);
//				//	viewHolder.userprofileImage.startAnimation(myFadeInAnimation);
//
//				}
//
//				if (arrayListOfFeeddata.get(position).getStrUName2() != null) {
//					viewHolder.txtUserName.setTypeface(face);
//					viewHolder.txtUserName.setText(arrayListOfFeeddata.get(
//							position).getStrUName2());
//				}
//				if (arrayListOfFeeddata.get(position).getStrPCreatedDate() != null) {
//					viewHolder.txtCreatedDate.setTypeface(face);
//					viewHolder.txtCreatedDate.setText(arrayListOfFeeddata.get(
//							position).getStrPCreatedDate());
//				}
                if (arrayListOfFeeddata.get(position).getStrPName2() != null) {
                    viewHolder.txtPickleQuestion.setTypeface(face);
                    viewHolder.txtPickleQuestion.setText(arrayListOfFeeddata.get(position).getStrPName2());
                }

                if (arrayListOfFeeddata.get(position).getStrFOptionImageUrl2() != null) {
                    if (arrayListOfFeeddata.get(position).getStrFOptionImageUrl2().equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                        viewHolder.firstOptionImage.setVisibility(View.GONE);

                    } else {
                        viewHolder.firstOptionImage.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(
//								arrayListOfFeeddata.get(position)
//										.getStrFOptionImageUrl2(),
//								viewHolder.firstOptionImage);

                        Picasso.with(mContext).load(arrayListOfFeeddata.get(position).getStrFOptionImageUrl2()).into(viewHolder.firstOptionImage);
                        //		viewHolder.firstOptionImage.startAnimation(myFadeInAnimation);

                    }

                }

                if (arrayListOfFeeddata.get(position).getStrFOptionLable2() != null) {

                    if (arrayListOfFeeddata.get(position).getStrFOptionLable2().equals("") || arrayListOfFeeddata.get(position).getStrFOptionLable2().equals("null")) {
                        viewHolder.txtFirstOptionName.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtFirstOptionName.setTypeface(face);
                        viewHolder.txtFirstOptionName.setVisibility(View.VISIBLE);
                        viewHolder.txtFirstOptionName.setText(arrayListOfFeeddata.get(position).getStrFOptionLable2());
                    }

                }

                if (arrayListOfFeeddata.get(position).getFirstOptionText() != null)
                    if (arrayListOfFeeddata.get(position).getFirstOptionText().equals("") || arrayListOfFeeddata.get(position).getFirstOptionText().equals("null"))
                        viewHolder.firstText.setVisibility(View.GONE);
                    else {
                        viewHolder.firstText.setTypeface(face);
                        viewHolder.firstText.setVisibility(View.VISIBLE);
                        viewHolder.firstText.setText(arrayListOfFeeddata.get(position).getFirstOptionText());
                    }
                viewHolder.firstOptionVoteCount.setText("" + arrayListOfFeeddata.get(position).getStrFirstOption2());

                if (arrayListOfFeeddata.get(position).getStrSOptionImageUrl2() != null) {

                    if (arrayListOfFeeddata.get(position).getStrSOptionImageUrl2().equals("http://54.251.33.194:8800/assets/missing.jpg")) {
                        viewHolder.secondOptionImage.setVisibility(View.GONE);

                    } else {
                        viewHolder.secondOptionImage.setVisibility(View.VISIBLE);
//						imageLoader.DisplayImage(
//								arrayListOfFeeddata.get(position)
//										.getStrSOptionImageUrl2(),
//								viewHolder.secondOptionImage);
                        Picasso.with(mContext).load(arrayListOfFeeddata.get(position).getStrSOptionImageUrl2()).into(viewHolder.secondOptionImage);
                        //			viewHolder.secondOptionImage.startAnimation(myFadeInAnimation);
                    }

                }

                if (arrayListOfFeeddata.get(position).getStrSOptionLable2() != null) {
                    if (arrayListOfFeeddata.get(position).getStrSOptionLable2().equals("") || arrayListOfFeeddata.get(position).getStrSOptionLable2().equals("null")) {
                        viewHolder.txtSecondOptionName.setVisibility(View.GONE);
                    } else {
                        viewHolder.txtSecondOptionName.setTypeface(face);
                        viewHolder.txtSecondOptionName.setVisibility(View.VISIBLE);
                        viewHolder.txtSecondOptionName.setText(arrayListOfFeeddata.get(position).getStrSOptionLable2());
                    }
                }

                if (arrayListOfFeeddata.get(position).getSecondOptionText() != null)
                    if (arrayListOfFeeddata.get(position).getSecondOptionText().equals("") || arrayListOfFeeddata.get(position).getSecondOptionText().equals("null"))
                        viewHolder.SecondText.setVisibility(View.GONE);
                    else {
                        viewHolder.SecondText.setTypeface(face);
                        viewHolder.SecondText.setVisibility(View.VISIBLE);
                        viewHolder.SecondText.setText(arrayListOfFeeddata.get(position).getSecondOptionText());
                    }
                viewHolder.secondOptionVoteCount.setText("" + arrayListOfFeeddata.get(position).getStrSecondOption2());

                if (arrayListOfFeeddata.get(position).getArrayOfComments2().length() > 2) {
                    viewHolder.txtLoadMoreComment.setVisibility(View.VISIBLE);
                    viewHolder.txtLoadMoreComment.setTypeface(face);
                } else {
                    viewHolder.txtLoadMoreComment.setVisibility(View.GONE);
                }

                viewHolder.txtLoadMoreComment.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {

// @PKDEC02
//                      Actual code is as follows. Now we are getting username as "name" in login itself. So using it now onwards.
//                                String string = HomeScreen.session_name;
//                                String[] parts = string.split("-");
//                                String part1 = parts[0];

                        // fallback if no string is in SaveUtils, so making 'partString1' as default text
                        String sessionString = HomeScreen.session_name;
                        String[] partsString = sessionString.split("-");
                        String partString1 = partsString[0];

                        String part1 = SaveUtils.getBrandOrUserName(getString(R.string.PreferenceFileName), getActivity(), SaveUtils.BrandOrUserName, partString1);

//								Intent i=new Intent(getActivity(),CommentFragment.class);
//								 i.putExtra("COMMENT_ARRAY", ""
//											+ arrayListOfFeeddata.get(position)
//											.getArrayOfComments2());
//								 i.putExtra("USER_ID", ""
//											+ HomeScreen.user_id);
//								 i.putExtra("PICKLE_ID", ""
//											+ arrayListOfFeeddata.get(position)
//											.getStrPId2());
//								 i.putExtra("USER_NAME", "" + part1);
//								 i.putExtra("SHARE_PICKLE_ID", ""
//											+ arrayListOfFeeddata.get(position)
//											.getSharePickleId());
//								 i.putExtra("MYPICKLE", arrayListOfFeeddata.get(position).isIs_my_pickle());
//								 i.putExtra("owner",arrayListOfFeeddata.get(position).getStrUName2());
//								 i.putExtra("profimg", arrayListOfFeeddata.get(position)
//											.getStrUProfilePicUrl2());
//								startActivity(i);
////
                        CommentFragment commentFragment = new CommentFragment();
                        Bundle args = new Bundle();
                        args.putString("COMMENT_ARRAY", "" + arrayListOfFeeddata.get(position).getArrayOfComments2());
                        args.putString("USER_ID", "" + HomeScreen.user_id);
                        args.putString("PICKLE_ID", "" + arrayListOfFeeddata.get(position).getStrPId2());
                        args.putString("USER_NAME", "" + part1);
                        args.putString("SHARE_PICKLE_ID", "" + arrayListOfFeeddata.get(position).getSharePickleId());
                        args.putBoolean("MYPICKLE", arrayListOfFeeddata.get(position).isIs_my_pickle());
                        args.putString("owner", arrayListOfFeeddata.get(position).getStrUName2());
                        args.putString("Pagecount", arrayListOfFeeddata.get(position).toString());
                        commentFragment.setArguments(args);
                        FragmentTransaction commentTransaction = getFragmentManager().beginTransaction();
                        commentTransaction.replace(R.id.content_frame, commentFragment);
                        commentTransaction.addToBackStack(null);
                        commentTransaction.commit();
                    }
                });

                if (arrayListOfFeeddata.get(position).getArrayOfComments2().length() != 0 || arrayListOfFeeddata.get(position).getArrayOfComments2().length() > 0) {

                    ArrayList<String> comment_contents = new ArrayList<String>();
                    ArrayList<String> comment_users = new ArrayList<String>();

                    for (int i = 0; i < arrayListOfFeeddata.get(position).getArrayOfComments2().length(); i++) {
//                        Log.e("COMMENTS#", "Val = " + arrayListOfFeeddata.get(position).getArrayOfComments2().get(i));
                        //@PKDEC02 : (if & else[with body]) hide comments if no comment present
                        //           (arrayListOfFeeddata.get(position).getArrayOfComments2().get(i) is null,i.e., [null])
                        // Comments are GONE initially so as to show only if present.

//                        viewHolder.comment_one.setVisibility(View.GONE);
//                        viewHolder.comment_two.setVisibility(View.GONE);
//                        viewHolder.comment_three.setVisibility(View.GONE);
                        if (arrayListOfFeeddata.get(position).getArrayOfComments2().get(i) instanceof JSONObject) {
                            JSONObject obj = arrayListOfFeeddata.get(position).getArrayOfComments2().getJSONObject(i);
                            if (obj != null) {
                                try {
                                    comment_contents.add(arrayListOfFeeddata
                                            .get(position).getArrayOfComments2()
                                            .getJSONObject(i)
                                            .getString("comment_content"));

                                    comment_users.add(arrayListOfFeeddata
                                            .get(position).getArrayOfComments2()
                                            .getJSONObject(i)
                                            .getString("comment_user_name"));

                                    Collections.reverse(comment_contents);
                                    Collections.reverse(comment_users);

                                    viewHolder.commentLayout
                                            .setVisibility(View.VISIBLE);
                                    if (comment_contents.size() == 1
                                            && comment_users.size() == 1) {
                                        viewHolder.comment_one.setVisibility(View.VISIBLE);
                                        viewHolder.comment_two.setVisibility(View.GONE);
                                        viewHolder.comment_three.setVisibility(View.GONE);
                                        viewHolder.commentUserOne.setTypeface(face);
                                        viewHolder.commentUserOne.setText(comment_users.get(0));
                                        viewHolder.commentOne.setTypeface(face);
                                        viewHolder.commentOne.setText(comment_contents.get(0));
                                    } else if (comment_contents.size() == 2
                                            && comment_users.size() == 2) {
                                        viewHolder.comment_one.setVisibility(View.VISIBLE);
                                        viewHolder.comment_two.setVisibility(View.VISIBLE);
                                        viewHolder.comment_three.setVisibility(View.GONE);
                                        viewHolder.commentUserOne.setTypeface(face);
                                        viewHolder.commentUserOne.setText(comment_users.get(0));
                                        viewHolder.commentOne.setTypeface(face);
                                        viewHolder.commentOne.setText(comment_contents.get(0));
                                        viewHolder.commentUserTwo.setTypeface(face);
                                        viewHolder.commentUserTwo.setText(comment_users.get(1));
                                        viewHolder.commentTwo.setTypeface(face);
                                        viewHolder.commentTwo.setText(comment_contents.get(1));
                                    } else if (comment_contents.size() == 3
                                            && comment_users.size() == 3) {
                                        viewHolder.comment_one.setVisibility(View.VISIBLE);
                                        viewHolder.comment_two.setVisibility(View.VISIBLE);
                                        viewHolder.comment_three.setVisibility(View.VISIBLE);
                                        viewHolder.commentUserOne.setTypeface(face);
                                        viewHolder.commentUserOne.setText(comment_users.get(0));
                                        viewHolder.commentOne.setTypeface(face);
                                        viewHolder.commentOne.setText(comment_contents.get(0));
                                        viewHolder.commentUserTwo.setTypeface(face);
                                        viewHolder.commentUserTwo.setText(comment_users.get(1));
                                        viewHolder.commentTwo.setTypeface(face);
                                        viewHolder.commentTwo.setText(comment_contents.get(1));
                                        viewHolder.commentUserThree.setTypeface(face);
                                        viewHolder.commentUserThree.setText(comment_users.get(2));
                                        viewHolder.commentThree.setTypeface(face);
                                        viewHolder.commentThree.setText(comment_contents.get(2));
                                    } else if (comment_contents.size() > 3
                                            && comment_users.size() > 3) {

                                        viewHolder.comment_one.setVisibility(View.VISIBLE);
                                        viewHolder.comment_two.setVisibility(View.VISIBLE);
                                        viewHolder.comment_three.setVisibility(View.VISIBLE);

                                    } else {
                                        viewHolder.comment_one.setVisibility(View.GONE);
                                        viewHolder.comment_two.setVisibility(View.GONE);
                                        viewHolder.comment_three.setVisibility(View.GONE);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                viewHolder.comment_one.setVisibility(View.GONE);
                                viewHolder.comment_two.setVisibility(View.GONE);
                                viewHolder.comment_three.setVisibility(View.GONE);
                            }
                        }
                        //@PKDEC02 : hide comments if no comment present (array is null : [null])
                        else {
//                            viewHolder.commentLayout.setVisibility(View.GONE);
                        }
                    }
                } else {
                    viewHolder.commentLayout.setVisibility(View.GONE);

                }

                if (arrayListOfFeeddata.get(position).isFirst_option_is_voted()
                        && !arrayListOfFeeddata.get(position)
                        .isSecond_option_is_voted())
                {
                    viewHolder.firstView.setBackgroundColor(Color.parseColor("#F08080"));
                    viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                    viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_color);
                    viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                    viewHolder.secondView.setBackgroundColor(Color
                            .parseColor("#11000000"));

                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_black);
                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#000000"));

                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(true);
                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);

                } else if (!arrayListOfFeeddata.get(position).isFirst_option_is_voted() && arrayListOfFeeddata.get(position).isSecond_option_is_voted()) {
                    viewHolder.firstView.setBackgroundColor(Color.parseColor("#11000000"));
                    viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                    viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_black);
                    viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#000000"));
                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#F08080"));
                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_color);
                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));
                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(true);
                } else {
                    viewHolder.firstView.setBackgroundColor(Color.parseColor("#11000000"));
                    viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                    viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_black);
                    viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#000000"));
                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#11000000"));
                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_black);
                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#000000"));
                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                }
//NS9DEC
                viewHolder.firstOptionImage
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                            new FullScreenImage().setDialog(arrayListOfFeeddata.get((Integer) v.getTag()),0);
                            }
                            });
                viewHolder.secondOptionImage
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                new FullScreenImage().setDialog(arrayListOfFeeddata.get((Integer) v.getTag()),1);
                            }
                        });
                viewHolder.firstText
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                new FullScreenImage().setDialog(arrayListOfFeeddata.get((Integer) v.getTag()),0);
                            }
                        });
                viewHolder.SecondText
                        .setOnClickListener(new OnClickListener() {
                            public void onClick(View v) {
                                new FullScreenImage().setDialog(arrayListOfFeeddata.get((Integer) v.getTag()),1);
                            }
                        });

               /* viewHolder.firstOptionImage
                        .setOnClickListener(new OnClickListener() {

                            public void onClick(View v) {

                                if (arrayListOfFeeddata.get(position).isFirst_option_is_voted() == false && arrayListOfFeeddata.get(position).isSecond_option_is_voted() == false) {
                                   viewHolder.firstView.setBackgroundColor(Color.parseColor("#F08080"));
                                    viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                                    viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_color_new);
                                    viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));
                                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(true);
                                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                                    long count_one = arrayListOfFeeddata.get(position).getStrFirstOption2();
                                    count_one = count_one + 1;
                                    viewHolder.firstOptionVoteCount.setText("" + (count_one + arrayListOfFeeddata.get(position).getStrFirstOption2()));
                                    arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);
                                    notifyDataSetChanged();
                                    pickleVote("" + arrayListOfFeeddata.get(position).getStrFId2(), arrayListOfFeeddata.get(position).getStrPId2(), share_pickle_id, "" + HomeScreen.user_id, HomeScreen.authentication_token, HomeScreen.key);
                              } else if (arrayListOfFeeddata.get(position).isFirst_option_is_voted() == true && arrayListOfFeeddata.get(position).isSecond_option_is_voted() == false) {
                                    viewHolder.firstView.setBackgroundColor(Color.parseColor("#11000000"));
                                    viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                                    viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_black_new);
                                    viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#000000"));
                                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                                    long count_one = arrayListOfFeeddata.get(position).getStrFirstOption2();
                                    count_one = count_one - 1;
                                    viewHolder.firstOptionVoteCount.setText("" + (count_one + arrayListOfFeeddata.get(position).getStrFirstOption2()));
                                    arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);
                                    notifyDataSetChanged();
                                    UNVOTED_URL = Constant.PickleUrl + "votes/" + arrayListOfFeeddata.get(position).getFirst_option_voted_id() + ".json";
                                    new UnVoted().execute();
                                } else if (arrayListOfFeeddata.get(position).isFirst_option_is_voted() == false && arrayListOfFeeddata.get(position).isSecond_option_is_voted() == true) {
                                }
                            }
                        });
                viewHolder.secondOptionImage
                        .setOnClickListener(new OnClickListener() {

                            @SuppressWarnings("null")
                            @Override
                            public void onClick(View v) {

                                if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == false
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == false) {

                                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#F08080"));
                                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_color_new);
                                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));
                                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(true);
                                    long count_two = arrayListOfFeeddata.get(position).getStrSecondOption2();
                                    count_two = count_two + 1;
                                    viewHolder.secondOptionVoteCount.setText("" + (count_two + arrayListOfFeeddata.get(position).getStrSecondOption2()));
                                    arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                                    notifyDataSetChanged();
                                    pickleVote("" + arrayListOfFeeddata.get(position).getStrSId2(), arrayListOfFeeddata.get(position).getStrPId2(), share_pickle_id, "" + HomeScreen.user_id, HomeScreen.authentication_token, HomeScreen.key);

                                } else if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == false
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == true) {

                                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#11000000"));
                                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_black_new);
                                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#000000"));
                                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                                    long count_two = arrayListOfFeeddata.get(position).getStrSecondOption2();
                                    count_two = count_two - 1;
                                    viewHolder.secondOptionVoteCount.setText("" + (count_two + arrayListOfFeeddata.get(position).getStrSecondOption2()));
                                    arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                                    notifyDataSetChanged();
                                    UNVOTED_URL = Constant.PickleUrl + "votes/" + arrayListOfFeeddata.get(position).getSecond_option_voted_id() + ".json";
                                    new UnVoted().execute();

                                } else if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == true
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == false) {
                                }
                            }
                        });
*/
       /*         viewHolder.firstText.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == false
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == false) {

                            viewHolder.firstView.setBackgroundColor(Color.parseColor("#F08080"));
                            viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                            viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_color);
                            viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                            arrayListOfFeeddata.get(position).setFirst_option_is_voted(true);
                            arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                            long count_one = arrayListOfFeeddata.get(position).getStrFirstOption2();
                            count_one = count_one + 1;
                            viewHolder.firstOptionVoteCount.setText(""
                                    + (count_one + arrayListOfFeeddata.get(
                                    position).getStrFirstOption2()));
                            arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);
                            notifyDataSetChanged();
                            pickleVote(""+ arrayListOfFeeddata.get(position)
                                            .getStrFId2(), arrayListOfFeeddata
                                            .get(position).getStrPId2(),
                                    share_pickle_id, "" + HomeScreen.user_id,
                                    HomeScreen.authentication_token,
                                    HomeScreen.key);

                        } else if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == true
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == false) {

                            viewHolder.firstView.setBackgroundColor(Color.parseColor("#11000000"));
                            viewHolder.firstOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                            viewHolder.firstOptionVoteImage.setImageResource(R.drawable.vote_black);
                            viewHolder.firstOptionVoteText.setTextColor(Color.parseColor("#000000"));

                            arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                            arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);

                            //long count_two = 0;
                            long count_one = arrayListOfFeeddata.get(position).getStrFirstOption2();
                            count_one = count_one - 1;
                            viewHolder.firstOptionVoteCount.setText(""
                                    + (count_one + arrayListOfFeeddata.get(
                                    position).getStrFirstOption2()));
                            arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);
                            notifyDataSetChanged();
                            UNVOTED_URL = Constant.PickleUrl
                                    + "votes/"
                                    + arrayListOfFeeddata.get(position).getFirst_option_voted_id()
                                    + ".json";

                            new UnVoted().execute();

                        } else if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == false
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == true) {
                        }
                    }
                });

                viewHolder.SecondText.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == false
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == false) {

                            viewHolder.secondView.setBackgroundColor(Color.parseColor("#F08080"));
                            viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#F08080"));
                            viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_color);
                            viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                            arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                            arrayListOfFeeddata.get(position).setSecond_option_is_voted(true);
                            long count_two = arrayListOfFeeddata.get(position).getStrSecondOption2();
                            count_two = count_two + 1;
                            viewHolder.secondOptionVoteCount.setText(""
                                    + (count_two + arrayListOfFeeddata.get(
                                    position).getStrSecondOption2()));
                            arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                            notifyDataSetChanged();
                            pickleVote(""
                                            + arrayListOfFeeddata.get(position)
                                            .getStrSId2(), arrayListOfFeeddata
                                            .get(position).getStrPId2(),
                                    share_pickle_id, "" + HomeScreen.user_id,
                                    HomeScreen.authentication_token,
                                    HomeScreen.key);

                        } else if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == false
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == true) {

                            viewHolder.secondView.setBackgroundColor(Color.parseColor("#11000000"));
                            viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                            viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_black);
                            viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#000000"));

                            arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                            arrayListOfFeeddata.get(position).setSecond_option_is_voted(false);
                            long count_two = arrayListOfFeeddata.get(position).getStrSecondOption2();
                            count_two = count_two - 1;
                            viewHolder.secondOptionVoteCount.setText(""
                                    + (count_two + arrayListOfFeeddata.get(
                                    position).getStrSecondOption2()));
                            arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                            notifyDataSetChanged();
                            UNVOTED_URL = Constant.PickleUrl
                                    + "votes/"
                                    + arrayListOfFeeddata.get(position).getSecond_option_voted_id()
                                    + ".json";

                            new UnVoted().execute();

                        } else if (arrayListOfFeeddata.get(position)
                                .isFirst_option_is_voted() == true
                                && arrayListOfFeeddata.get(position)
                                .isSecond_option_is_voted() == false) {
                        }
                    }
                });
*/
                viewHolder.firstOptionVoteImage
                        .setOnClickListener(new OnClickListener() {

                            @SuppressWarnings("null")
                            @Override
                            public void onClick(View v) {

                                if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == false
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == false) {

                                    viewHolder.firstView
                                            .setBackgroundColor(Color
                                                    .parseColor("#F08080"));
                                    viewHolder.firstOptionVoteCount
                                            .setTextColor(Color
                                                    .parseColor("#F08080"));
                                    viewHolder.firstOptionVoteImage
                                            .setImageResource(R.drawable.vote_color);
                                    viewHolder.firstOptionVoteText
                                            .setTextColor(Color
                                                    .parseColor("#F08080"));

                                    arrayListOfFeeddata.get(position)
                                            .setFirst_option_is_voted(true);
                                    arrayListOfFeeddata.get(position)
                                            .setSecond_option_is_voted(false);
                                    long count_one = arrayListOfFeeddata.get(position)
                                            .getStrFirstOption2();
                                    count_one = count_one + 1;
                                    viewHolder.firstOptionVoteCount.setText(""
                                            + (count_one + arrayListOfFeeddata
                                            .get(position)
                                            .getStrFirstOption2()));
                                    arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);

                                    pickleVote(arrayListOfFeeddata.get(
                                            position),true);
                                    notifyDataSetChanged();

                                } else if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == true
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == false) {

                                    viewHolder.firstView
                                            .setBackgroundColor(Color
                                                    .parseColor("#11000000"));
                                    viewHolder.firstOptionVoteCount
                                            .setTextColor(Color
                                                    .parseColor("#000000"));
                                    viewHolder.firstOptionVoteImage
                                            .setImageResource(R.drawable.vote_black);
                                    viewHolder.firstOptionVoteText
                                            .setTextColor(Color
                                                    .parseColor("#000000"));

                                    arrayListOfFeeddata.get(position)
                                            .setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position)
                                            .setSecond_option_is_voted(false);
                                    long count_one = arrayListOfFeeddata.get(position)
                                            .getStrFirstOption2();
                                    count_one = count_one - 1;
                                    viewHolder.firstOptionVoteCount.setText(""
                                            + (count_one + arrayListOfFeeddata
                                            .get(position)
                                            .getStrFirstOption2()));
                                    arrayListOfFeeddata.get(position).setStrFirstOption2(count_one);
                                    notifyDataSetChanged();
                                    UNVOTED_URL = Constant.PickleUrl
                                            + "votes/"
                                            + arrayListOfFeeddata.get(position)
                                            .getFirst_option_voted_id()
                                            + ".json";

                                    new UnVoted().execute();

                                } else if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == false
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == true) {
                                }
                            }
                        });
                viewHolder.secondOptionVoteImage
                        .setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {

                                if (arrayListOfFeeddata.get(position)
                                        .isFirst_option_is_voted() == false
                                        && arrayListOfFeeddata.get(position)
                                        .isSecond_option_is_voted() == false) {

                                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#F08080"));
                                    viewHolder.secondOptionVoteCount
                                            .setTextColor(Color
                                                    .parseColor("#F08080"));
                                    viewHolder.secondOptionVoteImage
                                            .setImageResource(R.drawable.vote_color);
                                    viewHolder.secondOptionVoteText
                                            .setTextColor(Color
                                                    .parseColor("#F08080"));

                                    arrayListOfFeeddata.get(position).setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position).setSecond_option_is_voted(true);
                                    long count_two = arrayListOfFeeddata.get(position).getStrSecondOption2();
                                    count_two = count_two + 1;
                                    viewHolder.secondOptionVoteCount.setText("" + (count_two + arrayListOfFeeddata.get(position).getStrSecondOption2()));
                                    arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                                    pickleVote( arrayListOfFeeddata
                                            .get(position),false);

                                    notifyDataSetChanged();

                                } else if (arrayListOfFeeddata.get(position).isFirst_option_is_voted() == false && arrayListOfFeeddata.get(position).isSecond_option_is_voted() == true) {

                                    viewHolder.secondView.setBackgroundColor(Color.parseColor("#11000000"));
                                    viewHolder.secondOptionVoteCount.setTextColor(Color.parseColor("#000000"));
                                    viewHolder.secondOptionVoteImage.setImageResource(R.drawable.vote_black);
                                    viewHolder.secondOptionVoteText.setTextColor(Color.parseColor("#000000"));

                                    arrayListOfFeeddata.get(position)
                                            .setFirst_option_is_voted(false);
                                    arrayListOfFeeddata.get(position)
                                            .setSecond_option_is_voted(false);
                                    long count_two = arrayListOfFeeddata.get(position)
                                            .getStrSecondOption2();
                                    count_two = count_two - 1;
                                    viewHolder.secondOptionVoteCount.setText("" + (count_two + arrayListOfFeeddata.get(position).getStrSecondOption2()));
                                    arrayListOfFeeddata.get(position).setStrSecondOption2(count_two);
                                    notifyDataSetChanged();
                                    UNVOTED_URL = Constant.PickleUrl
                                            + "votes/"
                                            + arrayListOfFeeddata
                                            .get(position)
                                            .getSecond_option_voted_id()
                                            + ".json";

                                    new UnVoted().execute();

                                } else if (arrayListOfFeeddata.get(position).isFirst_option_is_voted() == true && arrayListOfFeeddata.get(position).isSecond_option_is_voted() == false) {
                                }
                            }
                        });

                viewHolder.popUp.setOnClickListener(new OnClickListener() {
                    public void onClick(View v) {
                        dialog = new Dialog(getActivity());
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.popup_dialog);

                        TextView txtSharePickle = (TextView) dialog.findViewById(R.id.txtSharePickle);
                        TextView txtEditPickle = (TextView) dialog.findViewById(R.id.txtEditPickle);
                        TextView txtTagPickle = (TextView) dialog.findViewById(R.id.txtTagPickle);
                        TextView txtReportPickle = (TextView) dialog.findViewById(R.id.txtReportPickle);
                        TextView txtDeletePickle = (TextView) dialog.findViewById(R.id.txtDeletePickle);

                        txtSharePickle.setTypeface(face);
                        txtEditPickle.setTypeface(face);
                        txtTagPickle.setTypeface(face);
                        txtReportPickle.setTypeface(face);
                        txtDeletePickle.setTypeface(face);

                        View line = (View) dialog.findViewById(R.id.four);
                        View second = (View) dialog.findViewById(R.id.second);
                        View third = (View) dialog.findViewById(R.id.third);

                        if (arrayListOfFeeddata.get(position).isIs_my_pickle() && !arrayListOfFeeddata.get(position).isStrIsPickleShared()) {
                            txtDeletePickle.setVisibility(View.VISIBLE);
                            line.setVisibility(View.VISIBLE);

                            DELETE_SHARE_PICKLE_URL = Constant.PickleUrl
                                    + "questions/"
                                    + arrayListOfFeeddata.get(position).getStrPId2() + ".json";

                        } else if (!arrayListOfFeeddata.get(position).isIs_my_pickle() && arrayListOfFeeddata.get(position).isStrIsPickleShared()) {

                            DELETE_SHARE_PICKLE_URL = Constant.PickleUrl
                                    + "share_pickels/"
                                    + arrayListOfFeeddata.get(position).getSharePickleId() + ".json";

                        } else if (arrayListOfFeeddata.get(position).isIs_my_pickle() && arrayListOfFeeddata.get(position).isStrIsPickleShared()) {

                            DELETE_SHARE_PICKLE_URL = Constant.PickleUrl
                                    + "share_pickels/"
                                    + arrayListOfFeeddata.get(position).getSharePickleId() + ".json";

                        } else {
                            txtEditPickle.setVisibility(View.GONE);
                            txtDeletePickle.setVisibility(View.GONE);
                            txtTagPickle.setVisibility(View.GONE);

                            second.setVisibility(View.GONE);
                            third.setVisibility(View.GONE);
                            line.setVisibility(View.GONE);
                        }

                        txtSharePickle
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        SHAREBUNDLE_URL = Constant.PickleUrl + "share_pickels.json";

                                        rpd = ProgressDialog.show(getActivity(), "", "Sharing Pickle ...", true);
                                        rpd.setCancelable(false);
                                       // rpd.setOnCancelListener();
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {

                                                    sharePickle(
                                                            arrayListOfFeeddata.get(position).getStrPId2(),
                                                            HomeScreen.authentication_token,
                                                            HomeScreen.key);

                                                } catch (Exception e) {

                                                }
                                            }
                                        }).start();

                                    }
                                });
                        txtEditPickle.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
//								Intent intent = new Intent(getActivity(),
//										EditPickleFragmentActivity.class);
//								intent.putExtra("PICKLE_ID",
//										arrayListOfFeeddata.get(position)
//												.getStrPId2());
//								intent.putExtra("PICKLE_NAME",
//										arrayListOfFeeddata.get(position)
//												.getStrPName2());
//								intent.putExtra("AUTH",
//										HomeScreen.authentication_token);
//								intent.putExtra("KEY", HomeScreen.key);
//								intent.putExtra("USERID", arrayListOfFeeddata
//										.get(position).getStrUId2());
//								intent.putExtra("FLAG","1");
//								startActivity(intent);
//								dialog.dismiss();
//

                                if (arrayListOfFeeddata.get(position).isStrPFeatured2() == true) {

                                    EditPickleFragmentActivity ShowPickleFragmentobj = new EditPickleFragmentActivity();
                                    Bundle args = new Bundle();
                                    args.putString("PICKLE_ID", arrayListOfFeeddata.get(position).getStrPId2());
                                    args.putString("PICKLE_NAME", arrayListOfFeeddata.get(position).getStrPName2().replace("?", ""));
                                    args.putString("AUTH", HomeScreen.authentication_token);
                                    args.putString("KEY", HomeScreen.key);
                                    args.putString("USERID", HomeScreen.user_id);
                                    args.putString("USER_ONE_ID", Long.toString(strFId));
                                    args.putString("USER_ONE_NAME", strFOptionLable);
                                    args.putString("USER_TWO_ID", Long.toString(strSId));
                                    args.putString("USER_TWO_NAME", strSOptionLable);

                                    if (strPFeatured == false) {
                                        args.putString("FLAG", "1");
                                    } else {
                                        args.putString("FLAG", "0");
                                        args.putString("USER_ONE_IMG_URL", strFOptionImageUrl);
                                        args.putString("USER_ONE_ID", "" + strFId);
                                        args.putString("USER_ONE_NAME", strFOptionLable);
                                        args.putString("USER_TWO_IMG_URL", strSOptionImageUrl);
                                        args.putString("USER_TWO_ID", "" + strSId);
                                        args.putString("USER_TWO_NAME", strSOptionLable);
                                    }

                                    ShowPickleFragmentobj.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager()
                                            .beginTransaction();
                                    transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                    dialog.dismiss();
                                } else {

                                    EditPickleFragmentActivity ShowPickleFragmentobj = new EditPickleFragmentActivity();
                                    Bundle args = new Bundle();
                                    args.putString("PICKLE_ID", arrayListOfFeeddata.get(position).getStrPId2());
                                    args.putString("PICKLE_NAME", arrayListOfFeeddata.get(position).getStrPName2());
                                    args.putString("AUTH", HomeScreen.authentication_token);
                                    args.putString("KEY", HomeScreen.key);
                                    args.putString("USERID", HomeScreen.user_id);

                                    args.putString("FLAG", "1");
                                    ShowPickleFragmentobj.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.content_frame, ShowPickleFragmentobj);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                    dialog.dismiss();

                                }

                            }
                        });
                        txtTagPickle.setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                TaggedPickleFragment taggedPickleFragment = new TaggedPickleFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("PICKLE_ID", arrayListOfFeeddata.get(position).getStrPId2());
                                bundle.putString("FIRST_OPTION_IMAGE", arrayListOfFeeddata.get(position).getStrFOptionImageUrl2());
                                bundle.putString("SECOND_OPTION_IMAGE", arrayListOfFeeddata.get(position).getStrSOptionImageUrl2());
                                bundle.putString("FIRST_OPTION_ID", "" + arrayListOfFeeddata.get(position).getStrFId2());
                                bundle.putString("SECOND_OPTION_ID", "" + arrayListOfFeeddata.get(position).getStrSId2());
                                bundle.putString("USER_ID", "" + arrayListOfFeeddata.get(position).getStrUId2());
                                bundle.putString("FIRST_TEXT", arrayListOfFeeddata.get(position).getFirstOptionText());
                                bundle.putString("SECOND_TEXT", arrayListOfFeeddata.get(position).getSecondOptionText());
                                taggedPickleFragment.setArguments(bundle);
                                FragmentTransaction taggedPickleTransaction = getFragmentManager().beginTransaction();
                                taggedPickleTransaction.replace(R.id.content_frame, taggedPickleFragment);
                                taggedPickleTransaction.addToBackStack(null);
                                taggedPickleTransaction.commit();
                                dialog.dismiss();
                            }
                        });
                        txtReportPickle
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        reportDailog(
                                                arrayListOfFeeddata.get(position).getStrPId2(),
                                                "" + arrayListOfFeeddata.get(position).getStrUId2(), position);
                                        dialog.dismiss();
                                    }
                                });
                        txtDeletePickle
                                .setOnClickListener(new OnClickListener() {

                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();

                                        rpd = ProgressDialog.show(
                                                getActivity(), "",
                                                "Deleting Pickle ...", true);
                                        rpd.setCancelable(false);
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {

                                                    new DeleteSharePickle()
                                                            .execute();

                                                } catch (Exception e) {

                                                }
                                            }
                                        }).start();

                                    }
                                });
                        dialog.show();
                    }
                });
                viewHolder.moreComment.setOnClickListener(new OnClickListener() {

                    public void onClick(View v) {
//shared
// @PKDEC02
//                      Actual code is as follows. Now we are getting username as "name" in login itself. So using it now onwards.
//                                String string = HomeScreen.session_name;
//                                String[] parts = string.split("-");
//                                String part1 = parts[0];

                        // fallback if no string is in SaveUtils, so making 'partString1' as default text
                        String sessionString = HomeScreen.session_name;
                        String[] partsString = sessionString.split("-");
                        String partString1 = partsString[0];

                        String part1 = SaveUtils.getBrandOrUserName(getString(R.string.PreferenceFileName), getActivity(), SaveUtils.BrandOrUserName, partString1);

//								Intent i=new Intent(getActivity(),CommentFragment.class);
//								 i.putExtra("COMMENT_ARRAY", ""
//											+ arrayListOfFeeddata.get(position)
//											.getArrayOfComments2());
//								 i.putExtra("USER_ID", ""
//											+ HomeScreen.user_id);
//								 i.putExtra("PICKLE_ID", ""
//											+ arrayListOfFeeddata.get(position)
//											.getStrPId2());
//								 i.putExtra("USER_NAME", "" + part1);
//								 i.putExtra("SHARE_PICKLE_ID", ""
//											+ arrayListOfFeeddata.get(position)
//											.getSharePickleId());
//								 i.putExtra("MYPICKLE", arrayListOfFeeddata.get(position).isIs_my_pickle());
//								 i.putExtra("owner",arrayListOfFeeddata.get(position).getStrUName2());
//								 i.putExtra("profimg", arrayListOfFeeddata.get(position)
//							.getStrUProfilePicUrl2());
//								startActivity(i);

                        CommentFragment commentFragment = new CommentFragment();

                        Bundle args = new Bundle();
                        args.putString("profimg", arrayListOfFeeddata.get(position).getStrUProfilePicUrl2());
                        args.putString("COMMENT_ARRAY", "" + arrayListOfFeeddata.get(position).getArrayOfComments2());
                        args.putString("USER_ID", "" + HomeScreen.user_id);
                        args.putString("PICKLE_ID", "" + arrayListOfFeeddata.get(position).getStrPId2());
                        args.putString("USER_NAME", "" + part1);
                        args.putString("SHARE_PICKLE_ID", "" + arrayListOfFeeddata.get(position).getSharePickleId());
                        args.putBoolean("MYPICKLE", arrayListOfFeeddata.get(position).isIs_my_pickle());
                        args.putString("owner", arrayListOfFeeddata.get(position).getStrUName2());
                        commentFragment.setArguments(args);
                        FragmentTransaction commentTransaction = getFragmentManager().beginTransaction();
                        commentTransaction.replace(R.id.content_frame, commentFragment);
                        commentTransaction.addToBackStack(null);
                        commentTransaction.commit();

                    }
                });

                viewHolder.firstOptionVoteText
                        .setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {

                                VotingFragment votingfragment = new VotingFragment();
                                Bundle showArgs = new Bundle();
                                showArgs.putString("OPTION_ID", "" + arrayListOfFeeddata.get(position).getStrFId2());
                                votingfragment.setArguments(showArgs);
                                FragmentTransaction showTransaction = getFragmentManager().beginTransaction();
                                showTransaction.replace(R.id.content_frame, votingfragment);
                                showTransaction.addToBackStack(null);
                                showTransaction.commit();

                            }

                        });

                viewHolder.secondOptionVoteText
                        .setOnClickListener(new OnClickListener() {

                            @Override
                            public void onClick(View v) {

                                VotingFragment votingfragment = new VotingFragment();
                                Bundle showArgs = new Bundle();
                                showArgs.putString("OPTION_ID", "" + arrayListOfFeeddata.get(position).getStrSId2());
                                votingfragment.setArguments(showArgs);
                                FragmentTransaction showTransaction = getFragmentManager().beginTransaction();
                                showTransaction.replace(R.id.content_frame, votingfragment);
                                showTransaction.addToBackStack(null);
                                showTransaction.commit();

                            }
                        });

            } catch (Exception e) {
                e.printStackTrace();
            }

            SaveUtils.savePickleUserNameFrom(
                    getResources().getString(R.string.PreferenceFileName),
                    getActivity(), "USER_NAME",
                    arrayListOfFeeddata.get(position).getStrUName2());
        }

        @Override
        public View getHeaderView(final int position, View convertView, ViewGroup parent) {
            HeaderViewHolder holder;
            inflater = LayoutInflater.from(getActivity());
            if (convertView == null) {
                holder = new HeaderViewHolder();

                convertView = inflater.inflate(R.layout.header_feeds, parent, false);
                holder.userprofileImage = (ImageView) convertView.findViewById(R.id.userProfilePic);
                holder.txtUserName = (TextView) convertView.findViewById(R.id.userName);
                holder.txtCreatedDate = (TextView) convertView.findViewById(R.id.createdDate);
                holder.header = (RelativeLayout) convertView.findViewById(R.id.header);
                convertView.setTag(holder);

            } else {
                holder = (HeaderViewHolder) convertView.getTag();
            }
            //set header text as first char in name

            try {

                if (getItemViewType(position) != 0) {
                    holder.header.setVisibility(View.VISIBLE);//@PKDEC05
                    if (arrayListOfFeeddata.get(position).getStrUProfilePicUrl2() != null) {

                        //					imageLoader.DisplayImage(arrayListOfFeeddata.get(position)
                        //							.getStrUProfilePicUrl2(),
                        //							viewHolder.userprofileImage);
                        Picasso.with(mContext).load(arrayListOfFeeddata.get(position).getStrUProfilePicUrl2()).into(holder.userprofileImage);
                        //	viewHolder.userprofileImage.startAnimation(myFadeInAnimation);

                    }

                    if (arrayListOfFeeddata.get(position).getStrUName2() != null) {
                        holder.txtUserName.setTypeface(face);
                        holder.txtUserName.setText(arrayListOfFeeddata.get(position).getStrUName2());
                    }
                    if (arrayListOfFeeddata.get(position).getStrPCreatedDate() != null) {
                        holder.txtCreatedDate.setTypeface(face);
                        holder.txtCreatedDate.setText(arrayListOfFeeddata.get(position).getStrPCreatedDate());
                    }
                    //@PKDEC02 - To go to the user profile when clicked on header
                    holder.header.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.e("HeaderClick", "position = " + position + " - " + arrayListOfFeeddata.get(position).getStrUserName2());
                            UserProfileFragment userProfileFragment = new UserProfileFragment();
                            Bundle args = new Bundle();
                            args.putString("USER_NAME", arrayListOfFeeddata.get(position).getStrUserName2());
                            args.putString("FROM", "FEEDS");
                            args.putString("GENERATED_ID", "");
                            userProfileFragment.setArguments(args);
                            FragmentTransaction userProfileTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            userProfileTransaction.replace(R.id.content_frame, userProfileFragment);
                            userProfileTransaction.addToBackStack(null);
                            userProfileTransaction.commit();
                            ((HomeScreen) getActivity()).setProfileIcon();
                        }
                    });
                } else {
                    holder.header.setVisibility(View.GONE); //@PKDEC05 to hide the header for the main header view, i.e., "Feeds"
                }
            } catch (Exception e) {
            }

            return convertView;
        }

        @Override
        public long getHeaderId(int position) {
            //return the first character of the country as ID because this is what headers are based upon
            return position;
        }

        class FeedsViewHolder {
            ImageView userprofileImage;
            TextView txtUserName;
            TextView txtCreatedDate;
            TextView txtPickleQuestion;
            ImageView firstOptionImage;
            TextView txtFirstOptionName;
            ImageView firstOptionVoteImage;
            TextView firstOptionVoteCount;
            TextView firstOptionVoteText;
            TextView firstText;
            ImageView secondOptionImage;
            TextView txtSecondOptionName;
            ImageView secondOptionVoteImage;
            TextView secondOptionVoteCount;
            TextView secondOptionVoteText;
            TextView SecondText;
            ImageView moreComment;
            ImageView popUp;
            RelativeLayout firstView;
            RelativeLayout secondView;
            GridLayout commentLayout;
            TextView commentOne;
            TextView commentTwo;
            TextView commentThree;
            TextView commentUserOne;
            TextView commentUserTwo;
            TextView commentUserThree;
            RelativeLayout comment_one;
            RelativeLayout comment_two;
            RelativeLayout comment_three;

            TextView txtLoadMoreComment;
            JSONObject subMain;

            //@PKDEC12 added for up_from_bottom animation
            LinearLayout ll_main;

            public FeedsViewHolder(View base) {

                ll_main = (LinearLayout) base.findViewById(R.id.ll_main);

                userprofileImage = (ImageView) base.findViewById(R.id.userProfilePic);
                txtUserName = (TextView) base.findViewById(R.id.userName);
                txtCreatedDate = (TextView) base.findViewById(R.id.createdDate);
                txtPickleQuestion = (TextView) base.findViewById(R.id.pickleQuestion);
                firstOptionImage = (ImageView) base.findViewById(R.id.firstOptionImageUrl);
                txtFirstOptionName = (TextView) base.findViewById(R.id.firstOptionName);
                firstOptionVoteImage = (ImageView) base.findViewById(R.id.firstVoteImage);
                firstOptionVoteCount = (TextView) base.findViewById(R.id.firstVoteCount);
                firstOptionVoteText = (TextView) base.findViewById(R.id.firstVoteText);
                firstText = (TextView) base.findViewById(R.id.firstText);
                secondOptionImage = (ImageView) base.findViewById(R.id.secondOptionImageUrl);
                txtSecondOptionName = (TextView) base.findViewById(R.id.secondOptionName);
                secondOptionVoteImage = (ImageView) base.findViewById(R.id.secondVoteImage);
                secondOptionVoteCount = (TextView) base.findViewById(R.id.secondVoteCount);
                secondOptionVoteText = (TextView) base.findViewById(R.id.secondVoteText);
                SecondText = (TextView) base.findViewById(R.id.SecondText);
                moreComment = (ImageView) base.findViewById(R.id.moreComment);
                popUp = (ImageView) base.findViewById(R.id.popUp);

                firstView = (RelativeLayout) base.findViewById(R.id.firstView);
                secondView = (RelativeLayout) base.findViewById(R.id.secondView);

                commentLayout = (GridLayout) base.findViewById(R.id.commentLayout);
                commentOne = (TextView) base.findViewById(R.id.commentFirst);
                commentTwo = (TextView) base.findViewById(R.id.commentSecond);
                commentThree = (TextView) base.findViewById(R.id.commentThird);

                commentUserOne = (TextView) base.findViewById(R.id.commentUserNameFirst);
                commentUserTwo = (TextView) base.findViewById(R.id.commentUserNameSecond);
                commentUserThree = (TextView) base.findViewById(R.id.commentUserNameThird);
                comment_one = (RelativeLayout) base.findViewById(R.id.comment_one);
                comment_two = (RelativeLayout) base.findViewById(R.id.comment_two);
                comment_three = (RelativeLayout) base.findViewById(R.id.comment_three);

                txtLoadMoreComment = (TextView) base.findViewById(R.id.txtLoadMoreComment);
            }
        }

        class HeaderViewHolder {
            ImageView userprofileImage;
            TextView txtUserName, txtCreatedDate;
            //@PKDEC02 : To add click to go to profile
            RelativeLayout header;
        }

        class MainHeaderViewHolder {

            public MainHeaderViewHolder(View v) {

            }
        }

//NS
        private class FullScreenImage {

                Dialog dialog;
                AllFeedsModel model;
                RelativeLayout layout_first,layout_second;
                ImageView secondVoteImage,firstVoteImage;
                TextView txt_title,txtTitle_first,txtTitle_second,secondVoteCount,firstVoteCount,firstOptionVoteText,secondOptionVoteText;

                public void setDialog(AllFeedsModel allFeedsModel,int position) {

                    this.model=allFeedsModel;

                    dialog = new Dialog(getActivity(),android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.setContentView(R.layout.image_fullscreen);

                    ExtendedViewPager mViewPager = (ExtendedViewPager)dialog.findViewById(R.id.view_pager);
                    mViewPager.setAdapter(new TouchImageAdapter(model));
                    mViewPager.setCurrentItem(position);

                    txt_title=(TextView)dialog.findViewById(R.id.txtTitle);
                    txtTitle_first=(TextView)dialog.findViewById(R.id.txtTitle_first);
                    txtTitle_second=(TextView)dialog.findViewById(R.id.txtTitle_second);
                    secondVoteCount=(TextView)dialog.findViewById(R.id.secondVoteCount);
                    firstVoteCount=(TextView)dialog.findViewById(R.id.firstVoteCount);
                    layout_first=(RelativeLayout)dialog.findViewById(R.id.layout_one) ;
                    layout_second=(RelativeLayout)dialog.findViewById(R.id.layout_second) ;
                    secondVoteImage=(ImageView)dialog.findViewById(R.id.seconVoteImage);
                    firstVoteImage=(ImageView)dialog.findViewById(R.id.firstVoteImage);
                    firstOptionVoteText=(TextView)dialog.findViewById(R.id.firstVoteText);
                    secondOptionVoteText=(TextView)dialog.findViewById(R.id.secondVoteText);

                    txtTitle_first.setText(!model.getStrFOptionLable2().equals("null")?model.getStrFOptionLable2():"");
                    txtTitle_second.setText(!model.getStrSOptionLable2().equals("null")?model.getStrSOptionLable2():"");


                    txt_title.setText(model.getStrPName2());
                  if(position==0) {
                      txtTitle_first.setVisibility(View.VISIBLE);
                      txtTitle_second.setVisibility(View.GONE);
                      layout_second.setVisibility(View.GONE);
                      layout_first.setVisibility(View.VISIBLE);
                  }else{
                      layout_first.setVisibility(View.GONE);
                      layout_second.setVisibility(View.VISIBLE);
                      txtTitle_first.setVisibility(View.GONE);
                      txtTitle_second.setVisibility(View.VISIBLE);
                  }

                    firstVoteCount.setText(model.getStrFirstOption2()+"");
                    secondVoteCount.setText(model.getStrSecondOption2()+"");

                    if (model.isFirst_option_is_voted()
                            && !model
                            .isSecond_option_is_voted()) {

                        firstVoteCount.setTextColor(Color.parseColor("#F08080"));
                        firstVoteImage.setImageResource(R.drawable.vote_color);
                        firstOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                        secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                        secondVoteImage.setImageResource(R.drawable.vote_black);
                        secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                        model.setFirst_option_is_voted(true);
                        model.setSecond_option_is_voted(false);

                    } else if (!model.isFirst_option_is_voted() && model.isSecond_option_is_voted()) {

                        firstVoteCount.setTextColor(Color.parseColor("#ffffff"));
                        firstVoteImage.setImageResource(R.drawable.vote_black);
                        firstOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                        secondVoteCount.setTextColor(Color.parseColor("#F08080"));
                        secondVoteImage.setImageResource(R.drawable.vote_color);
                        secondOptionVoteText.setTextColor(Color.parseColor("#F08080"));

                        model.setFirst_option_is_voted(false);
                        model.setSecond_option_is_voted(true);
                    }

                    firstVoteImage.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view)
                        {
                            if (model
                                    .isFirst_option_is_voted() == false
                                    && model
                                    .isSecond_option_is_voted() == false) {

                                firstVoteCount
                                        .setTextColor(Color
                                                .parseColor("#F08080"));
                                firstVoteImage
                                        .setImageResource(R.drawable.vote_color);
                                firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#F08080"));

                                model.setFirst_option_is_voted(true);
                               model
                                        .setSecond_option_is_voted(false);
                                long count_one = model
                                        .getStrFirstOption2();
                                count_one = count_one + 1;
                                model.setStrFirstOption2(count_one);
                                firstVoteCount.setText(""
                                        + (model
                                        .getStrFirstOption2()));

                                pickleVote(model,true);
                                notifyDataSetChanged();



                            } else if (model
                                    .isFirst_option_is_voted() == true
                                    && model
                                    .isSecond_option_is_voted() == false) {


                                firstVoteCount
                                        .setTextColor(Color
                                                .parseColor("#FFFFFF"));
                                firstVoteImage
                                        .setImageResource(R.drawable.vote_black);
                                firstOptionVoteText
                                        .setTextColor(Color
                                                .parseColor("#FFFFFF"));

                                model
                                        .setFirst_option_is_voted(false);
                               model
                                        .setSecond_option_is_voted(false);
                                long count_one = model
                                        .getStrFirstOption2();
                                count_one = count_one - 1;
                                model.setStrFirstOption2(count_one);
                                firstVoteCount.setText("" + model.getStrFirstOption2());

                                Log.e("@Create"," model.getFirst_option_voted_id():"+model.getStrFirstOption2()+"----"+ model.getFirst_option_voted_id());

                                UNVOTED_URL = Constant.PickleUrl
                                        + "votes/"
                                        + model.getFirst_option_voted_id()
                                        + ".json";

                                new UnVoted().execute();
                                notifyDataSetChanged();
                            } else if (model
                                    .isFirst_option_is_voted() == false
                                    && model
                                    .isSecond_option_is_voted() == true) {
                                Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    secondVoteImage
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {

                                    if (model.isFirst_option_is_voted() == false
                                            && model
                                            .isSecond_option_is_voted() == false) {


                                        secondVoteCount
                                                .setTextColor(Color
                                                        .parseColor("#F08080"));
                                        secondVoteImage
                                                .setImageResource(R.drawable.vote_color);
                                        secondOptionVoteText
                                                .setTextColor(Color
                                                        .parseColor("#F08080"));
                                        model.setFirst_option_is_voted(false);
                                        model.setSecond_option_is_voted(true);
                                        long count_two = model.getStrSecondOption2();
                                        count_two = count_two + 1;
                                        model.setStrSecondOption2(count_two);
                                        secondVoteCount.setText("" + (model.getStrSecondOption2()));
                                        pickleVote(model,false);



                                        notifyDataSetChanged();
                                    } else if (model.isFirst_option_is_voted() == false && model.isSecond_option_is_voted() == true) {


                                        secondVoteCount.setTextColor(Color.parseColor("#ffffff"));
                                       secondVoteImage.setImageResource(R.drawable.vote_black);
                                       secondOptionVoteText.setTextColor(Color.parseColor("#ffffff"));

                                        model
                                                .setFirst_option_is_voted(false);
                                        model
                                                .setSecond_option_is_voted(false);
                                        long count_two = model
                                                .getStrSecondOption2();
                                        count_two = count_two - 1;
                                        model.setStrSecondOption2(count_two);
                                        secondVoteCount.setText(""+model.getStrSecondOption2());
                                        Log.e("@Create"," model.getSecond_option_voted_id():"+model.getStrSecondOption2()+"---"+ model.getSecond_option_voted_id());

                                        UNVOTED_URL = Constant.PickleUrl + "votes/" + model.getSecond_option_voted_id() + ".json";

                                        new UnVoted().execute();
                                        notifyDataSetChanged();

                                    } else if (model.isFirst_option_is_voted() == true && model.isSecond_option_is_voted() == false) {
                                        Toast.makeText(dialog.getContext(),"Please unvote another and vote",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                        if(position==0){
                            txtTitle_first.setVisibility(View.VISIBLE);
                            txtTitle_second.setVisibility(View.GONE);
                            layout_first.setVisibility(View.VISIBLE);
                            layout_second.setVisibility(View.GONE);
                        }else{
                            txtTitle_second.setVisibility(View.VISIBLE);
                            txtTitle_first.setVisibility(View.GONE);
                            layout_second.setVisibility(View.VISIBLE);
                            layout_first.setVisibility(View.GONE);
                        }
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                    dialog.show();
                }
                 class TouchImageAdapter extends PagerAdapter {

                     AllFeedsModel innerModel;

                     public TouchImageAdapter(AllFeedsModel model) {
                         this.innerModel=model;
                     }

                     @Override
                    public int getCount() {
                        return 2;
                    }

                    @Override
                    public View instantiateItem(ViewGroup container, int position) {
                        if (position == 0) {

                            if (innerModel.getFirstOptionText().equals("")||innerModel.getFirstOptionText().equals("null")) {


                                TouchImageView img = new TouchImageView(container.getContext());
                                Picasso.with(mContext).load(innerModel.getStrFOptionImageUrl2()).into(img);
                                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                return img;

                            } else {
                                TextView text = new TextView(container.getContext());
                                text.setText(innerModel.getFirstOptionText());
                                text.setGravity(Gravity.CENTER);
                                container.addView(text);
                                return text;
                            }
                        } else if (position == 1) {



                            if (innerModel.getSecondOptionText().equals("")||innerModel.getSecondOptionText().equals("null")) {

                                TouchImageView img = new TouchImageView(container.getContext());
                                Picasso.with(mContext).load(innerModel.getStrSOptionImageUrl2()).into(img);
                                container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                                return img;
                            } else {

                                TextView text = new TextView(container.getContext());
                                text.setText(innerModel.getSecondOptionText());
                                text.setGravity(Gravity.CENTER);
                                container.addView(text);
                                return text;
                            }

                        }
                        return null;
                    }

                    @Override
                    public void destroyItem(ViewGroup container, int position, Object object) {
                        container.removeView((View) object);
                    }

                    @Override
                    public boolean isViewFromObject(View view, Object object) {
                        return view == object;
                    }

                }
            }



    }

    private class UnVoted extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try {
                params.add(new BasicNameValuePair("authentication_token",
                        HomeScreen.authentication_token));
                params.add(new BasicNameValuePair("key", HomeScreen.key));

            } catch (Exception e) {
                e.printStackTrace();
            }
            String jsonStr = sh.makeServiceCall(UNVOTED_URL,
                    ServiceHandler.DELETE, params);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }

    }

    private class DeleteSharePickle extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();

            try {
                params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
                params.add(new BasicNameValuePair("key", HomeScreen.key));

            } catch (Exception e) {
                e.printStackTrace();
            }
            String jsonStr = sh.makeServiceCall(DELETE_SHARE_PICKLE_URL,
                    ServiceHandler.DELETE, params);

            if (jsonStr != null) {
                try {
                    final JSONObject obj = new JSONObject(jsonStr);

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            try {
                                if (obj.getString("success").equalsIgnoreCase("true")) {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            } else {
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            String text = "Delete Pickle Successfully !";
            showShareDialog(text);
        }

    }

}
