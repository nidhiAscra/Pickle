package com.ascra.pickle;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ascra.utils.ConnectionDetector;
import com.ascra.utils.Constant;
import com.ascra.utils.SaveUtils;
import com.ascra.utils.SessionManager;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.android.gcm.GCMRegistrar;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static com.ascra.pickle.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.ascra.pickle.CommonUtilities.EXTRA_MESSAGE;
import static com.ascra.pickle.CommonUtilities.SERVER_URL;

public class MainActivity extends AppCompatActivity implements
        OnClickListener {

    String regId;
    RelativeLayout btnLoginWithFacebook, btnBrandLogin;
    Intent intent;
    String stringUserName, stringPassword;
    //http://pickle.tingworks.in/
    private String URL_FEED = Constant.PickleUrl + "tokens/facebook_authentication.json";
    private String URL_FEED_TWO = Constant.PickleUrl + "tokens";

    SessionManager session;
    public static final String SENDER_ID = "264226440806";
    // san
    //private static String APP_ID = "396422687176859";
    //private static String APP_ID = "460090897457783";
    private static String APP_ID = "1451687078459724";
    // Instance of Facebook Class
    private Facebook facebook;
    @SuppressWarnings("deprecation")
    private AsyncFacebookRunner mAsyncRunner;
    String FILENAME = "AndroidSSO_data";
    private SharedPreferences mPrefs;
    RelativeLayout fb_btn;
    JSONObject obj;

    String authentication_token = null;
    String key = null;
    Integer user_id = 0;
    String user_name = null;

    //@PKDEC01 User/Brand Name : Used to show UserName when commenting in BundlePickles Comments
    String brandOrUserName = null;
    ProgressDialog rpd;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    AlertDialogManager alert = new AlertDialogManager();
    TextView copyright, yourABrand, txtClickHere, txtfacebook;

    Typeface face;

    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().hide();
        setContentView(R.layout.pickle_activity_main);

        Splash.str_option = "3";

        session = new SessionManager(getApplicationContext());

        // FAcebook initialization
        facebook = new Facebook(APP_ID);
        mAsyncRunner = new AsyncFacebookRunner(facebook);

        cd = new ConnectionDetector(MainActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.facebook.samples.hellofacebook",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0
                || SENDER_ID.length() == 0) {
            // GCM sernder id / server url is missing
            alert.showAlertDialog(MainActivity.this, "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
            // stop executing code by return
            return;
        }

        try{gcmRegistration();}catch (Exception e){e.printStackTrace();}
        setUpViews();

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ascra.pickle", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("Your Tag",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        face = Typeface.createFromAsset(getAssets(),
                "fonts/sniglet-regular.ttf");
    }

    private void setUpViews() {
        btnLoginWithFacebook = (RelativeLayout) findViewById(R.id.btnLoginWithFacebook);
        btnBrandLogin = (RelativeLayout) findViewById(R.id.btnBrandLogin);

        copyright = (TextView) findViewById(R.id.copyright);
        yourABrand = (TextView) findViewById(R.id.yourABrand);
        txtClickHere = (TextView) findViewById(R.id.txtClickHere);
        txtfacebook = (TextView) findViewById(R.id.txtfacebook);

        copyright.setTypeface(face);
        yourABrand.setTypeface(face);
        txtClickHere.setTypeface(face);
        txtfacebook.setTypeface(face);

        btnLoginWithFacebook.setOnClickListener(this);
        btnBrandLogin.setOnClickListener(this);
    }

    public void gcmRegistration() {
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        registerReceiver(mHandleMessageReceiver, new IntentFilter(
                DISPLAY_MESSAGE_ACTION));
        // Get GCM registration id
        regId = GCMRegistrar.getRegistrationId(this);
        // Check if regid already presents
        Log.e("Reg id", regId);
        if (regId.equals("")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(this, SENDER_ID);
        } else {

            if (GCMRegistrar.isRegisteredOnServer(this)) {

            } else {
                GCMRegistrar.setRegisteredOnServer(MainActivity.this, true);
            }

        }
    }

    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());
            WakeLocker.release();
        }
    };

    @SuppressLint("LongLogTag")
    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLoginWithFacebook:

                if (isInternetPresent) {
                    Log.e("HERE", "YEAH");
                    rpd = ProgressDialog.show(MainActivity.this, "", "Processing please wait ...", true);
                    rpd.setCancelable(false);

                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }).start();
                    getProfileInformation();
                } else {
                    String text = "No Internet Connection!";
                    showNoFeedsDialog(text);
                }
                break;
            case R.id.btnBrandLogin:
                intent = new Intent(MainActivity.this, BrandLogin.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @SuppressWarnings("deprecation")
    public void getProfileInformation() {

        mPrefs = getPreferences(MODE_PRIVATE);
        if (facebook.isSessionValid()) {
            Log.e("HERE", "HIIIIIIIII");
            mAsyncRunner.request("me", new RequestListener() {
                @Override
                public void onComplete(String response, Object state) {

                    Log.e("HERE", "complete");

                    SharedPreferences.Editor editor = mPrefs.edit();
                    editor.putString("access_token", facebook.getAccessToken());
                    editor.putLong("access_expires",
                            facebook.getAccessExpires());
                    editor.commit();
                    Log.e("@Create","FB " +
                            "Profile "+ response);
                    String json = response;
                    try {
                        JSONObject profile = new JSONObject(json);
                        // getting name of the user
                        final String name = profile.getString("name");

                        // getting email of the user
                        final String email = profile.getString("email");
                        final String uId = profile.getString("id");
                        final String provider = "facebook";
                        final String access_token = facebook.getAccessToken();
                        final String profile_pic="https://graph.facebook.com/" + uId + "/picture?type=large";
                     //   final String profile_pic=profile.getString();
                        SaveUtils.saveFacebookUId(
                                getResources().getString(
                                        R.string.PreferenceFileName),
                                MainActivity.this, "FACEBOOK_UID", uId);
                        SaveUtils.saveFacebookAccessToken(getResources()
                                        .getString(R.string.PreferenceFileName),
                                MainActivity.this, "FACEBOOK_ACCESS_TOKEN",
                                access_token);

//						jsonobject.put("registration_id", session.getRegID());
//						jsonobject.put("device_type","Android");

                        LoginAndRegister(email, provider, uId, name,
                                access_token,profile_pic);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onIOException(IOException e, Object state) {

                }

                @Override
                public void onFileNotFoundException(FileNotFoundException e,
                                                    Object state) {

                }

                @Override
                public void onMalformedURLException(MalformedURLException e,
                                                    Object state) {

                }

                @Override
                public void onFacebookError(FacebookError e, Object state) {

                }

            });
        } else {
            facebook.authorize(this,
                    new String[]{"email", "publish_stream"},
                    Facebook.FORCE_DIALOG_AUTH, new DialogListener() {

                        @Override
                        public void onCancel() {
                            // Function to handle cancel event
                        }

                        @Override
                        public void onComplete(Bundle values) {
                            // Function to handle complete event
                            // Edit Preferences and update facebook acess_token
                            SharedPreferences.Editor editor = mPrefs.edit();
                            editor.putString("access_token",
                                    facebook.getAccessToken());
                            editor.putLong("access_expires",
                                    facebook.getAccessExpires());
                            editor.commit();
                            getProfileInformation();

                        }

                        @Override
                        public void onFacebookError(FacebookError e) {

                        }

                        @Override
                        public void onError(DialogError e) {
                        }
                    });
        }
    }

    private void LoginAndRegister(final String email, String provider,
                                  String uId, String username, String access_token, final String profile_pic) {

        class SendPostReqAsyncTaskTwo extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                String paramEmail = params[0];
                String paramProvider = params[1];
                String paramUId = params[2];
                String paramUserName = params[3];
                String paramAccessToken = params[4];
                String paramProfilePic = params[5];

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(URL_FEED);
                Log.e("REQUEST SEND", "REQ-----");

                BasicNameValuePair emailBasicNameValuePair = new BasicNameValuePair(
                        "email", paramEmail);
                BasicNameValuePair providerBasicNameValuePair = new BasicNameValuePair(
                        "provider", paramProvider);
                BasicNameValuePair uIdBasicNameValuePair = new BasicNameValuePair(
                        "uid", paramUId);
                BasicNameValuePair userNameBasicNameValuePair = new BasicNameValuePair(
                        "name", paramUserName);
                BasicNameValuePair accessTokenBasicNameValuePair = new BasicNameValuePair(
                        "access_token", paramAccessToken);
                BasicNameValuePair registration_id = new BasicNameValuePair(
                        "registration_id", session.getRegID() == null
                        ? SaveUtils.getGcmRegistrationId(getResources().getString(R.string.PreferenceFileName), MainActivity.this, SaveUtils.GcmRegistraionId, "")
                        : session.getRegID());
                BasicNameValuePair devicetype = new BasicNameValuePair(
                        "device_type", "Android");
                BasicNameValuePair deviceToken = new BasicNameValuePair(
                        "device_token", session.getGcmToken() == null
                        ? SaveUtils.getGcmDeviceToken(getResources().getString(R.string.PreferenceFileName), MainActivity.this, SaveUtils.GcmDeviceToken, "")
                        : session.getGcmToken());
                // @NS19DEC : Sending facebook profile pic
                InputStream in = null;





                List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();
                nameValuePairList.add(emailBasicNameValuePair);
                nameValuePairList.add(providerBasicNameValuePair);
                nameValuePairList.add(uIdBasicNameValuePair);
                nameValuePairList.add(userNameBasicNameValuePair);
                nameValuePairList.add(accessTokenBasicNameValuePair);
                nameValuePairList.add(registration_id);
                nameValuePairList.add(devicetype);
                nameValuePairList.add(deviceToken);
                try {
                    in = (InputStream) new URL(paramProfilePic).getContent();
                    Bitmap bitmap_fb_profile_pic = BitmapFactory.decodeStream(in);
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bitmap_fb_profile_pic.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    byte[] data = bos.toByteArray();
                    String file = Base64.encodeToString(data,0);
                    BasicNameValuePair prof_pic = new BasicNameValuePair(
                            "profile_pic", file);
                    nameValuePairList.add(prof_pic);
                    Log.e("@Create","FB Profile pic url "+file);

                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(
                            nameValuePairList);

                    httpPost.setEntity(urlEncodedFormEntity);

                    try {
                        HttpResponse httpResponse = httpClient
                                .execute(httpPost);

                        InputStream inputStream = httpResponse.getEntity()
                                .getContent();
                        InputStreamReader inputStreamReader = new InputStreamReader(
                                inputStream);
                        BufferedReader bufferedReader = new BufferedReader(
                                inputStreamReader);
                        StringBuilder stringBuilder = new StringBuilder();
                        String bufferedStrChunk = null;

                        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
                            stringBuilder.append(bufferedStrChunk);

                        }
                        Log.e("GOT :: ", "" + stringBuilder.toString());
                        try {
                            JSONObject obj=new JSONObject(stringBuilder.toString());
                            if(obj.getString("status").equals("Failure")){
                                return null;
                            }
                            else{
                                return stringBuilder.toString();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    } catch (ClientProtocolException cpe) {
                        cpe.printStackTrace();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }

                } catch (UnsupportedEncodingException uee) {
                    uee.printStackTrace();
                }

              return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if(result!=null) {
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        key = jsonObject.getString("key");
                        authentication_token = jsonObject
                                .getString("authentication_token");
                        user_id = jsonObject.getInt("user_id");
                        user_name = jsonObject.getString("username");

                        //@PKDEC01
                        brandOrUserName = jsonObject.getString("name");

                        SaveUtils.saveUserID(
                                getResources().getString(
                                        R.string.PreferenceFileName),
                                MainActivity.this, "USER_ID",
                                String.valueOf(user_id));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    session.createLoginSession(user_name, email);

                    intent = new Intent(MainActivity.this, HomeScreen.class);
                    startActivity(intent);

                    SaveUtils.saveAuthKeyToPref(
                            getResources().getString(R.string.PreferenceFileName),
                            MainActivity.this, "AUTH", authentication_token);
                    SaveUtils.saveKeyToPref(
                            getResources().getString(R.string.PreferenceFileName),
                            MainActivity.this, "KEY", key);
                    SaveUtils.saveUserToPref(
                            getResources().getString(R.string.PreferenceFileName),
                            MainActivity.this, "USER", user_name);

                    SaveUtils.saveLoginSessionPref(Constant.LoginSession,
                            MainActivity.this, "LOGIN", true);

                    SaveUtils.saveUserLoginFrom(
                            getResources().getString(R.string.PreferenceFileName),
                            MainActivity.this, "WHICH_LOGIN", "FACEBOOK");

                    //@PKDEC01
                    SaveUtils.saveBrandOrUserName(
                            getResources().getString(R.string.PreferenceFileName),
                            MainActivity.this, SaveUtils.BrandOrUserName, brandOrUserName);
                }
                finish();
            }

        }

        SendPostReqAsyncTaskTwo sendPostReqAsyncTask = new SendPostReqAsyncTaskTwo();
        sendPostReqAsyncTask.execute(email, provider, uId, username,
                access_token,profile_pic);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Splash.str_option.equals("3")) {
            finish();
        }
    }

    private void showNoFeedsDialog(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                MainActivity.this);
        builder.setCancelable(true);

        TextView msg = new TextView(MainActivity.this);
        msg.setText(text);
        msg.setPadding(10, 35, 10, 10);
        msg.setGravity(Gravity.CENTER);
        msg.setTextSize(15);

        builder.setView(msg);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
