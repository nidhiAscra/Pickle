package com.ascra.pickle;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ascra.data.Vote;
import com.ascra.imageloader.ImageLoader;
import com.ascra.utils.CommonMethods;
import com.ascra.utils.Constant;
import com.ascra.utils.ServiceHandler;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VotingFragment extends Fragment {

    private String VOTING_LIST_URL = null;
    View view;
    ListView allVotes;
    ArrayList<Vote> voteData = new ArrayList<Vote>();
    ImageLoader imageLoader;
    String option_id;
//    RelativeLayout buttonLayout;
//    TextView btnBack;

    Typeface face;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //@PKDEC03 : Try Catch added as it was crashing (NullPointer atbtnBack.setTypeface(face));
        //			 when continuously vote unvote is done on same Pickle options
        //@PKDEC05
        try {
            ((HomeScreen) getActivity()).showActionBar(false);
            ((HomeScreen) getActivity()).showBottomStrip(false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            view = inflater.inflate(R.layout.votinglist_dialog, null);
            allVotes = (ListView) view.findViewById(R.id.allVotes);
//            buttonLayout = (RelativeLayout) view.findViewById(R.id.buttonlayout);
//            btnBack = (TextView) view.findViewById(R.id.btnBack);

            face = Typeface.createFromAsset(getActivity().getAssets(), "fonts/sniglet-regular.ttf");
//            btnBack.setTypeface(face);

            HomeScreen.whichFragment = "VOTING_LIST";

            try {//@PKDEC05
                ImageView back = (ImageView) view.findViewById(R.id.btnBackpress);
                back.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getFragmentManager().popBackStack();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            Bundle bundle = this.getArguments();
            option_id = bundle.getString("OPTION_ID");

            VOTING_LIST_URL = Constant.PickleUrl + "votes.json";

            try {
                new loadingVotingList().execute();
            } catch (Exception e) {
                e.printStackTrace();
            }

//            buttonLayout.setOnClickListener(new OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                    getFragmentManager().popBackStack();
//                }
//            });

            CommonMethods.addHeaderToListview(getActivity(), allVotes, getString(R.string.votesHeader), R.drawable.picklebundle);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private class loadingVotingList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... v) {
            ServiceHandler sh = new ServiceHandler();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("option_id", option_id));
            params.add(new BasicNameValuePair("authentication_token", HomeScreen.authentication_token));
            params.add(new BasicNameValuePair("key", HomeScreen.key));

            String jsonStr = sh.makeServiceCall(VOTING_LIST_URL, ServiceHandler.GET, params);

            try {
                final JSONObject mainObject = new JSONObject(jsonStr);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        parseVoteListJSON(mainObject);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

    }

    private void parseVoteListJSON(JSONObject mainObject) {
        try {
            Vote vote;
            JSONArray votingArray = mainObject.getJSONArray("votes");
            for (int voteElement = 0; voteElement < votingArray.length(); voteElement++) {
                JSONObject objVote = votingArray.getJSONObject(voteElement);
                String voter_id = objVote.getString("voter_id");
                String option_id = objVote.getString("option_id");
                String user_id = objVote.getString("user_id");
                String share_pickle_id = objVote.getString("share_pickle_id");
                String voter_name = objVote.getString("voter_name");
                String voter_username = objVote.getString("voter_username");
                String voter_profile_pic = objVote.getString("voter_profile_pic");

                vote = new Vote(voter_id, option_id, user_id, share_pickle_id, voter_name, voter_username, voter_profile_pic);
                voteData.add(vote);

            }
            //@PKDEC06 - moved out from the above for loop
            MyVoteListCustomAdapter myVoteListCustomAdapter = new MyVoteListCustomAdapter(getActivity(), voteData);
            allVotes.setAdapter(myVoteListCustomAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class MyVoteListCustomAdapter extends BaseAdapter {

        ArrayList<Vote> voteData = new ArrayList<Vote>();
        Context mContext;

        public MyVoteListCustomAdapter(Context mContext, ArrayList<Vote> voteData) {
            this.voteData = voteData;
            this.mContext = mContext;
            imageLoader = new ImageLoader(mContext.getApplicationContext());
        }

        @Override
        public int getCount() {
            return voteData.size();
        }

        @Override
        public Object getItem(int position) {
            return voteData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private class ViewHolder {
            ImageView profile_pic;
            TextView userName;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.vote_list_item, null);
                holder = new ViewHolder();
                holder.profile_pic = (ImageView) convertView.findViewById(R.id.profile_pic);
                holder.userName = (TextView) convertView.findViewById(R.id.name);
                holder.userName.setTypeface(face);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            imageLoader.DisplayImage(voteData.get(position).getVoter_profile_pic(), holder.profile_pic);
            holder.userName.setText(voteData.get(position).getVoter_name());

            return convertView;
        }

    }
}
